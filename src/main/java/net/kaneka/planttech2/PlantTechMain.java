package net.kaneka.planttech2;

import net.kaneka.planttech2.configuration.PlantTech2Configuration;
import net.kaneka.planttech2.crops.CropTypes;
import net.kaneka.planttech2.packets.PlantTech2PacketHandler;
import net.kaneka.planttech2.recipes.ModRecipeSerializers;
import net.kaneka.planttech2.recipes.ModRecipeTypes;
import net.kaneka.planttech2.registries.ModBlockEntities;
import net.kaneka.planttech2.registries.ModBlocks;
import net.kaneka.planttech2.registries.ModCommands;
import net.kaneka.planttech2.registries.ModContainers;
import net.kaneka.planttech2.registries.ModEntityTypes;
import net.kaneka.planttech2.registries.ModFluids;
import net.kaneka.planttech2.registries.ModItems;
import net.kaneka.planttech2.registries.ModSounds;
import net.kaneka.planttech2.utilities.ModCreativeTabs;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod(PlantTechMain.MODID)
public class PlantTechMain
{
    public static final String MODID = "planttech2";

    public static final Logger LOGGER = LogManager.getLogger(MODID);

    public PlantTechMain()
    {
        IEventBus modEventBus = FMLJavaModLoadingContext.get().getModEventBus();
        CropTypes.DEFAULT = CropTypes.IRON_CROP;
        ModLoadingContext.get().registerConfig(ModConfig.Type.SERVER, PlantTech2Configuration.SERVER);
        ModLoadingContext.get().registerConfig(ModConfig.Type.CLIENT, PlantTech2Configuration.CLIENT);
        

        modEventBus.addListener(this::setup);
        MinecraftForge.EVENT_BUS.addListener(ModCommands::onCommandRegister);

        ModCreativeTabs.TABS.register(modEventBus);
        ModBlocks.BLOCKS.register(modEventBus);
        ModBlocks.registerCrops();
        ModFluids.FLUIDS.register(modEventBus);
        ModBlocks.registerItemBlocks();
        ModItems.ITEMS.register(modEventBus);
        ModSounds.SOUNDS.register(modEventBus);
        ModItems.registerSeedsAndParticles();
        ModEntityTypes.ENTITY_TYPES.register(modEventBus);
        ModBlockEntities.BLOCK_ENTITIES.register(modEventBus);
        ModContainers.CONTAINERS.register(modEventBus);
        ModRecipeTypes.RECIPE_TYPES.register(modEventBus);
        ModRecipeSerializers.RECIPE_SERIALIZERS.register(modEventBus);
    }

    private void setup(final FMLCommonSetupEvent event)
    {
        PlantTech2PacketHandler.register();
    }
}
