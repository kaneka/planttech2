package net.kaneka.planttech2.utilities;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import net.kaneka.planttech2.crops.CropTypes;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.packs.resources.ResourceManager;
import net.minecraft.server.packs.resources.SimpleJsonResourceReloadListener;
import net.minecraft.util.GsonHelper;
import net.minecraft.util.profiling.ProfilerFiller;
import net.minecraftforge.common.crafting.CraftingHelper;
import net.minecraftforge.common.crafting.conditions.ICondition;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import static net.kaneka.planttech2.PlantTechMain.LOGGER;

public abstract class BaseReloadListener<T> extends SimpleJsonResourceReloadListener
{
    public static final String INDEX_FILE_NAME = "_index";
    private final String name;
    protected final Gson gson;

    public BaseReloadListener(String name, Gson gson, String folder)
    {
        super(gson, folder);
        this.name = name;
        this.gson = gson;
    }

    protected T loadIndex(JsonElement element)
    {
        return null;
    }

    @Override
    protected void apply(Map<ResourceLocation, JsonElement> elementMap, ResourceManager resourceManager, ProfilerFiller profiler)
    {
        LOGGER.debug("Loading " + getName());
        Map<ResourceLocation, Supplier<T>> map = new HashMap<>();

        for (ResourceLocation rel : elementMap.keySet())
        {
            JsonElement element = elementMap.get(rel);
            if (rel.getPath().equals(INDEX_FILE_NAME))
            {
                map.put(rel, () -> loadIndex(element));
                continue;
            }
            try
            {
                if (!CraftingHelper.processConditions(GsonHelper.convertToJsonObject(element, "top element"), "conditions", ICondition.IContext.EMPTY))
                {
                    LOGGER.debug("Skipping loading " + name + " {} as it's conditions were not met", rel);
                    continue;
                }

                map.put(rel, () -> fromJson(element));
            }
            catch (IllegalArgumentException | JsonSyntaxException ex)
            {
                LOGGER.error("Error while loading " + name + " {}", rel, ex);
            }
        }

        postReload(map);
    }

    protected abstract T fromJson(JsonElement element);

    protected void postReload(Map<ResourceLocation, Supplier<T>> map)
    {

    }

}
