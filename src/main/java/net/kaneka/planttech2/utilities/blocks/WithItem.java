package net.kaneka.planttech2.utilities.blocks;

import net.kaneka.planttech2.utilities.ModCreativeTabs;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraftforge.registries.RegistryObject;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(value= RetentionPolicy.RUNTIME)
@Target(value= ElementType.FIELD)
public @interface WithItem {
    enum Tab{
        MAIN(ModCreativeTabs.MAIN),
        BLOCKS(ModCreativeTabs.BLOCKS),
        MACHINES(ModCreativeTabs.MACHINES);
        private RegistryObject<CreativeModeTab> tab;

        Tab(RegistryObject<CreativeModeTab> tab){
            this.tab = tab;
        }

        public RegistryObject<CreativeModeTab> getTab(){
         return tab;
        }

    }
    boolean isWIP() default false;
    Tab tab() default Tab.BLOCKS;
}
