package net.kaneka.planttech2.utilities.blocks;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(value= RetentionPolicy.RUNTIME)
@Target(value= ElementType.FIELD)
@Deprecated // The render type is migrated to the model json
public @interface BlockRenderLayer {
    enum RenderLayer {
        CUTOUT, TRANSLUCENT;
    }

    RenderLayer layer() default RenderLayer.CUTOUT;
}