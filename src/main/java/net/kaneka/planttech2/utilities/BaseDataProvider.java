package net.kaneka.planttech2.utilities;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.kaneka.planttech2.PlantTechMain;
import net.minecraft.data.CachedOutput;
import net.minecraft.data.DataProvider;
import net.minecraft.data.PackOutput;
import net.minecraft.resources.ResourceLocation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;

public abstract class BaseDataProvider<T> implements DataProvider
{
    protected static final Logger LOGGER = LogManager.getLogger();
    protected final PackOutput dataGenerator;
    protected final Function<T, ResourceLocation> locationGetter;
    protected final List<T> data = new ArrayList<>();

    public BaseDataProvider(PackOutput generator, Function<T, ResourceLocation> locationGetter)
    {
        this.dataGenerator = generator;
        this.locationGetter = locationGetter;
    }

    protected JsonObject writeIndex()
    {
        return null;
    }

    @Override
    public CompletableFuture<?> run(CachedOutput cache)
    {
        Path path = this.dataGenerator.getOutputFolder();
        List<T> entryData = this.getData();
        this.validate(entryData);

        List<CompletableFuture<?>> completables = new ArrayList<>();
        entryData.forEach((entry) -> {
            Path outputFile = getPath(path, locationGetter.apply(entry));
            try
            {
                completables.add(DataProvider.saveStable(cache, toJson(entry), outputFile));
            }
            catch (Exception ioexception)
            {
                LOGGER.error("Couldn't save " + getName() + " {}", outputFile, ioexception);
            }
        });

        try
        {
            JsonObject index = writeIndex();
            if (index != null)
                completables.add(DataProvider.saveStable(cache, index, getPath(path, modLoc(BaseReloadListener.INDEX_FILE_NAME))));
        }
        catch (Exception ioexception)
        {
            LOGGER.error("Couldn't save index file for " + path, ioexception);
        }

        return CompletableFuture.allOf(completables.toArray(new CompletableFuture[0]));
    }

    protected static ResourceLocation modLoc(String path)
    {
        return new ResourceLocation(PlantTechMain.MODID, path);
    }

    protected abstract void validate(List<T> dataMap);

    protected abstract List<T> getData();

    protected abstract Path getPath(Path pathIn, ResourceLocation id);

    protected abstract JsonElement toJson(T data);

}
