package net.kaneka.planttech2.utilities;

import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.crops.TemperatureTolerance;
import net.kaneka.planttech2.gui.GuidePlantsScreen;
import net.kaneka.planttech2.gui.guide.GuideScreen;
import net.kaneka.planttech2.items.BiomassContainerItem;
import net.kaneka.planttech2.items.GuideItem;
import net.kaneka.planttech2.items.upgradeable.MultitoolItem;
import net.kaneka.planttech2.items.upgradeable.RangedWeaponItem;
import net.kaneka.planttech2.registries.ModItems;
import net.minecraft.ChatFormatting;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Font;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.client.renderer.item.ItemProperties;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;

import java.util.List;

public class PTClientUtil
{
    public static void drawJEIBiomass(int biomass, GuiGraphics mStack, int width)
    {
        if (biomass > 0)
            mStack.drawCenteredString(Minecraft.getInstance().font, Component.literal(String.valueOf(biomass)).append(" ").append(Component.translatable("fluid.biomass").getString()).withStyle(ChatFormatting.GREEN), width / 2, 19, PlantTechConstants.BIOMASS_JEI_COLOUR);
    }

    public static void drawCenteredString(GuiGraphics mStack, Font font, Component string, float posX, float posY, boolean shadow)
    {
        int c = string.getStyle().getColor() == null ? 0xFFFFFF : string.getStyle().getColor().getValue();
        mStack.drawString(font, string.getVisualOrderText(), posX - (font.width(string) / 2.0F), posY, c, shadow);
    }

    public static void drawCenteredString(GuiGraphics mStack, Font font, String string, int posX, int posY, int colour, boolean shadow)
    {
        mStack.drawString(font, string, posX - (font.width(string) / 2.0F), posY, colour, shadow);
    }

    private static final ResourceLocation DRILLING_PREDICATE = new ResourceLocation(PlantTechMain.MODID, "drilling");
    public static final ResourceLocation FILLED_PREDICATE = new ResourceLocation(PlantTechMain.MODID, "filled");
    private static final ResourceLocation PULL_PREDICATE = new ResourceLocation(PlantTechMain.MODID, "pull");
    private static final ResourceLocation PULLING_PREDICATE = new ResourceLocation(PlantTechMain.MODID, "pulling");
    private static final ResourceLocation THERMOMETER_TEMPERATURE_PREDICATE = new ResourceLocation(PlantTechMain.MODID, "temperature");

    public static void addAllItemModelsOverrides()
    {
        ItemProperties.register(
                ModItems.MULTITOOL.get(), DRILLING_PREDICATE,
                (stack, world, entity, seed) -> entity == null || !(stack.getItem() instanceof MultitoolItem) ? 0.0F : (entity.tickCount % 4) + 1
        );
        ItemProperties.register(ModItems.BIOMASSCONTAINER.get(), FILLED_PREDICATE,
                (stack, world, entity, seed) -> BiomassContainerItem.getFillLevelModel(stack));
        ItemProperties.register(
                ModItems.CYBERBOW.get(), PULL_PREDICATE,
                (stack, world, entity, seed) -> entity == null || !(entity.getUseItem().getItem() instanceof RangedWeaponItem) ? 0.0F : (float) (stack.getUseDuration() - entity.getUseItemRemainingTicks()) / 20.0F
        );
        ItemProperties.register(
                ModItems.CYBERBOW.get(), PULLING_PREDICATE,
                (stack, world, entity, seed) -> entity != null && entity.isUsingItem() && entity.getUseItem() == stack ? 1.0F : 0.0F);
        ItemProperties.register(
                ModItems.THERMOMETER.get(), THERMOMETER_TEMPERATURE_PREDICATE,
                (stack, world, entity, seed) -> {
                    if (world != null && entity != null)
                    {
                        float temp = world.getBiome(entity.blockPosition()).get().getModifiedClimateSettings().temperature();
                        if (TemperatureTolerance.EXTREME_COLD.inRange(temp))
                            return 0.1F;
                        if (TemperatureTolerance.COLD.inRange(temp))
                            return 0.1F;
                        if (TemperatureTolerance.NORMAL.inRange(temp))
                            return 0.2F;
                        if (TemperatureTolerance.WARM.inRange(temp))
                            return 0.3F;
                    }
                    return 0.4F;
                });
    }

    public static void openGuideScreen(GuideItem guide)
    {
        Screen screen = guide == ModItems.GUIDE_PLANTS.get() ? new GuidePlantsScreen() : new GuideScreen();
        openScreen(screen);
    }

    public static void openScreen(Screen screen)
    {
        Minecraft.getInstance().setScreen(screen);
    }

    public static int getBarWidthForEnergyItem(ItemStack stack)
    {
        return PTCommonUtil.tryAccessEnergyValue(stack, (storage) -> (int) (13 * ((float) storage.getEnergyStored() / storage.getMaxEnergyStored())), 13);
    }

    public static void appendEnergyTooltip(ItemStack stack, List<Component> tooltip)
    {
        PTCommonUtil.tryAccessEnergy(stack, (cap) -> tooltip.add(Component.translatable("planttech2.info.energy", cap.getEnergyStored() + "/" + cap.getMaxEnergyStored())));
    }
}
