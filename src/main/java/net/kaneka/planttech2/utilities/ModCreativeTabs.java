package net.kaneka.planttech2.utilities;

import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.crops.CropTypes;
import net.kaneka.planttech2.registries.ModBlocks;
import net.kaneka.planttech2.registries.ModItems;
import net.minecraft.core.registries.Registries;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.ItemLike;
import net.minecraftforge.event.BuildCreativeModeTabContentsEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.RegistryObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class ModCreativeTabs
{
	public static final DeferredRegister<CreativeModeTab> TABS = DeferredRegister.create(Registries.CREATIVE_MODE_TAB, PlantTechMain.MODID);

	private static final Map<RegistryObject<CreativeModeTab>, List<Supplier<ItemStack>>> ITEMS_MAP = new HashMap<>();
	public static final RegistryObject<CreativeModeTab> MAIN = tab("main", () -> ModItems.WRENCH.get());
	public static final RegistryObject<CreativeModeTab> BLOCKS = tabSearchable("blocks", () -> ModBlocks.KANEKIUM_BLOCK.get());
	public static final RegistryObject<CreativeModeTab> MACHINES = tab("machines", () -> ModBlocks.SOLARGENERATOR.get());
	public static final RegistryObject<CreativeModeTab> SEEDS = tabSearchable("seeds", () -> CropTypes.DIAMOND_CROP.getSeed());
	public static final RegistryObject<CreativeModeTab> PARTICLES = tabSearchable("particles", () -> ModItems.COLOR_PARTICLES.get());
	public static final RegistryObject<CreativeModeTab> TOOLS_AND_ARMOR = tab("toolsandarmor", () -> ModItems.CYBERARMOR_CHEST.get());
	public static final RegistryObject<CreativeModeTab> CHIPS = tabSearchable("chips", () -> ModItems.CAPACITYUPGRADE_TIER_1.get());

	private static <I extends ItemLike, S extends Supplier<I>> RegistryObject<CreativeModeTab> tab(String name, S icon)
	{
		return TABS.register(name, () -> CreativeModeTab.builder().icon(() -> new ItemStack(icon.get())).withTabsBefore(PlantTechConstants.VANILLA_TABS.toArray(new ResourceKey[]{})).title(Component.translatable("itemGroup." + PlantTechMain.MODID + "_" + name)).build());
	}

	private static <I extends ItemLike, S extends Supplier<I>> RegistryObject<CreativeModeTab> tabSearchable(String name, S icon)
	{
		return TABS.register(name, () -> CreativeModeTab.builder().icon(() -> new ItemStack(icon.get())).withSearchBar().withTabsBefore(PlantTechConstants.VANILLA_TABS.toArray(new ResourceKey[]{})).title(Component.translatable("itemGroup." + PlantTechMain.MODID + "_" + name)).build());
	}

	public static <I extends Item, T extends Supplier<I>> T putItem(RegistryObject<CreativeModeTab> category, T item)
	{
		ITEMS_MAP.computeIfAbsent(category, (e) -> new ArrayList<>()).add(() -> new ItemStack(item.get()));
		return item;
	}

	public static void putItemStack(RegistryObject<CreativeModeTab> category, Supplier<ItemStack> item)
	{
		ITEMS_MAP.computeIfAbsent(category, (e) -> new ArrayList<>()).add(item);
	}

	@SubscribeEvent
	public static void registerCreativeTabs(BuildCreativeModeTabContentsEvent event)
	{
		ITEMS_MAP.keySet().stream().filter((e) -> e != null && e.getKey().equals(event.getTabKey())).findFirst().ifPresent((e) -> {
			event.acceptAll(PTCommonUtil.collect(ITEMS_MAP.get(e), Supplier::get));
		});
	}
}
