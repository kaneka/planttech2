package net.kaneka.planttech2.utilities;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.common.util.INBTSerializable;

/**
 * Easier management of nbt data i/o, currently used for crop list syncing
 */
public interface ISerializable extends INBTSerializable<CompoundTag>
{
    @Override
    default void deserializeNBT(CompoundTag nbt)
    {

    }

    default void extraToNetwork(FriendlyByteBuf buf)
    {

    }

    default void extraFromNetwork(FriendlyByteBuf buf)
    {

    }
}
