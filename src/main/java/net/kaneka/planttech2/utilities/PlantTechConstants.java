package net.kaneka.planttech2.utilities;

import com.google.common.collect.ImmutableSet;
import net.kaneka.planttech2.PlantTechMain;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.item.CreativeModeTab;

import java.util.Set;

import static net.minecraft.world.item.CreativeModeTabs.*;

public class PlantTechConstants
{
    public static final int MACHINETIER_MACHINEBULBREPROCESSOR = 0; 
    public static final int MACHINETIER_SEEDSQUEEZER = 0; 
    public static final int MACHINETIER_COMPRESSOR = 1; 
    public static final int MACHINETIER_IDENTIFIER = 1; 
    public static final int MACHINETIER_ENERGY_SUPPLIER = 2; 
    public static final int MACHINETIER_INFUSER = 2; 
    public static final int MACHINETIER_CHIPALYZER = 2; 
    public static final int MACHINETIER_MEGAFURNACE = 3; 
    public static final int MACHINETIER_DNA_CLEANER = 4; 
    public static final int MACHINETIER_DNA_COMBINER = 4; 
    public static final int MACHINETIER_DNA_EXTRACTOR = 4; 
    public static final int MACHINETIER_DNA_REMOVER = 4; 
    public static final int MACHINETIER_SEEDCONSTRUCTOR = 4; 
    public static final int MACHINETIER_PLANTFARM = 5;
    public static final int MACHINETIER_SOLARGENERATOR = 5;
    public static final int MACHINETIER_CROP_AURA_GENERATOR = 5;

    public static final String FORGE_ID = "forge";
    public static final String MINECRAFT_ID = "minecraft";

    public static final int BIOMASS_JEI_COLOUR = 0xFF808080;
    public static final int WHITE = 0xFFFFFFFF;
    public static final Component EMPTY_TEXT = Component.literal("");
    public static final int DURABILITY_BAR_COLOUR = Integer.parseInt("06bc00", 16);
    public static final int DURABILITY_BAR_WIDTH = 13;

    public static final int ADVANCED_ANALYSER_ENERGY_CAPACITY = 1000;
    public static final int ADVANCED_ANALYSER_ENERGY_USE = 100;

    public static final int UPGRADABLE_ARMOUR_DEFAULT_ENERGY_USE = 20;

    public static final String BIOMASS_TAG = PlantTechMain.MODID + ":biomass";

    public static final int BIOMASS_CONTAINER_CAPACITY = 1000;

    // Make sure Vanilla tab orders correctly for the new way of creative tabs registry
    // These tabs should go BEFORE the mod ones
    public static final Set<ResourceKey<CreativeModeTab>> VANILLA_TABS = ImmutableSet.of(
            COLORED_BLOCKS,
            FOOD_AND_DRINKS,
            INGREDIENTS,
            SPAWN_EGGS
    );
}
