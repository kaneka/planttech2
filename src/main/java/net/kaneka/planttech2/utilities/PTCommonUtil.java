package net.kaneka.planttech2.utilities;

import net.kaneka.planttech2.addons.Pair;
import net.kaneka.planttech2.energy.BioEnergyStorage;
import net.kaneka.planttech2.registries.ModFluids;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.util.RandomSource;
import net.minecraft.world.MenuProvider;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.network.NetworkHooks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

public class PTCommonUtil
{
    public static <T> T randomByWeight(List<T> list, Function<T, Float> weight, RandomSource random)
    {
        float totalWeight = (float) list.stream().map(weight).mapToDouble(Float::doubleValue).sum();
        double minInterval = 0;
        double rand = random.nextDouble();
        for (T t : list)
        {
            double maxInterval = minInterval + weight.apply(t) / totalWeight;
            if (rand >= minInterval && rand < maxInterval)
                return t;
            minInterval = maxInterval;
        }
        return null;
    }

    public static void openItemContainer(ServerPlayer sp, String name, ItemStack stack, TriFunction<Integer, Inventory, ItemStack, AbstractContainerMenu> menu)
    {
        NetworkHooks.openScreen(sp, new NamedContainerProvider(name, stack, menu), (buffer) -> buffer.writeItem(stack));
    }

    public static void writeEnergySharedTag(ICapabilityProvider holder, CompoundTag tag)
    {
        tryAccessEnergyStorage(holder, (storage) -> tag.put("bioEnergyCap", storage.serializeNBT()));
    }

    public static void readEnergySharedTag(ICapabilityProvider holder, CompoundTag tag)
    {
        tryAccessEnergyStorage(holder, (storage) -> storage.deserializeNBT(tag.getCompound("bioEnergyCap")));
    }

    public static LazyOptional<IEnergyStorage> getEnergyCap(ICapabilityProvider holder)
    {
        return holder.getCapability(ForgeCapabilities.ENERGY);
    }

    public static Pair<LazyOptional<? extends IFluidHandler>, Integer> getBiomassCap(ICapabilityProvider holder)
    {
        Capability<? extends IFluidHandler> capability = holder instanceof ItemStack ? ForgeCapabilities.FLUID_HANDLER_ITEM : ForgeCapabilities.FLUID_HANDLER;
        LazyOptional<? extends IFluidHandler> optional = holder.getCapability(capability);
        AtomicInteger index = new AtomicInteger(0);
        AtomicBoolean containsBiomass = new AtomicBoolean(false);
        optional.ifPresent((cap) -> {
            for (int i = 0; i < cap.getTanks(); i++)
            {
                if (cap.isFluidValid(i, new FluidStack(ModFluids.BIOMASS.get(), 1)))
                {
                    containsBiomass.set(true);
                    index.set(i);
                    break;
                }
            }
        });
        return Pair.of(containsBiomass.get() ? optional : LazyOptional.empty(), index.get());
    }

    public static LazyOptional<IItemHandler> getInvCap(ICapabilityProvider holder)
    {
        return holder.getCapability(ForgeCapabilities.ITEM_HANDLER);
    }

    public static void tryAccessEnergy(ICapabilityProvider holder, Consumer<IEnergyStorage> action)
    {
        getEnergyCap(holder).ifPresent(action::accept);
    }

    public static void tryAccessEnergyStorage(ICapabilityProvider holder, Consumer<BioEnergyStorage> action)
    {
        getEnergyCap(holder).ifPresent((cap) -> {
            if (cap instanceof BioEnergyStorage storage)
                action.accept(storage);
        });
    }

    public static void tryAccessBiomass(ICapabilityProvider holder, BiConsumer<IFluidHandler, Integer> action)
    {
        Pair<LazyOptional<? extends IFluidHandler>, Integer> optional = getBiomassCap(holder);
        optional.getA().ifPresent((cap) -> {
            action.accept(cap, optional.getB());
        });
    }

    public static void tryAccessInv(ICapabilityProvider holder, Consumer<IItemHandler> action)
    {
        getInvCap(holder).ifPresent(action::accept);
    }

    public static <T> T tryAccessBiomassValue(ICapabilityProvider holder, BiFunction<IFluidHandler, Integer, T> action, T defau1t)
    {
        AtomicReference<T> value = new AtomicReference<>(defau1t);
        Capability<? extends IFluidHandler> capability = holder instanceof ItemStack ? ForgeCapabilities.FLUID_HANDLER_ITEM : ForgeCapabilities.FLUID_HANDLER;
        holder.getCapability(capability).ifPresent((cap) -> {
            for (int i = 0; i < cap.getTanks(); i++)
            {
                if (cap.isFluidValid(i, new FluidStack(ModFluids.BIOMASS.get(), 1)))
                {
                    value.set(action.apply(cap, i));
                    break;
                }
            }
        });
        return value.get();
    }

    public static <T> T tryAccessInvValue(ICapabilityProvider holder, Function<IItemHandler, T> action, T defau1t)
    {
        return tryAccessCapability(IItemHandler.class, ForgeCapabilities.ITEM_HANDLER, holder, action, defau1t);
    }

    public static <T> T tryAccessEnergyValue(ICapabilityProvider holder, Function<IEnergyStorage, T> action, T defau1t)
    {
        return tryAccessCapability(IEnergyStorage.class, ForgeCapabilities.ENERGY, holder, action, defau1t);
    }

    private static <T, S, C> T tryAccessCapability(Class<S> storageClass, Capability<C> type, ICapabilityProvider holder, Function<S, T> action, T defau1t)
    {
        AtomicReference<T> value = new AtomicReference<>(defau1t);
        holder.getCapability(type).ifPresent((cap) -> {
            if (storageClass.isInstance(cap))
                value.set(action.apply((S) cap));
        });
        return value.get();
    }

    public static <T, V> List<V> collect(Collection<T> list, Function<T, V> function)
    {
        return list.stream().collect(ArrayList::new, (list2, element) -> {
            V e = function.apply(element);
            if (e != null)
                list2.add(e);
        }, ArrayList::addAll);
    }

    private record NamedContainerProvider(String name, ItemStack stack, TriFunction<Integer, Inventory, ItemStack, AbstractContainerMenu> menu) implements MenuProvider
    {
        @Override
        public AbstractContainerMenu createMenu(int id, Inventory inv, Player player)
        {
            return menu.apply(id, inv, stack);
        }

        @Override
        public Component getDisplayName()
        {
            return Component.translatable(name);
        }
    }

    public interface TriFunction<A, B, C, R>
    {
        R apply(A a, B b, C c);
    }

    public interface TriConsumer<A, B, C>
    {
        void accept(A a, B b, C c);
    }

    public static void stacktrace(Object... objects)
    {
        try
        {
            throw new NullPointerException(Arrays.toString(objects));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
