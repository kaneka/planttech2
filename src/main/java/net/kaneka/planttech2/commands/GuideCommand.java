package net.kaneka.planttech2.commands;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import net.kaneka.planttech2.gui.GuidePlantsScreen;
import net.kaneka.planttech2.gui.guide.GuideScreen;
import net.minecraft.client.Minecraft;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.Commands;

public class GuideCommand
{
    public GuideCommand() {}

    public static ArgumentBuilder<CommandSourceStack, LiteralArgumentBuilder<CommandSourceStack>> register(CommandDispatcher<CommandSourceStack> dispatcher)
    {
        LiteralArgumentBuilder<CommandSourceStack> command = Commands.literal("guide");
        command.then(Commands.literal("overview").executes((context) -> {
            Minecraft.getInstance().setScreen(new GuideScreen());
            return 1;
        }));
        command.then(Commands.literal("plant").executes((context) -> {
            Minecraft.getInstance().setScreen(new GuidePlantsScreen());
            return 1;
        }));
        return command;
    }
}
