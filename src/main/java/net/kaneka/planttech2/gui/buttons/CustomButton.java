package net.kaneka.planttech2.gui.buttons;

import com.mojang.blaze3d.systems.RenderSystem;
import net.kaneka.planttech2.utilities.PTClientUtil;
import net.minecraft.ChatFormatting;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.components.AbstractButton;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.network.chat.Component;
import net.minecraftforge.client.gui.ScreenUtils;

public class CustomButton extends Button
{
	public final int id;
	public CustomButton(int id, int xPos, int yPos, int width, int height, String displayString, ICustomPressable pressable)
	{
		super(xPos, yPos, width, height, Component.literal(displayString).withStyle(ChatFormatting.WHITE), pressable, (msg) -> {
			return Component.literal(displayString).withStyle(ChatFormatting.WHITE);
		});
		this.id = id;
	}

	@Override
	public void renderWidget(GuiGraphics pStack, int mouseX, int mouseY, float partial)
	{
		if (this.visible)
		{
			Minecraft mc = Minecraft.getInstance();
			RenderSystem.setShader(GameRenderer::getPositionTexShader);
			RenderSystem.setShaderTexture(0, AbstractButton.WIDGETS_LOCATION);
			RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, this.alpha);
			RenderSystem.enableBlend();
			RenderSystem.defaultBlendFunc();
			RenderSystem.enableDepthTest();
			this.isHovered = mouseX >= this.getX() && mouseY >= this.getY() && mouseX < this.getX() + this.width && mouseY < this.getY() + this.height;
			int k = !this.active ? 0 : (this.isHoveredOrFocused() ? 2 : 1);
			ScreenUtils.blitWithBorder(pStack, this.getX(), this.getY(), 0, 46 + k * 20, this.width, this.height, 200, 20, 2, 3, 2, 2, 0);

			Component buttonText = this.getMessage();
			int strWidth = mc.font.width(buttonText);
			int ellipsisWidth = mc.font.width("...");

			if (strWidth > width - 6 && strWidth > ellipsisWidth)
				buttonText = Component.literal(mc.font.substrByWidth(buttonText, width - 6 - ellipsisWidth).getString() + "...");

			PTClientUtil.drawCenteredString(pStack, mc.font, buttonText, this.getX() + this.width / 2.0F, this.getY() + (this.height - 8) / 2.0F, true);
		}
	}

	@FunctionalInterface
	public interface ICustomPressable extends OnPress
	{
		@Override
		default void onPress(Button button)
		{
			this.onPress((CustomButton) button);
		}

		void onPress(CustomButton p_onPress_1_);
	}
}
