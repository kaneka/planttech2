package net.kaneka.planttech2.gui;

import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.blocks.entity.machine.PlantFarmBlockEntity;
import net.kaneka.planttech2.inventory.PlantFarmMenu;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;

public class PlantFarmScreen extends MachineScreen<PlantFarmMenu>
{ 
	private static final ResourceLocation BACKGROUND = new ResourceLocation(PlantTechMain.MODID + ":textures/gui/container/plantfarm.png");

	public PlantFarmScreen(PlantFarmMenu container, Inventory player, Component name)
    {
    	super(container, player, name, BACKGROUND);
    }

	@Override
	protected void renderBg(GuiGraphics pStack, float partialTicks, int mouseX, int mouseY)
	{
		super.renderBg(pStack, partialTicks, mouseX, mouseY);

		for (int i = 0; i < 5; i++)
		{
			int l = this.getCookProgressScaled(i);
			pStack.blit(getBackgroundTexture(), this.leftPos + 106, this.topPos + 37 + i * 5, 0, 200, l, 4);
		}

		renderEnergy(pStack);

		int j = this.getFluidStoredScaled(55);
		pStack.blit(getBackgroundTexture(), this.leftPos + 41, this.topPos + 28 + (55-j), 224, 55-j, 16, 0 + j);
	}

	@Override
	protected int getCookProgressScaled(int slot)
	{
		int i = menu.getValue(4 + slot);
		return i != 0 ? i * 16 / ((PlantFarmBlockEntity) this.te).getTicks() : 0;
	}
	
	@Override
	protected String getGuideEntryString()
	{
		return "void_plantfarm";
	}
}
