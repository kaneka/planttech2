package net.kaneka.planttech2.gui;

import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.inventory.SeedConstructorMenu;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;

public class SeedconstructorScreen extends MachineScreen<SeedConstructorMenu>
{ 
	private static final ResourceLocation BACKGROUND = new ResourceLocation(PlantTechMain.MODID + ":textures/gui/container/seedconstructor.png");

	public SeedconstructorScreen(SeedConstructorMenu container, Inventory player, Component name)
    {
    	super(container, player, name, BACKGROUND);
    }

	@Override
	protected void renderBg(GuiGraphics pStack, float partialTicks, int mouseX, int mouseY)
	{
		super.renderBg(pStack, partialTicks, mouseX, mouseY);

		int l = this.getCookProgressScaled(15);
		pStack.blit(getBackgroundTexture(), this.leftPos + 96, this.topPos + 48, 0, 200, 14, l);

		renderEnergy(pStack);

		int j = this.getFluidStoredScaled(55);
		pStack.blit(getBackgroundTexture(), this.leftPos + 41, this.topPos + 28 + (55-j), 224, 55-j, 16, 0 + j);
	}
	
	@Override
	protected void drawTooltips(GuiGraphics mStack, int mouseX, int mouseY)
	{
	    drawTooltip(mStack, menu.getValue(2) + "/" + menu.getValue(3), mouseX, mouseY, 41, 28, 16, 55);
	    super.drawTooltips(mStack, mouseX, mouseY);
	}
	
	@Override
	protected String getGuideEntryString()
	{
		return "seedconstructor";
	}
}
