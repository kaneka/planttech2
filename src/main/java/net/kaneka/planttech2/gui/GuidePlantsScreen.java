package net.kaneka.planttech2.gui;

import com.mojang.blaze3d.systems.RenderSystem;
import net.kaneka.planttech2.crops.CropConfiguration;
import net.kaneka.planttech2.crops.CropTypes;
import net.kaneka.planttech2.crops.DropEntry;
import net.kaneka.planttech2.crops.ParentPair;
import net.kaneka.planttech2.crops.TemperatureTolerance;
import net.kaneka.planttech2.gui.buttons.CustomButton;
import net.kaneka.planttech2.utilities.PTClientUtil;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.components.AbstractWidget;
import net.minecraft.core.Holder;
import net.minecraft.core.HolderLookup;
import net.minecraft.core.registries.Registries;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.biome.Biome;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

public class GuidePlantsScreen extends GuideBaseScreen
{
	private final String[] buttonEntryNames = new String[8];
	private Component selectedName = Component.literal("");
	protected ItemStack primarySeed = ItemStack.EMPTY;
	protected ItemStack soil = ItemStack.EMPTY;
	protected ItemStack[] seeds = new ItemStack[9];
	protected DropEntry[] drops = new DropEntry[9];
	protected ItemStack[][] parents = new ItemStack[4][2];
	protected TemperatureTolerance temp = TemperatureTolerance.NORMAL;
	protected List<Component> temperatureStrings = new ArrayList<>();

	protected List<Map<Integer, CropTypes>> familyTree;
	protected Boolean isFamilyTree = false;
	protected List<List<Point>> treePoints = new ArrayList<>();
	protected final List<Map<Integer, ItemStack>> parentsInTrees = new ArrayList<>();

	public GuidePlantsScreen()
	{
		super(CropTypes.getEnabledLength() - 8, true, "screen.guideplants");
		treePoints.add(new ArrayList<>(List.of(new Point(262, 22))));
		treePoints.add(new ArrayList<>(Arrays.asList(new Point(206, 88), new Point(318, 88))));
		treePoints.add(new ArrayList<>(Arrays.asList(new Point(178, 126), new Point(234, 126), new Point(290, 126), new Point(346, 126))));
		treePoints.add(new ArrayList<>(Arrays.asList(new Point(164, 150), new Point(192, 150), new Point(220, 150), new Point(248, 150),
				new Point(276, 150), new Point(304, 150), new Point(332, 150), new Point(360, 150)
		)));
		treePoints.add(new ArrayList<>(Arrays.asList(new Point(157, 167), new Point(171, 167), new Point(185, 167), new Point(199, 167),
				new Point(213, 167), new Point(227, 167), new Point(241, 167), new Point(255, 167),
				new Point(269, 167), new Point(283, 167), new Point(297, 167), new Point(311, 167),
				new Point(325, 167), new Point(339, 167), new Point(353, 167), new Point(367, 167)
		)));
	}

	@Override
	public void init()
	{
		super.init();
		final int xPos = this.guiLeft + 31, baseYPos = this.guiTop + 10, width = 100, height = 20;
		for (int id = 0; id < 8; id++)
			addRenderableWidget(new CustomButton(id, xPos, baseYPos + (id * 22), width, height, "Button " + (id + 1), GuidePlantsScreen.this::buttonClicked));

		addRenderableWidget(new CustomButton(8, this.guiLeft + 295, this.guiTop+ 151, 10, 10, "+", GuidePlantsScreen.this::buttonClicked));
		addRenderableWidget(new CustomButton(9, this.guiLeft + 350, this.guiTop + 12, 30, 12, Component.translatable("gui.back").getString(), GuidePlantsScreen.this::buttonClicked));
		if (buttonEntryNames != null) updateButtons();
	}

	@Override
	public void render(GuiGraphics pStack, int mouseX, int mouseY, float partialTicks)
	{
		renderBackground(pStack);
		super.render(pStack, mouseX, mouseY, partialTicks);
	}

	@Override
	protected void drawForeground(GuiGraphics pStack)
	{
		if (hasSelection && !isFamilyTree)
		{
			RenderSystem.enableBlend();
			pStack.blit(BACKGROUND, (int) (this.guiLeft + 292 + temp.getMinTempTolerancePercent() * 40.0F), this.guiTop + 65, 0, 276, 5, 17, 512, 512);
			pStack.blit(BACKGROUND, (int) (this.guiLeft + 292 + temp.getMaxTempTolerancePercent() * 40.0F), this.guiTop + 65, 0, 276, 5, 17, 512, 512);
			RenderSystem.disableBlend();
			renderItem(pStack, this.primarySeed, 261, 32);

//			RenderHelper.disableStandardItemLighting();
//			RenderSystem.enableDepthTest();
			if (!soil.isEmpty())
				this.renderItem(pStack, this.soil, 217, 65);
//			RenderSystem.enableDepthTest();
			for (int i = 0; i < 9; i++)
			{
				if (!seeds[i].isEmpty())
					this.renderItem(pStack, seeds[i], 189 + 18 * i, 98);
				if (drops[i] != DropEntry.EMPTY)
					this.renderItem(pStack, drops[i].getItemStack(), 189 + 18 * i, 131);
			}
			for (int i = 0; i < 4; i++)
			{
				if (!parents[i][0].isEmpty())
				{
					this.renderItem(pStack, parents[i][0], 162 + 56 * i, 164);
					this.renderItem(pStack, parents[i][1], 192 + 56 * i, 164);
				}
			}
		}
		else if (isFamilyTree)
		{
			for (int i = 0; i < Math.min(5, familyTree.size()); i++)
			{
				for (int k = 0; k < Math.pow(2,i); k++)
				{
					if (familyTree.get(i).containsKey(k) && treePoints.size() > i && treePoints.get(i).size() > k)
					{
						Point point = treePoints.get(i).get(k);
						float scale = 0.7F;
						float width = 7.5F / 2.0F;
						float height = 7.5F / 2.0F;
						renderGuiItem((poseStack) -> poseStack.scale(scale, scale, 1.0F), parentsInTrees.get(i).get(k), guiLeft + point.x - width, guiTop + point.y - height);
//						itemRenderer.renderAndDecorateItem(parentsInTrees.get(i).get(k), (int) (this.guiLeft + point.x - width / 2.0F), (int) (this.guiTop + point.y - height / 2.0F));
//						pStack.blit(getBackgroundTexture(), this.guiLeft + point.x, this.guiTop + point.y, 0, 347, 9, 9, 512, 512);
					}
				}
			}
		}

	}

	@Override
	protected void updateButtons()
	{
		List<CropTypes> list = CropTypes.crops(false);
		for (int i = 0; i < 8; i++)
		{
			if (scrollPos + i < list.size())
			{
				CropTypes entry = list.get(scrollPos + i);
				if (renderables.get(i) instanceof AbstractWidget aWidget)
					aWidget.setMessage(entry.getDisplayName());
				buttonEntryNames[i] = entry.getName();
			}
		}
		if (renderables.get(8) instanceof AbstractWidget treeButton) {
			if (hasSelection && !isFamilyTree)
				activateButton(treeButton);
			else
				deactivateButton(treeButton);
		}

		if (renderables.get(9) instanceof AbstractWidget backButton)
		{
			if (hasSelection && isFamilyTree)
				activateButton(backButton);
			else
				deactivateButton(backButton);
		}
	}

	@Override
	protected void drawStrings(GuiGraphics mStack)
	{
		if (!hasSelection)
			PTClientUtil.drawCenteredString(mStack, font, Component.translatable("gui.non_selected"), this.guiLeft + 255, this.guiTop + 90, true);
		else if(!isFamilyTree)
		{
			PTClientUtil.drawCenteredString(mStack, font, selectedName, this.guiLeft + 269, this.guiTop + 15, true);
			PTClientUtil.drawCenteredString(mStack, font, Component.translatable("gui.soil"), this.guiLeft + 225, this.guiTop + 54, false);
			PTClientUtil.drawCenteredString(mStack, font, Component.translatable("gui.temperature"), this.guiLeft + 314, this.guiTop + 54, false);
			PTClientUtil.drawCenteredString(mStack, font, Component.translatable("gui.seeds"), this.guiLeft + 271, this.guiTop + 87, false);
			PTClientUtil.drawCenteredString(mStack, font, Component.translatable("gui.drops"), this.guiLeft + 271, this.guiTop + 120, false);
			PTClientUtil.drawCenteredString(mStack, font, Component.translatable("gui.parents"), this.guiLeft + 271, this.guiTop + 153, false);
		}
	}

	protected void buttonClicked(CustomButton button)
	{
		int buttonID = button.id;

		if (buttonID >= 0 && buttonID < 8)
		{
			this.setItems(buttonEntryNames[buttonID]);
			isFamilyTree = false;
		}
		else if(buttonID == 8)
			isFamilyTree = true;
		else if(buttonID == 9)
			isFamilyTree = false;

		updateButtons();
	}

	@SuppressWarnings("deprecation")
	protected void setItems(String entryName)
	{
		CropTypes crop = CropTypes.fromName(entryName);
		CropConfiguration config = crop.getConfig();
		hasSelection = true;

		familyTree = CropTypes.getFamilyTree(crop);
		parentsInTrees.clear();
		for (Map<Integer, CropTypes> CropTypes : familyTree)
		{
			Map<Integer, ItemStack> parentSeed = new HashMap<>();
			for (Map.Entry<Integer, CropTypes> mapEntry : CropTypes.entrySet())
				parentSeed.put(mapEntry.getKey(), new ItemStack(mapEntry.getValue().getConfig().getPrimarySeed().getItem().get()));
			parentsInTrees.add(parentSeed);
		}

		this.selectedName = crop.getDisplayName();
		this.primarySeed = config.getPrimarySeed().getItemStack();
		this.temp = config.getTemperature();
		this.temperatureStrings = new ArrayList<>();
		temperatureStrings.add(Component.literal(temp.getDisplayString()));
		Level world = Minecraft.getInstance().level;
		if (world != null)
			world.holderLookup(Registries.BIOME).listElementIds().forEach((entry) -> {
				HolderLookup<Biome> biomeHolders = world.holderLookup(Registries.BIOME);
				Holder.Reference<Biome> biomeRef = biomeHolders.get(entry).get();
				float biomeTemp = biomeRef.get().getModifiedClimateSettings().temperature();
				if (temp.inRange(biomeTemp))
					temperatureStrings.add(Component.translatable("biome." + entry.location().getNamespace() + "." + entry.location().getPath()).append(" (").append(String.valueOf(biomeTemp)).append(")"));
			});
		this.soil = new ItemStack(config.getSoil().get());

		Arrays.fill(this.seeds, ItemStack.EMPTY);
		Arrays.fill(this.drops, DropEntry.EMPTY);
		for (int k = 0; k < 4; k++)
			Arrays.fill(this.parents[k], ItemStack.EMPTY);

		List<Supplier<Ingredient>> seeds = config.getSeeds();
		List<DropEntry> drops = config.getDrops();
		List<ParentPair> parents = config.getParents();

		this.seeds[0] = config.getPrimarySeed().getItemStack();
		int i = 1;
		for (Supplier<Ingredient> seed : seeds)
		{
			for (ItemStack item : seed.get().getItems())
			{
				if (item.getItem() != Items.BARRIER)
				{
					this.seeds[i] = item.copy();
					i++;
				}
			}
		}

		this.drops[0] = config.getPrimarySeed();
		for (int j = 0; j < 8 && j < drops.size(); j++)
			this.drops[j + 1] = drops.get(j);

		for (int j = 0; j < 4 && j < parents.size(); j++)
		{
			ParentPair pair = parents.get(j);
			this.parents[j][0] = pair.getFirstParent().getConfig().getPrimarySeed().getItemStack();
			this.parents[j][1] = pair.getSecondParent().getConfig().getPrimarySeed().getItemStack();
		}
		updateButtons();
	}

	@Override
	protected void drawTooltips(GuiGraphics mStack, int mouseX, int mouseY)
	{
		if (hasSelection && !isFamilyTree)
		{
			this.drawTooltip(mStack, primarySeed.getHoverName(), mouseX, mouseY, 261, 32);
			if (!soil.isEmpty())
				this.drawTooltip(mStack, soil.getHoverName(), mouseX, mouseY, 217, 65);
			this.drawTooltip(mStack, temperatureStrings, mouseX, mouseY, 293, 64, 41, 17);

			for (int i = 0; i < 9; i++)
			{
				if (!seeds[i].isEmpty())
					this.drawTooltip(mStack, seeds[i].getHoverName(), mouseX, mouseY, 189 + 18 * i, 98);

				if (drops[i] != DropEntry.EMPTY)
					this.drawTooltip(mStack, Component.literal("").append(drops[i].getMin() + "-" + drops[i].getMax() + "x ")
									.append(drops[i].getItem().get().asItem().getDescription()), mouseX, mouseY,
							189 + 18 * i, 131);
			}

			for (int i = 0; i < 4; i++)
			{
				if (!parents[i][0].isEmpty())
				{
					this.drawTooltip(mStack, parents[i][0].getHoverName(), mouseX, mouseY, 162 + 56 * i, 164);
					this.drawTooltip(mStack, parents[i][1].getHoverName(), mouseX, mouseY, 192 + 56 * i, 164);
				}
			}
		}
		else if(isFamilyTree)
		{
			for (int i = 0; i < Math.min(5, familyTree.size()); i++)
			{
				for (int k = 0; k < Math.pow(2,i); k++)
				{
					if (familyTree.get(i).containsKey(k) && treePoints.size() > i && treePoints.get(i).size() > k)
					{
						Point point = treePoints.get(i).get(k);
						this.drawTooltip(mStack, familyTree.get(i).get(k).getConfig().getPrimarySeed().getItemStack().getHoverName(), mouseX, mouseY, point.x, point.y, 9, 9);
					}
				}
			}
		}
	}

	@Override
	public boolean mouseClicked(double mouseX, double mouseY, int mouseButton)
	{
		if (hasSelection && !isFamilyTree && mouseButton == 0)
		{
			Item clickedOn = null;
			if (this.inItemStackArea(mouseX, mouseY, 261, 32))
				clickedOn = this.primarySeed.getItem();
			if (this.inItemStackArea(mouseX, mouseY, 217, 65))
				if (soil != null)
					if (!soil.isEmpty())
						clickedOn = this.soil.getItem();

			for (int i = 0; i < 9; i++)
			{
				if (seeds[i] != null)
				{
					if (this.inItemStackArea(mouseX, mouseY, 189 + 18 * i, 98))
						clickedOn = this.seeds[i].getItem();
				}

				if (drops[i] != null)
				{
					if (this.inItemStackArea(mouseX, mouseY, 189 + 18 * i, 131))
						clickedOn = this.drops[i].getItem().get().asItem();
				}
			}

			for (int i = 0; i < 4; i++)
			{
				if (parents[i][0] != null)
				{
					if (this.inItemStackArea(mouseX, mouseY, 162 + 56 * i, 164))
						clickedOn = this.parents[i][0].getItem();
					if (this.inItemStackArea(mouseX, mouseY, 192 + 56 * i, 164))
						clickedOn = this.parents[i][1].getItem();
				}
			}
			if (clickedOn != null)
				CropTypes.getBySeed(new ItemStack(clickedOn)).ifPresent((entry) -> setItems(entry.getName()));
		}
		else if(isFamilyTree){
			String clickedName = null;
			for (int i = 0; i < Math.min(5, familyTree.size()); i++)
			{
				for (int k = 0; k < Math.pow(2, i); k++)
				{
					if (familyTree.get(i).containsKey(k) && treePoints.size() > i && treePoints.get(i).size() > k)
					{
						Point point = treePoints.get(i).get(k);
						if (this.inArea(mouseX, mouseY, point.x, point.y, 9, 9))
							clickedName = familyTree.get(i).get(k).getName();
					}
				}
			}
			if (clickedName != null)
			{
				isFamilyTree = false;
				this.setItems(clickedName);
			}
		}
		return super.mouseClicked(mouseX, mouseY, mouseButton);
	}

	private boolean inArea(double mouseX, double mouseY, int posX, int posY, int width, int height)
	{
		posX += this.guiLeft;
		posY += this.guiTop;
		return mouseX >= posX && mouseX <= posX + width && mouseY >= posY && mouseY <= posY + height;
	}

	private boolean inItemStackArea(double mouseX, double mouseY, int posX, int posY)
	{
		return this.inArea(mouseX, mouseY, posX, posY, 16, 16);
	}

	@Override
	protected void drawBackground(GuiGraphics mStack)
	{
		if (!hasSelection || isFamilyTree)
		{
			mStack.blit(BACKGROUND, this.guiLeft + 100, this.guiTop, 212, 0, 300, this.ySize, 512, 512);
			if (isFamilyTree)
				mStack.blit(BACKGROUND, this.guiLeft + 156, this.guiTop + 21, 0, 356, 221, 156, 512, 512);
		}
		else
			mStack.blit(BACKGROUND, this.guiLeft + 100, this.guiTop, 212, 196, 300, this.ySize, 512, 512);
		mStack.blit(BACKGROUND, this.guiLeft, this.guiTop, 0, 0, 150, this.ySize, 512, 512);
	}

	private void activateButton(AbstractWidget button)
	{
		button.active = true;
		button.visible = true;
	}

	private void deactivateButton(AbstractWidget button)
	{
		button.active = false;
		button.visible = false;
	}
}
