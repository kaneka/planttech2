package net.kaneka.planttech2.gui;

import com.mojang.blaze3d.systems.RenderSystem;
import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.inventory.SeedSqueezerMenu;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;

public class SeedSqueezerScreen extends MachineScreen<SeedSqueezerMenu>
{ 
	private static final ResourceLocation BACKGROUND = new ResourceLocation(PlantTechMain.MODID + ":textures/gui/container/seedsqueezer.png");

	public SeedSqueezerScreen(SeedSqueezerMenu container, Inventory player, Component name)
    {
    	super(container, player, name, BACKGROUND);
    }

	@Override
	protected void renderLabels(GuiGraphics pStack, int mouseX, int mouseY)
	{
		super.renderLabels(pStack, mouseX, mouseY);

		RenderSystem.setShaderColor(1.0f, 1.0f, 1.0f, 1.0f);
		int l = this.getCookProgressScaled(7);
		pStack.blit(BACKGROUND, 122, 36, 0, 202 + 8 - l, 16, l + 12);

		pStack.blit(BACKGROUND, 122, 62 - l, 16, 202, 16, l + 12);
	}

	@Override
	protected void renderBg(GuiGraphics mStack, float partialTicks, int mouseX, int mouseY)
	{
		super.renderBg(mStack, partialTicks, mouseX, mouseY);

		renderEnergy(mStack);
		int j = this.getFluidStoredScaled(55);
		mStack.blit(getBackgroundTexture(), this.leftPos + 41, this.topPos + 28 + (55-j), 224, 55-j, 16, j);
	}

	@Override
	protected void drawTooltips(GuiGraphics mStack, int mouseX, int mouseY)
	{
		drawTooltip(mStack, menu.getValue(2) + "/" + menu.getValue(3), mouseX, mouseY, 41, 28, 16, 55);
	    super.drawTooltips(mStack, mouseX, mouseY);
	}
	
	@Override
	protected String getGuideEntryString()
	{
		return "seed_squeezer";
	}
}
