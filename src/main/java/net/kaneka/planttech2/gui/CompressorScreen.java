package net.kaneka.planttech2.gui;

import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.inventory.CompressorMenu;
import net.kaneka.planttech2.packets.ButtonPressMessage;
import net.kaneka.planttech2.packets.PlantTech2PacketHandler;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;

public class CompressorScreen extends MachineScreen<CompressorMenu>
{
    private static final ResourceLocation BACKGROUND = new ResourceLocation(PlantTechMain.MODID + ":textures/gui/container/compressor.png");

    public CompressorScreen(CompressorMenu container, Inventory player, Component name)
    {
    	super(container, player, name, BACKGROUND);
    }

	@Override
    public boolean mouseClicked(double mouseX, double mouseY, int p_mouseClicked_5_)
    {
		for (int y = 0; y < 3; y++)
			for (int x = 0; x < 6; x++)
				if (inArea(mouseX, mouseY, 35 + x * 18, 26 + y * 18))
					PlantTech2PacketHandler.sendToServer(new ButtonPressMessage(te.getBlockPos().getX(), te.getBlockPos().getY(), te.getBlockPos().getZ(), x + y * 6));
        return super.mouseClicked(mouseX, mouseY, p_mouseClicked_5_);
    }

	@Override
	protected void renderBg(GuiGraphics pStack, float partialTicks, int mouseX, int mouseY)
	{
		super.renderBg(pStack, partialTicks, mouseX, mouseY);

		int l = this.getCookProgressScaled(70);
		pStack.blit(getBackgroundTexture(), this.leftPos + 53, this.topPos + 81, 0, 200, l, 8);

		int k = this.getEnergyStoredScaled(55);
		renderEnergy(pStack);
		int i = menu.getValue(3) - 2;
		if (i >= 0)
			pStack.blit(getBackgroundTexture(), this.leftPos + 34 + (i % 6) * 18, this.topPos + 25 + (i / 6) * 18, 224, 0, 18, 18);
	}

    private boolean inArea(double mouseX, double mouseY, int posX, int posY)
    {
		posX += this.leftPos;
		posY += this.topPos;
		return mouseX >= posX && mouseX <= posX + 18 && mouseY >= posY && mouseY <= posY + 18;
    }

	@Override
	protected String getGuideEntryString()
	{
		return "compressor";
	}
}
