package net.kaneka.planttech2.gui;

import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.inventory.IdentifierMenu;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;

public class IdentifierScreen extends MachineScreen<IdentifierMenu>
{ 
	private static final ResourceLocation BACKGROUND = new ResourceLocation(PlantTechMain.MODID + ":textures/gui/container/identifier.png");

	public IdentifierScreen(IdentifierMenu container, Inventory player, Component name)
    {
    	super(container, player, name, BACKGROUND);
    }

	@Override
	protected void renderBg(GuiGraphics pStack, float partialTicks, int mouseX, int mouseY)
	{
		super.renderBg(pStack, partialTicks, mouseX, mouseY);

		int l = this.getCookProgressScaled(18);
		pStack.blit(getBackgroundTexture(), this.leftPos + 74, this.topPos + 44, 0, 200, l, 18);

		renderEnergy(pStack);
	}
	
	@Override
	protected String getGuideEntryString()
	{
		return "identifier";
	}
}
