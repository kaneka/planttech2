package net.kaneka.planttech2.gui;

import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.blocks.entity.machine.MegaFurnaceBlockEntity;
import net.kaneka.planttech2.inventory.MegaFurnaceMenu;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;

public class MegaFurnaceScreen extends MachineScreen<MegaFurnaceMenu>
{ 
	private static final ResourceLocation BACKGROUND = new ResourceLocation(PlantTechMain.MODID + ":textures/gui/container/megafurnace.png");

	public MegaFurnaceScreen(MegaFurnaceMenu container, Inventory player, Component name)
    {
    	super(container, player, name, BACKGROUND);
    }

	@Override
	protected void renderBg(GuiGraphics pStack, float partialTicks, int mouseX, int mouseY)
	{
		super.renderBg(pStack, partialTicks, mouseX, mouseY);

		for (int p = 0; p < 6; p++)
		{
			int l = this.getCookProgressScaled(p);
			pStack.blit(getBackgroundTexture(), this.leftPos + 23 + p * 22, this.topPos + 46, 0, 200, 12, l);
		}

		renderEnergy(pStack);
	}

	@Override
	protected int getCookProgressScaled(int id)
	{
		int i = menu.getValue(id + 2);
		return i != 0 ? i * 15 / ((MegaFurnaceBlockEntity) this.te).ticksPerItem() : 0;
	}

	@Override
	protected String getGuideEntryString()
	{
		return "mega_furnace";
	}
}
