package net.kaneka.planttech2.gui;

import com.mojang.blaze3d.systems.RenderSystem;
import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.energy.BioEnergyStorage;
import net.kaneka.planttech2.inventory.ItemUpgradeableMenu;
import net.kaneka.planttech2.utilities.PTCommonUtil;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.registries.ForgeRegistries;

import java.util.HashMap;
import java.util.Map;

public class ItemUpgradeableScreen extends AbstractContainerScreen<ItemUpgradeableMenu>
{
	protected static final Map<Integer, ResourceLocation> BACKGROUND = new HashMap<Integer, ResourceLocation>()	{
		private static final long serialVersionUID = 1L; {
			put(10, new ResourceLocation(PlantTechMain.MODID + ":textures/gui/container/itemupgradeable_10.png"));
	    }
	};
	protected final Inventory player;
    protected ItemStack stack; 
    protected int invsize; 
    protected IEnergyStorage energystorage; 

    private static boolean WARNED = false;
    public ItemUpgradeableScreen(ItemUpgradeableMenu container, Inventory inv, Component name)
    {
    	super(container, inv, name); 
    	this.player = inv; 
    	stack = container.getStack(); 
    	invsize = PTCommonUtil.tryAccessInvValue(stack, IItemHandler::getSlots, 0);
		LazyOptional<IEnergyStorage> optional = PTCommonUtil.getEnergyCap(stack);
		if (!optional.isPresent() && !WARNED)
		{
			PlantTechMain.LOGGER.error("Cannot access capability for provider, this should not happen " + ForgeRegistries.ITEMS.getKey(stack.getItem()));
			WARNED = true;
		}
		energystorage = optional.orElse(new BioEnergyStorage(0));
    }

	@Override
	public void init()
    {
        super.init();
        this.imageWidth = 205; 
        this.imageHeight = 202;     
        this.leftPos = (this.width - this.imageWidth) / 2;
        this.topPos = (this.height - this.imageHeight) / 2;
    }
	
	@Override
	public void render(GuiGraphics pStack, int mouseX, int mouseY, float partialTicks)
	{
		this.renderBackground(pStack);
		this.renderBg(pStack, partialTicks, mouseX, mouseY);
		super.render(pStack, mouseX, mouseY, partialTicks);
		this.drawTooltips(pStack, mouseX, mouseY);
		this.renderTooltip(pStack, mouseX, mouseY);
	}

	@Override
	protected void renderLabels(GuiGraphics pStack, int mouseX, int mouseY)
	{
		String tileName = title.getString();
		int textcolor = Integer.parseInt("000000",16);
		pStack.drawString(font, tileName, (this.imageWidth / 2.0F - font.width(tileName) / 2.0F) + 1, 14, textcolor, false);
	}

	protected void drawTooltips(GuiGraphics pStack, int mouseX, int mouseY)
	{
		if (energystorage != null)
			drawTooltip(pStack, energystorage.getEnergyStored() + "/" + energystorage.getMaxEnergyStored(), mouseX, mouseY, 148, 27, 16, 74);
	}
	
	public void drawTooltip(GuiGraphics mStack, String lines, int mouseX, int mouseY, int posX, int posY, int width, int height)
	{
		posX += this.leftPos;
		posY += this.topPos;
        if (mouseX >= posX && mouseX <= posX + width && mouseY >= posY && mouseY <= posY + height)
			mStack.renderTooltip(font, Component.literal(lines), mouseX, mouseY);
    }

	@Override
	protected void renderBg(GuiGraphics pStack, float partialTicks, int x, int y)
	{
		RenderSystem.setShaderColor(1.0f, 1.0f, 1.0f, 1.0f);
		pStack.blit(BACKGROUND.get(invsize), this.leftPos, this.topPos, 0, 0, this.imageWidth, this.imageHeight);

		int k = this.getEnergyStoredScaled(55);
		pStack.blit(BACKGROUND.get(invsize), this.leftPos + 149, this.topPos + 28 + (55 - k), 208, 55 - k, 16, k);
	}

	protected int getEnergyStoredScaled(int pixels)
	{
		if (energystorage != null)
		{
			int i = energystorage.getEnergyStored();
			int j = energystorage.getMaxEnergyStored();
			return i != 0 && j != 0 ? i * pixels / j : 0;
		}
		return 0; 
	}

	@Override
	protected boolean checkHotbarKeyPressed(int keyCode, int scanCode)
	{
		return false;
	}
}
