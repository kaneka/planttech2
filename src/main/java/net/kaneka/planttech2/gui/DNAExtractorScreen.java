package net.kaneka.planttech2.gui;

import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.inventory.DNAExtractorMenu;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;

public class DNAExtractorScreen extends MachineScreen<DNAExtractorMenu>
{ 
	private static final ResourceLocation BACKGROUND = new ResourceLocation(PlantTechMain.MODID + ":textures/gui/container/dna_extractor.png");

	public DNAExtractorScreen(DNAExtractorMenu container, Inventory player, Component name)
    {
    	super(container, player, name, BACKGROUND);
    }

	@Override
	protected void renderBg(GuiGraphics pStack, float partialTicks, int mouseX, int mouseY)
	{
		super.renderBg(pStack, partialTicks, mouseX, mouseY);

		int l = this.getCookProgressScaled(52);
		pStack.blit(getBackgroundTexture(), this.leftPos + 76, this.topPos + 37, 0, 200, l, 22);

		renderEnergy(pStack);
	}

//	@Override
//	protected void drawGuiContainerBackgroundLayer(GuiGraphics mStack, float partialTicks, int mouseX, int mouseY)
//	{
//		super.drawGuiContainerBackgroundLayer(mStack, partialTicks, mouseX, mouseY);
//
//		int l = this.getCookProgressScaled(52);
//		mStack.blit( ,this.guiLeft + 76, this.guiTop + 37, 0, 200, l, 22);
//
//		int k = this.getEnergyStoredScaled(55);
//		mStack.blit( ,this.guiLeft + 149, this.guiTop + 28 + (55 - k), 208, 55 - k, 16, 0 + k);
//	}
	
	@Override
	protected String getGuideEntryString()
	{
		return "dna_extractor";
	}
}
