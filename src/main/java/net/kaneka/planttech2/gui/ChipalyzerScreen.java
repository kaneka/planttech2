package net.kaneka.planttech2.gui;


import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.inventory.ChipalyzerMenu;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;

public class ChipalyzerScreen extends MachineScreen<ChipalyzerMenu>
{ 
	private static final ResourceLocation BACKGROUND = new ResourceLocation(PlantTechMain.MODID + ":textures/gui/container/chipalyzer.png");

	public ChipalyzerScreen(ChipalyzerMenu container, Inventory player, Component name)
    {
    	super(container, player, name, BACKGROUND);
    }

	@Override
	protected void renderBg(GuiGraphics pStack, float partialTicks, int mouseX, int mouseY)
	{
		super.renderBg(pStack, partialTicks, mouseX, mouseY);

		int l = this.getCookProgressScaled(32);
		pStack.blit(getBackgroundTexture(), this.leftPos + 60, this.topPos + 46, 0, 200, l, 16);

		renderEnergy(pStack);
	}

	@Override
	protected String getGuideEntryString()
	{
		return "chipalyzer";
	}
}
