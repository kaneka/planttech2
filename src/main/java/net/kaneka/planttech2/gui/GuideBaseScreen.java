package net.kaneka.planttech2.gui;

import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.platform.Lighting;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.items.GuideItem;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.components.Renderable;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.ItemRenderer;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.client.renderer.texture.TextureAtlas;
import net.minecraft.client.resources.model.BakedModel;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemDisplayContext;
import net.minecraft.world.item.ItemStack;

import java.util.List;
import java.util.function.Consumer;

public abstract class GuideBaseScreen extends Screen
{
	protected static final ResourceLocation BACKGROUND = new ResourceLocation(PlantTechMain.MODID, "textures/gui/plantencyclopaedia_big.png");
	protected static final int TEXT_COLOR = 0x000000;
	protected int xSize = 512;
	protected int ySize = 196;
	protected int guiLeft;
	protected int guiTop;
	protected int scrollMax;
	protected int scrollPos = 0;
	protected int fadeInTimer = 50;
	protected boolean hasSelection;
	protected boolean allowScroll;

	public GuideBaseScreen(int scrollMax, boolean allowScroll, String title)
	{
		super(Component.translatable(title));
		this.scrollMax = scrollMax;
		this.allowScroll = allowScroll;
	}

	@SuppressWarnings("resource")
	@Override
	public void init()
	{
		super.init();
		Minecraft minecraft = Minecraft.getInstance();
		Player player = minecraft.player;
		if (player != null && player.getMainHandItem().getItem() instanceof GuideItem)
			// TODO: move this to use a custom item property
			player.getMainHandItem().setDamageValue(1);
		hasSelection = false;
		this.guiLeft = (this.width - 400) / 2;
		this.guiTop = (this.height - this.ySize) / 2;
	}

	//SHould probably move this to some render util...etc
	protected void renderGuiItem(Consumer<PoseStack> transform, ItemStack item, float x, float y)
	{
		Minecraft minecraft = Minecraft.getInstance();
		if (minecraft.player == null)
			return;
		ItemRenderer itemRenderer = minecraft.getItemRenderer();
		BakedModel model = itemRenderer.getModel(item, null, null, minecraft.player.getId());
		minecraft.textureManager.getTexture(TextureAtlas.LOCATION_BLOCKS).setFilter(false, false);
		RenderSystem.setShaderTexture(0, TextureAtlas.LOCATION_BLOCKS);
		RenderSystem.enableBlend();
		RenderSystem.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
		RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
		PoseStack posestack = RenderSystem.getModelViewStack();
		posestack.pushPose();
		posestack.translate(x, y, 0);
		posestack.translate(8.0D, 8.0D, 0.0D);
		posestack.scale(1.0F, -1.0F, 1.0F);
		posestack.scale(16.0F, 16.0F, 16.0F);
		transform.accept(posestack);
		RenderSystem.applyModelViewMatrix();
		PoseStack posestack1 = new PoseStack();
		posestack1.pushPose();
//		transform.accept(posestack1);
		MultiBufferSource.BufferSource multibuffersource$buffersource = Minecraft.getInstance().renderBuffers().bufferSource();
		boolean flag = !model.usesBlockLight();
		if (flag)
			Lighting.setupForFlatItems();

		itemRenderer.render(item, ItemDisplayContext.GUI, false, posestack1, multibuffersource$buffersource, 15728880, OverlayTexture.NO_OVERLAY, model);
		posestack1.popPose();
		multibuffersource$buffersource.endBatch();
		RenderSystem.enableDepthTest();
		if (flag)
			Lighting.setupFor3DItems();

		posestack.popPose();
		RenderSystem.applyModelViewMatrix();
	}

	protected abstract void updateButtons();

	@Override
	public void render(GuiGraphics pStack, int mouseX, int mouseY, float partialTicks)
	{
		if (minecraft == null)
			return;
		RenderSystem.setShaderColor(1.0f, 1.0f, 1.0f, 1.0f);
		if (fadeInTimer > 0)
		{
			fadeInTimer--;
			drawFadeInEffect(pStack);
		}
		else
		{
			this.drawBackground(pStack);
			this.drawForeground(pStack);
			this.drawButtons(pStack, mouseX, mouseY, partialTicks);
			this.drawStrings(pStack);
			this.drawTooltips(pStack, mouseX, mouseY);
		}
	}

	private void drawButtons(GuiGraphics mStack, int mouseX, int mouseY, float partialTicks)
	{
		for (Renderable button : this.renderables)
			 button.render(mStack, mouseX, mouseY, partialTicks);
	}

	protected void drawBackground(GuiGraphics mStack)
	{
		mStack.blit(BACKGROUND ,this.guiLeft + 100, this.guiTop, 212, 0, 300, this.ySize, 512, 512);
		mStack.blit(BACKGROUND ,this.guiLeft, this.guiTop, 0, 0, 150, this.ySize, 512, 512);
	}

	protected abstract void drawForeground(GuiGraphics mStack);

	private void drawFadeInEffect(GuiGraphics mStack)
	{
		float percentage = 1f - ((float) fadeInTimer / 50f);
		mStack.blit(BACKGROUND ,this.guiLeft + 100, this.guiTop, this.xSize - (300 * percentage), 0, (int) (300 * percentage), this.ySize, 512, 512);
		mStack.blit(BACKGROUND ,this.guiLeft, this.guiTop, 0, 0, 150, this.ySize, 512, 512);
	}

	@Override
	public boolean mouseScrolled(double mouseX, double mouseY, double delta)
	{

		if (allowScroll && mouseX != 0)
		{
			scrollPos += delta > 0 ? -1 : 1;
			scrollPos = Math.max(0, scrollPos);
			scrollPos = Math.min(scrollMax, scrollPos);

			updateButtons();
		}
		return super.mouseScrolled(mouseX, mouseY, delta);
	}

	protected abstract void drawStrings(GuiGraphics mStack);

	public void renderItem(GuiGraphics stack, ItemStack itemstack, int x, int y)
	{
		stack.renderFakeItem(itemstack, this.guiLeft + x, this.guiTop + y);
		stack.renderItemDecorations(font, itemstack, this.guiLeft + x, this.guiTop + y);
	}

	public void drawTooltip(GuiGraphics pStack, Component lines, int mouseX, int mouseY, int posX, int posY)
	{
		drawTooltip(pStack, lines, mouseX, mouseY, posX, posY, 16, 16);
	}

	public void drawTooltip(GuiGraphics pStack, Component lines, int mouseX, int mouseY, int posX, int posY, int width, int height)
	{
		posX += this.guiLeft;
		posY += this.guiTop;
		if (mouseX >= posX && mouseX <= posX + width && mouseY >= posY && mouseY <= posY + height)
			pStack.renderTooltip(font, lines, mouseX, mouseY);
	}

	public void drawTooltip(GuiGraphics pStack, List<Component> lines, int mouseX, int mouseY, int posX, int posY, int width, int height)
	{
		posX += this.guiLeft;
		posY += this.guiTop;
		if (mouseX >= posX && mouseX <= posX + width && mouseY >= posY && mouseY <= posY + height)
			pStack.renderComponentTooltip(font, lines, mouseX, mouseY);
	}

	protected abstract void drawTooltips(GuiGraphics pStack, int mouseX, int mouseY);

	@SuppressWarnings("resource")
	@Override
	public void removed()
	{
		Player player = Minecraft.getInstance().player;
		if (player != null && player.getMainHandItem().getItem() instanceof GuideItem)
			player.getMainHandItem().setDamageValue(0);
		super.removed();
	}

	@Override
	public boolean isPauseScreen()
	{
		return false;
	}
}
