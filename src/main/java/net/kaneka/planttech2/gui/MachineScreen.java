package net.kaneka.planttech2.gui;

import net.kaneka.planttech2.blocks.entity.machine.baseclasses.EnergyInventoryBlockEntity;
import net.kaneka.planttech2.blocks.entity.machine.baseclasses.EnergyInventoryFluidBlockEntity;
import net.kaneka.planttech2.inventory.BlockBaseMenu;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;

public abstract class MachineScreen<T extends BlockBaseMenu> extends BaseContainerScreen<T>
{
    private final ResourceLocation texture;
    public MachineScreen(BlockBaseMenu inventorySlotsIn, Inventory inventoryPlayer, Component title, ResourceLocation texture)
    {
        super(inventorySlotsIn, inventoryPlayer, title);
        this.texture = texture;
    }

    @Override
    protected void renderBg(GuiGraphics mStack, float partialTicks, int mouseX, int mouseY)
    {
        super.renderBg(mStack, partialTicks, mouseX, mouseY);
    }

    protected void renderEnergy(GuiGraphics pStack)
    {
        int k = getEnergyStoredScaled(55);
        pStack.blit(texture, this.leftPos + 149, this.topPos + 28 + (55 - k), 208, 55 - k, 16, k);
    }

    protected int getCookProgressScaled(int pixels)
    {
        int i = menu.getValue(te instanceof EnergyInventoryFluidBlockEntity ? 4 : 2);
        return i != 0 ? i * pixels / ((EnergyInventoryBlockEntity) te).ticksPerItem() : 0;
    }

    @Override
    protected ResourceLocation getBackgroundTexture()
    {
        return texture;
    }
}
