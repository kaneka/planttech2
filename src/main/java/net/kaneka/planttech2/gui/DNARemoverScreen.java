package net.kaneka.planttech2.gui;

import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.inventory.DNARemoverMenu;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;

public class DNARemoverScreen extends MachineScreen<DNARemoverMenu>
{ 
	private static final ResourceLocation BACKGROUND = new ResourceLocation(PlantTechMain.MODID + ":textures/gui/container/dna_remover.png");

	public DNARemoverScreen(DNARemoverMenu container, Inventory player, Component name)
    {
    	super(container, player, name, BACKGROUND);
    }

	@Override
	protected void renderBg(GuiGraphics pStack, float partialTicks, int mouseX, int mouseY)
	{
		super.renderBg(pStack, partialTicks, mouseX, mouseY);

		int l = this.getCookProgressScaled(32);
		pStack.blit(getBackgroundTexture(), this.leftPos + 60, this.topPos + 48, 0, 200, l, 14);

		renderEnergy(pStack);
	}

//	@Override
//	protected void drawGuiContainerBackgroundLayer(GuiGraphics mStack, float partialTicks, int mouseX, int mouseY)
//	{
//		super.drawGuiContainerBackgroundLayer(mStack, partialTicks, mouseX, mouseY);
//
//		int l = this.getCookProgressScaled(32);
//		mStack.blit( ,this.guiLeft + 60, this.guiTop + 48, 0, 200, l, 14);
//
//		int k = this.getEnergyStoredScaled(55);
//		mStack.blit( ,this.guiLeft + 149, this.guiTop + 28 + (55 - k), 208, 55 - k, 16, 0 + k);
//	}

	@Override
	protected String getGuideEntryString()
	{
		return "dna_remover";
	}
}
