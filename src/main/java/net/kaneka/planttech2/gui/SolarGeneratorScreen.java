package net.kaneka.planttech2.gui;

import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.blocks.entity.machine.SolarGeneratorBlockEntity;
import net.kaneka.planttech2.inventory.SolarGeneratorMenu;
import net.kaneka.planttech2.items.TierItem;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;

public class SolarGeneratorScreen extends MachineScreen<SolarGeneratorMenu>
{
	private static final ResourceLocation BACKGROUND = new ResourceLocation(PlantTechMain.MODID + ":textures/gui/container/solargenerator.png");

	public SolarGeneratorScreen(SolarGeneratorMenu container, Inventory player, Component name)
    {
    	super(container, player, name, BACKGROUND);
    }

	@Override
	protected void renderBg(GuiGraphics pStack, float partialTicks, int mouseX, int mouseY)
	{
		super.renderBg(pStack, partialTicks, mouseX, mouseY);

		renderEnergy(pStack);

		int l = switch (((SolarGeneratorBlockEntity) this.te).getUpgradeTier(0, TierItem.ItemType.SOLAR_FOCUS))
				{
					case 1 -> 6;
					case 2 -> 15;
					case 3 -> 25;
					case 4 -> 35;
					default -> 0;
				};
		int j = getWorkLoadScaled(17);
		pStack.blit(getBackgroundTexture(), this.leftPos + 133, this.topPos + 36, 205, 56, j, l);
	}

	private int getWorkLoadScaled(int pixels)
	{
		int i = menu.getValue(2);
		int j = ((SolarGeneratorBlockEntity) this.te).getTicksPerAmount();
		return i != 0 && j != 0 ? i * pixels / j : 0; 
	}
	
	@Override
	protected String getGuideEntryString()
	{
		return "solar_generator";
	}
}
