package net.kaneka.planttech2.addons;

import java.util.Objects;
import java.util.function.Function;

public class Pair<A, B>
{
    private A a;
    private B b;

    protected Pair(A a, B b)
    {
        this.a = a;
        this.b = b;
    }

    public static <E> Pair<E, E> of(E e)
    {
        return new Pair<>(e, e);
    }

    public static <A, B> Pair<A, B> of(A a, B b)
    {
        return new Pair<>(a, b);
    }

    public static <A, B> Pair<A, B> empty()
    {
        return of(null, null);
    }

    public static <E> E or(Pair<E, E> pair, boolean bool)
    {
        return bool ? pair.getA() : pair.getB();
    }

    public static <N extends Number> Pair<N, N> ascending(N n, N n2)
    {
        return n.doubleValue() > n2.doubleValue() ? Pair.of(n2, n) : Pair.of(n, n2);
    }

    public static <N extends Number> Pair<N, N> decending(N n, N n2)
    {
        return n.doubleValue() < n2.doubleValue() ? Pair.of(n2, n) : Pair.of(n, n2);
    }

    public static <E> void swap(Pair<E, E> pair)
    {
        E a = pair.getA();
        pair.setA(pair.getB());
        pair.setB(a);
    }

    public Pair<A, B> clear()
    {
        return map((a) -> null, (b) -> null);
    }

    public boolean isEmpty()
    {
        return a == null && b == null;
    }

    public boolean hasA()
    {
        return a != null;
    }

    public boolean hasB()
    {
        return b != null;
    }

    public Pair<A, B> mapNew(Function<A, A> a, Function<B, B> b)
    {
        return of(a.apply(this.a), b.apply(this.b));
    }

    public Pair<A, B> map(Function<A, A> a, Function<B, B> b)
    {
        mapA(a);
        return mapB(b);
    }

    public Pair<A, B> mapA(Function<A, A> a)
    {
        this.a = a.apply(this.a);
        return this;
    }

    public Pair<A, B> mapB(Function<B, B> b)
    {
        this.b = b.apply(this.b);
        return this;
    }

    public Pair<A, B> copy()
    {
        return of(a, b);
    }

    public Pair<A, B> set(A a, B b)
    {
        setA(a);
        return setB(b);
    }

    public A getA()
    {
        return a;
    }

    public Pair<A, B> setA(A a)
    {
        this.a = a;
        return this;
    }

    public B getB()
    {
        return b;
    }

    public Pair<A, B> setB(B b)
    {
        this.b = b;
        return this;
    }

    public boolean contains(Object obj)
    {
        return (this.a != null && this.a.equals(obj)) || (this.b != null && this.b.equals(obj));
    }

    @Override
    public String toString()
    {
        return "Pair{" +
                "a=" + a +
                ", b=" + b +
                '}';
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        Pair<?, ?> pair = (Pair<?, ?>) o;
        return Objects.equals(a, pair.a) && Objects.equals(b, pair.b);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(a, b);
    }
}
