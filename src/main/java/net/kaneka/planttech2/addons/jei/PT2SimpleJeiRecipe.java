package net.kaneka.planttech2.addons.jei;

import net.minecraft.world.item.ItemStack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PT2SimpleJeiRecipe
{
	protected final List<ItemStack> inputs = new ArrayList<>();
	protected final List<ItemStack> outputs = new ArrayList<>();
	private final int biomass;

	private PT2SimpleJeiRecipe(List<ItemStack> inputs, List<ItemStack> outputs, int biomass)
	{
		this.biomass = biomass;
		this.inputs.addAll(inputs);
		this.outputs.addAll(outputs);
	}
	
	public ItemStack getInput(int i)
	{
		return i > inputs.size() ? ItemStack.EMPTY : inputs.get(i);
	}
	
	public List<ItemStack> getInputs()
	{
		return inputs; 
	}
	
	public ItemStack getOutput(int i)
	{
		return i > outputs.size() ? ItemStack.EMPTY : outputs.get(i);
	}

	public int getBiomass()
	{
		return biomass;
	}

	public static Builder of()
	{
		return new Builder();
	}

	public static class Builder
	{
		private final List<ItemStack> inputs = new ArrayList<>();
		private final List<ItemStack> outputs = new ArrayList<>();
		private int biomass = 0;

		public Builder inputs(ItemStack... inputs)
		{
            Collections.addAll(this.inputs, inputs);
			return this;
		}

		public Builder outputs(ItemStack... output)
		{
			Collections.addAll(this.outputs, output);
			return this;
		}

		public Builder biomass(int biomass)
		{
			this.biomass = biomass;
			return this;
		}

		public PT2SimpleJeiRecipe build()
		{
			return new PT2SimpleJeiRecipe(inputs, outputs, biomass);
		}
	}
}
