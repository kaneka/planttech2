package net.kaneka.planttech2.addons.jei;

import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.builder.IRecipeLayoutBuilder;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.gui.ingredient.IRecipeSlotsView;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.recipe.IFocusGroup;
import mezz.jei.api.recipe.RecipeIngredientRole;
import mezz.jei.api.recipe.RecipeType;
import mezz.jei.api.recipe.category.IRecipeCategory;
import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.recipes.recipeclasses.ChipalyzerRecipe;
import net.kaneka.planttech2.recipes.recipeclasses.CompressorRecipe;
import net.kaneka.planttech2.recipes.recipeclasses.InfuserRecipe;
import net.kaneka.planttech2.registries.ModBlocks;
import net.kaneka.planttech2.registries.ModItems;
import net.kaneka.planttech2.utilities.PTClientUtil;
import net.kaneka.planttech2.utilities.PTCommonUtil;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.ItemLike;

import javax.annotation.Nullable;
import java.util.function.Function;

public final class PT2JeiCategory<T> implements IRecipeCategory<T>
{
	private final static ResourceLocation TEXTURE = new ResourceLocation(PlantTechMain.MODID, "textures/gui/jei/jeibackground.png");;
	private final Class<T> clazz;
	private final Component localizedName;
    private final ResourceLocation UID;
    private final IDrawable icon;
    private final IDrawable background;
	private final PTCommonUtil.TriConsumer<IRecipeLayoutBuilder, T, IFocusGroup> setRecipe;
	@Nullable
	private final Function<T, Integer> biomass;

	public static PT2JeiCategory<PT2SimpleJeiRecipe> carver(IGuiHelper helper)
	{
		return new PT2JeiCategory<>(PT2SimpleJeiRecipe.class, "carver", ModBlocks.CARVER.get(), helper, 32, 0, 60, 18, (builder, recipe, focuses) -> {
			builder.addSlot(RecipeIngredientRole.INPUT, 1, 1).addItemStack(recipe.getInput(0));
			builder.addSlot(RecipeIngredientRole.OUTPUT, 43, 1).addItemStack(recipe.getOutput(0));
		});
	}

	public static PT2JeiCategory<ChipalyzerRecipe> chipalyzer(IGuiHelper helper)
	{
		return new PT2JeiCategory<>(ChipalyzerRecipe.class, "chipalyzer", ModBlocks.CHIPALYZER.get(), helper, 32, 32, 58, 32, (builder, recipe, focuses) -> {
			builder.addSlot(RecipeIngredientRole.INPUT, 1, 15).addIngredients(recipe.getComponents().get(0));
			builder.addSlot(RecipeIngredientRole.INPUT, 21, 1).addIngredients(recipe.getComponents().get(1));
			builder.addSlot(RecipeIngredientRole.OUTPUT, 41, 15).addItemStack(recipe.getResultItem(Minecraft.getInstance().level.registryAccess()));
		});
	}

	public static PT2JeiCategory<CompressorRecipe> compressor(IGuiHelper helper)
	{
		return new PT2JeiCategory<>(CompressorRecipe.class, "compressor", ModBlocks.COMPRESSOR.get().asItem(), helper, 32, 64, 60, 18, (builder, recipe, focuses) -> {
			builder.addSlot(RecipeIngredientRole.INPUT, 1, 1).addItemStack(recipe.getInput());
			builder.addSlot(RecipeIngredientRole.OUTPUT, 43, 1).addIngredients(recipe.getOutput());
		});
	}

	public static PT2JeiCategory<PT2SimpleJeiRecipe> crossbreeding(IGuiHelper helper)
	{
		return new PT2JeiCategory<>(PT2SimpleJeiRecipe.class, "crossbreeding", ModItems.COLOR_PARTICLES.get(), helper, 32, 96, 78, 18, (builder, recipe, focuses) -> {
			builder.addSlot(RecipeIngredientRole.INPUT, 1, 1).addItemStack(recipe.getInput(0));
			builder.addSlot(RecipeIngredientRole.INPUT, 27, 1).addItemStack(recipe.getInput(1));
			builder.addSlot(RecipeIngredientRole.OUTPUT, 61, 1).addItemStack(recipe.getOutput(0));
		});
	}

	public static PT2JeiCategory<InfuserRecipe> infuser(IGuiHelper helper)
	{
		return new PT2JeiCategory<>(InfuserRecipe.class, "infuser", ModBlocks.INFUSER.get(), helper, 32, 128, 60, 26, (builder, recipe, focuses) -> {
			builder.addSlot(RecipeIngredientRole.INPUT, 1, 1).addIngredients(recipe.getInput());
			builder.addSlot(RecipeIngredientRole.OUTPUT, 43, 1).addItemStack(recipe.getResultItem(Minecraft.getInstance().level.registryAccess()));
		}, InfuserRecipe::getBiomass);
	}

	public static PT2JeiCategory<PT2SimpleJeiRecipe> machinebulbReprocessor(IGuiHelper helper)
	{
		return new PT2JeiCategory<>(PT2SimpleJeiRecipe.class, "machinebulbreprocessor", ModBlocks.MACHINEBULBREPROCESSOR.get(), helper, 32, 192, 60, 26, (builder, recipe, focuses) -> {
			builder.addSlot(RecipeIngredientRole.INPUT, 1, 1).addItemStack(recipe.getInput(0));
			builder.addSlot(RecipeIngredientRole.OUTPUT, 43, 1).addItemStack(recipe.getOutput(0));
		}, PT2SimpleJeiRecipe::getBiomass);
	}

	public static PT2JeiCategory<PT2SimpleJeiRecipe> machineGrowing(IGuiHelper helper)
	{
		return new PT2JeiCategory<>(PT2SimpleJeiRecipe.class, "machine_growing", ModItems.MACHINEBULBREPROCESSOR_BULB.get(), helper, 32, 160, 86, 18, (builder, recipe, focuses) -> {
			builder.addSlot(RecipeIngredientRole.INPUT, 1, 1).addItemStack(recipe.getInput(0));
			builder.addSlot(RecipeIngredientRole.INPUT, 27, 1).addItemStack(recipe.getInput(1));
			builder.addSlot(RecipeIngredientRole.OUTPUT, 69, 1).addItemStack(recipe.getOutput(0));
		});
	}

	private PT2JeiCategory(Class<T> clazz, String name, ItemLike icon, IGuiHelper helper, int u, int v, int width, int height, PTCommonUtil.TriConsumer<IRecipeLayoutBuilder, T, IFocusGroup> setRecipe)
	{
		this(clazz, name, icon, helper, u, v, width, height, setRecipe, null);
	}

    private PT2JeiCategory(Class<T> clazz, String name, ItemLike icon, IGuiHelper helper, int u, int v, int width, int height, PTCommonUtil.TriConsumer<IRecipeLayoutBuilder, T, IFocusGroup> setRecipe, Function<T, Integer> biomass)
    {
    	this.clazz = clazz;
    	this.UID = new ResourceLocation(PlantTechMain.MODID, name);
		this.background = helper.createDrawable(TEXTURE, u, v, width, height);
    	this.icon = helper.createDrawableIngredient(VanillaTypes.ITEM_STACK, new ItemStack(icon));
    	this.localizedName = Component.translatable(PlantTechMain.MODID + "." + name);
		this.setRecipe = setRecipe;
		this.biomass = biomass;
	}

	@Override
	public RecipeType<T> getRecipeType()
	{
		return new RecipeType<>(UID, clazz);
	}

	@Override
	public Component getTitle()
	{
		return localizedName;
	}

	@Override
	public IDrawable getBackground()
	{
		return background;
	}

	@Override
	public IDrawable getIcon()
	{
		return icon;
	}

	@Override
	public void setRecipe(IRecipeLayoutBuilder builder, T recipe, IFocusGroup focuses)
	{
		setRecipe.accept(builder, recipe, focuses);
	}

	@Override
	public void draw(T recipe, IRecipeSlotsView recipeSlotsView, GuiGraphics stack, double mouseX, double mouseY)
	{
		if (biomass != null)
			PTClientUtil.drawJEIBiomass(biomass.apply(recipe), stack, getBackground().getWidth());
	}
}
