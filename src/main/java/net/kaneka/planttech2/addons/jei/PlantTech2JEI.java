package net.kaneka.planttech2.addons.jei;

import com.google.common.collect.ImmutableList;
import net.kaneka.planttech2.blocks.GrowingBlock;
import net.kaneka.planttech2.crops.CropConfiguration;
import net.kaneka.planttech2.crops.CropTypes;
import net.kaneka.planttech2.crops.ParentPair;
import net.kaneka.planttech2.items.KnowledgeChip;
import net.kaneka.planttech2.items.MachineBulbItem;
import net.kaneka.planttech2.recipes.ModRecipeTypes;
import net.kaneka.planttech2.recipes.recipeclasses.ChipalyzerRecipe;
import net.kaneka.planttech2.recipes.recipeclasses.CompressorRecipe;
import net.kaneka.planttech2.recipes.recipeclasses.InfuserRecipe;
import net.kaneka.planttech2.registries.ModBlocks;
import net.kaneka.planttech2.registries.ModItems;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Recipe;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraft.world.level.block.Blocks;
import net.minecraftforge.registries.RegistryObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class PlantTech2JEI
{
	public static List<ChipalyzerRecipe> chipalyzerRecipes()
	{
		return getRecipes(ModRecipeTypes.CHIPALYZER.get());
	}

	public static List<CompressorRecipe> compressorRecipes()
	{
		return getRecipes(ModRecipeTypes.COMPRESSING.get());
	}
	
	public static List<InfuserRecipe> infuserRecipes()
	{
		return getRecipes(ModRecipeTypes.INFUSING.get());
	}

	public static List<PT2SimpleJeiRecipe> getCrossbreedingRecipes()
	{
		List<PT2SimpleJeiRecipe> recipes = new ArrayList<>();
		for (CropConfiguration config : CropTypes.configs())
		{
			if (config.shouldBeInGame() && !config.getParents().isEmpty())
			{
				ItemStack output = config.getPrimarySeed().getItemStack();
				for (ParentPair parent : config.getParents())
					recipes.add(PT2SimpleJeiRecipe.of()
							.inputs(
									parent.getFirstParent().getConfig().getPrimarySeed().getItemStack(),
									parent.getSecondParent().getConfig().getPrimarySeed().getItemStack())
							.outputs(
									output
							).build());
			}
		}
		return recipes;
	}
	
	public static List<PT2SimpleJeiRecipe> getCarverRecipes()
	{
		return ImmutableList.of(
				PT2SimpleJeiRecipe.of().inputs(new ItemStack(Blocks.IRON_BLOCK)).outputs(new ItemStack(ModBlocks.MACHINESHELL_IRON.get())).build(),
				PT2SimpleJeiRecipe.of().inputs(new ItemStack(ModBlocks.PLANTIUM_BLOCK.get())).outputs(new ItemStack(ModBlocks.MACHINESHELL_PLANTIUM.get())).build()
		);
	}
	
	public static List<PT2SimpleJeiRecipe> getMachinebulbReprocessorRecipes()
	{
		List<PT2SimpleJeiRecipe> recipes = new ArrayList<>();
		for (RegistryObject<Item> obj : ModItems.MACHINE_BULBS)
		{
			MachineBulbItem bulb = (MachineBulbItem) obj.get();
			recipes.add(PT2SimpleJeiRecipe.of().inputs(KnowledgeChip.getByTier(bulb.getTier())).outputs(new ItemStack(bulb)).biomass(bulb.getNeededBiomass()).build());
		}
		return recipes;
	}

	public static List<PT2SimpleJeiRecipe> getMachineGrowingRecipes()
	{
		List<PT2SimpleJeiRecipe> recipes = new ArrayList<>();
		for (RegistryObject<Item> obj : ModItems.MACHINE_BULBS)
		{
			MachineBulbItem bulb = (MachineBulbItem) obj.get();
			recipes.add(PT2SimpleJeiRecipe.of().inputs(new ItemStack(bulb), new ItemStack(bulb.getHull())).outputs(new ItemStack(((GrowingBlock) bulb.getMachine()).blockSupplier.get())).build());
		}
		return recipes;
	}

	public static <R extends Recipe<?>> List<R> getRecipes(RecipeType<R> recipeType)
	{
		ClientLevel level = Minecraft.getInstance().level;
		if (level == null)
			return Collections.emptyList();
		List<R> recipes = level.getRecipeManager().getRecipes().stream().filter((recipe) -> recipe.getType() == recipeType).collect(ArrayList::new, (list, recipe) -> list.add((R) recipe), ArrayList::addAll);
		recipes.sort(Comparator.comparing((recipe) -> recipe.getId().getPath()));
		return recipes;
	}
}
