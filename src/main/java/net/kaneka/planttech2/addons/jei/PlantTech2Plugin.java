package net.kaneka.planttech2.addons.jei;

import mezz.jei.api.IModPlugin;
import mezz.jei.api.JeiPlugin;
import mezz.jei.api.constants.ModIds;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.helpers.IJeiHelpers;
import mezz.jei.api.recipe.RecipeType;
import mezz.jei.api.registration.IRecipeCategoryRegistration;
import mezz.jei.api.registration.IRecipeRegistration;
import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.recipes.recipeclasses.ChipalyzerRecipe;
import net.kaneka.planttech2.recipes.recipeclasses.CompressorRecipe;
import net.kaneka.planttech2.recipes.recipeclasses.InfuserRecipe;
import net.minecraft.resources.ResourceLocation;

@JeiPlugin
public class PlantTech2Plugin implements IModPlugin
{
	public static final RecipeType<ChipalyzerRecipe> CHIPALYZING = RecipeType.create(PlantTechMain.MODID, "chipalyzer", ChipalyzerRecipe.class);
	public static final RecipeType<CompressorRecipe> COMPRESSING = RecipeType.create(PlantTechMain.MODID, "compressor", CompressorRecipe.class);
	public static final RecipeType<PT2SimpleJeiRecipe> CROSSBREEDING = RecipeType.create(PlantTechMain.MODID, "crossbreeding", PT2SimpleJeiRecipe.class);
	public static final RecipeType<InfuserRecipe> INFUSING = RecipeType.create(PlantTechMain.MODID, "infuser", InfuserRecipe.class);
	public static final RecipeType<PT2SimpleJeiRecipe> CARVING = RecipeType.create(PlantTechMain.MODID, "carver", PT2SimpleJeiRecipe.class);
	public static final RecipeType<PT2SimpleJeiRecipe> MACHINE_BULB_PROCESSING = RecipeType.create(PlantTechMain.MODID, "machinebulbreprocessor", PT2SimpleJeiRecipe.class);
	public static final RecipeType<PT2SimpleJeiRecipe> MACHINE_GROWING = RecipeType.create(PlantTechMain.MODID, "machine_growing", PT2SimpleJeiRecipe.class);

	@Override
	public ResourceLocation getPluginUid() 
	{
		return new ResourceLocation(ModIds.JEI_ID, "planttech2");
	}
	
	@Override
	public void registerCategories(IRecipeCategoryRegistration registration)
	{
		IJeiHelpers jeiHelpers = registration.getJeiHelpers();
		IGuiHelper guiHelper = jeiHelpers.getGuiHelper();
		registration.addRecipeCategories(
				PT2JeiCategory.carver(guiHelper),
				PT2JeiCategory.chipalyzer(guiHelper),
				PT2JeiCategory.compressor(guiHelper),
				PT2JeiCategory.crossbreeding(guiHelper),
				PT2JeiCategory.infuser(guiHelper),
				PT2JeiCategory.machinebulbReprocessor(guiHelper),
				PT2JeiCategory.machineGrowing(guiHelper)
		);
	}
	
	@Override
	public void registerRecipes(IRecipeRegistration registration)
	{
		registration.addRecipes(CHIPALYZING, PlantTech2JEI.chipalyzerRecipes());
		registration.addRecipes(COMPRESSING, PlantTech2JEI.compressorRecipes());
		registration.addRecipes(CROSSBREEDING, PlantTech2JEI.getCrossbreedingRecipes());
		registration.addRecipes(INFUSING, PlantTech2JEI.infuserRecipes());
		registration.addRecipes(CARVING, PlantTech2JEI.getCarverRecipes());
		registration.addRecipes(MACHINE_BULB_PROCESSING, PlantTech2JEI.getMachinebulbReprocessorRecipes());
		registration.addRecipes(MACHINE_GROWING, PlantTech2JEI.getMachineGrowingRecipes());
	}
}
