package net.kaneka.planttech2.registries;

import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.fluids.BiomassFluid;
import net.minecraft.world.level.material.FlowingFluid;
import net.minecraft.world.level.material.Fluid;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class ModFluids
{
	public static final DeferredRegister<Fluid> FLUIDS = DeferredRegister.create(ForgeRegistries.FLUIDS, PlantTechMain.MODID);
	public static RegistryObject<FlowingFluid> BIOMASS_FLOWING = FLUIDS.register("biomass_flowing", BiomassFluid.Flowing::new);
	public static RegistryObject<FlowingFluid> BIOMASS = FLUIDS.register("biomass", BiomassFluid.Source::new);
}
