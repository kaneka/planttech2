package net.kaneka.planttech2.registries;

import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.blocks.entity.CropsBlockEntity;
import net.kaneka.planttech2.blocks.entity.cable.CableBlockEntity;
import net.kaneka.planttech2.blocks.entity.machine.ChipalyzerBlockEntity;
import net.kaneka.planttech2.blocks.entity.machine.CompressorBlockEntity;
import net.kaneka.planttech2.blocks.entity.machine.DNACleanerBlockEntity;
import net.kaneka.planttech2.blocks.entity.machine.DNACombinerBlockEntity;
import net.kaneka.planttech2.blocks.entity.machine.DNAExtractorBlockEntity;
import net.kaneka.planttech2.blocks.entity.machine.DNARemoverBlockEntity;
import net.kaneka.planttech2.blocks.entity.machine.EnergySupplierBlockEntity;
import net.kaneka.planttech2.blocks.entity.machine.IdentifierBlockEntity;
import net.kaneka.planttech2.blocks.entity.machine.InfuserBlockEntity;
import net.kaneka.planttech2.blocks.entity.machine.MachineBulbReprocessorBlockEntity;
import net.kaneka.planttech2.blocks.entity.machine.MegaFurnaceBlockEntity;
import net.kaneka.planttech2.blocks.entity.machine.PlantFarmBlockEntity;
import net.kaneka.planttech2.blocks.entity.machine.SeedConstructorBlockEntity;
import net.kaneka.planttech2.blocks.entity.machine.SeedSqueezerBlockEntity;
import net.kaneka.planttech2.blocks.entity.machine.SolarGeneratorBlockEntity;
import net.kaneka.planttech2.crops.CropTypes;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class ModBlockEntities {
	public static final DeferredRegister<BlockEntityType<?>> BLOCK_ENTITIES = DeferredRegister.create(ForgeRegistries.BLOCK_ENTITY_TYPES, PlantTechMain.MODID);

	public static RegistryObject<BlockEntityType<ChipalyzerBlockEntity>> CHIPALYZER_TE = BLOCK_ENTITIES.register("tileentitychipalyzer", () -> BlockEntityType.Builder.of(ChipalyzerBlockEntity::new, ModBlocks.CHIPALYZER.get()).build(null));
	public static RegistryObject<BlockEntityType<CompressorBlockEntity>> COMPRESSOR_TE = BLOCK_ENTITIES.register("tileentitycompressor", () -> BlockEntityType.Builder.of(CompressorBlockEntity::new, ModBlocks.COMPRESSOR.get()).build(null));
	public static RegistryObject<BlockEntityType<CropsBlockEntity>> CROPS_TE = BLOCK_ENTITIES.register("tileentitycrops", () -> BlockEntityType.Builder.of(CropsBlockEntity::new, CropTypes.CROP_BLOCKS.values().stream().map(RegistryObject::get).toArray(Block[]::new)).build(null));
	public static RegistryObject<BlockEntityType<DNACleanerBlockEntity>> DNACLEANER_TE = BLOCK_ENTITIES.register("tileentitydnacleaner", () -> BlockEntityType.Builder.of(DNACleanerBlockEntity::new, ModBlocks.DNA_CLEANER.get()).build(null));
	public static RegistryObject<BlockEntityType<DNACombinerBlockEntity>> DNACOMBINER_TE = BLOCK_ENTITIES.register("tileentitydnacombiner", () -> BlockEntityType.Builder.of(DNACombinerBlockEntity::new, ModBlocks.DNA_COMBINER.get()).build(null));
	public static RegistryObject<BlockEntityType<DNAExtractorBlockEntity>> DNAEXTRACTOR_TE = BLOCK_ENTITIES.register("tileentitydnaextractor", () -> BlockEntityType.Builder.of(DNAExtractorBlockEntity::new, ModBlocks.DNA_EXTRACTOR.get()).build(null));
	public static RegistryObject<BlockEntityType<DNARemoverBlockEntity>> DNAREMOVER_TE = BLOCK_ENTITIES.register("tileentitydnaremover", () -> BlockEntityType.Builder.of(DNARemoverBlockEntity::new, ModBlocks.DNA_REMOVER.get()).build(null));
	public static RegistryObject<BlockEntityType<EnergySupplierBlockEntity>> ENERGY_SUPPLIER_TE = BLOCK_ENTITIES.register("tileentityenergysupplier", () -> BlockEntityType.Builder.of(EnergySupplierBlockEntity::new, ModBlocks.ENERGY_SUPPLIER.get()).build(null));
	public static RegistryObject<BlockEntityType<IdentifierBlockEntity>> IDENTIFIER_TE = BLOCK_ENTITIES.register("tileentityidentifier", () -> BlockEntityType.Builder.of(IdentifierBlockEntity::new, ModBlocks.IDENTIFIER.get()).build(null));
	public static RegistryObject<BlockEntityType<InfuserBlockEntity>> INFUSER_TE = BLOCK_ENTITIES.register("tileentityinfuser", () -> BlockEntityType.Builder.of(InfuserBlockEntity::new, ModBlocks.INFUSER.get()).build(null));
	public static RegistryObject<BlockEntityType<MachineBulbReprocessorBlockEntity>> MACHINEBULBREPROCESSOR_TE = BLOCK_ENTITIES.register("tileentitymachinebulbreprocessor", () -> BlockEntityType.Builder.of(MachineBulbReprocessorBlockEntity::new, ModBlocks.MACHINEBULBREPROCESSOR.get()).build(null));
	public static RegistryObject<BlockEntityType<MegaFurnaceBlockEntity>> MEGAFURNACE_TE = BLOCK_ENTITIES.register("tileentitymegafurnace", () -> BlockEntityType.Builder.of(MegaFurnaceBlockEntity::new, ModBlocks.MEGAFURNACE.get()).build(null));
	public static RegistryObject<BlockEntityType<PlantFarmBlockEntity>> PLANTFARM_TE = BLOCK_ENTITIES.register("tileentityplantfarm", () -> BlockEntityType.Builder.of(PlantFarmBlockEntity::new, ModBlocks.PLANTFARM.get()).build(null));
	public static RegistryObject<BlockEntityType<SeedConstructorBlockEntity>> SEEDCONSTRUCTOR_TE = BLOCK_ENTITIES.register("tileentityseedconstructor", () -> BlockEntityType.Builder.of(SeedConstructorBlockEntity::new, ModBlocks.SEEDCONSTRUCTOR.get()).build(null));
	public static RegistryObject<BlockEntityType<SeedSqueezerBlockEntity>> SEEDSQUEEZER_TE = BLOCK_ENTITIES.register("tileentityseedsqueezer", () -> BlockEntityType.Builder.of(SeedSqueezerBlockEntity::new, ModBlocks.SEEDSQUEEZER.get()).build(null));
	public static RegistryObject<BlockEntityType<SolarGeneratorBlockEntity>> SOLARGENERATOR_TE = BLOCK_ENTITIES.register("tileentitysolargenerator", () -> BlockEntityType.Builder.of(SolarGeneratorBlockEntity::new, ModBlocks.SOLARGENERATOR.get()).build(null));
	public static RegistryObject<BlockEntityType<CableBlockEntity>> CABLE_TE = BLOCK_ENTITIES.register("tileentitycable", () -> BlockEntityType.Builder.of(CableBlockEntity::new, ModBlocks.CABLE.get()).build(null));

}
