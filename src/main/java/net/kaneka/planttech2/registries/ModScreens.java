package net.kaneka.planttech2.registries;

import net.kaneka.planttech2.gui.ChipalyzerScreen;
import net.kaneka.planttech2.gui.CompressorScreen;
import net.kaneka.planttech2.gui.DNACleanerScreen;
import net.kaneka.planttech2.gui.DNACombinerScreen;
import net.kaneka.planttech2.gui.DNAExtractorScreen;
import net.kaneka.planttech2.gui.DNARemoverScreen;
import net.kaneka.planttech2.gui.EnergySupplierScreen;
import net.kaneka.planttech2.gui.IdentifierScreen;
import net.kaneka.planttech2.gui.InfuserScreen;
import net.kaneka.planttech2.gui.ItemUpgradeableScreen;
import net.kaneka.planttech2.gui.MachineBulbReprocessorScreen;
import net.kaneka.planttech2.gui.MegaFurnaceScreen;
import net.kaneka.planttech2.gui.PlantFarmScreen;
import net.kaneka.planttech2.gui.SeedSqueezerScreen;
import net.kaneka.planttech2.gui.SeedconstructorScreen;
import net.kaneka.planttech2.gui.SolarGeneratorScreen;
import net.minecraft.client.gui.screens.MenuScreens;

public class ModScreens
{
	public static void registerGUI()
	{
		MenuScreens.register(ModContainers.COMPRESSOR.get(), CompressorScreen::new);
		MenuScreens.register(ModContainers.DNACLEANER.get(),  DNACleanerScreen::new);
		MenuScreens.register(ModContainers.DNACOMBINER.get(),  DNACombinerScreen::new);
		MenuScreens.register(ModContainers.DNAEXTRACTOR.get(),  DNAExtractorScreen::new);
		MenuScreens.register(ModContainers.DNAREMOVER.get(),  DNARemoverScreen::new);
		MenuScreens.register(ModContainers.ENERGYSUPPLIER.get(),  EnergySupplierScreen::new);
		MenuScreens.register(ModContainers.IDENTIFIER.get(),  IdentifierScreen::new);
		MenuScreens.register(ModContainers.INFUSER.get(),  InfuserScreen::new);
		MenuScreens.register(ModContainers.UPGRADEABLEITEM.get(),  ItemUpgradeableScreen::new);
		MenuScreens.register(ModContainers.MEGAFURNACE.get(),  MegaFurnaceScreen::new);
		MenuScreens.register(ModContainers.PLANTFARM.get(),  PlantFarmScreen::new);
		MenuScreens.register(ModContainers.SEEDCONSTRUCTOR.get(),  SeedconstructorScreen::new);
		MenuScreens.register(ModContainers.SEEDQUEEZER.get(),  SeedSqueezerScreen::new);
		MenuScreens.register(ModContainers.SOLARGENERATOR.get(),  SolarGeneratorScreen::new);
		MenuScreens.register(ModContainers.CHIPALYZER.get(),  ChipalyzerScreen::new);
		MenuScreens.register(ModContainers.MACHINEBULBREPROCESSOR.get(),  MachineBulbReprocessorScreen::new);
	}
}
