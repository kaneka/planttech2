package net.kaneka.planttech2.registries;

import net.kaneka.planttech2.PlantTechMain;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class ModSounds
{
    public static final DeferredRegister<SoundEvent> SOUNDS = DeferredRegister.create(ForgeRegistries.SOUND_EVENTS, PlantTechMain.MODID);
    public static final RegistryObject<SoundEvent> ELECTRIC_FENCE_IDLE = make("electric_fence_idle");

    private static RegistryObject<SoundEvent> make(String soundName)
    {
        return SOUNDS.register(soundName, () -> SoundEvent.createVariableRangeEvent(new ResourceLocation(PlantTechMain.MODID, soundName)));
    }
}
