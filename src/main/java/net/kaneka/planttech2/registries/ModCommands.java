package net.kaneka.planttech2.registries;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.commands.DevListCommand;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.Commands;
import net.minecraftforge.event.RegisterCommandsEvent;

public class ModCommands
{
    public ModCommands() {}

    public static void onCommandRegister(RegisterCommandsEvent event)
    {
        CommandDispatcher<CommandSourceStack> dispatcher = event.getDispatcher();
        LiteralArgumentBuilder<CommandSourceStack> pt2command = Commands.literal(PlantTechMain.MODID)
                .then(DevListCommand.register());
        dispatcher.register(pt2command);
    }
}
