package net.kaneka.planttech2.registries;

import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.inventory.ChipalyzerMenu;
import net.kaneka.planttech2.inventory.CompressorMenu;
import net.kaneka.planttech2.inventory.DNACleanerMenu;
import net.kaneka.planttech2.inventory.DNACombinerMenu;
import net.kaneka.planttech2.inventory.DNAExtractorMenu;
import net.kaneka.planttech2.inventory.DNARemoverMenu;
import net.kaneka.planttech2.inventory.EnergySupplierMenu;
import net.kaneka.planttech2.inventory.IdentifierMenu;
import net.kaneka.planttech2.inventory.InfuserMenu;
import net.kaneka.planttech2.inventory.ItemUpgradeableMenu;
import net.kaneka.planttech2.inventory.MachineBulbReprocessorMenu;
import net.kaneka.planttech2.inventory.MegaFurnaceMenu;
import net.kaneka.planttech2.inventory.PlantFarmMenu;
import net.kaneka.planttech2.inventory.SeedConstructorMenu;
import net.kaneka.planttech2.inventory.SeedSqueezerMenu;
import net.kaneka.planttech2.inventory.SolarGeneratorMenu;
import net.kaneka.planttech2.inventory.TeleporterMenu;
import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.flag.FeatureFlags;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.MenuType;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.network.IContainerFactory;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

import java.util.function.Supplier;

import static net.kaneka.planttech2.registries.ModReferences.*;

public class ModContainers
{
	public static final DeferredRegister<MenuType<?>> CONTAINERS = DeferredRegister.create(ForgeRegistries.MENU_TYPES, PlantTechMain.MODID);

	public static RegistryObject<MenuType<CompressorMenu>> COMPRESSOR = CONTAINERS.register(COMPRESSORCONTAINER, makeBlock(CompressorMenu::new));
	public static RegistryObject<MenuType<DNACleanerMenu>> DNACLEANER = CONTAINERS.register(DNACLEANERCONTAINER, makeBlock(DNACleanerMenu::new));
	public static RegistryObject<MenuType<DNACombinerMenu>> DNACOMBINER = CONTAINERS.register(DNACOMBINERCONTAINER, makeBlock(DNACombinerMenu::new));
	public static RegistryObject<MenuType<DNAExtractorMenu>> DNAEXTRACTOR = CONTAINERS.register(DNAEXTRACTORCONTAINER, makeBlock(DNAExtractorMenu::new));
	public static RegistryObject<MenuType<DNARemoverMenu>> DNAREMOVER = CONTAINERS.register(DNAREMOVERCONTAINER, makeBlock(DNARemoverMenu::new));
	public static RegistryObject<MenuType<IdentifierMenu>> IDENTIFIER = CONTAINERS.register(IDENTIFIERCONTAINER, makeBlock(IdentifierMenu::new));
	public static RegistryObject<MenuType<InfuserMenu>> INFUSER = CONTAINERS.register(INFUSERCONTAINER, makeBlock(InfuserMenu::new));
	public static RegistryObject<MenuType<ItemUpgradeableMenu>> UPGRADEABLEITEM = CONTAINERS.register(UPGRADEABLEITEMCONTAINER, makeItem( ItemUpgradeableMenu::new));
	public static RegistryObject<MenuType<MegaFurnaceMenu>> MEGAFURNACE = CONTAINERS.register(MEGAFURNACECONTAINER, makeBlock(MegaFurnaceMenu::new));
	public static RegistryObject<MenuType<PlantFarmMenu>> PLANTFARM = CONTAINERS.register(PLANTFARMCONTAINER, makeBlock(PlantFarmMenu::new));
	public static RegistryObject<MenuType<SeedConstructorMenu>> SEEDCONSTRUCTOR = CONTAINERS.register(SEEDCONSTRUCTORCONTAINER, makeBlock(SeedConstructorMenu::new));
	public static RegistryObject<MenuType<SeedSqueezerMenu>> SEEDQUEEZER = CONTAINERS.register(SEEDQUEEZERCONTAINER, makeBlock(SeedSqueezerMenu::new));
	public static RegistryObject<MenuType<SolarGeneratorMenu>> SOLARGENERATOR = CONTAINERS.register(SOLARGENERATORCONTAINER, makeBlock(SolarGeneratorMenu::new));
	public static RegistryObject<MenuType<ChipalyzerMenu>> CHIPALYZER = CONTAINERS.register(CHIPALYZERCONTAINER, makeBlock(ChipalyzerMenu::new));
	public static RegistryObject<MenuType<MachineBulbReprocessorMenu>> MACHINEBULBREPROCESSOR = CONTAINERS.register(MACHINEBULBREPROCESSORCONTAINER, makeBlock(MachineBulbReprocessorMenu::new));
	public static RegistryObject<MenuType<TeleporterMenu>> TELEPORTERITEM = CONTAINERS.register(TELEPORTERITEMCONTAINER, makeItem( TeleporterMenu::new ));
	public static RegistryObject<MenuType<EnergySupplierMenu>> ENERGYSUPPLIER = CONTAINERS.register(ENERGYSUPPLIERCONTAINER, makeBlock(EnergySupplierMenu::new));

	static <M extends AbstractContainerMenu> Supplier<MenuType<M>> makeItem(PlantTechItemMenuFactory<M> factory)
	{
		return make(factory);
	}

	static <M extends AbstractContainerMenu> Supplier<MenuType<M>> makeBlock(PlantTechMachineMenuFactory<M> factory)
	{
		return make(factory);
	}

	static <M extends AbstractContainerMenu> Supplier<MenuType<M>> make(MenuType.MenuSupplier<M> factory)
	{
		return () -> new MenuType<>(factory, FeatureFlags.DEFAULT_FLAGS);
	}

	interface PlantTechMachineMenuFactory<M extends AbstractContainerMenu> extends IContainerFactory<M>
	{
		@Override
		default M create(int windowId, Inventory inv, FriendlyByteBuf data)
		{
			return create(windowId, inv, data.readBlockPos());
		}

		M create(int id, Inventory inventory, BlockPos te);
	}

	interface PlantTechItemMenuFactory<M extends AbstractContainerMenu> extends IContainerFactory<M>
	{
		@Override
		default M create(int windowId, Inventory inv, FriendlyByteBuf data)
		{
			return create(windowId, inv, data.readItem());
		}

		M create(int id, Inventory inventory, ItemStack type);
	}
}
