package net.kaneka.planttech2.registries;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;
import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.addons.Pair;
import net.kaneka.planttech2.crops.CropTypes;
import net.kaneka.planttech2.enums.CropTraitsTypes;
import net.kaneka.planttech2.items.AdvancedAnalyserItem;
import net.kaneka.planttech2.items.AnalyserItem;
import net.kaneka.planttech2.items.BiomassContainerItem;
import net.kaneka.planttech2.items.CropRemover;
import net.kaneka.planttech2.items.CropSeedItem;
import net.kaneka.planttech2.items.DNAContainerItem;
import net.kaneka.planttech2.items.EnergyStorageItem;
import net.kaneka.planttech2.items.FertilizerItem;
import net.kaneka.planttech2.items.GuideItem;
import net.kaneka.planttech2.items.KnowledgeChip;
import net.kaneka.planttech2.items.MachineBulbItem;
import net.kaneka.planttech2.items.ParticleItem;
import net.kaneka.planttech2.items.TestItem;
import net.kaneka.planttech2.items.ThermometerItem;
import net.kaneka.planttech2.items.TierItem;
import net.kaneka.planttech2.items.WrenchItem;
import net.kaneka.planttech2.items.upgradeable.BaseUpgradeableItem;
import net.kaneka.planttech2.items.upgradeable.MultitoolItem;
import net.kaneka.planttech2.items.upgradeable.RangedWeaponItem;
import net.kaneka.planttech2.items.upgradeable.UpgradeChipItem;
import net.kaneka.planttech2.items.upgradeable.UpgradeableArmorItem;
import net.kaneka.planttech2.utilities.ModCreativeTabs;
import net.kaneka.planttech2.utilities.PlantTechConstants;
import net.minecraft.Util;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.ai.attributes.Attribute;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.food.FoodProperties;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.BucketItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.TieredItem;
import net.minecraft.world.item.Tiers;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.enchantment.Enchantments;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.ComposterBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.util.Lazy;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import static net.kaneka.planttech2.items.TierItem.ItemType.*;
import static net.kaneka.planttech2.utilities.ModCreativeTabs.*;


public class ModItems
{
    public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, PlantTechMain.MODID);

    public static final List<RegistryObject<Item>> MACHINE_BULBS = new ArrayList<>();
    public static final HashMap<String, RegistryObject<Item>> PARTICLES = new HashMap<>();

    public static RegistryObject<Item> ADVANCED_ANALYSER = ITEMS.register("advanced_analyser",() -> new AdvancedAnalyserItem());
    public static RegistryObject<Item> ANALYSER = ITEMS.register("analyser", () -> new AnalyserItem());
    public static RegistryObject<Item> AQUA_AFFINITY_CHIP = ITEMS.register("aqua_affinity_chip", () -> new UpgradeChipItem(new UpgradeChipItem.Properties(UpgradeChipItem.RestrictionTypes.HELMET).setEnchantment(Enchantments.AQUA_AFFINITY).setEnergyCost(5)));
    public static RegistryObject<Item> ARMORCHIP_TIER_1 = ITEMS.register("armorchip_tier_1", () -> new UpgradeChipItem(new UpgradeChipItem.Properties().setIncreaseArmor(1).setEnergyCost(1)));
    public static RegistryObject<Item> ARMORCHIP_TIER_2 = ITEMS.register("armorchip_tier_2", () -> new UpgradeChipItem(new UpgradeChipItem.Properties().setIncreaseArmor(2).setEnergyCost(3)));
    public static RegistryObject<Item> ARMORCHIP_TIER_3 = ITEMS.register("armorchip_tier_3", () -> new UpgradeChipItem(new UpgradeChipItem.Properties().setIncreaseArmor(4).setEnergyCost(7)));
    public static RegistryObject<Item> ATTACKCHIP_TIER_1 = ITEMS.register("attackchip_tier_1", () -> new UpgradeChipItem(new UpgradeChipItem.Properties().setIncreaseAttack(0.5F).setEnergyCost(1)));
    public static RegistryObject<Item> ATTACKCHIP_TIER_2 = ITEMS.register("attackchip_tier_2", () -> new UpgradeChipItem(new UpgradeChipItem.Properties().setIncreaseAttack(1F).setEnergyCost(2)));
    public static RegistryObject<Item> ATTACKCHIP_TIER_3 = ITEMS.register("attackchip_tier_3", () -> new UpgradeChipItem(new UpgradeChipItem.Properties().setIncreaseAttack(2F).setEnergyCost(4)));
    public static RegistryObject<Item> ATTACKSPEEDCHIP_TIER_1 = ITEMS.register("attackspeedchip_tier_1", () -> new UpgradeChipItem(new UpgradeChipItem.Properties().setIncreaseAttackSpeed(0.1F).setEnergyCost(1)));
    public static RegistryObject<Item> ATTACKSPEEDCHIP_TIER_2 = ITEMS.register("attackspeedchip_tier_2", () -> new UpgradeChipItem(new UpgradeChipItem.Properties().setIncreaseAttackSpeed(0.25F).setEnergyCost(2)));
    public static RegistryObject<Item> ATTACKSPEEDCHIP_TIER_3 = ITEMS.register("attackspeedchip_tier_3", () -> new UpgradeChipItem(new UpgradeChipItem.Properties().setIncreaseAttackSpeed(0.5F).setEnergyCost(4)));
    public static RegistryObject<Item> BANE_OF_ARTHROPODS_CHIP = ITEMS.register("bane_of_arthropods_chip", () -> new UpgradeChipItem(new UpgradeChipItem.Properties(UpgradeChipItem.RestrictionTypes.MELEE_WEAPON).setEnchantment(Enchantments.BANE_OF_ARTHROPODS).setEnergyCost(5)));
    public static RegistryObject<Item> BIOMASS = ModCreativeTabs.putItem(MAIN, ITEMS.register("biomass", () -> new Item(new Item.Properties())));
    public static RegistryObject<Item> BIOMASS_BUCKET = ModCreativeTabs.putItem(TOOLS_AND_ARMOR, ITEMS.register("biomass_bucket", () -> new BucketItem(ModFluids.BIOMASS, new Item.Properties().craftRemainder(Items.BUCKET).stacksTo(1))));
    public static RegistryObject<Item> BIOMASSCONTAINER = ITEMS.register("biomasscontainer", () -> new BiomassContainerItem());
    public static RegistryObject<Item> BLAST_PROTECTION_CHIP = ITEMS.register("blast_protection_chip", () -> new UpgradeChipItem(new UpgradeChipItem.Properties(UpgradeChipItem.RestrictionTypes.HELMET, UpgradeChipItem.RestrictionTypes.CHEST, UpgradeChipItem.RestrictionTypes.LEGGINGS, UpgradeChipItem.RestrictionTypes.BOOTS).setEnchantment(Enchantments.BLAST_PROTECTION).setEnergyCost(5)));
    public static RegistryObject<Item> BREAKDOWNRATECHIP_TIER_1 = ITEMS.register("breakdownratechip_tier_1", () -> new UpgradeChipItem(new UpgradeChipItem.Properties().setIncreaseBreakdownRate(0.5F).setEnergyCost(1)));
    public static RegistryObject<Item> BREAKDOWNRATECHIP_TIER_2 = ITEMS.register("breakdownratechip_tier_2", () -> new UpgradeChipItem(new UpgradeChipItem.Properties().setIncreaseBreakdownRate(1F).setEnergyCost(3)));
    public static RegistryObject<Item> BREAKDOWNRATECHIP_TIER_3 = ITEMS.register("breakdownratechip_tier_3", () -> new UpgradeChipItem(new UpgradeChipItem.Properties().setIncreaseBreakdownRate(2.5F).setEnergyCost(8)));
    public static RegistryObject<Item> CAPACITYCHIP_TIER_1 = ITEMS.register("capacitychip_tier_1", () -> new UpgradeChipItem(new UpgradeChipItem.Properties().setIncreaseCapacity(2000).setEnergyCost(1)));
    public static RegistryObject<Item> CAPACITYCHIP_TIER_2 = ITEMS.register("capacitychip_tier_2", () -> new UpgradeChipItem(new UpgradeChipItem.Properties().setIncreaseCapacity(5000).setEnergyCost(2)));
    public static RegistryObject<Item> CAPACITYCHIP_TIER_3 = ITEMS.register("capacitychip_tier_3", () -> new UpgradeChipItem(new UpgradeChipItem.Properties().setIncreaseCapacity(10000).setEnergyCost(5)));
    public static RegistryObject<Item> CAPACITYUPGRADE_TIER_1 = ModCreativeTabs.putItem(CHIPS, ITEMS.register("capacityupgrade_1", () -> new TierItem(new Item.Properties(), 1, CAPACITY_UPGRADE)));
    public static RegistryObject<Item> CAPACITYUPGRADE_TIER_2 = ModCreativeTabs.putItem(CHIPS, ITEMS.register("capacityupgrade_2", () -> new TierItem(new Item.Properties(), 2, CAPACITY_UPGRADE)));
    public static RegistryObject<Item> CAPACITYUPGRADE_TIER_3 = ModCreativeTabs.putItem(CHIPS, ITEMS.register("capacityupgrade_3", () -> new TierItem(new Item.Properties(), 3, CAPACITY_UPGRADE)));
    public static RegistryObject<Item> CAPACITYUPGRADE_TIER_4 = ModCreativeTabs.putItem(CHIPS, ITEMS.register("capacityupgrade_4", () -> new TierItem(new Item.Properties(), 3, CAPACITY_UPGRADE)));
    public static RegistryObject<Item> CATALYST_TEMPERATURE_TOLERANCE = ModCreativeTabs.putItem(MAIN, catalyst("catalyst_temperature_tolerance", CropTraitsTypes.TEMPERATURE_TOLERANCE, 0));
    public static RegistryObject<Item> CATALYST_GENE_STRENGTH = ModCreativeTabs.putItem(MAIN, catalyst("catalyst_gene_strength", CropTraitsTypes.GENE_STRENGTH, 0));
    public static RegistryObject<Item> CATALYST_ENERGY_VALUE = ModCreativeTabs.putItem(MAIN, catalyst("catalyst_energy_value", CropTraitsTypes.ENERGY_VALUE, 0));
    public static RegistryObject<Item> CATALYST_GROWTH_RATE = ModCreativeTabs.putItem(MAIN, catalyst("catalyst_growth_rate", CropTraitsTypes.GROW_SPEED, 0));
    public static RegistryObject<Item> CATALYST_WATER_SENSITIVITY = ModCreativeTabs.putItem(MAIN, catalyst("catalyst_water_sensitivity", CropTraitsTypes.WATER_SENSITIVITY, 0));
    public static RegistryObject<Item> CATALYST_TEMPERATURE_TOLERANCE_I = ModCreativeTabs.putItem(MAIN, catalyst("catalyst_temperature_tolerance_i", CropTraitsTypes.TEMPERATURE_TOLERANCE, 1));
    public static RegistryObject<Item> CATALYST_GENE_STRENGTH_I = ModCreativeTabs.putItem(MAIN, catalyst("catalyst_gene_strength_i", CropTraitsTypes.GENE_STRENGTH, 1));
    public static RegistryObject<Item> CATALYST_ENERGY_VALUE_I = ModCreativeTabs.putItem(MAIN, catalyst("catalyst_energy_value_i", CropTraitsTypes.ENERGY_VALUE, 1));
    public static RegistryObject<Item> CATALYST_GROWTH_RATE_I = ModCreativeTabs.putItem(MAIN, catalyst("catalyst_growth_rate_i", CropTraitsTypes.GENE_STRENGTH, 1));
    public static RegistryObject<Item> CATALYST_WATER_SENSITIVITY_I = ModCreativeTabs.putItem(MAIN, catalyst("catalyst_water_sensitivity_i", CropTraitsTypes.WATER_SENSITIVITY, 1));
    public static RegistryObject<Item> CATALYST_TEMPERATURE_TOLERANCE_II =ModCreativeTabs.putItem(MAIN, catalyst("catalyst_temperature_tolerance_ii", CropTraitsTypes.TEMPERATURE_TOLERANCE, 2));
    public static RegistryObject<Item> CATALYST_GENE_STRENGTH_II = ModCreativeTabs.putItem(MAIN, catalyst("catalyst_gene_strength_ii", CropTraitsTypes.GENE_STRENGTH, 2));
    public static RegistryObject<Item> CATALYST_ENERGY_VALUE_II = ModCreativeTabs.putItem(MAIN, catalyst("catalyst_energy_value_ii", CropTraitsTypes.ENERGY_VALUE, 2));
    public static RegistryObject<Item> CATALYST_GROWTH_RATE_II = ModCreativeTabs.putItem(MAIN, catalyst("catalyst_growth_rate_ii", CropTraitsTypes.GROW_SPEED, 2));
    public static RegistryObject<Item> CATALYST_WATER_SENSITIVITY_II = ModCreativeTabs.putItem(MAIN, catalyst("catalyst_water_sensitivity_ii", CropTraitsTypes.WATER_SENSITIVITY, 2));
    public static RegistryObject<Item> CHIP_UPGRADEPACK_CAPACITY_1 = ModCreativeTabs.putItem(MAIN, ITEMS.register("chip_upgradepack_capacity_1", () -> new TierItem(new Item.Properties(), 2, UPGRADE_CHIP)));
    public static RegistryObject<Item> CHIP_UPGRADEPACK_CAPACITY_2 = ModCreativeTabs.putItem(MAIN,  ITEMS.register("chip_upgradepack_capacity_2", () -> new TierItem(new Item.Properties(), 3, UPGRADE_CHIP)));
    public static RegistryObject<Item> CHIP_UPGRADEPACK_HARVESTLEVEL_1 = ModCreativeTabs.putItem(MAIN, ITEMS.register("chip_upgradepack_harvestlevel_1", () -> new TierItem(new Item.Properties(), 2, UPGRADE_CHIP)));
    public static RegistryObject<Item> CHIP_UPGRADEPACK_HARVESTLEVEL_2 = ModCreativeTabs.putItem(MAIN, ITEMS.register("chip_upgradepack_harvestlevel_2", () -> new TierItem(new Item.Properties(), 3, UPGRADE_CHIP)));
    public static RegistryObject<Item> CHIP_UPGRADEPACK_REACTOR_1 = ModCreativeTabs.putItem(MAIN, ITEMS.register("chip_upgradepack_reactor_1", () -> new TierItem(new Item.Properties(), 2, UPGRADE_CHIP)));
    public static RegistryObject<Item> CHIP_UPGRADEPACK_REACTOR_2 = ModCreativeTabs.putItem(MAIN, ITEMS.register("chip_upgradepack_reactor_2", () -> new TierItem(new Item.Properties(), 3, UPGRADE_CHIP)));
    public static RegistryObject<Item> CHIPALYZER_BULB = registerBulb("chipalyzer_bulb", () -> new MachineBulbItem(() -> ModBlocks.MACHINESHELL_PLANTIUM.get(), () -> ModBlocks.CHIPALYZER_GROWING.get(), PlantTechConstants.MACHINETIER_CHIPALYZER, 1000));
    public static RegistryObject<Item> COLOR_PARTICLES = ITEMS.register("color_particles", () -> new ParticleItem("color"));
    public static RegistryObject<Item> COMPRESSOR_BULB = registerBulb("compressor_bulb", () -> new MachineBulbItem(() -> ModBlocks.MACHINESHELL_IRON.get(), () -> ModBlocks.COMPRESSOR_GROWING.get(), PlantTechConstants.MACHINETIER_COMPRESSOR, 100));
    public static RegistryObject<Item> CRACKED_SNAIL_SHELL_BLACK = simpleCompostable("cracked_snail_shell_black", 0.3F);
    public static RegistryObject<Item> CRACKED_SNAIL_SHELL_CYBER = simpleCompostable("cracked_snail_shell_cyber", 1.0F);
    public static RegistryObject<Item> CRACKED_SNAIL_SHELL_GARDEN = simpleCompostable("cracked_snail_shell_garden", 0.5F);
    public static RegistryObject<Item> CRACKED_SNAIL_SHELL_MILK = simpleCompostable("cracked_snail_shell_milk", 0.6F);
    public static RegistryObject<Item> CRACKED_SNAIL_SHELL_PINK = simpleCompostable("cracked_snail_shell_pink", 0.8F);
    public static RegistryObject<Item> SNAIL_SHELL_DUST_BLACK = snailDust("snail_shell_dust_black", 0.3F, new FoodProperties.Builder().nutrition(3).saturationMod(0.75F).effect(() -> new MobEffectInstance(MobEffects.ABSORPTION, 40), 0.5F).fast().alwaysEat().build());
    public static RegistryObject<Item> SNAIL_SHELL_DUST_CYBER = snailDust("snail_shell_dust_cyber", 1.0F, new FoodProperties.Builder().nutrition(3).saturationMod(0.75F).effect(() -> new MobEffectInstance(MobEffects.INVISIBILITY, 40), 0.5F).fast().alwaysEat().build());
    public static RegistryObject<Item> SNAIL_SHELL_DUST_GARDEN = snailDust("snail_shell_dust_garden", 0.5F, new FoodProperties.Builder().nutrition(3).saturationMod(0.75F).fast().alwaysEat().build());
    public static RegistryObject<Item> SNAIL_SHELL_DUST_MILK = snailDust("snail_shell_dust_milk", 0.6F, new FoodProperties.Builder().nutrition(3).saturationMod(0.75F).effect(() -> new MobEffectInstance(MobEffects.SLOW_FALLING, 40), 0.5F).fast().alwaysEat().build());
    public static RegistryObject<Item> SNAIL_SHELL_DUST_PINK = snailDust("snail_shell_dust_pink", 0.8F, new FoodProperties.Builder().nutrition(3).saturationMod(0.75F).effect(() -> new MobEffectInstance(MobEffects.REGENERATION, 40), 0.5F).fast().alwaysEat().build());
    public static RegistryObject<Item> CROPREMOVER = ITEMS.register("cropremover", () -> new CropRemover());
    public static RegistryObject<Item> CYBERARMOR_BOOTS = ITEMS.register("cyberarmor_boots", () -> new UpgradeableArmorItem("cyberarmor", ArmorItem.Type.BOOTS, 1000, 10, 1, 0, UpgradeChipItem.RestrictionTypes.BOOTS));
    public static RegistryObject<Item> CYBERARMOR_CHEST = ITEMS.register("cyberarmor_chest", () -> new UpgradeableArmorItem("cyberarmor", ArmorItem.Type.CHESTPLATE, 1000, 10, 3, 0, UpgradeChipItem.RestrictionTypes.CHEST));
    public static RegistryObject<Item> CYBERARMOR_HELMET = ITEMS.register("cyberarmor_helmet", () -> new UpgradeableArmorItem("cyberarmor", ArmorItem.Type.HELMET, 1000, 10, 1, 0, UpgradeChipItem.RestrictionTypes.HELMET));
    public static RegistryObject<Item> CYBERARMOR_LEGGINGS = ITEMS.register("cyberarmor_leggings", () -> new UpgradeableArmorItem("cyberarmor", ArmorItem.Type.LEGGINGS, 1000, 10, 2, 0, UpgradeChipItem.RestrictionTypes.LEGGINGS));
    public static RegistryObject<Item> CYBERBOW = ModCreativeTabs.putItem(TOOLS_AND_ARMOR, ITEMS.register("cyberbow", () -> new RangedWeaponItem(new Item.Properties(), 1000, 10)));
    public static RegistryObject<Item> CYBERDAGGER = ModCreativeTabs.putItem(TOOLS_AND_ARMOR, ITEMS.register("cyberdagger", () -> new BaseUpgradeableItem(new Item.Properties(), 1000, 10, 1, -1.4F, UpgradeChipItem.RestrictionTypes.MELEE_WEAPON)));
    public static RegistryObject<Item> CYBERKATANA = ModCreativeTabs.putItem(TOOLS_AND_ARMOR, ITEMS.register("cyberkatana", () -> new BaseUpgradeableItem(new Item.Properties(), 1000, 10, 8, -3.4F, UpgradeChipItem.RestrictionTypes.MELEE_WEAPON)));
    public static RegistryObject<Item> CYBERRAPIER = ModCreativeTabs.putItem(TOOLS_AND_ARMOR, ITEMS.register("cyberrapier", () -> new BaseUpgradeableItem(new Item.Properties(), 1000, 10, 4, -2.4F, UpgradeChipItem.RestrictionTypes.MELEE_WEAPON)));
    public static RegistryObject<Item> DANCIUM_INGOT = ModCreativeTabs.putItem(MAIN, ITEMS.register("dancium_ingot", () -> new Item(new Item.Properties())));
    public static RegistryObject<Item> DANCIUM_NUGGET = ModCreativeTabs.putItem(MAIN, ITEMS.register("dancium_nugget", () -> new Item(new Item.Properties())));
//    public static RegistryObject<Item> DARK_CRYSTAL = ITEMS.register("dark_crystal", () -> new Item(new Item.Properties().tab(MAIN)));
    public static RegistryObject<Item> DEPTH_STRIDER_CHIP = ITEMS.register("depth_strider_chip", () -> new UpgradeChipItem(new UpgradeChipItem.Properties(UpgradeChipItem.RestrictionTypes.BOOTS).setEnchantment(Enchantments.DEPTH_STRIDER).setEnergyCost(5)));
    public static RegistryObject<Item> DNA_CLEANER_BULB = registerBulb("dna_cleaner_bulb", () -> new MachineBulbItem(() -> ModBlocks.MACHINESHELL_PLANTIUM.get(), () -> ModBlocks.DNA_CLEANER_GROWING.get(), PlantTechConstants.MACHINETIER_DNA_CLEANER, 2000));
    public static RegistryObject<Item> DNA_COMBINER_BULB = registerBulb("dna_combiner_bulb", () -> new MachineBulbItem(() -> ModBlocks.MACHINESHELL_PLANTIUM.get(), () -> ModBlocks.DNA_COMBINER_GROWING.get(), PlantTechConstants.MACHINETIER_DNA_COMBINER, 2000));
    public static RegistryObject<Item> DNA_CONTAINER = ITEMS.register("dna_container", () -> new DNAContainerItem());
    public static RegistryObject<Item> DNA_CONTAINER_EMPTY = ModCreativeTabs.putItem(MAIN, ITEMS.register("dna_container_empty", () -> new Item(new Item.Properties())));
    public static RegistryObject<Item> DNA_EXTRACTOR_BULB = registerBulb("dna_extractor_bulb", () -> new MachineBulbItem(() -> ModBlocks.MACHINESHELL_PLANTIUM.get(), () -> ModBlocks.DNA_EXTRACTOR_GROWING.get(), PlantTechConstants.MACHINETIER_DNA_EXTRACTOR, 2000));
    public static RegistryObject<Item> DNA_REMOVER_BULB = registerBulb("dna_remover_bulb", () -> new MachineBulbItem(() -> ModBlocks.MACHINESHELL_PLANTIUM.get(), () -> ModBlocks.DNA_REMOVER_GROWING.get(), PlantTechConstants.MACHINETIER_DNA_REMOVER, 2000));
    public static RegistryObject<Item> EFFICIENCY_CHIP = ITEMS.register("efficiency_chip", () -> new UpgradeChipItem(new UpgradeChipItem.Properties(UpgradeChipItem.RestrictionTypes.TOOL).setEnchantment(Enchantments.BLOCK_EFFICIENCY).setEnergyCost(5)));
    public static RegistryObject<Item> EMPTY_UPGRADECHIP_TIER_1 = ModCreativeTabs.putItem(MAIN, ITEMS.register("empty_upgradechip_1", () -> new TierItem(new Item.Properties(), 1, UPGRADE_CHIP)));
    public static RegistryObject<Item> EMPTY_UPGRADECHIP_TIER_2 = ModCreativeTabs.putItem(MAIN, ITEMS.register("empty_upgradechip_2", () -> new TierItem(new Item.Properties(), 2, UPGRADE_CHIP)));
    public static RegistryObject<Item> EMPTY_UPGRADECHIP_TIER_3 = ModCreativeTabs.putItem(MAIN, ITEMS.register("empty_upgradechip_3", () -> new TierItem(new Item.Properties(), 3, UPGRADE_CHIP)));
    public static RegistryObject<Item> ENERGY_SUPPLIER_BULB = registerBulb("energy_supplier_bulb", () -> new MachineBulbItem(() -> ModBlocks.MACHINESHELL_IRON.get(), () -> ModBlocks.ENERGY_SUPPLIER_GROWING.get(), PlantTechConstants.MACHINETIER_ENERGY_SUPPLIER, 100));
    public static RegistryObject<Item> ENERGYSTORAGE_TIER_1 = ModCreativeTabs.putItem(MAIN, ITEMS.register("energystorage_tier_1", () -> new EnergyStorageItem(new Item.Properties(), 500)));
    public static RegistryObject<Item> ENERGYSTORAGE_TIER_2 = ModCreativeTabs.putItem(MAIN, ITEMS.register("energystorage_tier_2", () -> new EnergyStorageItem(new Item.Properties(), 5000)));
    public static RegistryObject<Item> ENERGYSTORAGE_TIER_3 = ModCreativeTabs.putItem(MAIN, ITEMS.register("energystorage_tier_3", () -> new EnergyStorageItem(new Item.Properties(), 50000)));
    public static RegistryObject<Item> FEATHER_FALLING_CHIP = ITEMS.register("feather_falling_chip", () -> new UpgradeChipItem(new UpgradeChipItem.Properties(UpgradeChipItem.RestrictionTypes.BOOTS).setEnchantment(Enchantments.FALL_PROTECTION).setEnergyCost(5)));
    public static RegistryObject<Item> FERTILIZER_CREATIVE = ITEMS.register("fertilizer_creative", () -> new FertilizerItem(MAIN));
    public static RegistryObject<Item> FERTILIZER_TIER_1 = ITEMS.register("fertilizer_tier_1", () -> new FertilizerItem(MAIN));
    public static RegistryObject<Item> FERTILIZER_TIER_2 = ITEMS.register("fertilizer_tier_2", () -> new FertilizerItem(MAIN));
    public static RegistryObject<Item> FERTILIZER_TIER_3 = ITEMS.register("fertilizer_tier_3", () -> new FertilizerItem(MAIN));
    public static RegistryObject<Item> FERTILIZER_TIER_4 = ITEMS.register("fertilizer_tier_4", () -> new FertilizerItem(MAIN));
    public static RegistryObject<Item> FIRE_ASPECT_CHIP = ITEMS.register("fire_aspect_chip", () -> new UpgradeChipItem(new UpgradeChipItem.Properties(UpgradeChipItem.RestrictionTypes.MELEE_WEAPON).setEnchantment(Enchantments.FIRE_ASPECT).setEnergyCost(5)));
    public static RegistryObject<Item> FIRE_PROTECTION_CHIP = ITEMS.register("fire_protection_chip", () -> new UpgradeChipItem(new UpgradeChipItem.Properties(UpgradeChipItem.RestrictionTypes.HELMET, UpgradeChipItem.RestrictionTypes.CHEST, UpgradeChipItem.RestrictionTypes.LEGGINGS, UpgradeChipItem.RestrictionTypes.BOOTS).setEnchantment(Enchantments.FIRE_PROTECTION).setEnergyCost(5)));
    public static RegistryObject<Item> FLAME_CHIP = ITEMS.register("flame_chip", () -> new UpgradeChipItem(new UpgradeChipItem.Properties(UpgradeChipItem.RestrictionTypes.RANGED_WEAPON).setEnchantment(Enchantments.FLAMING_ARROWS).setEnergyCost(5)));
    public static RegistryObject<Item> FORTUNE_CHIP = ITEMS.register("fortune_chip", () -> new UpgradeChipItem(new UpgradeChipItem.Properties(UpgradeChipItem.RestrictionTypes.TOOL).setEnchantment(Enchantments.BLOCK_FORTUNE).setEnergyCost(5)));
    public static RegistryObject<Item> FROST_WALKER_CHIP = ITEMS.register("frost_walker_chip", () -> new UpgradeChipItem(new UpgradeChipItem.Properties(UpgradeChipItem.RestrictionTypes.BOOTS).setEnchantment(Enchantments.FROST_WALKER).setEnergyCost(5)));
    public static RegistryObject<Item> GEAR_DANCIUM = ModCreativeTabs.putItem(MAIN, ITEMS.register("gear_dancium", () -> new Item(new Item.Properties())));
    public static RegistryObject<Item> GEAR_DANCIUM_INFUSED = ModCreativeTabs.putItem(MAIN, ITEMS.register("gear_dancium_infused", () -> new Item(new Item.Properties())));
    public static RegistryObject<Item> GEAR_IRON = ModCreativeTabs.putItem(MAIN, ITEMS.register("gear_iron", () -> new Item(new Item.Properties())));
    public static RegistryObject<Item> GEAR_IRON_INFUSED = ModCreativeTabs.putItem(MAIN, ITEMS.register("gear_iron_infused", () -> new Item(new Item.Properties())));
    public static RegistryObject<Item> GEAR_KANEKIUM = ModCreativeTabs.putItem(MAIN, ITEMS.register("gear_kanekium", () -> new Item(new Item.Properties())));
    public static RegistryObject<Item> GEAR_KANEKIUM_INFUSED = ModCreativeTabs.putItem(MAIN, ITEMS.register("gear_kanekium_infused", () -> new Item(new Item.Properties())));
    public static RegistryObject<Item> GEAR_KINNOIUM = ModCreativeTabs.putItem(MAIN, ITEMS.register("gear_kinnoium", () -> new Item(new Item.Properties())));
    public static RegistryObject<Item> GEAR_KINNOIUM_INFUSED = ModCreativeTabs.putItem(MAIN, ITEMS.register("gear_kinnoium_infused", () -> new Item(new Item.Properties())));
    public static RegistryObject<Item> GEAR_LENTHURIUM = ModCreativeTabs.putItem(MAIN, ITEMS.register("gear_lenthurium", () -> new Item(new Item.Properties())));
    public static RegistryObject<Item> GEAR_LENTHURIUM_INFUSED = ModCreativeTabs.putItem(MAIN, ITEMS.register("gear_lenthurium_infused", () -> new Item(new Item.Properties())));
    public static RegistryObject<Item> GEAR_PLANTIUM = ModCreativeTabs.putItem(MAIN, ITEMS.register("gear_plantium", () -> new Item(new Item.Properties())));
    public static RegistryObject<Item> GEAR_PLANTIUM_INFUSED = ModCreativeTabs.putItem(MAIN, ITEMS.register("gear_plantium_infused", () -> new Item(new Item.Properties())));
    public static RegistryObject<Item> GUIDE_GENETIC_ENGINEERING = ITEMS.register("guide_genetic_engineering", () -> new GuideItem());
    public static RegistryObject<Item> GUIDE_OVERVIEW = ITEMS.register("guide_overview", () -> new GuideItem());
    public static RegistryObject<Item> GUIDE_PLANTS = ITEMS.register("guide_plants", () -> new GuideItem());
    public static RegistryObject<Item> HARVESTLEVELCHIP_TIER_1 = ITEMS.register("harvestlevelchip_tier_1", () -> new UpgradeChipItem(new UpgradeChipItem.Properties().setIncreaseHarvestlevel(1).setEnergyCost(2)));
    public static RegistryObject<Item> HARVESTLEVELCHIP_TIER_2 = ITEMS.register("harvestlevelchip_tier_2", () -> new UpgradeChipItem(new UpgradeChipItem.Properties().setIncreaseHarvestlevel(2).setEnergyCost(6)));
    public static RegistryObject<Item> HARVESTLEVELCHIP_TIER_3 = ITEMS.register("harvestlevelchip_tier_3", () -> new UpgradeChipItem(new UpgradeChipItem.Properties().setIncreaseHarvestlevel(4).setEnergyCost(15)));
    public static RegistryObject<Item> IDENTIFIER_BULB = registerBulb("identifier_bulb", () -> new MachineBulbItem(() -> ModBlocks.MACHINESHELL_IRON.get(), () -> ModBlocks.IDENTIFIER_GROWING.get(), PlantTechConstants.MACHINETIER_IDENTIFIER, 100));
    public static RegistryObject<Item> INFINITY_CHIP = ITEMS.register("infinity_chip", () -> new UpgradeChipItem(new UpgradeChipItem.Properties(UpgradeChipItem.RestrictionTypes.RANGED_WEAPON).setEnchantment(Enchantments.INFINITY_ARROWS).setEnergyCost(20)));
    public static RegistryObject<Item> INFUSER_BULB = registerBulb("infuser_bulb", () -> new MachineBulbItem(() -> ModBlocks.MACHINESHELL_IRON.get(), () -> ModBlocks.INFUSER_GROWING.get(), PlantTechConstants.MACHINETIER_INFUSER, 1000));
    public static RegistryObject<Item> KANEKIUM_INGOT = ModCreativeTabs.putItem(MAIN, ITEMS.register("kanekium_ingot", () -> new Item(new Item.Properties())));
    public static RegistryObject<Item> KANEKIUM_NUGGET = ModCreativeTabs.putItem(MAIN, ITEMS.register("kanekium_nugget", () -> new Item(new Item.Properties())));
    public static RegistryObject<Item> KINNOIUM_INGOT = ModCreativeTabs.putItem(MAIN, ITEMS.register("kinnoium_ingot", () -> new Item(new Item.Properties())));
    public static RegistryObject<Item> KINNOIUM_NUGGET = ModCreativeTabs.putItem(MAIN, ITEMS.register("kinnoium_nugget", () -> new Item(new Item.Properties())));
    public static RegistryObject<Item> KNOCKBACK_CHIP = ITEMS.register("knockback_chip", () -> new UpgradeChipItem(new UpgradeChipItem.Properties(UpgradeChipItem.RestrictionTypes.MELEE_WEAPON).setEnchantment(Enchantments.KNOCKBACK).setEnergyCost(5)));
    public static RegistryObject<Item> KNOWLEDGECHIP_TIER_0 = ITEMS.register("knowledgechip_0", () -> new KnowledgeChip(0, 50));
    public static RegistryObject<Item> KNOWLEDGECHIP_TIER_1 = ITEMS.register("knowledgechip_1", () -> new KnowledgeChip(1, 250));
    public static RegistryObject<Item> KNOWLEDGECHIP_TIER_2 = ITEMS.register("knowledgechip_2", () -> new KnowledgeChip(2, 1250));
    public static RegistryObject<Item> KNOWLEDGECHIP_TIER_3 = ITEMS.register("knowledgechip_3", () -> new KnowledgeChip(3, 6250));
    public static RegistryObject<Item> KNOWLEDGECHIP_TIER_4 = ITEMS.register("knowledgechip_4", () -> new KnowledgeChip(4, 31250));
    public static RegistryObject<Item> KNOWLEDGECHIP_TIER_5 = ITEMS.register("knowledgechip_5", () -> new KnowledgeChip(5, 156250));
    public static RegistryObject<Item> LENTHURIUM_INGOT = ModCreativeTabs.putItem(MAIN, ITEMS.register("lenthurium_ingot", () -> new Item(new Item.Properties())));
    public static RegistryObject<Item> LENTHURIUM_NUGGET = ModCreativeTabs.putItem(MAIN, ITEMS.register("lenthurium_nugget", () -> new Item(new Item.Properties())));
    public static RegistryObject<Item> LOOTING_CHIP = ITEMS.register("looting_chip", () -> new UpgradeChipItem(new UpgradeChipItem.Properties(UpgradeChipItem.RestrictionTypes.MELEE_WEAPON).setEnchantment(Enchantments.MOB_LOOTING).setEnergyCost(5)));
    public static RegistryObject<Item> MACHINEBULBREPROCESSOR_BULB = registerBulb("machinebulbreprocessor_bulb", () -> new MachineBulbItem(() -> ModBlocks.MACHINESHELL_IRON.get(), () -> ModBlocks.MACHINEBULBREPROCESSOR_GROWING.get(), PlantTechConstants.MACHINETIER_MACHINEBULBREPROCESSOR, 0));
    public static RegistryObject<Item> MEGAFURNACE_BULB = registerBulb("mega_furnace_bulb", () -> new MachineBulbItem(() -> ModBlocks.MACHINESHELL_PLANTIUM.get(), () -> ModBlocks.MEGAFURNACE_GROWING.get(), PlantTechConstants.MACHINETIER_MEGAFURNACE, 1000));
    public static RegistryObject<Item> MULTITOOL = ITEMS.register("multitool", () -> new MultitoolItem());
//    public static RegistryObject<Item> PLANT_OBTAINER = ITEMS.register("plant_obtainer", () -> new PlantObtainerItem(new Item.Properties().tab(TOOLS_AND_ARMOR)));
//    public static RegistryObject<Item> PLANTCARD = ITEMS.register("plantcard", () -> new CreditCardItem(new Item.Properties().tab(MAIN).stacksTo(1)));
    public static RegistryObject<Item> PLANTFARM_BULB = registerBulb("plantfarm_bulb", () -> new MachineBulbItem(() -> ModBlocks.MACHINESHELL_PLANTIUM.get(), () -> ModBlocks.PLANTFARM_GROWING.get(), PlantTechConstants.MACHINETIER_PLANTFARM, 2000));
    public static RegistryObject<Item> PLANTIUM_INGOT = ModCreativeTabs.putItem(MAIN, ITEMS.register("plantium_ingot", () -> new Item(new Item.Properties())));
    public static RegistryObject<Item> PLANTIUM_NUGGET = ModCreativeTabs.putItem(MAIN, ITEMS.register("plantium_nugget", () -> new Item(new Item.Properties())));
    public static RegistryObject<Item> POWER_CHIP = ITEMS.register("power_chip", () -> new UpgradeChipItem(new UpgradeChipItem.Properties(UpgradeChipItem.RestrictionTypes.RANGED_WEAPON).setEnchantment(Enchantments.POWER_ARROWS).setEnergyCost(5)));
    public static RegistryObject<Item> PROJECTILE_PROTECTION_CHIP = ITEMS.register("projectile_protection_chip", () -> new UpgradeChipItem(new UpgradeChipItem.Properties(UpgradeChipItem.RestrictionTypes.HELMET, UpgradeChipItem.RestrictionTypes.CHEST, UpgradeChipItem.RestrictionTypes.LEGGINGS, UpgradeChipItem.RestrictionTypes.BOOTS).setEnchantment(Enchantments.PROJECTILE_PROTECTION).setEnergyCost(5)));
    public static RegistryObject<Item> PROTECTION_CHIP = ITEMS.register("protection_chip", () -> new UpgradeChipItem(new UpgradeChipItem.Properties(UpgradeChipItem.RestrictionTypes.HELMET, UpgradeChipItem.RestrictionTypes.CHEST, UpgradeChipItem.RestrictionTypes.LEGGINGS, UpgradeChipItem.RestrictionTypes.BOOTS).setEnchantment(Enchantments.ALL_DAMAGE_PROTECTION).setEnergyCost(5)));
    public static RegistryObject<Item> PUNCH_CHIP = ITEMS.register("punch_chip", () -> new UpgradeChipItem(new UpgradeChipItem.Properties(UpgradeChipItem.RestrictionTypes.RANGED_WEAPON).setEnchantment(Enchantments.PUNCH_ARROWS).setEnergyCost(5)));
//    public static RegistryObject<Item> RADIATION_METRE = ITEMS.register("radiation_metre", () -> new RadiationMetreItem(new Item.Properties().tab(TOOLS_AND_ARMOR)));
    public static RegistryObject<Item> RAKE = ModCreativeTabs.putItem(TOOLS_AND_ARMOR, ITEMS.register("rake", () -> new TieredItem(Tiers.IRON, new Item.Properties()) {
        private final Multimap<Attribute, AttributeModifier> map = Util.make(() -> ImmutableMultimap.of(
                Attributes.ATTACK_DAMAGE, new AttributeModifier(BASE_ATTACK_DAMAGE_UUID, "Tool modifier", 3.0D, AttributeModifier.Operation.ADDITION)
        ));
        @Override
        public boolean canAttackBlock(BlockState p_43291_, Level p_43292_, BlockPos p_43293_, Player p_43294_)
        {
            return !p_43294_.isCreative();
        }

        @Override
        public Multimap<Attribute, AttributeModifier> getAttributeModifiers(EquipmentSlot slot, ItemStack stack)
        {
            return slot == EquipmentSlot.MAINHAND ? map : super.getAttributeModifiers(slot, stack);
        }

        @Override
        public void appendHoverText(ItemStack p_41421_, @Nullable Level p_41422_, List<Component> list, TooltipFlag p_41424_)
        {
            list.add(Component.translatable("planttech2.tip.rake"));
        }
    }));
    public static RegistryObject<Item> RANGEUPGRADE_TIER_1 = ModCreativeTabs.putItem(CHIPS, ITEMS.register("rangeupgrade_1", () -> new TierItem(new Item.Properties(), 1, RANGE_UPGRADE)));
    public static RegistryObject<Item> RANGEUPGRADE_TIER_2 = ModCreativeTabs.putItem(CHIPS, ITEMS.register("rangeupgrade_2", () -> new TierItem(new Item.Properties(), 2, RANGE_UPGRADE)));
    public static RegistryObject<Item> RANGEUPGRADE_TIER_3 = ModCreativeTabs.putItem(CHIPS, ITEMS.register("rangeupgrade_3", () -> new TierItem(new Item.Properties(), 3, RANGE_UPGRADE)));
    public static RegistryObject<Item> RANGEUPGRADE_TIER_4 = ModCreativeTabs.putItem(CHIPS, ITEMS.register("rangeupgrade_4", () -> new TierItem(new Item.Properties(), 4, RANGE_UPGRADE)));
    public static RegistryObject<Item> REACTORCHIP_TIER_1 = ITEMS.register("reactorchip_tier_1", () -> new UpgradeChipItem(new UpgradeChipItem.Properties().setEnergyProduction(1).setEnergyCost(1)));
    public static RegistryObject<Item> REACTORCHIP_TIER_2 = ITEMS.register("reactorchip_tier_2", () -> new UpgradeChipItem(new UpgradeChipItem.Properties().setEnergyProduction(3).setEnergyCost(2)));
    public static RegistryObject<Item> REACTORCHIP_TIER_3 = ITEMS.register("reactorchip_tier_3", () -> new UpgradeChipItem(new UpgradeChipItem.Properties().setEnergyProduction(5).setEnergyCost(5)));
    public static RegistryObject<Item> REDSTONE_INFUSED = ModCreativeTabs.putItem(MAIN, ITEMS.register("redstone_infused", () -> new Item(new Item.Properties())));
    public static RegistryObject<Item> RESPIRATION_CHIP = ITEMS.register("respiration_chip", () -> new UpgradeChipItem(new UpgradeChipItem.Properties(UpgradeChipItem.RestrictionTypes.HELMET).setEnchantment(Enchantments.RESPIRATION).setEnergyCost(5)));
    public static RegistryObject<Item> SEEDCONSTRUCTOR_BULB = registerBulb("seedconstructor_bulb", () -> new MachineBulbItem(() -> ModBlocks.MACHINESHELL_PLANTIUM.get(), () -> ModBlocks.SEEDCONSTRUCTOR_GROWING.get(), PlantTechConstants.MACHINETIER_SEEDCONSTRUCTOR, 2000));
    public static RegistryObject<Item> SEEDSQUEEZER_BULB = registerBulb("seedsqueezer_bulb", () -> new MachineBulbItem(() -> ModBlocks.MACHINESHELL_IRON.get(), () -> ModBlocks.SEEDSQUEEZER_GROWING.get(), PlantTechConstants.MACHINETIER_SEEDSQUEEZER, 0));
    public static RegistryObject<Item> SHARPNESS_CHIP = ITEMS.register("sharpness_chip", () -> new UpgradeChipItem(new UpgradeChipItem.Properties(UpgradeChipItem.RestrictionTypes.MELEE_WEAPON).setEnchantment(Enchantments.SHARPNESS).setEnergyCost(5)));
    public static RegistryObject<Item> SILK_TOUCH_CHIP = ITEMS.register("silk_touch_chip", () -> new UpgradeChipItem(new UpgradeChipItem.Properties(UpgradeChipItem.RestrictionTypes.TOOL).setEnchantment(Enchantments.SILK_TOUCH).setEnergyCost(5)));
    public static RegistryObject<Item> SMITE_CHIP = ITEMS.register("smite_chip", () -> new UpgradeChipItem(new UpgradeChipItem.Properties(UpgradeChipItem.RestrictionTypes.MELEE_WEAPON).setEnchantment(Enchantments.SMITE).setEnergyCost(5)));
    public static RegistryObject<Item> SOLARFOCUS_TIER_1 = ModCreativeTabs.putItem(CHIPS, ITEMS.register("solarfocus_1", () -> new TierItem(new Item.Properties(), 1, SOLAR_FOCUS)));
    public static RegistryObject<Item> SOLARFOCUS_TIER_2 = ModCreativeTabs.putItem(CHIPS, ITEMS.register("solarfocus_2", () -> new TierItem(new Item.Properties(), 2, SOLAR_FOCUS)));
    public static RegistryObject<Item> SOLARFOCUS_TIER_3 = ModCreativeTabs.putItem(CHIPS, ITEMS.register("solarfocus_3", () -> new TierItem(new Item.Properties(), 3, SOLAR_FOCUS)));
    public static RegistryObject<Item> SOLARFOCUS_TIER_4 = ModCreativeTabs.putItem(CHIPS, ITEMS.register("solarfocus_4", () -> new TierItem(new Item.Properties(), 4, SOLAR_FOCUS)));
    public static RegistryObject<Item> SOLARGENERATOR_BULB = registerBulb("solargenerator_bulb", () -> new MachineBulbItem(() -> ModBlocks.MACHINESHELL_PLANTIUM.get(), () -> ModBlocks.SOLARGENERATOR_GROWING.get(), PlantTechConstants.MACHINETIER_SOLARGENERATOR, 2000));
    public static RegistryObject<Item> SPEEDUPGRADE_TIER_1 = ModCreativeTabs.putItem(CHIPS, ITEMS.register("speedupgrade_1", () -> new TierItem(new Item.Properties(), 1, SPEED_UPGRADE)));
    public static RegistryObject<Item> SPEEDUPGRADE_TIER_2 = ModCreativeTabs.putItem(CHIPS, ITEMS.register("speedupgrade_2", () -> new TierItem(new Item.Properties(), 2, SPEED_UPGRADE)));
    public static RegistryObject<Item> SPEEDUPGRADE_TIER_3 = ModCreativeTabs.putItem(CHIPS, ITEMS.register("speedupgrade_3", () -> new TierItem(new Item.Properties(), 3, SPEED_UPGRADE)));
    public static RegistryObject<Item> SPEEDUPGRADE_TIER_4 = ModCreativeTabs.putItem(CHIPS, ITEMS.register("speedupgrade_4", () -> new TierItem(new Item.Properties(), 4, SPEED_UPGRADE)));
    public static RegistryObject<Item> SWEEPING_CHIP = ITEMS.register("sweeping_chip", () -> new UpgradeChipItem(new UpgradeChipItem.Properties(UpgradeChipItem.RestrictionTypes.MELEE_WEAPON).setEnchantment(Enchantments.SWEEPING_EDGE).setEnergyCost(5)));
//    public static RegistryObject<Item> TELEPORTER = ITEMS.register("teleporter", () -> new TeleporterItem(new Item.Properties().tab(MAIN).setNoRepair().durability(5000)));
    public static RegistryObject<Item> TESTITEM = ITEMS.register("testitem", () -> new TestItem());
    public static RegistryObject<Item> THERMOMETER = ITEMS.register("thermometer", () -> new ThermometerItem());
    public static RegistryObject<Item> THORNS_CHIP = ITEMS.register("thorns_chip", () -> new UpgradeChipItem(new UpgradeChipItem.Properties(UpgradeChipItem.RestrictionTypes.CHEST).setEnchantment(Enchantments.THORNS).setEnergyCost(5)));
    public static RegistryObject<Item> TOUGHNESSCHIP_TIER_1 = ITEMS.register("toughnesschip_tier_1", () -> new UpgradeChipItem(new UpgradeChipItem.Properties().setIncreaseToughness(0.5F).setEnergyCost(1)));
    public static RegistryObject<Item> TOUGHNESSCHIP_TIER_2 = ITEMS.register("toughnesschip_tier_2", () -> new UpgradeChipItem(new UpgradeChipItem.Properties().setIncreaseToughness(1F).setEnergyCost(3)));
    public static RegistryObject<Item> TOUGHNESSCHIP_TIER_3 = ITEMS.register("toughnesschip_tier_3", () -> new UpgradeChipItem(new UpgradeChipItem.Properties().setIncreaseToughness(2F).setEnergyCost(7)));
    public static RegistryObject<Item> UNBREAKING_CHIP = ITEMS.register("unbreaking_chip", () -> new UpgradeChipItem(new UpgradeChipItem.Properties(UpgradeChipItem.RestrictionTypes.HELMET, UpgradeChipItem.RestrictionTypes.CHEST, UpgradeChipItem.RestrictionTypes.LEGGINGS, UpgradeChipItem.RestrictionTypes.BOOTS).setEnchantment(Enchantments.UNBREAKING).setEnergyCost(5)));
    public static RegistryObject<Item> UNLOCKCHIP_AXE = ITEMS.register("unlockchip_axe", () -> new UpgradeChipItem(new UpgradeChipItem.Properties().setUnlockAxeFeat().setEnergyCost(2)));
    public static RegistryObject<Item> UNLOCKCHIP_HOE = ITEMS.register("unlockchip_hoe", () -> new UpgradeChipItem(new UpgradeChipItem.Properties().setUnlockHoeFeat().setEnergyCost(2)));
    public static RegistryObject<Item> UNLOCKCHIP_SHEARS = ITEMS.register("unlockchip_shears", () -> new UpgradeChipItem(new UpgradeChipItem.Properties().setUnlockShearsFeat().setEnergyCost(2)));
    public static RegistryObject<Item> UNLOCKCHIP_SHOVEL = ITEMS.register("unlockchip_shovel", () -> new UpgradeChipItem(new UpgradeChipItem.Properties().setUnlockShovelFeat().setEnergyCost(2)));
//    public static RegistryObject<Item> WHITE_CRYSTAL = ITEMS.register("white_crystal", () -> new Item(new Item.Properties().tab(MAIN)));
    public static RegistryObject<Item> WRENCH = ITEMS.register("wrench", () -> new WrenchItem());

    private static RegistryObject<Item> catalyst(String name, CropTraitsTypes type, int level)
    {
        return ITEMS.register(name, () -> new Item(new Item.Properties()) {
            @Override
            public void appendHoverText(ItemStack p_41421_, @Nullable Level p_41422_, List<Component> list, TooltipFlag p_41424_)
            {
                list.add(Component.translatable("planttech2.tip.catalyst"));
                list.add(Component.translatable("planttech2.tip.catalyst_2", type.getDisplayName()));
                list.add(Component.translatable("planttech2.tip.catalyst_3", (level + 1) + "%"));
            }
        });
    }

    public static final Lazy<Map<Item, Pair<Item, Pair<CropTraitsTypes, Integer>>>> CATALYSTS = Lazy.of(() -> ImmutableMap.<Item, Pair<Item, Pair<CropTraitsTypes, Integer>>>builder()
            .put(CATALYST_GENE_STRENGTH.get(), Pair.of(SNAIL_SHELL_DUST_GARDEN.get(), Pair.of(CropTraitsTypes.GENE_STRENGTH, 0)))
            .put(CATALYST_GENE_STRENGTH_I.get(), Pair.of(CATALYST_GENE_STRENGTH.get(), Pair.of(CropTraitsTypes.GENE_STRENGTH, 1)))
            .put(CATALYST_GENE_STRENGTH_II.get(), Pair.of(CATALYST_GENE_STRENGTH_I.get(), Pair.of(CropTraitsTypes.GENE_STRENGTH, 2)))
            .put(CATALYST_ENERGY_VALUE.get(), Pair.of(SNAIL_SHELL_DUST_BLACK.get(), Pair.of(CropTraitsTypes.ENERGY_VALUE, 0)))
            .put(CATALYST_ENERGY_VALUE_I.get(), Pair.of(CATALYST_ENERGY_VALUE.get(), Pair.of(CropTraitsTypes.ENERGY_VALUE, 1)))
            .put(CATALYST_ENERGY_VALUE_II.get(), Pair.of(CATALYST_ENERGY_VALUE_I.get(), Pair.of(CropTraitsTypes.ENERGY_VALUE, 2)))
            .put(CATALYST_WATER_SENSITIVITY.get(), Pair.of(SNAIL_SHELL_DUST_MILK.get(), Pair.of(CropTraitsTypes.TEMPERATURE_TOLERANCE, 0)))
            .put(CATALYST_WATER_SENSITIVITY_I.get(), Pair.of(CATALYST_WATER_SENSITIVITY.get(), Pair.of(CropTraitsTypes.TEMPERATURE_TOLERANCE, 1)))
            .put(CATALYST_WATER_SENSITIVITY_II.get(), Pair.of(CATALYST_WATER_SENSITIVITY_I.get(), Pair.of(CropTraitsTypes.TEMPERATURE_TOLERANCE, 2)))
            .put(CATALYST_TEMPERATURE_TOLERANCE.get(), Pair.of(SNAIL_SHELL_DUST_PINK.get(), Pair.of(CropTraitsTypes.TEMPERATURE_TOLERANCE, 0)))
            .put(CATALYST_TEMPERATURE_TOLERANCE_I.get(), Pair.of(CATALYST_TEMPERATURE_TOLERANCE.get(), Pair.of(CropTraitsTypes.TEMPERATURE_TOLERANCE, 1)))
            .put(CATALYST_TEMPERATURE_TOLERANCE_II.get(), Pair.of(CATALYST_TEMPERATURE_TOLERANCE_I.get(), Pair.of(CropTraitsTypes.TEMPERATURE_TOLERANCE, 2)))
            .put(CATALYST_GROWTH_RATE.get(), Pair.of(SNAIL_SHELL_DUST_CYBER.get(), Pair.of(CropTraitsTypes.GROW_SPEED, 0)))
            .put(CATALYST_GROWTH_RATE_I.get(), Pair.of(CATALYST_GROWTH_RATE.get(), Pair.of(CropTraitsTypes.GROW_SPEED, 1)))
            .put(CATALYST_GROWTH_RATE_II.get(), Pair.of(CATALYST_GROWTH_RATE_I.get(), Pair.of(CropTraitsTypes.GROW_SPEED, 2)))
            .build());

    public static void registerSeedsAndParticles()
    {
        RegistryObject<Item> tempseed, tempparticle;
        for (CropTypes entry : CropTypes.crops())
        {
            final String name = entry.getName();
            tempseed = ITEMS.register(name + "_seeds", () -> new CropSeedItem(entry));
            entry.setSeed(tempseed);
            if (entry.hasParticle())
            {
                tempparticle = ITEMS.register(name + "_particles", () -> new ParticleItem(name));
                PARTICLES.put(name, tempparticle);
            }
        }
    }

    private static RegistryObject<Item> registerBulb(String registryName, Supplier<Item> bulbItem)
    {
        RegistryObject<Item> registryObject = ITEMS.register(registryName, bulbItem);
        MACHINE_BULBS.add(registryObject);
        return registryObject;
    }

    private static RegistryObject<Item> simpleCompostable(String name, float chance)
    {
        return ModCreativeTabs.putItem(MAIN, ITEMS.register(name, () -> {
            Item i = new Item(new Item.Properties());
            ComposterBlock.COMPOSTABLES.put(i, chance);
            return i;
        }));
    }

    private static RegistryObject<Item> snailDust(String name, float chance, FoodProperties food)
    {
        return ModCreativeTabs.putItem(MAIN, ITEMS.register(name, () -> {
            Item i = new Item(new Item.Properties().food(food));
            ComposterBlock.COMPOSTABLES.put(i, chance);
            return i;
        }));
    }
}
