package net.kaneka.planttech2.registries;

import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.entities.passive.plant_mite.PlantMiteEntity;
import net.kaneka.planttech2.entities.passive.snail.SnailEntity;
import net.kaneka.planttech2.entities.passive.tech_trader.TechTrader;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.MobCategory;
import net.minecraftforge.event.entity.EntityAttributeCreationEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class ModEntityTypes
{
    public static final DeferredRegister<EntityType<?>> ENTITY_TYPES = DeferredRegister.create(ForgeRegistries.ENTITY_TYPES, PlantTechMain.MODID);

    public static final RegistryObject<EntityType<TechTrader>> TECH_TRADER = ENTITY_TYPES.register("tech_trader", () -> EntityType.Builder.of(TechTrader::new, MobCategory.CREATURE).sized(0.6F, 1.95F).clientTrackingRange(10).build("tech_trader"));
    public static final RegistryObject<EntityType<SnailEntity>> SNAIL = ENTITY_TYPES.register("snail", () -> EntityType.Builder.of(SnailEntity::new, MobCategory.CREATURE).sized(0.4F, 0.4F).clientTrackingRange(10).build("snail"));
    public static final RegistryObject<EntityType<PlantMiteEntity>> PLANT_MITE = ENTITY_TYPES.register("plant_mite", () -> EntityType.Builder.of(PlantMiteEntity::new, MobCategory.CREATURE).sized(0.2F, 0.15F).clientTrackingRange(8).build("plant_mite"));

    @SubscribeEvent
    public static void onEntityAttributesRegister(EntityAttributeCreationEvent event)
    {
        event.put(TECH_TRADER.get(), Mob.createMobAttributes().build());
        event.put(SNAIL.get(), SnailEntity.createSnailAttributes().build());
        event.put(PLANT_MITE.get(), PlantMiteEntity.createPlantMiteAttributes().build());
    }
}
