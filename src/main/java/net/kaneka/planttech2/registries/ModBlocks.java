package net.kaneka.planttech2.registries;

import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.blocks.CarverBlock;
import net.kaneka.planttech2.blocks.CropBarsBlock;
import net.kaneka.planttech2.blocks.CropBaseBlock;
import net.kaneka.planttech2.blocks.ElectricFence;
import net.kaneka.planttech2.blocks.ElectricFenceGate;
import net.kaneka.planttech2.blocks.ElectricFenceTop;
import net.kaneka.planttech2.blocks.FacingGrowingBlock;
import net.kaneka.planttech2.blocks.GrowingBlock;
import net.kaneka.planttech2.blocks.Hedge;
import net.kaneka.planttech2.blocks.entity.machine.ChipalyzerBlockEntity;
import net.kaneka.planttech2.blocks.entity.machine.CompressorBlockEntity;
import net.kaneka.planttech2.blocks.entity.machine.DNACleanerBlockEntity;
import net.kaneka.planttech2.blocks.entity.machine.DNACombinerBlockEntity;
import net.kaneka.planttech2.blocks.entity.machine.DNAExtractorBlockEntity;
import net.kaneka.planttech2.blocks.entity.machine.DNARemoverBlockEntity;
import net.kaneka.planttech2.blocks.entity.machine.EnergySupplierBlockEntity;
import net.kaneka.planttech2.blocks.entity.machine.IdentifierBlockEntity;
import net.kaneka.planttech2.blocks.entity.machine.InfuserBlockEntity;
import net.kaneka.planttech2.blocks.entity.machine.MachineBulbReprocessorBlockEntity;
import net.kaneka.planttech2.blocks.entity.machine.MegaFurnaceBlockEntity;
import net.kaneka.planttech2.blocks.entity.machine.PlantFarmBlockEntity;
import net.kaneka.planttech2.blocks.entity.machine.SeedConstructorBlockEntity;
import net.kaneka.planttech2.blocks.entity.machine.SeedSqueezerBlockEntity;
import net.kaneka.planttech2.blocks.entity.machine.SolarGeneratorBlockEntity;
import net.kaneka.planttech2.blocks.machines.CableBlock;
import net.kaneka.planttech2.blocks.machines.EnergySupplierBlock;
import net.kaneka.planttech2.blocks.machines.MachineBaseBlock;
import net.kaneka.planttech2.blocks.machines.MachineFacingBlock;
import net.kaneka.planttech2.crops.CropTypes;
import net.kaneka.planttech2.utilities.ModCreativeTabs;
import net.kaneka.planttech2.utilities.PlantTechConstants;
import net.kaneka.planttech2.utilities.blocks.BlockRenderLayer;
import net.kaneka.planttech2.utilities.blocks.WithItem;
import net.kaneka.planttech2.utilities.blocks.WithItem.Tab;
import net.minecraft.ChatFormatting;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.CandleBlock;
import net.minecraft.world.level.block.FenceBlock;
import net.minecraft.world.level.block.FenceGateBlock;
import net.minecraft.world.level.block.LiquidBlock;
import net.minecraft.world.level.block.SlabBlock;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.StairBlock;
import net.minecraft.world.level.block.TrapDoorBlock;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.properties.BlockSetType;
import net.minecraft.world.level.block.state.properties.WoodType;
import net.minecraft.world.level.material.MapColor;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

import javax.annotation.Nullable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class ModBlocks {
    public static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS, PlantTechMain.MODID);
    public static List<RegistryObject<Block>> HEDGE_BLOCKS = new ArrayList<>();

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> ACACIA_ACACIA_DIRT_HEDGE = registerHedge("hedge_acacia_acacia_dirt",
            () -> new Hedge(Blocks.ACACIA_LEAVES, Blocks.ACACIA_LOG, Blocks.DIRT));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> ACACIA_ACACIA_GRASS_HEDGE = registerHedge("hedge_acacia_acacia_grass",
            () -> new Hedge(Blocks.ACACIA_LEAVES, Blocks.ACACIA_LOG, Blocks.GRASS_BLOCK));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> ACACIA_ACACIA_PODZOL_HEDGE = registerHedge("hedge_acacia_acacia_podzol",
            () -> new Hedge(Blocks.ACACIA_LEAVES, Blocks.ACACIA_LOG, Blocks.PODZOL));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> ACACIA_BIRCH_DIRT_HEDGE = registerHedge("hedge_acacia_birch_dirt",
            () -> new Hedge(Blocks.ACACIA_LEAVES, Blocks.BIRCH_LOG, Blocks.DIRT));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> ACACIA_BIRCH_GRASS_HEDGE = registerHedge("hedge_acacia_birch_grass",
            () -> new Hedge(Blocks.ACACIA_LEAVES, Blocks.BIRCH_LOG, Blocks.GRASS_BLOCK));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> ACACIA_BIRCH_PODZOL_HEDGE = registerHedge("hedge_acacia_birch_podzol",
            () -> new Hedge(Blocks.ACACIA_LEAVES, Blocks.BIRCH_LOG, Blocks.PODZOL));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> ACACIA_DARK_OAK_DIRT_HEDGE = registerHedge("hedge_acacia_dark_oak_dirt",
            () -> new Hedge(Blocks.ACACIA_LEAVES, Blocks.DARK_OAK_LOG, Blocks.DIRT));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> ACACIA_DARK_OAK_GRASS_HEDGE = registerHedge("hedge_acacia_dark_oak_grass",
            () -> new Hedge(Blocks.ACACIA_LEAVES, Blocks.DARK_OAK_LOG, Blocks.GRASS_BLOCK));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> ACACIA_DARK_OAK_PODZOL_HEDGE = registerHedge("hedge_acacia_dark_oak_podzol",
            () -> new Hedge(Blocks.ACACIA_LEAVES, Blocks.DARK_OAK_LOG, Blocks.PODZOL));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> ACACIA_JUNGLE_DIRT_HEDGE = registerHedge("hedge_acacia_jungle_dirt",
            () -> new Hedge(Blocks.ACACIA_LEAVES, Blocks.JUNGLE_LOG, Blocks.DIRT));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> ACACIA_JUNGLE_GRASS_HEDGE = registerHedge("hedge_acacia_jungle_grass",
            () -> new Hedge(Blocks.ACACIA_LEAVES, Blocks.JUNGLE_LOG, Blocks.GRASS_BLOCK));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> ACACIA_JUNGLE_PODZOL_HEDGE = registerHedge("hedge_acacia_jungle_podzol",
            () -> new Hedge(Blocks.ACACIA_LEAVES, Blocks.JUNGLE_LOG, Blocks.PODZOL));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> ACACIA_OAK_DIRT_HEDGE = registerHedge("hedge_acacia_oak_dirt",
            () -> new Hedge(Blocks.ACACIA_LEAVES, Blocks.OAK_LOG, Blocks.DIRT));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> ACACIA_OAK_GRASS_HEDGE = registerHedge("hedge_acacia_oak_grass",
            () -> new Hedge(Blocks.ACACIA_LEAVES, Blocks.OAK_LOG, Blocks.GRASS_BLOCK));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> ACACIA_OAK_PODZOL_HEDGE = registerHedge("hedge_acacia_oak_podzol",
            () -> new Hedge(Blocks.ACACIA_LEAVES, Blocks.OAK_LOG, Blocks.PODZOL));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> ACACIA_SPRUCE_DIRT_HEDGE = registerHedge("hedge_acacia_spruce_dirt",
            () -> new Hedge(Blocks.ACACIA_LEAVES, Blocks.SPRUCE_LOG, Blocks.DIRT));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> ACACIA_SPRUCE_GRASS_HEDGE = registerHedge("hedge_acacia_spruce_grass",
            () -> new Hedge(Blocks.ACACIA_LEAVES, Blocks.SPRUCE_LOG, Blocks.GRASS_BLOCK));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> ACACIA_SPRUCE_PODZOL_HEDGE = registerHedge("hedge_acacia_spruce_podzol",
            () -> new Hedge(Blocks.ACACIA_LEAVES, Blocks.SPRUCE_LOG, Blocks.PODZOL));

    public static RegistryObject<Block> BIOMASSFLUIDBLOCK = BLOCKS.register("biomassfluid_block",
            () -> new LiquidBlock(ModFluids.BIOMASS, BlockBehaviour.Properties.of().sound(SoundType.EMPTY).noCollission().strength(100.0F).noLootTable()));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> BIRCH_ACACIA_DIRT_HEDGE = registerHedge("hedge_birch_acacia_dirt",
            () -> new Hedge(Blocks.BIRCH_LEAVES, Blocks.ACACIA_LOG, Blocks.DIRT));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> BIRCH_ACACIA_GRASS_HEDGE = registerHedge("hedge_birch_acacia_grass",
            () -> new Hedge(Blocks.BIRCH_LEAVES, Blocks.ACACIA_LOG, Blocks.GRASS_BLOCK));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> BIRCH_ACACIA_PODZOL_HEDGE = registerHedge("hedge_birch_acacia_podzol",
            () -> new Hedge(Blocks.BIRCH_LEAVES, Blocks.ACACIA_LOG, Blocks.PODZOL));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> BIRCH_BIRCH_DIRT_HEDGE = registerHedge("hedge_birch_birch_dirt",
            () -> new Hedge(Blocks.BIRCH_LEAVES, Blocks.BIRCH_LOG, Blocks.DIRT));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> BIRCH_BIRCH_GRASS_HEDGE = registerHedge("hedge_birch_birch_grass",
            () -> new Hedge(Blocks.BIRCH_LEAVES, Blocks.BIRCH_LOG, Blocks.GRASS_BLOCK));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> BIRCH_BIRCH_PODZOL_HEDGE = registerHedge("hedge_birch_birch_podzol",
            () -> new Hedge(Blocks.BIRCH_LEAVES, Blocks.BIRCH_LOG, Blocks.PODZOL));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> BIRCH_DARK_OAK_DIRT_HEDGE = registerHedge("hedge_birch_dark_oak_dirt",
            () -> new Hedge(Blocks.BIRCH_LEAVES, Blocks.DARK_OAK_LOG, Blocks.DIRT));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> BIRCH_DARK_OAK_GRASS_HEDGE = registerHedge("hedge_birch_dark_oak_grass",
            () -> new Hedge(Blocks.BIRCH_LEAVES, Blocks.DARK_OAK_LOG, Blocks.GRASS_BLOCK));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> BIRCH_DARK_OAK_PODZOL_HEDGE = registerHedge("hedge_birch_dark_oak_podzol",
            () -> new Hedge(Blocks.BIRCH_LEAVES, Blocks.DARK_OAK_LOG, Blocks.PODZOL));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> BIRCH_JUNGLE_DIRT_HEDGE = registerHedge("hedge_birch_jungle_dirt",
            () -> new Hedge(Blocks.BIRCH_LEAVES, Blocks.JUNGLE_LOG, Blocks.DIRT));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> BIRCH_JUNGLE_GRASS_HEDGE = registerHedge("hedge_birch_jungle_grass",
            () -> new Hedge(Blocks.BIRCH_LEAVES, Blocks.JUNGLE_LOG, Blocks.GRASS_BLOCK));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> BIRCH_JUNGLE_PODZOL_HEDGE = registerHedge("hedge_birch_jungle_podzol",
            () -> new Hedge(Blocks.BIRCH_LEAVES, Blocks.JUNGLE_LOG, Blocks.PODZOL));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> BIRCH_OAK_DIRT_HEDGE = registerHedge("hedge_birch_oak_dirt",
            () -> new Hedge(Blocks.BIRCH_LEAVES, Blocks.OAK_LOG, Blocks.DIRT));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> BIRCH_OAK_GRASS_HEDGE = registerHedge("hedge_birch_oak_grass",
            () -> new Hedge(Blocks.BIRCH_LEAVES, Blocks.OAK_LOG, Blocks.GRASS_BLOCK));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> BIRCH_OAK_PODZOL_HEDGE = registerHedge("hedge_birch_oak_podzol",
            () -> new Hedge(Blocks.BIRCH_LEAVES, Blocks.OAK_LOG, Blocks.PODZOL));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> BIRCH_SPRUCE_DIRT_HEDGE = registerHedge("hedge_birch_spruce_dirt",
            () -> new Hedge(Blocks.BIRCH_LEAVES, Blocks.SPRUCE_LOG, Blocks.DIRT));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> BIRCH_SPRUCE_GRASS_HEDGE = registerHedge("hedge_birch_spruce_grass",
            () -> new Hedge(Blocks.BIRCH_LEAVES, Blocks.SPRUCE_LOG, Blocks.GRASS_BLOCK));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> BIRCH_SPRUCE_PODZOL_HEDGE = registerHedge("hedge_birch_spruce_podzol",
            () -> new Hedge(Blocks.BIRCH_LEAVES, Blocks.SPRUCE_LOG, Blocks.PODZOL));

//    @BlockRenderLayer(layer = BlockRenderLayer.RenderLayer.TRANSLUCENT)
//    @WithItem
//    public static RegistryObject<Block> BLACK_ICE = BLOCKS.register("black_ice",
//            () -> new IceBlock(BlockBehaviour.Properties.of(Material.ICE).friction(0.98F).randomTicks().strength(0.5F).sound(SoundType.GLASS).noOcclusion()));

    @WithItem()
    public static RegistryObject<Block> BROWN_MUSHROOM_CANDLE = BLOCKS.register("brown_mushroom_candle",
            () -> new CandleBlock(BlockBehaviour.Properties.of().sound(SoundType.CANDLE).mapColor(MapColor.COLOR_BROWN).noOcclusion().strength(0.1F).sound(SoundType.CANDLE).lightLevel(CandleBlock.LIGHT_EMISSION)));

    @WithItem()
    public static RegistryObject<Block> BROWN_MUSHROOM_FENCE = BLOCKS.register("brown_mushroom_fence",
            () -> new FenceBlock(BlockBehaviour.Properties.of().sound(SoundType.WOOD).strength(2.0F, 3.0F).sound(SoundType.WOOD)));

    @WithItem()
    public static RegistryObject<Block> BROWN_MUSHROOM_FENCE_GATE = BLOCKS.register("brown_mushroom_fence_gate",
            () -> new FenceGateBlock(BlockBehaviour.Properties.of().sound(SoundType.WOOD).strength(2.0F, 3.0F).sound(SoundType.WOOD), WoodType.OAK));

    @WithItem()
    public static RegistryObject<Block> BROWN_MUSHROOM_SLAB = BLOCKS.register("brown_mushroom_slab",
            () -> new SlabBlock(BlockBehaviour.Properties.of().sound(SoundType.WOOD).sound(SoundType.WOOD).strength(0.2F)));

    @WithItem()
    public static RegistryObject<Block> BROWN_MUSHROOM_STAIRS = BLOCKS.register("brown_mushroom_stairs",
            () -> new StairBlock(Blocks.BROWN_MUSHROOM_BLOCK::defaultBlockState, BlockBehaviour.Properties.of().sound(SoundType.WOOD).sound(SoundType.WOOD).strength(0.2F)));

    @WithItem()
    public static RegistryObject<Block> BROWN_MUSHROOM_TRAPDOOR = BLOCKS.register("brown_mushroom_trapdoor",
            () -> new TrapDoorBlock(BlockBehaviour.Properties.of().sound(SoundType.WOOD).mapColor(MapColor.WOOD).strength(3.0F).sound(SoundType.WOOD).noOcclusion(), BlockSetType.OAK));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> CABLE = BLOCKS.register("cable",
            () -> new CableBlock());

    @BlockRenderLayer
    @WithItem(tab = Tab.MAIN)
    public static RegistryObject<Block> CARVER = BLOCKS.register("carver",
            () -> new CarverBlock());

    @BlockRenderLayer
    @WithItem(tab = Tab.MACHINES)
    public static RegistryObject<Block> CHIPALYZER = BLOCKS.register("chipalyzer",
            () -> new MachineFacingBlock(ChipalyzerBlockEntity::new, PlantTechConstants.MACHINETIER_CHIPALYZER));

    @BlockRenderLayer
    public static RegistryObject<Block> CHIPALYZER_GROWING = BLOCKS.register("chipalyzer_growing",
            () -> new FacingGrowingBlock(ModBlocks.CHIPALYZER, true));

    @BlockRenderLayer
    @WithItem(tab = Tab.MACHINES)
    public static RegistryObject<Block> COMPRESSOR = BLOCKS.register("compressor",
            () -> new MachineFacingBlock(CompressorBlockEntity::new, PlantTechConstants.MACHINETIER_COMPRESSOR));

    @BlockRenderLayer
    public static RegistryObject<Block> COMPRESSOR_GROWING = BLOCKS.register("compressor_growing",
            () -> new FacingGrowingBlock(ModBlocks.COMPRESSOR, true));

    @WithItem()
    public static RegistryObject<Block> CRIMSON_HYPHAE_CANDLE = BLOCKS.register("crimson_hyphae_candle",
            () -> new CandleBlock(BlockBehaviour.Properties.of().sound(SoundType.CANDLE).mapColor(MapColor.CRIMSON_HYPHAE).noOcclusion().strength(0.1F).sound(SoundType.CANDLE).lightLevel(CandleBlock.LIGHT_EMISSION)));

    @WithItem()
    public static RegistryObject<Block> CRIMSON_HYPHAE_FENCE = BLOCKS.register("crimson_hyphae_fence",
            () -> new FenceBlock(BlockBehaviour.Properties.of().sound(SoundType.WOOD).mapColor(MapColor.CRIMSON_HYPHAE).strength(2.0F, 3.0F).sound(SoundType.WOOD)));

    @WithItem()
    public static RegistryObject<Block> CRIMSON_HYPHAE_FENCE_GATE = BLOCKS.register("crimson_hyphae_fence_gate",
            () -> new FenceGateBlock(BlockBehaviour.Properties.of().sound(SoundType.WOOD).mapColor(MapColor.CRIMSON_HYPHAE).strength(2.0F, 3.0F).sound(SoundType.WOOD), WoodType.OAK));

    @WithItem()
    public static RegistryObject<Block> CRIMSON_HYPHAE_SLAB = BLOCKS.register("crimson_hyphae_slab",
            () -> new SlabBlock(BlockBehaviour.Properties.of().sound(SoundType.WOOD).mapColor(MapColor.CRIMSON_HYPHAE).sound(SoundType.WOOD).strength(0.2F)));

    @WithItem()
    public static RegistryObject<Block> CRIMSON_HYPHAE_STAIRS = BLOCKS.register("crimson_hyphae_stairs",
            () -> new StairBlock(Blocks.CRIMSON_HYPHAE::defaultBlockState, BlockBehaviour.Properties.of().sound(SoundType.WOOD).mapColor(MapColor.CRIMSON_HYPHAE).sound(SoundType.WOOD).strength(0.2F)));

    @WithItem()
    public static RegistryObject<Block> CRIMSON_HYPHAE_TRAPDOOR = BLOCKS.register("crimson_hyphae_trapdoor",
            () -> new TrapDoorBlock(BlockBehaviour.Properties.of().sound(SoundType.WOOD).mapColor(MapColor.CRIMSON_HYPHAE).strength(3.0F).sound(SoundType.WOOD).noOcclusion(), BlockSetType.OAK));

    @BlockRenderLayer
    @WithItem(tab = Tab.MAIN)
    public static RegistryObject<Block> CROPBARS = BLOCKS.register("cropbars",
            () -> new CropBarsBlock());

    @WithItem
    public static RegistryObject<Block> DANCIUM_BLOCK = BLOCKS.register("dancium_block",
            () -> new Block(BlockBehaviour.Properties.of().sound(SoundType.METAL).strength(0.9F)));

//    @WithItem
//    public static RegistryObject<Block> DARK_CRYSTAL_BLOCK = BLOCKS.register("dark_crystal_block",
//            () -> new Block(BlockBehaviour.Properties.of(Material.STONE).sound(SoundType.STONE).strength(0.9F)));

//    @WithItem
//    public static RegistryObject<Block> DARK_CRYSTAL_BRICK = BLOCKS.register("dark_crystal_brick",
//            () -> new Block(BlockBehaviour.Properties.of(Material.STONE).sound(SoundType.STONE).strength(0.9F)));

//    @BlockRenderLayer
//    @WithItem
//    public static RegistryObject<Block> DARK_CRYSTAL_DOOR = BLOCKS.register("dark_crystal_door",
//            () -> new DoorBlock(BlockBehaviour.Properties.of(Material.GLASS).sound(SoundType.GLASS).strength(0.2F).noOcclusion()));

//    @WithItem
//    public static RegistryObject<Block> DARK_CRYSTAL_FENCE = BLOCKS.register("dark_crystal_fence",
//            () -> new CustomFenceBlock(BlockBehaviour.Properties.of(Material.STONE).sound(SoundType.STONE).strength(0.9F)));

//    @BlockRenderLayer
//    @WithItem
//    public static RegistryObject<Block> DARK_CRYSTAL_GLASSPANE_CROSS = BLOCKS.register("dark_crystal_glasspane_cross",
//            () -> new GlassPanePillar(BlockBehaviour.Properties.of(Material.GLASS).sound(SoundType.GLASS).strength(0.2F)));

//    @BlockRenderLayer
//    @WithItem
//    public static RegistryObject<Block> DARK_CRYSTAL_GLASSPANE_END = BLOCKS.register("dark_crystal_glasspane_end",
//            () -> new GlassPaneEnd(BlockBehaviour.Properties.of(Material.GLASS).sound(SoundType.GLASS).strength(0.2F), Color.WHITE.getRGB()));

//    @BlockRenderLayer
//    @WithItem
//    public static RegistryObject<Block> DARK_CRYSTAL_GLASSPANE_MIDDLE = BLOCKS.register("dark_crystal_glasspane_middle",
//            () -> new GlassPanePillar(BlockBehaviour.Properties.of(Material.GLASS).sound(SoundType.GLASS).strength(0.2F)));
//
//    @WithItem
//    public static RegistryObject<Block> DARK_CRYSTAL_LAMP = BLOCKS.register("dark_crystal_lamp",
//            () -> new Block(BlockBehaviour.Properties.of(Material.STONE).sound(SoundType.STONE).strength(0.9F).lightLevel((p) -> {return 15;}).noOcclusion()));
//
//    @WithItem
//    public static RegistryObject<Block> DARK_CRYSTAL_ORE = BLOCKS.register("dark_crystal_ore",
//            () -> new BaseOreBlock(BlockBehaviour.Properties.of().sound(SoundTypes.DIRT).sound(SoundType.GRAVEL).strength(0.2F), 1, 3));
//
//    @WithItem
//    public static RegistryObject<Block> DARK_CRYSTAL_SLAB = BLOCKS.register("dark_crystal_slab",
//            () -> new SlabBlock(BlockBehaviour.Properties.of(Material.STONE).sound(SoundType.STONE).strength(0.2F)));
//
//    @WithItem
//    public static RegistryObject<Block> DARK_CRYSTAL_STAIRS = BLOCKS.register("dark_crystal_stairs",
//            () -> new DoorBlock(BlockBehaviour.Properties.of(Material.GLASS).sound(SoundType.GLASS).strength(0.2F).noOcclusion()));
//
//    @WithItem
//    public static RegistryObject<Block> DARK_CRYSTAL_TILING = BLOCKS.register("dark_crystal_tiling",
//            () -> new Block(BlockBehaviour.Properties.of(Material.STONE).sound(SoundType.STONE).strength(0.9F)));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> DARK_OAK_ACACIA_DIRT_HEDGE = registerHedge("hedge_dark_oak_acacia_dirt",
            () -> new Hedge(Blocks.DARK_OAK_LEAVES, Blocks.ACACIA_LOG, Blocks.DIRT));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> DARK_OAK_ACACIA_GRASS_HEDGE = registerHedge("hedge_dark_oak_acacia_grass",
            () -> new Hedge(Blocks.DARK_OAK_LEAVES, Blocks.ACACIA_LOG, Blocks.GRASS_BLOCK));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> DARK_OAK_ACACIA_PODZOL_HEDGE = registerHedge("hedge_dark_oak_acacia_podzol",
            () -> new Hedge(Blocks.DARK_OAK_LEAVES, Blocks.ACACIA_LOG, Blocks.PODZOL));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> DARK_OAK_BIRCH_DIRT_HEDGE = registerHedge("hedge_dark_oak_birch_dirt",
            () -> new Hedge(Blocks.DARK_OAK_LEAVES, Blocks.BIRCH_LOG, Blocks.DIRT));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> DARK_OAK_BIRCH_GRASS_HEDGE = registerHedge("hedge_dark_oak_birch_grass",
            () -> new Hedge(Blocks.DARK_OAK_LEAVES, Blocks.BIRCH_LOG, Blocks.GRASS_BLOCK));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> DARK_OAK_BIRCH_PODZOL_HEDGE = registerHedge("hedge_dark_oak_birch_podzol",
            () -> new Hedge(Blocks.DARK_OAK_LEAVES, Blocks.BIRCH_LOG, Blocks.PODZOL));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> DARK_OAK_DARK_OAK_DIRT_HEDGE = registerHedge("hedge_dark_oak_dark_oak_dirt",
            () -> new Hedge(Blocks.DARK_OAK_LEAVES, Blocks.DARK_OAK_LOG, Blocks.DIRT));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> DARK_OAK_DARK_OAK_GRASS_HEDGE = registerHedge("hedge_dark_oak_dark_oak_grass",
            () -> new Hedge(Blocks.DARK_OAK_LEAVES, Blocks.DARK_OAK_LOG, Blocks.GRASS_BLOCK));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> DARK_OAK_DARK_OAK_PODZOL_HEDGE = registerHedge("hedge_dark_oak_dark_oak_podzol",
            () -> new Hedge(Blocks.DARK_OAK_LEAVES, Blocks.DARK_OAK_LOG, Blocks.PODZOL));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> DARK_OAK_JUNGLE_DIRT_HEDGE = registerHedge("hedge_dark_oak_jungle_dirt",
            () -> new Hedge(Blocks.DARK_OAK_LEAVES, Blocks.JUNGLE_LOG, Blocks.DIRT));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> DARK_OAK_JUNGLE_GRASS_HEDGE = registerHedge("hedge_dark_oak_jungle_grass",
            () -> new Hedge(Blocks.DARK_OAK_LEAVES, Blocks.JUNGLE_LOG, Blocks.GRASS_BLOCK));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> DARK_OAK_JUNGLE_PODZOL_HEDGE = registerHedge("hedge_dark_oak_jungle_podzol",
            () -> new Hedge(Blocks.DARK_OAK_LEAVES, Blocks.JUNGLE_LOG, Blocks.PODZOL));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> DARK_OAK_OAK_DIRT_HEDGE = registerHedge("hedge_dark_oak_oak_dirt",
            () -> new Hedge(Blocks.DARK_OAK_LEAVES, Blocks.OAK_LOG, Blocks.DIRT));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> DARK_OAK_OAK_GRASS_HEDGE = registerHedge("hedge_dark_oak_oak_grass",
            () -> new Hedge(Blocks.DARK_OAK_LEAVES, Blocks.OAK_LOG, Blocks.GRASS_BLOCK));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> DARK_OAK_OAK_PODZOL_HEDGE = registerHedge("hedge_dark_oak_oak_podzol",
            () -> new Hedge(Blocks.DARK_OAK_LEAVES, Blocks.OAK_LOG, Blocks.PODZOL));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> DARK_OAK_SPRUCE_DIRT_HEDGE = registerHedge("hedge_dark_oak_spruce_dirt",
            () -> new Hedge(Blocks.DARK_OAK_LEAVES, Blocks.SPRUCE_LOG, Blocks.DIRT));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> DARK_OAK_SPRUCE_GRASS_HEDGE = registerHedge("hedge_dark_oak_spruce_grass",
            () -> new Hedge(Blocks.DARK_OAK_LEAVES, Blocks.SPRUCE_LOG, Blocks.GRASS_BLOCK));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> DARK_OAK_SPRUCE_PODZOL_HEDGE = registerHedge("hedge_dark_oak_spruce_podzol",
            () -> new Hedge(Blocks.DARK_OAK_LEAVES, Blocks.SPRUCE_LOG, Blocks.PODZOL));

    @BlockRenderLayer
    @WithItem(tab = Tab.MACHINES)
    public static RegistryObject<Block> DNA_CLEANER = BLOCKS.register("dna_cleaner",
            () -> new MachineFacingBlock(DNACleanerBlockEntity::new, PlantTechConstants.MACHINETIER_DNA_CLEANER));

    @BlockRenderLayer
    public static RegistryObject<Block> DNA_CLEANER_GROWING = BLOCKS.register("dna_cleaner_growing",
            () -> new FacingGrowingBlock(ModBlocks.DNA_CLEANER, true));

    @BlockRenderLayer
    @WithItem(tab = Tab.MACHINES)
    public static RegistryObject<Block> DNA_COMBINER = BLOCKS.register("dna_combiner",
            () -> new MachineFacingBlock(DNACombinerBlockEntity::new, PlantTechConstants.MACHINETIER_DNA_COMBINER));

    @BlockRenderLayer
    public static RegistryObject<Block> DNA_COMBINER_GROWING = BLOCKS.register("dna_combiner_growing",
            () -> new FacingGrowingBlock(ModBlocks.DNA_COMBINER, true));

    @BlockRenderLayer
    @WithItem(tab = Tab.MACHINES)
    public static RegistryObject<Block> DNA_EXTRACTOR = BLOCKS.register("dna_extractor",
            () -> new MachineFacingBlock(DNAExtractorBlockEntity::new, PlantTechConstants.MACHINETIER_DNA_EXTRACTOR));

    @BlockRenderLayer
    public static RegistryObject<Block> DNA_EXTRACTOR_GROWING = BLOCKS.register("dna_extractor_growing",
            () -> new FacingGrowingBlock(ModBlocks.DNA_EXTRACTOR, true));

    @BlockRenderLayer
    @WithItem(tab = Tab.MACHINES)
    public static RegistryObject<Block> DNA_REMOVER = BLOCKS.register("dna_remover",
            () -> new MachineFacingBlock(DNARemoverBlockEntity::new, PlantTechConstants.MACHINETIER_DNA_REMOVER));

    @BlockRenderLayer
    public static RegistryObject<Block> DNA_REMOVER_GROWING = BLOCKS.register("dna_remover_growing",
            () -> new FacingGrowingBlock(ModBlocks.DNA_REMOVER, true));

    @WithItem
    public static RegistryObject<Block> ELECTRIC_FENCE = BLOCKS.register("electric_fence",
            () -> new ElectricFence(BlockBehaviour.Properties.of().sound(SoundType.METAL).strength(30.0F)));

    @WithItem
    public static RegistryObject<Block> ELECTRIC_FENCE_GATE = BLOCKS.register("electric_fence_gate",
            () -> new ElectricFenceGate(BlockBehaviour.Properties.of().sound(SoundType.METAL).strength(30.0F)));

    @WithItem
    public static RegistryObject<Block> ELECTRIC_FENCE_TOP = BLOCKS.register("electric_fence_top",
            () -> new ElectricFenceTop(BlockBehaviour.Properties.of().sound(SoundType.METAL).strength(30.0F)));

    @BlockRenderLayer
    @WithItem(tab = Tab.MACHINES)
    public static RegistryObject<Block> ENERGY_SUPPLIER = BLOCKS.register("energy_supplier",
            () -> new EnergySupplierBlock(EnergySupplierBlockEntity::new, PlantTechConstants.MACHINETIER_ENERGY_SUPPLIER));

    @BlockRenderLayer
    public static RegistryObject<Block> ENERGY_SUPPLIER_GROWING = BLOCKS.register("energy_supplier_growing",
            () -> new GrowingBlock(ModBlocks.ENERGY_SUPPLIER, true));

//    @WithItem
//    public static RegistryObject<Block> FIBRE_FENCE = BLOCKS.register("fibre_fence",
//            () -> new Block(BlockBehaviour.Properties.of().sound(SoundType.WOOD).sound(SoundType.WOOD)));

    @BlockRenderLayer
    @WithItem(tab = Tab.MACHINES)
    public static RegistryObject<Block> IDENTIFIER = BLOCKS.register("identifier",
            () -> new MachineFacingBlock(IdentifierBlockEntity::new, PlantTechConstants.MACHINETIER_IDENTIFIER));

    @BlockRenderLayer
    public static RegistryObject<Block> IDENTIFIER_GROWING = BLOCKS.register("identifier_growing",
            () -> new FacingGrowingBlock(ModBlocks.IDENTIFIER, true));

//    @WithItem
//    public static RegistryObject<Block> INFUSED_BLUE_ICE = BLOCKS.register("infused_blue_ice",
//            () -> new HalfTransparentBlock(BlockBehaviour.Properties.of(Material.ICE_SOLID).strength(2.8F).friction(0.989F).sound(SoundType.GLASS)));
//
//    @WithItem
//    public static RegistryObject<Block> INFUSED_COBBLESTONE = BLOCKS.register("infused_cobblestone",
//            () -> new Block(BlockBehaviour.Properties.of(Material.STONE).requiresCorrectToolForDrops().strength(2.0F, 6.0F)));
//
//    @BlockRenderLayer(layer = BlockRenderLayer.RenderLayer.TRANSLUCENT)
//    @WithItem
//    public static RegistryObject<Block> INFUSED_ICE = BLOCKS.register("infused_ice",
//            () -> new InfusedIceBlock(BlockBehaviour.Properties.of(Material.ICE).friction(0.98F).randomTicks().strength(0.5F).sound(SoundType.GLASS).noOcclusion()));
//
//    @WithItem
//    public static RegistryObject<Block> INFUSED_PACKED_ICE = BLOCKS.register("infused_packed_ice",
//            () -> new Block(BlockBehaviour.Properties.of(Material.ICE_SOLID).friction(0.98F).strength(0.5F).sound(SoundType.GLASS)));
//
//    @WithItem
//    public static RegistryObject<Block> INFUSED_STONE = BLOCKS.register("infused_stone",
//            () -> new Block(BlockBehaviour.Properties.of(Material.STONE, MaterialColor.STONE).requiresCorrectToolForDrops().strength(1.5F, 6.0F)));

    @BlockRenderLayer
    @WithItem(tab = Tab.MACHINES)
    public static RegistryObject<Block> INFUSER = BLOCKS.register("infuser",
            () -> new MachineFacingBlock(InfuserBlockEntity::new, PlantTechConstants.MACHINETIER_INFUSER));

    @BlockRenderLayer
    public static RegistryObject<Block> INFUSER_GROWING = BLOCKS.register("infuser_growing",
            () -> new FacingGrowingBlock(ModBlocks.INFUSER, true));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> JUNGLE_ACACIA_DIRT_HEDGE = registerHedge("hedge_jungle_acacia_dirt",
            () -> new Hedge(Blocks.JUNGLE_LEAVES, Blocks.ACACIA_LOG, Blocks.DIRT));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> JUNGLE_ACACIA_GRASS_HEDGE = registerHedge("hedge_jungle_acacia_grass",
            () -> new Hedge(Blocks.JUNGLE_LEAVES, Blocks.ACACIA_LOG, Blocks.GRASS_BLOCK));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> JUNGLE_ACACIA_PODZOL_HEDGE = registerHedge("hedge_jungle_acacia_podzol",
            () -> new Hedge(Blocks.JUNGLE_LEAVES, Blocks.ACACIA_LOG, Blocks.PODZOL));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> JUNGLE_BIRCH_DIRT_HEDGE = registerHedge("hedge_jungle_birch_dirt",
            () -> new Hedge(Blocks.JUNGLE_LEAVES, Blocks.BIRCH_LOG, Blocks.DIRT));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> JUNGLE_BIRCH_GRASS_HEDGE = registerHedge("hedge_jungle_birch_grass",
            () -> new Hedge(Blocks.JUNGLE_LEAVES, Blocks.BIRCH_LOG, Blocks.GRASS_BLOCK));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> JUNGLE_BIRCH_PODZOL_HEDGE = registerHedge("hedge_jungle_birch_podzol",
            () -> new Hedge(Blocks.JUNGLE_LEAVES, Blocks.BIRCH_LOG, Blocks.PODZOL));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> JUNGLE_DARK_OAK_DIRT_HEDGE = registerHedge("hedge_jungle_dark_oak_dirt",
            () -> new Hedge(Blocks.JUNGLE_LEAVES, Blocks.DARK_OAK_LOG, Blocks.DIRT));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> JUNGLE_DARK_OAK_GRASS_HEDGE = registerHedge("hedge_jungle_dark_oak_grass",
            () -> new Hedge(Blocks.JUNGLE_LEAVES, Blocks.DARK_OAK_LOG, Blocks.GRASS_BLOCK));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> JUNGLE_DARK_OAK_PODZOL_HEDGE = registerHedge("hedge_jungle_dark_oak_podzol",
            () -> new Hedge(Blocks.JUNGLE_LEAVES, Blocks.DARK_OAK_LOG, Blocks.PODZOL));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> JUNGLE_JUNGLE_DIRT_HEDGE = registerHedge("hedge_jungle_jungle_dirt",
            () -> new Hedge(Blocks.JUNGLE_LEAVES, Blocks.JUNGLE_LOG, Blocks.DIRT));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> JUNGLE_JUNGLE_GRASS_HEDGE = registerHedge("hedge_jungle_jungle_grass",
            () -> new Hedge(Blocks.JUNGLE_LEAVES, Blocks.JUNGLE_LOG, Blocks.GRASS_BLOCK));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> JUNGLE_JUNGLE_PODZOL_HEDGE = registerHedge("hedge_jungle_jungle_podzol",
            () -> new Hedge(Blocks.JUNGLE_LEAVES, Blocks.JUNGLE_LOG, Blocks.PODZOL));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> JUNGLE_OAK_DIRT_HEDGE = registerHedge("hedge_jungle_oak_dirt",
            () -> new Hedge(Blocks.JUNGLE_LEAVES, Blocks.OAK_LOG, Blocks.DIRT));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> JUNGLE_OAK_GRASS_HEDGE = registerHedge("hedge_jungle_oak_grass",
            () -> new Hedge(Blocks.JUNGLE_LEAVES, Blocks.OAK_LOG, Blocks.GRASS_BLOCK));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> JUNGLE_OAK_PODZOL_HEDGE = registerHedge("hedge_jungle_oak_podzol",
            () -> new Hedge(Blocks.JUNGLE_LEAVES, Blocks.OAK_LOG, Blocks.PODZOL));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> JUNGLE_SPRUCE_DIRT_HEDGE = registerHedge("hedge_jungle_spruce_dirt",
            () -> new Hedge(Blocks.JUNGLE_LEAVES, Blocks.SPRUCE_LOG, Blocks.DIRT));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> JUNGLE_SPRUCE_GRASS_HEDGE = registerHedge("hedge_jungle_spruce_grass",
            () -> new Hedge(Blocks.JUNGLE_LEAVES, Blocks.SPRUCE_LOG, Blocks.GRASS_BLOCK));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> JUNGLE_SPRUCE_PODZOL_HEDGE = registerHedge("hedge_jungle_spruce_podzol",
            () -> new Hedge(Blocks.JUNGLE_LEAVES, Blocks.SPRUCE_LOG, Blocks.PODZOL));

    @WithItem
    public static RegistryObject<Block> KANEKIUM_BLOCK = BLOCKS.register("kanekium_block",
            () -> new Block(BlockBehaviour.Properties.of().sound(SoundType.METAL).strength(0.9F)));

    @WithItem
    public static RegistryObject<Block> KINNOIUM_BLOCK = BLOCKS.register("kinnoium_block",
            () -> new Block(BlockBehaviour.Properties.of().sound(SoundType.METAL).strength(0.9F)));

    @WithItem
    public static RegistryObject<Block> LENTHURIUM_BLOCK = BLOCKS.register("lenthurium_block",
            () -> new Block(BlockBehaviour.Properties.of().sound(SoundType.METAL).strength(0.9F)));

//    @WithItem(isWIP = true)
//    public static RegistryObject<Block> LIGNIFIED_MUSHROOM_CANDLE = BLOCKS.register("lignified_mushroom_candle",
//            () -> new CandleBlock(BlockBehaviour.Properties.of(Material.DECORATION, MaterialColor.COLOR_RED).noOcclusion().strength(0.1F).sound(SoundType.CANDLE).lightLevel(CandleBlock.LIGHT_EMISSION)));
//
//    @BlockRenderLayer
//    @WithItem(isWIP = true)
//    public static RegistryObject<Block> LIGNIFIED_MUSHROOM_DOOR_BROWN = BLOCKS.register("lignified_mushroom_door_brown",
//            () -> new DoorBlock(BlockBehaviour.Properties.of(Material.GLASS).sound(SoundType.GLASS).strength(0.2F).noOcclusion()));
//
//    @BlockRenderLayer
//    @WithItem(isWIP = true)
//    public static RegistryObject<Block> LIGNIFIED_MUSHROOM_DOOR_RED = BLOCKS.register("lignified_mushroom_door_red",
//            () -> new DoorBlock(BlockBehaviour.Properties.of(Material.GLASS).sound(SoundType.GLASS).strength(0.2F).noOcclusion()));
//
//    @WithItem(isWIP = true)
//    public static RegistryObject<Block> LIGNIFIED_MUSHROOM_FENCE = BLOCKS.register("lignified_mushroom_fence",
//            () -> new FenceBlock(BlockBehaviour.Properties.of(Material.WOOD, MaterialColor.WOOL).strength(2.0F, 3.0F).sound(SoundType.WOOD)));
//
//    @WithItem(isWIP = true)
//    public static RegistryObject<Block> LIGNIFIED_MUSHROOM_FENCE_GATE = BLOCKS.register("lignified_mushroom_fence_gate",
//            () -> new FenceGateBlock(BlockBehaviour.Properties.of(Material.WOOD, MaterialColor.WOOL).strength(2.0F, 3.0F).sound(SoundType.WOOD)));
//
//    @WithItem(isWIP = true)
//    public static RegistryObject<Block> LIGNIFIED_MUSHROOM_LAMP = BLOCKS.register("lignified_mushroom_lamp",
//            () -> new ShapedBlock(BlockBehaviour.Properties.of(Material.WOOD, MaterialColor.WOOL).sound(SoundType.WOOD).strength(0.2F).lightLevel((state) -> 13), Shapes.or(Block.box(4, 0, 4, 12, 7, 12), Block.box(2, 7, 2, 14, 14, 14))));
//
//    @WithItem(isWIP = true)
//    public static RegistryObject<Block> LIGNIFIED_MUSHROOM_SLAB = BLOCKS.register("lignified_mushroom_slab",
//            () -> new SlabBlock(BlockBehaviour.Properties.of(Material.WOOD, MaterialColor.WOOL).sound(SoundType.WOOD).strength(0.2F)));
//
//    @WithItem(isWIP = true)
//    public static RegistryObject<Block> LIGNIFIED_MUSHROOM_STEM = BLOCKS.register("lignified_mushroom_stem",
//            () -> new RotatedPillarBlock(BlockBehaviour.Properties.of().sound(SoundType.WOOD).sound(SoundType.WOOD).strength(0.2F)));
//
//    @WithItem(isWIP = true)
//    public static RegistryObject<Block> LIGNIFIED_MUSHROOM_STAIRS = BLOCKS.register("lignified_mushroom_stairs",
//            () -> new StairBlock(() -> LIGNIFIED_MUSHROOM_STEM.get().defaultBlockState(), BlockBehaviour.Properties.of(Material.WOOD, MaterialColor.WOOL).sound(SoundType.WOOD).strength(0.2F)));
//
//    @WithItem(isWIP = true)
//    public static RegistryObject<Block> LIGNIFIED_MUSHROOM_STEM_STRIPPED = BLOCKS.register("lignified_mushroom_stem_stripped",
//            () -> new CustomStrippableBlock(BlockBehaviour.Properties.of().sound(SoundType.WOOD).sound(SoundType.WOOD).strength(0.2F)));
//
//    @WithItem(isWIP = true)
//    public static RegistryObject<Block> LIGNIFIED_MUSHROOM_TRAPDOOR = BLOCKS.register("lignified_mushroom_trapdoor",
//            () -> new TrapDoorBlock(BlockBehaviour.Properties.of(Material.WOOD, MaterialColor.WOOL).strength(3.0F).sound(SoundType.WOOD).noOcclusion()));

    @BlockRenderLayer
    @WithItem(tab = Tab.MACHINES)
    public static RegistryObject<Block> MACHINEBULBREPROCESSOR = BLOCKS.register("machinebulbreprocessor",
            () -> new MachineBaseBlock(MachineBulbReprocessorBlockEntity::new, PlantTechConstants.MACHINETIER_MACHINEBULBREPROCESSOR));

    @BlockRenderLayer
    public static RegistryObject<Block> MACHINEBULBREPROCESSOR_GROWING = BLOCKS.register("machinebulbreprocessor_growing",
            () -> new GrowingBlock(ModBlocks.MACHINEBULBREPROCESSOR, true));

    @BlockRenderLayer
    @WithItem(tab = Tab.MAIN)
    public static RegistryObject<Block> MACHINESHELL_IRON = BLOCKS.register("machineshell_iron",
            () -> new Block(BlockBehaviour.Properties.of().sound(SoundType.METAL).strength(0.9F).noOcclusion()));

    @BlockRenderLayer
    public static RegistryObject<Block> MACHINESHELL_IRON_GROWING = BLOCKS.register("machineshell_iron_growing",
            () -> new GrowingBlock(ModBlocks.MACHINESHELL_IRON, false));

    @BlockRenderLayer
    @WithItem(tab = Tab.MAIN)
    public static RegistryObject<Block> MACHINESHELL_PLANTIUM = BLOCKS.register("machineshell_plantium",
            () -> new Block(BlockBehaviour.Properties.of().sound(SoundType.METAL).strength(0.9F).noOcclusion()));

    @BlockRenderLayer
    public static RegistryObject<Block> MACHINESHELL_PLANTIUM_GROWING = BLOCKS.register("machineshell_plantium_growing",
            () -> new GrowingBlock(ModBlocks.MACHINESHELL_PLANTIUM, false));

    @BlockRenderLayer
    @WithItem(tab = Tab.MACHINES)
    public static RegistryObject<Block> MEGAFURNACE = BLOCKS.register("mega_furnace",
            () -> new MachineFacingBlock(MegaFurnaceBlockEntity::new, PlantTechConstants.MACHINETIER_MEGAFURNACE));

    @BlockRenderLayer
    public static RegistryObject<Block> MEGAFURNACE_GROWING = BLOCKS.register("mega_furnace_growing",
            () -> new FacingGrowingBlock(ModBlocks.MEGAFURNACE, true));

//    @BlockRenderLayer(layer = RenderLayer.TRANSLUCENT)
//    @WithItem(isWIP = true)
//    public static RegistryObject<Block> MUSHROOM_CYAN = BLOCKS.register("mushroom_cyan",
//            () -> new ShapedBlock(BlockBehaviour.Properties.of(Material.CLAY).lightLevel((state) -> 10).noOcclusion().noCollission().instabreak().sound(SoundType.SLIME_BLOCK), Block.box(4, 0, 4, 12, 9, 12)));
//
//    @BlockRenderLayer(layer = RenderLayer.TRANSLUCENT)
//    @WithItem(isWIP = true)
//    public static RegistryObject<Block> MUSHROOM_PINK = BLOCKS.register("mushroom_pink",
//            () -> new ShapedBlock(BlockBehaviour.Properties.of(Material.CLAY).lightLevel((state) -> 10).noOcclusion().noCollission().instabreak().sound(SoundType.SLIME_BLOCK), Block.box(5, 0, 5, 11, 12, 11)));
//
//    @BlockRenderLayer(layer = RenderLayer.TRANSLUCENT)
//    @WithItem(isWIP = true)
//    public static RegistryObject<Block> MUSHROOM_PURPLE = BLOCKS.register("mushroom_purple",
//            () -> new ShapedBlock(BlockBehaviour.Properties.of(Material.CLAY).lightLevel((state) -> 10).noOcclusion().noCollission().instabreak().sound(SoundType.SLIME_BLOCK), Block.box(5, 0, 5, 11, 6, 11)));
//
//    @BlockRenderLayer(layer = RenderLayer.TRANSLUCENT)
//    @WithItem(isWIP = true)
//    public static RegistryObject<Block> MUSHROOM_YELLOW = BLOCKS.register("mushroom_yellow",
//            () -> new ShapedBlock(BlockBehaviour.Properties.of(Material.CLAY).lightLevel((state) -> 10).noOcclusion().noCollission().instabreak().sound(SoundType.SLIME_BLOCK), Block.box(4, 0, 4, 12, 16, 12)));

//    @BlockRenderLayer
//    @WithItem
//    public static RegistryObject<Block> MUTATED_ALLIUM = BLOCKS.register("mutated_allium",
//            () -> new ObtainableNaturalPlants());
//
//    @BlockRenderLayer
//    @WithItem
//    public static RegistryObject<Block> MUTATED_AZURE_BLUET = BLOCKS.register("mutated_azure_bluet",
//            () -> new ObtainableNaturalPlants());
//
//    @BlockRenderLayer
//    @WithItem
//    public static RegistryObject<Block> MUTATED_BLUE_ORCHID = BLOCKS.register("mutated_blue_orchid",
//            () -> new ObtainableNaturalPlants());
//
//    @BlockRenderLayer
//    @WithItem
//    public static RegistryObject<Block> MUTATED_CORNFLOWER = BLOCKS.register("mutated_cornflower",
//            () -> new ObtainableNaturalPlants());
//
//    @BlockRenderLayer
//    @WithItem
//    public static RegistryObject<Block> MUTATED_DANDELION = BLOCKS.register("mutated_dandelion",
//            () -> new ObtainableNaturalPlants());
//
//    @BlockRenderLayer
//    @WithItem
//    public static RegistryObject<Block> MUTATED_LILAC = BLOCKS.register("mutated_lilac",
//            () -> new ObtainableTallBushBlock());
//
//    @BlockRenderLayer
//    @WithItem
//    public static RegistryObject<Block> MUTATED_LILY_OF_THE_VALLEY = BLOCKS.register("mutated_lily_of_the_valley",
//            () -> new ObtainableNaturalPlants());
//
//    @BlockRenderLayer
//    @WithItem
//    public static RegistryObject<Block> MUTATED_ORANGE_TULIP = BLOCKS.register("mutated_orange_tulip",
//            () -> new ObtainableNaturalPlants());
//
//    @BlockRenderLayer
//    @WithItem
//    public static RegistryObject<Block> MUTATED_OXEYE_DAISY = BLOCKS.register("mutated_oxeye_daisy",
//            () -> new ObtainableNaturalPlants());
//
//    @BlockRenderLayer
//    @WithItem
//    public static RegistryObject<Block> MUTATED_PEONY = BLOCKS.register("mutated_peony",
//            () -> new ObtainableTallBushBlock());
//
//    @BlockRenderLayer
//    @WithItem
//    public static RegistryObject<Block> MUTATED_PINK_TULIP = BLOCKS.register("mutated_pink_tulip",
//            () -> new ObtainableNaturalPlants());
//
//    @BlockRenderLayer
//    @WithItem
//    public static RegistryObject<Block> MUTATED_POPPY = BLOCKS.register("mutated_poppy",
//            () -> new ObtainableNaturalPlants());
//
//    @BlockRenderLayer
//    @WithItem
//    public static RegistryObject<Block> MUTATED_RED_TULIP = BLOCKS.register("mutated_red_tulip",
//            () -> new ObtainableNaturalPlants());
//
//    @BlockRenderLayer
//    @WithItem
//    public static RegistryObject<Block> MUTATED_ROSE_BUSH = BLOCKS.register("mutated_rose_bush",
//            () -> new ObtainableTallBushBlock());
//
//    @BlockRenderLayer
//    @WithItem
//    public static RegistryObject<Block> MUTATED_WHITE_TULIP = BLOCKS.register("mutated_white_tulip",
//            () -> new ObtainableNaturalPlants());

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> OAK_ACACIA_GRASS_HEDGE = registerHedge("hedge_oak_acacia_grass",
            () -> new Hedge(Blocks.OAK_LEAVES, Blocks.ACACIA_LOG, Blocks.GRASS_BLOCK));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> OAK_ACACIA_PODZOL_HEDGE = registerHedge("hedge_oak_acacia_podzol",
            () -> new Hedge(Blocks.OAK_LEAVES, Blocks.ACACIA_LOG, Blocks.PODZOL));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> OAK_ACACIADIRT_HEDGE = registerHedge("hedge_oak_acacia_dirt",
            () -> new Hedge(Blocks.OAK_LEAVES, Blocks.ACACIA_LOG, Blocks.DIRT));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> OAK_BIRCH_DIRT_HEDGE = registerHedge("hedge_oak_birch_dirt",
            () -> new Hedge(Blocks.OAK_LEAVES, Blocks.BIRCH_LOG, Blocks.DIRT));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> OAK_BIRCH_GRASS_HEDGE = registerHedge("hedge_oak_birch_grass",
            () -> new Hedge(Blocks.OAK_LEAVES, Blocks.BIRCH_LOG, Blocks.GRASS_BLOCK));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> OAK_BIRCH_PODZOL_HEDGE = registerHedge("hedge_oak_birch_podzol",
            () -> new Hedge(Blocks.OAK_LEAVES, Blocks.BIRCH_LOG, Blocks.PODZOL));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> OAK_DARK_OAK_DIRT_HEDGE = registerHedge("hedge_oak_dark_oak_dirt",
            () -> new Hedge(Blocks.OAK_LEAVES, Blocks.DARK_OAK_LOG, Blocks.DIRT));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> OAK_DARK_OAK_GRASS_HEDGE = registerHedge("hedge_oak_dark_oak_grass",
            () -> new Hedge(Blocks.OAK_LEAVES, Blocks.DARK_OAK_LOG, Blocks.GRASS_BLOCK));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> OAK_DARK_OAK_PODZOL_HEDGE = registerHedge("hedge_oak_dark_oak_podzol",
            () -> new Hedge(Blocks.OAK_LEAVES, Blocks.DARK_OAK_LOG, Blocks.PODZOL));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> OAK_JUNGLE_GRASS_HEDGE = registerHedge("hedge_oak_jungle_grass",
            () -> new Hedge(Blocks.OAK_LEAVES, Blocks.JUNGLE_LOG, Blocks.GRASS_BLOCK));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> OAK_JUNGLE_PODZOL_HEDGE = registerHedge("hedge_oak_jungle_podzol",
            () -> new Hedge(Blocks.OAK_LEAVES, Blocks.JUNGLE_LOG, Blocks.PODZOL));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> OAK_JUNGLEDIRT_HEDGE = registerHedge("hedge_oak_jungle_dirt",
            () -> new Hedge(Blocks.OAK_LEAVES, Blocks.JUNGLE_LOG, Blocks.DIRT));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> OAK_OAK_DIRT_HEDGE = registerHedge("hedge_oak_oak_dirt",
            () -> new Hedge(Blocks.OAK_LEAVES, Blocks.OAK_LOG, Blocks.DIRT));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> OAK_OAK_GRASS_HEDGE = registerHedge("hedge_oak_oak_grass",
            () -> new Hedge(Blocks.OAK_LEAVES, Blocks.OAK_LOG, Blocks.GRASS_BLOCK));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> OAK_OAK_PODZOL_HEDGE = registerHedge("hedge_oak_oak_podzol",
            () -> new Hedge(Blocks.OAK_LEAVES, Blocks.OAK_LOG, Blocks.PODZOL));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> OAK_SPRUCE_DIRT_HEDGE = registerHedge("hedge_oak_spruce_dirt",
            () -> new Hedge(Blocks.OAK_LEAVES, Blocks.SPRUCE_LOG, Blocks.DIRT));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> OAK_SPRUCE_GRASS_HEDGE = registerHedge("hedge_oak_spruce_grass",
            () -> new Hedge(Blocks.OAK_LEAVES, Blocks.SPRUCE_LOG, Blocks.GRASS_BLOCK));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> OAK_SPRUCE_PODZOL_HEDGE = registerHedge("hedge_oak_spruce_podzol",
            () -> new Hedge(Blocks.OAK_LEAVES, Blocks.SPRUCE_LOG, Blocks.PODZOL));

    @BlockRenderLayer
    @WithItem(tab = Tab.MACHINES)
    public static RegistryObject<Block> PLANTFARM = BLOCKS.register("plantfarm",
            () -> new MachineBaseBlock(PlantFarmBlockEntity::new, PlantTechConstants.MACHINETIER_PLANTFARM));

    @BlockRenderLayer
    public static RegistryObject<Block> PLANTFARM_GROWING = BLOCKS.register("plantfarm_growing",
            () -> new GrowingBlock(ModBlocks.PLANTFARM, true));

    @WithItem
    public static RegistryObject<Block> PLANTIUM_BLOCK = BLOCKS.register("plantium_block",
            () -> new Block(BlockBehaviour.Properties.of().sound(SoundType.METAL).strength(0.9F)));

    @WithItem()
    public static RegistryObject<Block> RED_MUSHROOM_CANDLE = BLOCKS.register("red_mushroom_candle",
            () -> new CandleBlock(BlockBehaviour.Properties.of().sound(SoundType.CANDLE).mapColor(MapColor.COLOR_RED).noOcclusion().strength(0.1F).sound(SoundType.CANDLE).lightLevel(CandleBlock.LIGHT_EMISSION)));

    @WithItem()
    public static RegistryObject<Block> RED_MUSHROOM_FENCE = BLOCKS.register("red_mushroom_fence",
            () -> new FenceBlock(BlockBehaviour.Properties.of().sound(SoundType.WOOD).strength(2.0F, 3.0F).sound(SoundType.WOOD)));

    @WithItem()
    public static RegistryObject<Block> RED_MUSHROOM_FENCE_GATE = BLOCKS.register("red_mushroom_fence_gate",
            () -> new FenceGateBlock(BlockBehaviour.Properties.of().sound(SoundType.WOOD).strength(2.0F, 3.0F).sound(SoundType.WOOD), WoodType.OAK));

    @WithItem()
    public static RegistryObject<Block> RED_MUSHROOM_SLAB = BLOCKS.register("red_mushroom_slab",
            () -> new SlabBlock(BlockBehaviour.Properties.of().sound(SoundType.WOOD).sound(SoundType.WOOD).strength(0.2F)));

    @WithItem()
    public static RegistryObject<Block> RED_MUSHROOM_STAIRS = BLOCKS.register("red_mushroom_stairs",
            () -> new StairBlock(Blocks.RED_MUSHROOM_BLOCK::defaultBlockState, BlockBehaviour.Properties.of().sound(SoundType.WOOD).sound(SoundType.WOOD).strength(0.2F)));

    @WithItem()
    public static RegistryObject<Block> RED_MUSHROOM_TRAPDOOR = BLOCKS.register("red_mushroom_trapdoor",
            () -> new TrapDoorBlock(BlockBehaviour.Properties.of().sound(SoundType.WOOD).strength(3.0F).sound(SoundType.WOOD).noOcclusion(), BlockSetType.OAK));

    @BlockRenderLayer
    @WithItem(tab = Tab.MACHINES)
    public static RegistryObject<Block> SEEDCONSTRUCTOR = BLOCKS.register("seedconstructor",
            () -> new MachineFacingBlock(SeedConstructorBlockEntity::new, PlantTechConstants.MACHINETIER_SEEDCONSTRUCTOR));

    @BlockRenderLayer
    public static RegistryObject<Block> SEEDCONSTRUCTOR_GROWING = BLOCKS.register("seedconstructor_growing",
            () -> new FacingGrowingBlock(ModBlocks.SEEDCONSTRUCTOR, true));

    @BlockRenderLayer
    @WithItem(tab = Tab.MACHINES)
    public static RegistryObject<Block> SEEDSQUEEZER = BLOCKS.register("seedsqueezer",
            () -> new MachineFacingBlock(SeedSqueezerBlockEntity::new, PlantTechConstants.MACHINETIER_SEEDSQUEEZER));

    @BlockRenderLayer
    public static RegistryObject<Block> SEEDSQUEEZER_GROWING = BLOCKS.register("seedsqueezer_growing",
            () -> new FacingGrowingBlock(ModBlocks.SEEDSQUEEZER, true));

    @BlockRenderLayer
    @WithItem(tab = Tab.MACHINES)
    public static RegistryObject<Block> SOLARGENERATOR = BLOCKS.register("solargenerator",
            () -> new MachineBaseBlock(SolarGeneratorBlockEntity::new, PlantTechConstants.MACHINETIER_SOLARGENERATOR));

    @BlockRenderLayer
    public static RegistryObject<Block> SOLARGENERATOR_GROWING = BLOCKS.register("solargenerator_growing",
            () -> new GrowingBlock(ModBlocks.SOLARGENERATOR, true));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> SPRUCE_ACACIA_DIRT_HEDGE = registerHedge("hedge_spruce_acacia_dirt",
            () -> new Hedge(Blocks.SPRUCE_LEAVES, Blocks.ACACIA_LOG, Blocks.DIRT));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> SPRUCE_ACACIA_GRASS_HEDGE = registerHedge("hedge_spruce_acacia_grass",
            () -> new Hedge(Blocks.SPRUCE_LEAVES, Blocks.ACACIA_LOG, Blocks.GRASS_BLOCK));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> SPRUCE_ACACIA_PODZOL_HEDGE = registerHedge("hedge_spruce_acacia_podzol",
            () -> new Hedge(Blocks.SPRUCE_LEAVES, Blocks.ACACIA_LOG, Blocks.PODZOL));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> SPRUCE_BIRCH_DIRT_HEDGE = registerHedge("hedge_spruce_birch_dirt",
            () -> new Hedge(Blocks.SPRUCE_LEAVES, Blocks.BIRCH_LOG, Blocks.DIRT));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> SPRUCE_BIRCH_GRASS_HEDGE = registerHedge("hedge_spruce_birch_grass",
            () -> new Hedge(Blocks.SPRUCE_LEAVES, Blocks.BIRCH_LOG, Blocks.GRASS_BLOCK));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> SPRUCE_BIRCH_PODZOL_HEDGE = registerHedge("hedge_spruce_birch_podzol",
            () -> new Hedge(Blocks.SPRUCE_LEAVES, Blocks.BIRCH_LOG, Blocks.PODZOL));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> SPRUCE_DARK_OAK_DIRT_HEDGE = registerHedge("hedge_spruce_dark_oak_dirt",
            () -> new Hedge(Blocks.SPRUCE_LEAVES, Blocks.DARK_OAK_LOG, Blocks.DIRT));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> SPRUCE_DARK_OAK_GRASS_HEDGE = registerHedge("hedge_spruce_dark_oak_grass",
            () -> new Hedge(Blocks.SPRUCE_LEAVES, Blocks.DARK_OAK_LOG, Blocks.GRASS_BLOCK));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> SPRUCE_DARK_OAK_PODZOL_HEDGE = registerHedge("hedge_spruce_dark_oak_podzol",
            () -> new Hedge(Blocks.SPRUCE_LEAVES, Blocks.DARK_OAK_LOG, Blocks.PODZOL));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> SPRUCE_JUNGLE_DIRT_HEDGE = registerHedge("hedge_spruce_jungle_dirt",
            () -> new Hedge(Blocks.SPRUCE_LEAVES, Blocks.JUNGLE_LOG, Blocks.DIRT));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> SPRUCE_JUNGLE_GRASS_HEDGE = registerHedge("hedge_spruce_jungle_grass",
            () -> new Hedge(Blocks.SPRUCE_LEAVES, Blocks.JUNGLE_LOG, Blocks.GRASS_BLOCK));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> SPRUCE_JUNGLE_PODZOL_HEDGE = registerHedge("hedge_spruce_jungle_podzol",
            () -> new Hedge(Blocks.SPRUCE_LEAVES, Blocks.JUNGLE_LOG, Blocks.PODZOL));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> SPRUCE_OAK_DIRT_HEDGE = registerHedge("hedge_spruce_oak_dirt",
            () -> new Hedge(Blocks.SPRUCE_LEAVES, Blocks.OAK_LOG, Blocks.DIRT));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> SPRUCE_OAK_GRASS_HEDGE = registerHedge("hedge_spruce_oak_grass",
            () -> new Hedge(Blocks.SPRUCE_LEAVES, Blocks.OAK_LOG, Blocks.GRASS_BLOCK));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> SPRUCE_OAK_PODZOL_HEDGE = registerHedge("hedge_spruce_oak_podzol",
            () -> new Hedge(Blocks.SPRUCE_LEAVES, Blocks.OAK_LOG, Blocks.PODZOL));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> SPRUCE_SPRUCE_DIRT_HEDGE = registerHedge("hedge_spruce_spruce_dirt",
            () -> new Hedge(Blocks.SPRUCE_LEAVES, Blocks.SPRUCE_LOG, Blocks.DIRT));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> SPRUCE_SPRUCE_GRASS_HEDGE = registerHedge("hedge_spruce_spruce_grass",
            () -> new Hedge(Blocks.SPRUCE_LEAVES, Blocks.SPRUCE_LOG, Blocks.GRASS_BLOCK));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> SPRUCE_SPRUCE_PODZOL_HEDGE = registerHedge("hedge_spruce_spruce_podzol",
            () -> new Hedge(Blocks.SPRUCE_LEAVES, Blocks.SPRUCE_LOG, Blocks.PODZOL));

    @BlockRenderLayer
    @WithItem
    public static RegistryObject<Block> TEST_BLOCK = BLOCKS.register("testblock",
            () -> new Block(BlockBehaviour.Properties.of().sound(SoundType.STONE).strength(1.0F)));

    @WithItem
    public static RegistryObject<Block> UNIVERSAL_SOIL = BLOCKS.register("universal_soil",
            () -> new Block(BlockBehaviour.Properties.of().sound(SoundType.GRAVEL).mapColor(MapColor.DIRT).strength(0.5F)));

    @WithItem
    public static RegistryObject<Block> UNIVERSAL_SOIL_INFUSED = BLOCKS.register("universal_soil_infused",
            () -> new Block(BlockBehaviour.Properties.of().sound(SoundType.GRAVEL).mapColor(MapColor.DIRT).strength(0.7F)));

//    @WithItem
//    public static RegistryObject<Block> WALL_LIGHT = BLOCKS.register("wall_light",
//            () -> new WallLight(BlockBehaviour.Properties.of(Material.STONE).sound(SoundType.GLASS)));

    @WithItem()
    public static RegistryObject<Block> WARPED_HYPHAE_CANDLE = BLOCKS.register("warped_hyphae_candle",
            () -> new CandleBlock(BlockBehaviour.Properties.of().sound(SoundType.CANDLE).mapColor(MapColor.WARPED_HYPHAE).noOcclusion().strength(0.1F).sound(SoundType.CANDLE).lightLevel(CandleBlock.LIGHT_EMISSION)));

    @WithItem()
    public static RegistryObject<Block> WARPED_HYPHAE_FENCE = BLOCKS.register("warped_hyphae_fence",
            () -> new FenceBlock(BlockBehaviour.Properties.of().sound(SoundType.WOOD).mapColor(MapColor.WARPED_HYPHAE).strength(2.0F, 3.0F).sound(SoundType.WOOD)));

    @WithItem()
    public static RegistryObject<Block> WARPED_HYPHAE_FENCE_GATE = BLOCKS.register("warped_hyphae_fence_gate",
            () -> new FenceGateBlock(BlockBehaviour.Properties.of().sound(SoundType.WOOD).mapColor(MapColor.WARPED_HYPHAE).strength(2.0F, 3.0F).sound(SoundType.WOOD), WoodType.OAK));

    @WithItem()
    public static RegistryObject<Block> WARPED_HYPHAE_SLAB = BLOCKS.register("warped_hyphae_slab",
            () -> new SlabBlock(BlockBehaviour.Properties.of().sound(SoundType.WOOD).mapColor(MapColor.WARPED_HYPHAE).sound(SoundType.WOOD).strength(0.2F)));

    @WithItem()
    public static RegistryObject<Block> WARPED_HYPHAE_STAIRS = BLOCKS.register("warped_hyphae_stairs",
            () -> new StairBlock(Blocks.WARPED_HYPHAE::defaultBlockState, BlockBehaviour.Properties.of().sound(SoundType.WOOD).mapColor(MapColor.WARPED_HYPHAE).sound(SoundType.WOOD).strength(0.2F)));

    @WithItem()
    public static RegistryObject<Block> WARPED_HYPHAE_TRAPDOOR = BLOCKS.register("warped_hyphae_trapdoor",
            () -> new TrapDoorBlock(BlockBehaviour.Properties.of().sound(SoundType.WOOD).mapColor(MapColor.WARPED_HYPHAE).strength(3.0F).sound(SoundType.WOOD).noOcclusion(), BlockSetType.OAK));

//    @WithItem
//    public static RegistryObject<Block> WHITE_CRYSTAL_BLOCK = BLOCKS.register("white_crystal_block",
//            () -> new Block(BlockBehaviour.Properties.of(Material.STONE).sound(SoundType.STONE).strength(0.9F)));
//
//    @WithItem
//    public static RegistryObject<Block> WHITE_CRYSTAL_BRICK = BLOCKS.register("white_crystal_brick",
//            () -> new Block(BlockBehaviour.Properties.of(Material.STONE).sound(SoundType.STONE).strength(0.9F)));
//
//    @BlockRenderLayer
//    @WithItem
//    public static RegistryObject<Block> WHITE_CRYSTAL_DOOR = BLOCKS.register("white_crystal_door",
//            () -> new DoorBlock(BlockBehaviour.Properties.of(Material.GLASS).sound(SoundType.GLASS).strength(0.2F).noOcclusion()));
//
//    @WithItem
//    public static RegistryObject<Block> WHITE_CRYSTAL_FENCE = BLOCKS.register("white_crystal_fence",
//            () -> new CustomFenceBlock(BlockBehaviour.Properties.of(Material.STONE).sound(SoundType.STONE).strength(0.9F)));
//
//    @BlockRenderLayer
//    @WithItem
//    public static RegistryObject<Block> WHITE_CRYSTAL_GLASSPANE_CROSS = BLOCKS.register("white_crystal_glasspane_cross",
//            () -> new GlassPanePillar(BlockBehaviour.Properties.of(Material.GLASS).sound(SoundType.GLASS).strength(0.2F)));
//
//    @BlockRenderLayer
//    @WithItem
//    public static RegistryObject<Block> WHITE_CRYSTAL_GLASSPANE_END = BLOCKS.register("white_crystal_glasspane_end",
//            () -> new GlassPaneEnd(BlockBehaviour.Properties.of(Material.GLASS).sound(SoundType.GLASS).strength(0.2F), Color.WHITE.getRGB()));
//
//    @BlockRenderLayer
//    @WithItem
//    public static RegistryObject<Block> WHITE_CRYSTAL_GLASSPANE_MIDDLE = BLOCKS.register("white_crystal_glasspane_middle",
//            () -> new GlassPanePillar(BlockBehaviour.Properties.of(Material.GLASS).sound(SoundType.GLASS).strength(0.2F)));
//
//    @WithItem
//    public static RegistryObject<Block> WHITE_CRYSTAL_LAMP = BLOCKS.register("white_crystal_lamp",
//            () -> new Block(BlockBehaviour.Properties.of(Material.STONE).sound(SoundType.STONE).strength(0.9F).lightLevel((p) -> {
//        return 15;
//    }).noOcclusion()));
//
//    @WithItem
//    public static RegistryObject<Block> WHITE_CRYSTAL_ORE = BLOCKS.register("white_crystal_ore",
//            () -> new BaseOreBlock(BlockBehaviour.Properties.of().sound(SoundTypes.DIRT).sound(SoundType.GRAVEL).strength(0.2F), 1, 3));
//
//    @WithItem
//    public static RegistryObject<Block> WHITE_CRYSTAL_SLAB = BLOCKS.register("white_crystal_slab",
//            () -> new SlabBlock(BlockBehaviour.Properties.of(Material.STONE).sound(SoundType.STONE).strength(0.2F)));
//
//    @WithItem
//    public static RegistryObject<Block> WHITE_CRYSTAL_STAIRS = BLOCKS.register("white_crystal_stairs",
//            () -> new StairBlock(() -> WHITE_CRYSTAL_BLOCK.get().defaultBlockState(), BlockBehaviour.Properties.of(Material.STONE).sound(SoundType.STONE).strength(0.2F)));
//
//    @WithItem
//    public static RegistryObject<Block> WHITE_CRYSTAL_TILING = BLOCKS.register("white_crystal_tiling",
//            () -> new Block(BlockBehaviour.Properties.of(Material.STONE).sound(SoundType.STONE).strength(0.9F)));


    public static void registerCrops()
    {
        for (CropTypes entry : CropTypes.crops())
            entry.setCropBlock(BLOCKS.register(entry.getName() + "_crop", () -> new CropBaseBlock(entry)));
    }

    static RegistryObject<Block> registerHedge(String registryName, Supplier<Block> block) {
        RegistryObject<Block> registryObject = BLOCKS.register(registryName, block);
        HEDGE_BLOCKS.add(registryObject);
        return registryObject;
    }

    public static void registerItemBlocks() {
        try {
            List<Field> list = Arrays.stream(ModBlocks.class.getDeclaredFields()).filter(f -> f.isAnnotationPresent(WithItem.class)).collect(Collectors.toList());
            for (Field field : list) {
                WithItem annotation = field.getAnnotation(WithItem.class);
                RegistryObject<Block> registryObject = (RegistryObject<Block>) field.get(RegistryObject.class);
                ModCreativeTabs.putItem(annotation.tab().getTab(), ModItems.ITEMS.register(registryObject.getId().getPath(), () -> new BlockItem(registryObject.get(), new Item.Properties()) {
                    @Override
                    public void appendHoverText(ItemStack stack, @Nullable Level level, List<Component> tips, TooltipFlag advanced) {
                        if (annotation.isWIP())
                            tips.add(Component.literal("WIP object, may not be obtainable through normal ways.").withStyle(ChatFormatting.RED));
                    }
                }));
            }
        } catch (IllegalAccessException e) {
            System.err.println(e.getMessage());
        }
    }
}
