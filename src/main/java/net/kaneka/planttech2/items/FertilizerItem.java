package net.kaneka.planttech2.items;

import net.kaneka.planttech2.blocks.CropBaseBlock;
import net.kaneka.planttech2.blocks.entity.CropsBlockEntity;
import net.kaneka.planttech2.registries.ModItems;
import net.kaneka.planttech2.utilities.ModCreativeTabs;
import net.minecraft.core.BlockPos;
import net.minecraft.core.BlockSource;
import net.minecraft.core.dispenser.OptionalDispenseItemBehavior;
import net.minecraft.network.chat.Component;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.DispenserBlock;
import net.minecraftforge.registries.RegistryObject;

import java.util.List;
import java.util.Random;

public class FertilizerItem extends Item
{
    public FertilizerItem(RegistryObject<CreativeModeTab> group)
    {
		super(new Item.Properties());
		DispenserBlock.registerBehavior(this, new OptionalDispenseItemBehavior()
		{
			@Override
			protected ItemStack execute(BlockSource source, ItemStack stack)
			{
				Level level = source.getLevel();
				BlockPos target = source.getPos().relative(source.getBlockState().getValue(DispenserBlock.FACING));
				this.setSuccess(applyFertillizer(level, target, stack));
				if (!level.isClientSide() && this.isSuccess())
					level.levelEvent(2005, target, 0);
				return stack;
			}
		});
		ModCreativeTabs.putItem(group, () -> this);
    }

    @Override
    public InteractionResult useOn(UseOnContext context)
    {
		ItemStack stack = context.getItemInHand();
		BlockPos pos = context.getClickedPos();
		Level level = context.getLevel();
		if (!level.isClientSide())
			if (applyFertillizer(level, pos, stack))
			{
				if (context.getPlayer() != null && !context.getPlayer().getAbilities().instabuild)
					stack.shrink(1);
				return InteractionResult.CONSUME;
			}
		return super.useOn(context);
    }

    public static float getIncreaseChance(Item item)
    {
		if (item == ModItems.FERTILIZER_TIER_1.get())
			return 0.05F;
		else if (item == ModItems.FERTILIZER_TIER_2.get())
			return 0.15F;
		else if (item == ModItems.FERTILIZER_TIER_3.get())
			return 0.35F;
		else if (item == ModItems.FERTILIZER_TIER_4.get())
			return 0.75F;
		else if (item == ModItems.FERTILIZER_CREATIVE.get())
			return 1F;
		return 0F;
    }

    @Override
    public void appendHoverText(ItemStack stack, Level level, List<Component> tooltip, TooltipFlag flagIn)
    {
		tooltip.add(Component.translatable("planttech2.info.fertilizer").append(": " + (int) (getIncreaseChance(stack.getItem()) * 100) + "%"));
		if (stack.getItem() == ModItems.FERTILIZER_CREATIVE.get())
			tooltip.add(Component.translatable("planttech2.info.fertilizer_creative"));
    }

	private static final Random RANDOM = new Random();
	public static boolean applyFertillizer(Level world, BlockPos pos, ItemStack stack)
	{
		if (world.getBlockState(pos).getBlock() instanceof CropBaseBlock cbb)
		{
			if (RANDOM.nextFloat() < getIncreaseChance(stack.getItem()))
			{
				if (stack.getItem() != ModItems.FERTILIZER_CREATIVE.get())
				{
					if (world.getBlockEntity(pos) instanceof CropsBlockEntity cbe)
						cbb.updateCrop(world, pos);
				}
				else
					cbb.updateCreative(world, pos);
			}
			return true;
		}
		return false;
	}

}
