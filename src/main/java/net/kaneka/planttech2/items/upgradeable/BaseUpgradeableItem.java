package net.kaneka.planttech2.items.upgradeable;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import net.kaneka.planttech2.inventory.ItemUpgradeableMenu;
import net.kaneka.planttech2.utilities.NBTHelper;
import net.kaneka.planttech2.utilities.PTClientUtil;
import net.kaneka.planttech2.utilities.PTCommonUtil;
import net.kaneka.planttech2.utilities.PlantTechConstants;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.attributes.Attribute;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.enchantment.Enchantment;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.capabilities.ICapabilityProvider;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class BaseUpgradeableItem extends Item implements IUpgradeable
{
	private final UpgradeChipItem.RestrictionTypes restrictionType;
	private final int basecapacity, maxInvSize;
	private final float baseAttack, baseAttackSpeed;
	
	public BaseUpgradeableItem(Properties property, int basecapacity, int maxInvSize, float baseAttack, float baseAttackSpeed, UpgradeChipItem.RestrictionTypes restrictionType)
	{
		super(property.stacksTo(1));
		this.basecapacity = basecapacity;
		this.maxInvSize = maxInvSize; 
		this.baseAttack = baseAttack; 
		this.baseAttackSpeed = baseAttackSpeed; 
		this.restrictionType = restrictionType;
	}

	@Override
	public ICapabilityProvider initCapabilities(ItemStack stack, CompoundTag nbt)
	{
		return new InventoryEnergyProvider(basecapacity, maxInvSize);
	}

	@Nullable
	@Override
	public CompoundTag getShareTag(ItemStack stack)
	{
		CompoundTag compound = super.getShareTag(stack);
		if (compound != null)
			PTCommonUtil.writeEnergySharedTag(stack, compound);
		return compound;
	}

	@Override
	public void readShareTag(ItemStack stack, @Nullable CompoundTag nbt)
	{
		if (nbt != null)
			PTCommonUtil.readEnergySharedTag(stack, nbt);
		super.readShareTag(stack, nbt);
	}

	@Override
	public boolean hurtEnemy(ItemStack stack, LivingEntity target, LivingEntity attacker)
	{
		if (!((Player) attacker).getAbilities().instabuild)
			PTCommonUtil.tryAccessEnergy(stack, (cap) -> cap.extractEnergy(getEnergyCost(stack), false));
		return true;
	}

	@Override
	public boolean mineBlock(ItemStack stack, Level level, BlockState state, BlockPos pos, LivingEntity entityLiving)
	{
		if (!level.isClientSide && state.getDestroySpeed(level, pos) != 0.0F && !((Player) entityLiving).getAbilities().instabuild)
			PTCommonUtil.tryAccessEnergy(stack, (cap) -> cap.extractEnergy(getEnergyCost(stack), false));
		return true;
	}

	protected static int getEnergyCost(ItemStack stack)
	{
		return 20 + NBTHelper.getInt(stack, "energycost", 0);
	}

	private double getAttackSpeed(ItemStack stack)
	{
		return Math.min(baseAttackSpeed + NBTHelper.getFloat(stack, "attackspeed", 0), -0.4D);
	}

	private double getAttackDamage(ItemStack stack)
	{ 
		return Math.min(baseAttack + NBTHelper.getFloat(stack, "attack", 0), UpgradeChipItem.getAttackMax());
	}

	@Override
	public Multimap<Attribute, AttributeModifier> getAttributeModifiers(EquipmentSlot equipmentSlot, ItemStack stack)
	{
		Multimap<Attribute, AttributeModifier> multimap = HashMultimap.create();
		if (equipmentSlot == EquipmentSlot.MAINHAND)
		{
			multimap.put(Attributes.ATTACK_DAMAGE, new AttributeModifier(BASE_ATTACK_DAMAGE_UUID, "Tool modifier", getAttackDamage(stack), AttributeModifier.Operation.ADDITION));
			multimap.put(Attributes.ATTACK_SPEED, new AttributeModifier(BASE_ATTACK_SPEED_UUID, "Tool modifier", getAttackSpeed(stack), AttributeModifier.Operation.ADDITION));
		}
		return multimap;
	}

	@Override
	public void appendHoverText(ItemStack stack, Level level, List<Component> tooltip, TooltipFlag flagIn)
	{
		PTClientUtil.appendEnergyTooltip(stack, tooltip);
		tooltip.add(Component.translatable("planttech2.info.energycosts", getEnergyCost(stack)));
		tooltip.add(Component.translatable("planttech2.info.openwithshift"));
	}

	@Override
	public boolean isBarVisible(ItemStack stack)
	{
		return getBarWidth(stack) < 13;
	}

	@Override
	@Deprecated
	public int getBarWidth(ItemStack stack)
	{
		return PTClientUtil.getBarWidthForEnergyItem(stack);
	}

	@Override
	public int getBarColor(ItemStack stack)
	{
		return PlantTechConstants.DURABILITY_BAR_COLOUR;
	}

	@Override
	public boolean isEnchantable(ItemStack stack)
	{
		return false;
	}
	
	@Override
	public InteractionResultHolder<ItemStack> use(Level world, Player player, InteractionHand hand)
	{
		ItemStack stack = player.getItemInHand(hand);
		if (player.isCrouching())
		{
			if (!world.isClientSide && player instanceof ServerPlayer sp)
				PTCommonUtil.openItemContainer(sp, "container.upgradeableitem", stack, ItemUpgradeableMenu::new);
			return InteractionResultHolder.sidedSuccess(stack, world.isClientSide());
		}
		return new InteractionResultHolder<>(InteractionResult.FAIL, stack);
	}

	@Override
	public void updateNBTValues(ItemStack stack)
	{
		AtomicInteger increaseCapacity = new AtomicInteger();
		PTCommonUtil.tryAccessInv(stack, (inv) -> {
			int energyCost = 0, energyProduction = 0, increaseHarvestlevel = 0;
			float increaseAttack = 0, increaseAttackSpeed = 0, increaseBreakdownRate = 0; 
			boolean unlockShovelFeat = false, unlockAxeFeat = false, unlockHoeFeat = false, unlockShearsFeat = false; 
			HashMap<Enchantment, Integer> enchantments = new HashMap<Enchantment, Integer>();
			
			ItemStack stackInSlot;
			for (int i = 0; i < inv.getSlots(); i++)
			{
				stackInSlot = inv.getStackInSlot(i);
				if (!stackInSlot.isEmpty())
				{
					if (stackInSlot.getItem() instanceof UpgradeChipItem item)
					{
						energyCost += item.getEnergyCost();
						increaseCapacity.addAndGet(item.getIncreaseCapacity());
						energyProduction += item.getEnergyProduction();
						increaseHarvestlevel += item.getIncreaseHarvestlevel();
						increaseAttack += item.getIncreaseAttack();
						increaseAttackSpeed += item.getIncreaseAttackSpeed();
						increaseBreakdownRate += item.getIncreaseBreakdownRate();
						if (item.isUnlockShovelFeat()) unlockShovelFeat = true;
						if (item.isUnlockAxeFeat()) unlockAxeFeat = true;
						if (item.isUnlockHoeFeat()) unlockHoeFeat = true;
						if (item.isUnlockShearsFeat()) unlockShearsFeat = true;
						Enchantment ench = item.getEnchantment();
						if (item.isAllowed(restrictionType))
						{
    						if (ench != null)
    						{
    							if (enchantments.containsKey(ench))
    							{
    								int nextlevel = enchantments.get(ench) + 1; 
    								enchantments.put(ench, nextlevel); 
    							}
    							else
    								enchantments.put(ench, 1);
    						}
						}
					}
				}
			}
			stack.getEnchantmentTags().clear();
			for (Enchantment ench: enchantments.keySet())
			{
				int level = enchantments.get(ench);
				stack.enchant(ench, Math.min(ench.getMaxLevel(), level));
			}

			CompoundTag nbt = stack.getOrCreateTag();
			nbt.putInt("energycost", energyCost);
			nbt.putInt("energyproduction", energyProduction);
			nbt.putInt("harvestlevel", increaseHarvestlevel);
			nbt.putFloat("attack", increaseAttack);
			nbt.putFloat("attackspeed", increaseAttackSpeed);
			nbt.putFloat("breakdownrate", 0.5F + increaseBreakdownRate);
			nbt.putBoolean("unlockshovel", unlockShovelFeat);
			nbt.putBoolean("unlockaxe", unlockAxeFeat);
			nbt.putBoolean("unlockhoe", unlockHoeFeat);
			nbt.putBoolean("unlockshears", unlockShearsFeat);
		});
		PTCommonUtil.tryAccessEnergyStorage(stack, (storage) -> storage.setEnergyMaxStored(basecapacity + increaseCapacity.get()));
	}
	
	@Override
	public void inventoryTick(ItemStack stack, Level world, Entity entityIn, int itemSlot, boolean isSelected)
	{
		if (!world.isClientSide)
			if (!stack.isEmpty())
				if (world.getGameTime() % 200L == 0)
					PTCommonUtil.tryAccessEnergy(stack, (cap) -> cap.receiveEnergy(NBTHelper.getInt(stack, "energyproduction", 0), false));
	}
}
