package net.kaneka.planttech2.items.upgradeable;

import net.kaneka.planttech2.utilities.ModCreativeTabs;
import net.kaneka.planttech2.utilities.NBTHelper;
import net.kaneka.planttech2.utilities.PTCommonUtil;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.RandomSource;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.ShovelItem;
import net.minecraft.world.item.Tier;
import net.minecraft.world.item.Tiers;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.item.enchantment.EnchantmentHelper;
import net.minecraft.world.item.enchantment.Enchantments;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.RotatedPillarBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.IForgeShearable;
import net.minecraftforge.common.TierSortingRegistry;
import net.minecraftforge.common.ToolActions;

import java.util.List;

public class MultitoolItem extends BaseUpgradeableItem
{
	public MultitoolItem()
	{
		super(new Item.Properties(), 10000, 10, 2F, 2.4F, UpgradeChipItem.RestrictionTypes.TOOL);
		ModCreativeTabs.putItem(ModCreativeTabs.TOOLS_AND_ARMOR, () -> this);
	}

	@Override
	public boolean isCorrectToolForDrops(ItemStack stack, BlockState state)
	{
		if (PTCommonUtil.tryAccessEnergyValue(stack, (cap) -> (float) cap.getEnergyStored(), 0.0F) >= getEnergyCost(stack))
		{
			if (state.is(BlockTags.MINEABLE_WITH_AXE) || state.is(BlockTags.MINEABLE_WITH_PICKAXE) || state.is(BlockTags.MINEABLE_WITH_SHOVEL))
			{
				int i = getHarvestLevel(stack);
				Tier tier = Tiers.WOOD;
				if(i == 1) tier = Tiers.STONE;
				if(i == 2) tier = Tiers.IRON;
				if(i == 3) tier = Tiers.GOLD;
				if(i == 4) tier = Tiers.DIAMOND;
				if(i >= 5) tier = Tiers.NETHERITE;
				return TierSortingRegistry.isCorrectTierForDrops(tier, state);
			}
			return state.getBlock() == Blocks.COBWEB;
		}
		return false;
	}

	@Override
	public float getDestroySpeed(ItemStack stack, BlockState state)
	{
		if (PTCommonUtil.tryAccessEnergyValue(stack, (cap) -> (float) cap.getEnergyStored(), 0.0F) >= getEnergyCost(stack))
		{
			if (state.is(BlockTags.MINEABLE_WITH_AXE) || state.is(BlockTags.MINEABLE_WITH_PICKAXE) || state.is(BlockTags.MINEABLE_WITH_SHOVEL))
				return getEfficiency(stack);
			if (state.getBlock() == Blocks.COBWEB)
				return 15.0F;
		}
		return super.getDestroySpeed(stack, state);
	}

	@Override
	public InteractionResult useOn(UseOnContext ctx)
	{
		ItemStack stack = ctx.getItemInHand();
		if (PTCommonUtil.tryAccessEnergyValue(stack, (cap) -> (float) cap.getEnergyStored(), 0.0F) >= getEnergyCost(stack))
		{
			Level level = ctx.getLevel();
			BlockPos blockpos = ctx.getClickedPos();
			BlockState state = level.getBlockState(blockpos);
			BlockState state_for_spade = ShovelItem.getShovelPathingState(state);
			BlockState state_for_hoe = level.getBlockState(blockpos).getToolModifiedState(ctx, net.minecraftforge.common.ToolActions.HOE_TILL, false);
			BlockState block_for_strinping = level.getBlockState(blockpos).getToolModifiedState(ctx, ToolActions.AXE_STRIP, false);
			Player PlayerEntity = ctx.getPlayer();
			if (ctx.getClickedFace() != Direction.DOWN && level.getBlockState(blockpos.above()).isAir() && state_for_spade != null)
			{
				if (NBTHelper.getBoolean(ctx.getItemInHand(), "unlockshovel", false))
				{
					level.playSound(PlayerEntity, blockpos, SoundEvents.SHOVEL_FLATTEN, SoundSource.BLOCKS, 1.0F, 1.0F);
					if (!level.isClientSide)
					{
						level.setBlock(blockpos, state_for_spade, 11);
						if (PlayerEntity != null && !PlayerEntity.isCreative())
							PTCommonUtil.tryAccessEnergy(stack, (cap) -> cap.extractEnergy(getEnergyCost(stack), false));
					}
				}

				return InteractionResult.SUCCESS;
			}
			else if (block_for_strinping != null)
			{
				if (NBTHelper.getBoolean(ctx.getItemInHand(), "unlockaxe", false))
				{
					level.playSound(PlayerEntity, blockpos, SoundEvents.AXE_STRIP, SoundSource.BLOCKS, 1.0F, 1.0F);
					if (!level.isClientSide)
					{
						level.setBlock(blockpos, block_for_strinping.setValue(RotatedPillarBlock.AXIS, state.getValue(RotatedPillarBlock.AXIS)), 11);
						if (PlayerEntity != null && !PlayerEntity.isCreative())
							PTCommonUtil.tryAccessEnergy(stack, (cap) -> cap.extractEnergy(getEnergyCost(stack), false));
					}
				}
				return InteractionResult.SUCCESS;
			}
			else if (ctx.getClickedFace() != Direction.DOWN && level.isEmptyBlock(blockpos.above()) && state_for_hoe != null)
			{
				if (NBTHelper.getBoolean(ctx.getItemInHand(), "unlockhoe", false))
				{
					level.playSound(PlayerEntity, blockpos, SoundEvents.HOE_TILL, SoundSource.BLOCKS, 1.0F, 1.0F);
					if (!level.isClientSide)
					{
						level.setBlock(blockpos, state_for_hoe, 11);
						if (PlayerEntity != null && !PlayerEntity.isCreative())
							PTCommonUtil.tryAccessEnergy(stack, (cap) -> cap.extractEnergy(getEnergyCost(stack), false));
					}
					return InteractionResult.SUCCESS;
				}
			}
		}
		return InteractionResult.PASS;
	}

	public int getHarvestLevel(ItemStack stack)
	{
		return NBTHelper.getInt(stack, "harvestlevel", 0);
	}

	public float getEfficiency(ItemStack stack)
	{
		return Math.min(NBTHelper.getFloat(stack, "breakdownrate", 4), UpgradeChipItem.getBreakdownRateMax());
	}

	@Override
	public boolean mineBlock(ItemStack stack, Level level, BlockState state, BlockPos pos, LivingEntity entityLiving)
	{
		return super.mineBlock(stack, level, state, pos, entityLiving);
	}

	@Override
	public InteractionResult interactLivingEntity(ItemStack stack, Player player, LivingEntity entity, InteractionHand hand)
	{
		if (entity.level().isClientSide)
			return InteractionResult.PASS;
	  	if (entity instanceof IForgeShearable fs)
	  	{
	         BlockPos pos = entity.blockPosition();
	         if (fs.isShearable(stack, entity.level(), pos))
	         {
	            List<ItemStack> drops = fs.onSheared(player, stack, entity.level(), pos,
	            EnchantmentHelper.getItemEnchantmentLevel(Enchantments.BLOCK_FORTUNE, stack));
	            RandomSource rand = player.level().random;
	            drops.forEach(d -> {
					ItemEntity ent = entity.spawnAtLocation(d, 1.0F);
					if (ent != null)
						ent.setDeltaMovement(ent.getDeltaMovement().add((rand.nextFloat() - rand.nextFloat()) * 0.1F, rand.nextFloat() * 0.05F, (rand.nextFloat() - rand.nextFloat()) * 0.1F));
	            });
	            stack.hurtAndBreak(1, entity, e -> e.broadcastBreakEvent(hand));
	         }
	         return InteractionResult.SUCCESS;
	  	}
	  	return InteractionResult.PASS;
	}

}
