package net.kaneka.planttech2.items.upgradeable;


import net.kaneka.planttech2.configuration.PlantTech2Configuration;
import net.kaneka.planttech2.utilities.ModCreativeTabs;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.enchantment.Enchantment;
import net.minecraft.world.level.Level;

import java.util.Arrays;
import java.util.List;

public class UpgradeChipItem extends Item
{
	private final int energyCost, increaseCapacity, energyProduction, increaseHarvestlevel, increaseArmor;
	private final float increaseAttack, increaseAttackSpeed, increaseBreakdownRate, increaseToughness;
	private final boolean unlockShovelFeat, unlockAxeFeat, unlockHoeFeat, unlockShearFeat;
	private final Enchantment enchantment;
	private final List<RestrictionTypes> restrictions;

	public UpgradeChipItem(Properties properties)
	{
		super(new Item.Properties());
		ModCreativeTabs.putItem(ModCreativeTabs.CHIPS, () -> this);
		this.energyCost = properties.energyCost;
		this.increaseCapacity = properties.increaseCapacity;
		this.energyProduction = properties.energyProduction;
		this.increaseHarvestlevel = properties.increaseHarvestlevel;
		this.increaseArmor = properties.increaseArmor;
		this.increaseAttack = properties.increaseAttack;
		this.increaseAttackSpeed = properties.increaseAttackSpeed;
		this.increaseBreakdownRate = properties.increaseBreakdownRate;
		this.increaseToughness = properties.increaseToughness;
		this.unlockShovelFeat = properties.unlockShovelFeat;
		this.unlockAxeFeat = properties.unlockAxeFeat;
		this.unlockHoeFeat = properties.unlockHoeFeat;
		this.unlockShearFeat = properties.unlockShearFeat;
		this.enchantment = properties.enchantment;
		this.restrictions = properties.restrictions;
	}

	public int getEnergyCost()
	{
		return (int) (energyCost * PlantTech2Configuration.MULTIPLIER_ENERGYCOSTS.get());
	}

	public int getIncreaseCapacity()
	{
		return (int) (increaseCapacity * PlantTech2Configuration.MULTIPLIER_CAPACITY.get());
	}

	public static int getCapacityMax()
	{
		return (int) (50000 * PlantTech2Configuration.MULTIPLIER_CAPACITY_MAX.get());
	}

	public int getEnergyProduction()
	{
		return (int) (energyProduction * PlantTech2Configuration.MULTIPLIER_ENERGYPRODUCTION.get());
	}

	public int getIncreaseHarvestlevel()
	{
		return increaseHarvestlevel;
	}

	public float getIncreaseAttack()
	{
		return (float) (increaseAttack * PlantTech2Configuration.MULTIPLIER_ATTACK.get());
	}

	public static float getAttackMax()
	{
		return (float) (20F * PlantTech2Configuration.MULTIPLIER_ATTACK_MAX.get());
	}

	public float getIncreaseAttackSpeed()
	{
		return (float) (increaseAttackSpeed * PlantTech2Configuration.MULTIPLIER_ATTACKSPEED.get());
	}

	public static float getAttackSpeedMax()
	{
		return (float) (2F * PlantTech2Configuration.MULTIPLIER_ATTACKSPEED_MAX.get());
	}

	public float getIncreaseBreakdownRate()
	{
		return (float) (increaseBreakdownRate * PlantTech2Configuration.MULTIPLIER_BREAKDOWNRATE.get());
	}

	public static float getBreakdownRateMax()
	{
		return (float) (10F * PlantTech2Configuration.MULTIPLIER_BREAKDOWNRATE_MAX.get());
	}

	public int getIncreaseArmor()
	{
		return (int) (increaseArmor * PlantTech2Configuration.MULTIPLIER_ARMOR.get());
	}

	public float getIncreaseToughness()
	{
		return (float) (increaseToughness * PlantTech2Configuration.MULTIPLIER_THOUGHNESS.get());
	}

	public static float getToughnessMax()
	{
		return (float) (6 * PlantTech2Configuration.MULTIPLIER_THOUGHNESS_MAX.get());
	}

	public boolean isUnlockShovelFeat()
	{
		return unlockShovelFeat;
	}

	public boolean isUnlockAxeFeat()
	{
		return unlockAxeFeat;
	}

	public boolean isUnlockHoeFeat()
	{
		return unlockHoeFeat;
	}

	public boolean isUnlockShearsFeat()
	{
		return unlockShearFeat;
	}

	public Enchantment getEnchantment()
	{
		return enchantment;
	}

	public boolean isAllowed(int index)
	{
		return restrictions.contains(RestrictionTypes.values()[index]);
	}

	public boolean isAllowed(RestrictionTypes type)
	{
		return restrictions.contains(type);
	}

	@Override
	public void appendHoverText(ItemStack stack, Level level, List<Component> tooltip, TooltipFlag flagIn)
	{
		if (increaseCapacity > 0)
		{
			tooltip.add(Component.translatable("planttech2.info.upgradechip.increasecapacity", getIncreaseCapacity()));
			tooltip.add(Component.translatable("planttech2.info.upgradechip.increasecapacitymax", getCapacityMax()));
		}
		if (energyProduction > 0)
			tooltip.add(Component.translatable("planttech2.info.upgradechip.energyproduction", getEnergyProduction()));
		if (increaseHarvestlevel > 0)
			tooltip.add(Component.translatable("planttech2.info.upgradechip.increaseharvestlevel", getIncreaseHarvestlevel()));
		if (increaseArmor > 0)
			tooltip.add(Component.translatable("planttech2.info.upgradechip.increasearmor", getIncreaseArmor()));
		if (increaseAttack > 0)
		{
			tooltip.add(Component.translatable("planttech2.info.upgradechip.increaseattack", getIncreaseAttack()));
			tooltip.add(Component.translatable("planttech2.info.upgradechip.increaseattackmax", getAttackMax()));
		}
		
		if (increaseAttackSpeed > 0)
		{
			tooltip.add(Component.translatable("planttech2.info.upgradechip.increaseattackspeed", getIncreaseAttackSpeed()));
			tooltip.add(Component.translatable("planttech2.info.upgradechip.increaseattackspeedmax", getAttackSpeedMax()));
		}
		
		if (increaseBreakdownRate > 0)
		{
			tooltip.add(Component.translatable("planttech2.info.upgradechip.increasebreakdownrate", getIncreaseBreakdownRate()));
			tooltip.add(Component.translatable("planttech2.info.upgradechip.increasebreakdownratemax", getBreakdownRateMax()));
		}
		
		if (increaseToughness > 0)
		{
			tooltip.add(Component.translatable("planttech2.info.upgradechip.increasetougness", getIncreaseToughness()));
			tooltip.add(Component.translatable("planttech2.info.upgradechip.increasetougnessmax", getToughnessMax()));
		}
		
		if (unlockAxeFeat)
			tooltip.add(Component.translatable("planttech2.info.upgradechip.unlockaxefeat"));
		if (unlockShovelFeat)
			tooltip.add(Component.translatable("planttech2.info.upgradechip.unlockshovelfeat"));
		if (unlockShearFeat)
			tooltip.add(Component.translatable("planttech2.info.upgradechip.unlockshearsfeat"));
		if (unlockHoeFeat)
			tooltip.add(Component.translatable("planttech2.info.upgradechip.unlockhoefeat"));

		if (enchantment != null)
		{
			tooltip.add(Component.translatable("planttech2.info.upgradechip.add").append(" ").append(Component.translatable(enchantment.getDescriptionId())));
			tooltip.add(Component.translatable("planttech2.info.upgradechip.stackable"));
		}
		
		if (energyCost > 0)
			tooltip.add(Component.translatable("planttech2.info.upgradechip.energycosts", getEnergyCost()));
	}

	public static class Properties
	{
		private int energyCost = 0, increaseCapacity = 0, energyProduction = 0, increaseHarvestlevel = 0, increaseArmor = 0;
		private float increaseAttack = 0, increaseAttackSpeed = 0, increaseBreakdownRate = 0, increaseToughness = 0;
		private boolean unlockShovelFeat = false, unlockAxeFeat = false, unlockHoeFeat = false, unlockShearFeat = false;
		private Enchantment enchantment;
		private final List<RestrictionTypes> restrictions;

		public Properties(RestrictionTypes... restrictions)
		{
			this.restrictions = Arrays.stream(restrictions).toList();
		}

		public static Properties of(RestrictionTypes... restrictions)
		{
			return new Properties(restrictions);
		}

		public Properties setEnergyCost(int energyCost)
		{
			this.energyCost = energyCost;
			return this;
		}

		public Properties setIncreaseCapacity(int increaseCapacity)
		{
			this.increaseCapacity = increaseCapacity;
			return this;
		}

		public Properties setEnergyProduction(int energyProduction)
		{
			this.energyProduction = energyProduction;
			return this;
		}

		public Properties setIncreaseHarvestlevel(int increaseHarvestlevel)
		{
			this.increaseHarvestlevel = increaseHarvestlevel;
			return this;
		}

		public Properties setIncreaseAttack(float increaseAttack)
		{
			this.increaseAttack = increaseAttack;
			return this;
		}

		public Properties setIncreaseAttackSpeed(float increaseAttackSpeed)
		{
			this.increaseAttackSpeed = increaseAttackSpeed;
			return this;
		}

		public Properties setIncreaseBreakdownRate(float increaseBreakdownRate)
		{
			this.increaseBreakdownRate = increaseBreakdownRate;
			return this;
		}

		public Properties setIncreaseArmor(int increaseArmor)
		{
			this.increaseArmor = increaseArmor;
			return this;
		}

		public Properties setIncreaseToughness(float increaseToughness)
		{
			this.increaseToughness = increaseToughness;
			return this;
		}

		public Properties setUnlockShovelFeat()
		{
			this.unlockShovelFeat = true;
			return this;
		}

		public Properties setUnlockAxeFeat()
		{
			this.unlockAxeFeat = true;
			return this;
		}

		public Properties setUnlockHoeFeat()
		{
			this.unlockHoeFeat = true;
			return this;
		}

		public Properties setUnlockShearsFeat()
		{
			this.unlockShearFeat = true;
			return this;
		}

		public Properties setEnchantment(Enchantment ench)
		{
			this.enchantment = ench;
			return this;
		}
	}

	public enum RestrictionTypes
	{
		HELMET,
		CHEST,
		LEGGINGS,
		BOOTS,
		RANGED_WEAPON,
		MELEE_WEAPON,
		TOOL;
	}
}
