package net.kaneka.planttech2.items.upgradeable;

import net.minecraft.world.item.ItemStack;

//Listener for UIs
public interface IUpgradeable
{
	void updateNBTValues(ItemStack stack);
}
