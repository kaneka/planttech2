package net.kaneka.planttech2.items.upgradeable;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import net.kaneka.planttech2.inventory.ItemUpgradeableMenu;
import net.kaneka.planttech2.items.armors.ArmorBaseItem;
import net.kaneka.planttech2.items.armors.CustomArmorMaterial;
import net.kaneka.planttech2.utilities.ModCreativeTabs;
import net.kaneka.planttech2.utilities.NBTHelper;
import net.kaneka.planttech2.utilities.PTClientUtil;
import net.kaneka.planttech2.utilities.PTCommonUtil;
import net.kaneka.planttech2.utilities.PlantTechConstants;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.ai.attributes.Attribute;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.enchantment.Enchantment;
import net.minecraft.world.level.Level;
import net.minecraftforge.common.capabilities.ICapabilityProvider;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

public class UpgradeableArmorItem extends ArmorBaseItem implements IUpgradeable
{
	private static final UUID[] ARMOR_MODIFIERS = new UUID[] { UUID.fromString("845DB27C-C624-495F-8C9F-6020A9A58B6B"), UUID.fromString("D8499B04-0E66-4726-AB29-64469D734E0D"),
	        UUID.fromString("9F3D476D-C118-4544-8365-64846904B48E"), UUID.fromString("2AD3F246-FEE1-4E67-B886-69FD380BB150") };

	private final UpgradeChipItem.RestrictionTypes restrictionType;
	private final int basecapacity;
	private final int maxInvSize;
	private final int baseDamageReduction;
	private final float baseToughness;

	public UpgradeableArmorItem(String resname, Type slot, int basecapacity, int maxInvSize, int baseDamageReduction, float baseToughness, UpgradeChipItem.RestrictionTypes restrictionType)
	{
		super(resname, CustomArmorMaterial.UNNECESSARY, slot, new Item.Properties());
		ModCreativeTabs.putItem(ModCreativeTabs.TOOLS_AND_ARMOR, () -> this);
		this.basecapacity = basecapacity;
		this.maxInvSize = maxInvSize;
		this.baseDamageReduction = baseDamageReduction; 
		this.baseToughness = baseToughness;
		this.restrictionType = restrictionType;
	}

	@Override
	public ICapabilityProvider initCapabilities(ItemStack stack, CompoundTag nbt)
	{
		return new InventoryEnergyProvider(basecapacity, maxInvSize);
	}

	public static int getEnergyCost(ItemStack stack)
	{
		return PlantTechConstants.UPGRADABLE_ARMOUR_DEFAULT_ENERGY_USE + NBTHelper.getInt(stack, "energycost", 0);
	}

	@Override
	public boolean isBarVisible(ItemStack stack)
	{
		return getBarWidth(stack) < 13;
	}

	@Override
	@Deprecated
	public int getBarWidth(ItemStack stack)
	{
		return PTClientUtil.getBarWidthForEnergyItem(stack);
	}

	@Override
	public int getBarColor(ItemStack p_150901_)
	{
		return PlantTechConstants.DURABILITY_BAR_COLOUR;
	}

	@Override
	public boolean isEnchantable(ItemStack stack)
	{
		return false;
	}

	@Override
	public InteractionResultHolder<ItemStack> use(Level world, Player player, InteractionHand hand)
	{
		ItemStack stack = player.getItemInHand(hand);
		if (player.isCrouching())
		{
			if (!world.isClientSide && player instanceof ServerPlayer sp)
				PTCommonUtil.openItemContainer(sp, "container.upgradeableitem", stack, ItemUpgradeableMenu::new);
			return InteractionResultHolder.sidedSuccess(stack, world.isClientSide());
		}
		return new InteractionResultHolder<>(InteractionResult.FAIL, stack);
	}

	@Override
	public void updateNBTValues(ItemStack stack)
	{
		AtomicInteger increaseCapacity = new AtomicInteger();
		PTCommonUtil.tryAccessInv(stack, (inv) -> {
			int energyCost = 0, energyProduction = 0, increaseArmor = 0;
			float increaseToughness = 0;
			HashMap<Enchantment, Integer> enchantments = new HashMap<Enchantment, Integer>();
			ItemStack stackInSlot;
			for (int i = 0; i < inv.getSlots(); i++)
			{
				stackInSlot = inv.getStackInSlot(i);
				if (stackInSlot.getItem() instanceof UpgradeChipItem item)
				{
					energyCost += item.getEnergyCost();
					increaseCapacity.addAndGet(item.getIncreaseCapacity());
					energyProduction += item.getEnergyProduction();
					increaseArmor += item.getIncreaseArmor();
					increaseToughness += item.getIncreaseToughness();
					Enchantment ench = item.getEnchantment();
					if (item.isAllowed(this.type.getSlot().getIndex()))
					{
						if (ench != null)
						{
							if (enchantments.containsKey(ench))
							{
								int nextlevel = enchantments.get(ench) + 1;
								enchantments.put(ench, nextlevel);
							}
							else
								enchantments.put(ench, 1);
						}
					}
				}
			}

			stack.getEnchantmentTags().clear();
			for (Enchantment ench: enchantments.keySet())
			{
				int level = enchantments.get(ench);
				stack.enchant(ench, Math.min(ench.getMaxLevel(), level));
			}

			CompoundTag nbt = stack.getOrCreateTag();

			nbt.putInt("energycost", energyCost);
			nbt.putInt("energyproduction", energyProduction);
			nbt.putInt("increasearmor", increaseArmor);
			nbt.putFloat("increasetoughness", increaseToughness);


		});

		PTCommonUtil.tryAccessEnergyStorage(stack, (storage) -> storage.setEnergyMaxStored(basecapacity + increaseCapacity.get()));
	}

	@Override
	public Multimap<Attribute, AttributeModifier> getAttributeModifiers(EquipmentSlot equipmentSlot, ItemStack stack)
	{
		Multimap<Attribute, AttributeModifier> multimap = HashMultimap.create();
		if (equipmentSlot == type.getSlot())
		{
			multimap.put(Attributes.ARMOR, new AttributeModifier(ARMOR_MODIFIERS[equipmentSlot.getIndex()], "Armor modifier", getDamageReduceAmount(stack), AttributeModifier.Operation.ADDITION));
			multimap.put(Attributes.ARMOR_TOUGHNESS, new AttributeModifier(ARMOR_MODIFIERS[equipmentSlot.getIndex()], "Armor toughness", getToughness(stack), AttributeModifier.Operation.ADDITION));
		}
		return multimap;
	}
	
	public int getDamageReduceAmount(ItemStack stack)
	{
		return baseDamageReduction + NBTHelper.getInt(stack, "increasearmor", 0);
	}
	
	public float getToughness(ItemStack stack)
	{
		return baseToughness + NBTHelper.getFloat(stack, "increasetoughness", 0);
	}
	
	@Override
	public boolean canBeDepleted()
	{
		return false;
	}
	
	@Override
	public void onArmorTick(ItemStack stack, Level world, Player player)
	{
		if (!world.isClientSide && !stack.isEmpty() && world.getGameTime() % 200L == 0)
			PTCommonUtil.tryAccessEnergy(stack, (cap) -> cap.receiveEnergy(NBTHelper.getInt(stack, "energyproduction", 0), false));
	}

	@Override
	public void appendHoverText(ItemStack stack, Level level, List<Component> tooltip, TooltipFlag flagIn)
	{
		PTClientUtil.appendEnergyTooltip(stack, tooltip);
		tooltip.add(Component.translatable("planttech2.info.energycosts", getEnergyCost(stack)));
		tooltip.add(Component.translatable("planttech2.info.openwithshift"));
	}
}
