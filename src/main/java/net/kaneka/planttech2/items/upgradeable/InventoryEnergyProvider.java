package net.kaneka.planttech2.items.upgradeable;

import net.kaneka.planttech2.energy.BioEnergyStorage;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;

public class InventoryEnergyProvider implements ICapabilitySerializable<CompoundTag>
{
	protected final BioEnergyStorage energystorage;
	private final LazyOptional<IEnergyStorage> energyCap;
	protected final ItemStackHandler itemhandler;
    private final LazyOptional<IItemHandler> inventoryCap;
	
	public InventoryEnergyProvider(int storage, int invSize)
	{
		energystorage = new BioEnergyStorage(storage);
		energystorage.setEnergyStored(storage);
		energyCap = LazyOptional.of(() -> energystorage);
		itemhandler = new ItemStackHandler(invSize);
		inventoryCap = LazyOptional.of(() -> itemhandler);
	}

	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side)
	{
		if (cap == ForgeCapabilities.ENERGY)
		    return energyCap.cast();
		if (cap == ForgeCapabilities.ITEM_HANDLER)
		    return inventoryCap.cast();
		return LazyOptional.empty();
	}

	@Override
	public CompoundTag serializeNBT()
	{
		CompoundTag compound = new CompoundTag();
		compound.put("inventory", itemhandler.serializeNBT());
		compound.put("energy", energystorage.serializeNBT());
		return compound;
	}

	@Override
	public void deserializeNBT(CompoundTag nbt)
	{
		itemhandler.deserializeNBT(nbt.getCompound("inventory"));
		energystorage.deserializeNBT(nbt.getCompound("energy"));
	}
}
