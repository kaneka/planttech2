package net.kaneka.planttech2.items;

import net.kaneka.planttech2.energy.BioEnergyProvider;
import net.kaneka.planttech2.energy.BioEnergyStorage;
import net.kaneka.planttech2.utilities.ModCreativeTabs;
import net.kaneka.planttech2.utilities.PTClientUtil;
import net.kaneka.planttech2.utilities.PTCommonUtil;
import net.kaneka.planttech2.utilities.PlantTechConstants;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraftforge.common.capabilities.ICapabilityProvider;

import javax.annotation.Nullable;
import java.util.List;

public class EnergyStorageItem extends Item
{
	private final int capacity;

	public EnergyStorageItem(Properties property, int capacity)
	{
		super(property.durability(capacity));
		this.capacity = capacity;
		ModCreativeTabs.putItemStack(ModCreativeTabs.MAIN, () -> {
			ItemStack full = new ItemStack(this);
			PTCommonUtil.tryAccessEnergyStorage(full, BioEnergyStorage::max);
			return full;
		});
	}

	@Override
	public ICapabilityProvider initCapabilities(ItemStack stack, CompoundTag nbt)
	{
		return new BioEnergyProvider(capacity);
	}

	@Nullable
	@Override
	public CompoundTag getShareTag(ItemStack stack)
	{
		CompoundTag compound = super.getShareTag(stack);
		if (compound != null)
			PTCommonUtil.writeEnergySharedTag(stack, compound);
		return compound;
	}

	@Override
	public void readShareTag(ItemStack stack, @Nullable CompoundTag nbt)
	{
		if (nbt != null)
			PTCommonUtil.readEnergySharedTag(stack, nbt);
		super.readShareTag(stack, nbt);
	}
	
	@Override
	public void appendHoverText(ItemStack stack, Level level, List<Component> tooltip, TooltipFlag flagIn)
	{
		PTCommonUtil.tryAccessEnergy(stack, (storage) -> tooltip.add(Component.literal(storage.getEnergyStored() + "/" + storage.getMaxEnergyStored() + "BE")));
	}

	@Override
	public boolean isBarVisible(ItemStack stack)
	{
		return true;
	}

	@Override
	@Deprecated
	public int getBarWidth(ItemStack stack)
	{
		return PTClientUtil.getBarWidthForEnergyItem(stack);
	}

	@Override
	public int getBarColor(ItemStack stack)
	{
		return PlantTechConstants.DURABILITY_BAR_COLOUR;
	}
}
