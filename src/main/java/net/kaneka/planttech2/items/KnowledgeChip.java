package net.kaneka.planttech2.items;

import net.kaneka.planttech2.registries.ModItems;
import net.kaneka.planttech2.utilities.ModCreativeTabs;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

import java.util.List;

public class KnowledgeChip extends Item
{		
	private final int tier;
	private final int maxKnowledge;
	
	public KnowledgeChip(int tier, int maxKnowledge)
	{
		super(new Item.Properties().stacksTo(1));
		ModCreativeTabs.putItem(ModCreativeTabs.MAIN, () -> this);
		this.tier = tier;
		this.maxKnowledge = maxKnowledge; 
	}
	
	public int getTier()
	{
		return tier; 
	}
	
	public int getMaxKnowledge()
	{
		return maxKnowledge; 
	}
	
	public static int getMaxTier()
	{
		return 5; 
	}
	
	public static ItemStack addKnowledge(ItemStack stack, int amount, int maxTier)
	{
		if (!stack.isEmpty())
		{
			Item item = stack.getItem(); 
			if (item instanceof KnowledgeChip chip)
			{
				if (chip.getTier() <= maxTier)
				{
					CompoundTag nbt = stack.getOrCreateTag();
					int newknowledge;
					if (nbt.contains("knowledge"))
						newknowledge = nbt.getInt("knowledge") + amount;
					else
						newknowledge = amount;

					if (newknowledge < chip.getMaxKnowledge())
					{
						nbt.putInt("knowledge", newknowledge);
						stack.setTag(nbt);
					}
					else
					{
						ItemStack nextChip = getByTier(chip.tier + 1);
						if(!nextChip.isEmpty())
							return nextChip;
						else
						{
							nbt.putInt("knowledge", chip.getMaxKnowledge());
							stack.setTag(nbt);
						}
					}
				}
			}
		}
		return stack; 
	}
	
	public static ItemStack getByTier(int tier)
	{
		return switch (tier)
				{
					case 0 -> new ItemStack(ModItems.KNOWLEDGECHIP_TIER_0.get());
					case 1 -> new ItemStack(ModItems.KNOWLEDGECHIP_TIER_1.get());
					case 2 -> new ItemStack(ModItems.KNOWLEDGECHIP_TIER_2.get());
					case 3 -> new ItemStack(ModItems.KNOWLEDGECHIP_TIER_3.get());
					case 4 -> new ItemStack(ModItems.KNOWLEDGECHIP_TIER_4.get());
					case 5 -> new ItemStack(ModItems.KNOWLEDGECHIP_TIER_5.get());
					default -> ItemStack.EMPTY;
				};
	}
	
	@Override
	public void appendHoverText(ItemStack stack, Level level, List<Component> tooltip, TooltipFlag flagIn)
	{
		CompoundTag nbt = stack.getOrCreateTag();
		if (nbt.contains("knowledge"))
			tooltip.add(Component.translatable("planttech2.info.knowledge").append(": " + nbt.getInt("knowledge") + "/" + getMaxKnowledge()));
		else
			tooltip.add(Component.translatable("planttech2.info.knowledge").append(": 0/" + getMaxKnowledge()));
	}
}
