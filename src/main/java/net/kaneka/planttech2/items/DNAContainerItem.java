package net.kaneka.planttech2.items;

import net.kaneka.planttech2.hashmaps.TraitsManager;
import net.kaneka.planttech2.utilities.ModCreativeTabs;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import javax.annotation.Nullable;
import java.util.List;

public class DNAContainerItem extends Item
{
	public DNAContainerItem()
	{
		super(new Item.Properties());
		ModCreativeTabs.putItem(ModCreativeTabs.MAIN, () -> this);
	}

	@Override
	@OnlyIn(Dist.CLIENT)
	public void appendHoverText(ItemStack stack, @Nullable Level level, List<Component> tooltip, TooltipFlag flagIn)
	{
		TraitsManager.ItemImpl manager = TraitsManager.ItemImpl.of(stack);
		if (manager.type != null)
			tooltip.add(Component.translatable("planttech2.info.type").append(": ").append(manager.type.getDisplayName()));
		tooltip.addAll(CropSeedItem.getTips(manager));
	}
}
