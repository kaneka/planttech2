package net.kaneka.planttech2.items;

import net.kaneka.planttech2.fluids.BiomassContainerBiomassHandler;
import net.kaneka.planttech2.registries.ModFluids;
import net.kaneka.planttech2.registries.ModItems;
import net.kaneka.planttech2.utilities.ModCreativeTabs;
import net.kaneka.planttech2.utilities.PTCommonUtil;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.IFluidHandler;

import java.util.List;

public class BiomassContainerItem extends Item
{
	public BiomassContainerItem()
	{
		super(new Item.Properties().stacksTo(1));
		ModCreativeTabs.putItem(ModCreativeTabs.MAIN, () -> this);
		ModCreativeTabs.putItemStack(ModCreativeTabs.MAIN, () -> {
			ItemStack full = new ItemStack(ModItems.BIOMASSCONTAINER.get());
			PTCommonUtil.tryAccessBiomass(full, (storage, index) -> storage.fill(new FluidStack(ModFluids.BIOMASS.get(), storage.getTankCapacity(index)), IFluidHandler.FluidAction.EXECUTE));
			return full;
		});
	}

	public static float getFillLevelModel(ItemStack stack)
	{
		return PTCommonUtil.tryAccessBiomassValue(stack, (storage, index) -> (float) storage.getFluidInTank(index).getAmount() / storage.getTankCapacity(index), 0.0F) * 10.0F;
	}

	@Override
	public ICapabilityProvider initCapabilities(ItemStack stack, CompoundTag nbt)
	{
		return new BiomassContainerBiomassHandler(stack);
	}

	@Override
	public void appendHoverText(ItemStack stack, Level level, List<Component> tooltip, TooltipFlag flagIn)
	{
		PTCommonUtil.tryAccessBiomass(stack, (storage, index) -> tooltip.add(Component.literal(storage.getFluidInTank(index).getAmount() + "/" + storage.getTankCapacity(index))));
	}
}
