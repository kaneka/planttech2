package net.kaneka.planttech2.items;

import net.kaneka.planttech2.crops.CropTypes;
import net.kaneka.planttech2.utilities.ModCreativeTabs;
import net.minecraft.client.color.item.ItemColor;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.ComposterBlock;

public class ParticleItem extends Item
{
	private final String entryName;
	
	public ParticleItem(String name)
	{
		super(new Item.Properties());
		ComposterBlock.COMPOSTABLES.put(this, 0.06F);
		ModCreativeTabs.putItem(ModCreativeTabs.PARTICLES, () -> this);
		this.entryName = name;
	}
	
	private String getEntryName()
	{
		return entryName;
	}
	
	public static class ColorHandler implements ItemColor
	{
		@Override
		public int getColor(ItemStack stack, int tintIndex)
		{
		    return CropTypes.fromName(((ParticleItem) stack.getItem()).getEntryName()).getSeedColor();
		}
	}
}
