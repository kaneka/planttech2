package net.kaneka.planttech2.items;


import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.blocks.entity.CropsBlockEntity;
import net.kaneka.planttech2.crops.CropTypes;
import net.kaneka.planttech2.datagen.helper.JsonFileConverter;
import net.kaneka.planttech2.utilities.ModCreativeTabs;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.Level;
import net.minecraftforge.registries.ForgeRegistries;

import javax.annotation.Nullable;
import java.io.BufferedWriter;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TestItem extends Item
{
	public TestItem()
	{
		super(new Item.Properties());
		ModCreativeTabs.putItem(ModCreativeTabs.MAIN, () -> this);
	}

	private BufferedWriter out;
	@Override
	public InteractionResult useOn(UseOnContext ctx)
	{
		if (ctx.getLevel() instanceof ServerLevel world)
		{
			CropTypes.crops().forEach((c) -> {
				System.out.println(c.name() + ": " + c.getConfig().isEnabled());
			});
//			if (world.getBlockEntity(ctx.getClickedPos()) instanceof CropsBlockEntity crop)
//				crop.setSick(true);
//			TraitsManager.ItemImpl manager = TraitsManager.ItemImpl.of(new ItemStack(CropTypes.CACTUS_CROP.getSeed()));
//			manager.setTrait(CropTraitsTypes.FERTILITY, 3);
//			manager.save();
//			ctx.getPlayer().getInventory().add(manager.stack);
//			world.getAllEntities().forEach((e) -> {
//				if (e instanceof SnailEntity)
//					System.out.println(e.position());
//			});
//			Map<ResourceLocation, Float> map = new TreeMap<>();
//			ctx.getLevel().holderLookup(Registries.BIOME).listElementIds().forEach((entry) -> map.put(entry.location(), ctx.getLevel().holderLookup(Registries.BIOME).get(entry).get().get().getModifiedClimateSettings().temperature()));
//			map.keySet().stream().sorted(Comparator.comparing(map::get)).forEach((r) -> System.out.println(r + " " + map.get(r)));
		}
		//		System.out.println(ModDimensions.getPlantTopiaDimensionType());
		if (false && !ctx.getLevel().isClientSide)
		{
			JsonFileConverter.act();
			//https://planttech-2.fandom.com/wiki/Items
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(FileDescriptor.out), StandardCharsets.US_ASCII));
			List<Item> items = new ArrayList<>(ForgeRegistries.ITEMS.getValues());
			items.sort(Comparator.comparing(item -> item.getDescription().getString()));
			items.stream().filter((item) -> ForgeRegistries.ITEMS.getKey(item).getNamespace().equals(PlantTechMain.MODID) && !(item instanceof CropSeedItem || item instanceof ParticleItem || item instanceof BlockItem)).forEach((item) ->
			{
				try
				{
					this.out = out;
					write("-");
					write(item.getDescription().getString());
					write(ForgeRegistries.ITEMS.getKey(item).getPath());
					int size = item.getMaxStackSize();
					write((size == 64 ? "" : String.valueOf(size)));
					int durability = item.getMaxDamage();
					write((durability == 0 ? "" : String.valueOf(durability)));
//					write(item.getCreativeTabs().stream().findFirst().get().getDisplayName().getString());
					out.flush();
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			});
			//				|-
			//				|Crop Remover
			//				|cropremover
			//				|1
			//				|1024
			//				|"
		}
			/*
			CropTypes entry = CropTypes.fromString("coal");
			System.out.println(entry);
			if (entry != null)
			{
				List<Supplier<Item>> seeds = entry.getSeeds();
				for (Supplier<Item> seed : seeds)
					System.out.println(seed.get());
			}
			System.out.println(Feature.STRUCTURES); 
			
			System.out.println(" ");
    		System.out.println("Blocks: "); 
    		for(Entry<ResourceLocation, Block> list: ForgeRegistries.BLOCKS.getEntries())
    		{
    			if(list.getKey().getNamespace().contains("planttech2") && !(list.getValue() instanceof CropBaseBlock))
    			{
    				System.out.println(list.getValue().getNameTextComponent().getFormattedText()); 
    			}
    		}
    		
    		System.out.println(" ");
    		System.out.println("Items: "); 
    		for(Entry<ResourceLocation, Item> list: ForgeRegistries.ITEMS.getEntries())
    		{
    			if(list.getKey().getNamespace().contains("planttech2") && !(list.getValue() instanceof CropSeedItem) && !(list.getValue() instanceof ParticleItem))
    			{
    				System.out.println(list.getValue().getDisplayName(new ItemStack(list.getValue())).getFormattedText()); 
    			}
    		}
		
		*/
		//TeleportationUtils.changeDimension(ctx.getWorld(), ctx.getPos(), ctx.getPlayer(), ModDimensions.getPlantTopiaDimensionType(), Blocks.DIRT);


		//Minecraft.getInstance().displayGuiScreen(new GuideScreen());
		/*
		if(!ctx.getWorld().isRemote)
		{
			new JsonGenerator().create();
		}
		
		if(!ctx.getWorld().isRemote)
		{
		for(Enchantment ench: ForgeRegistries.ENCHANTMENTS)
		{
			System.out.println("\"item.planttech2."+ ench.getName().replace("enchantment.minecraft.", "") + "_chip\": \""+ench.getName().replace("enchantment.minecraft.", "")+" Chip\""); 
		}
		}
		*/
		// place(Sets.newHashSet(), ctx.getWorld(), new Random(), ctx.getPos().up(),
		// MutableBoundingBox.getNewBoundingBox());
		//		 System.out.println(ModDimensionPlantTopia.getDimensionType());
		return super.useOn(ctx);
	}

	private void write(String text) throws IOException
	{
		out.write("|" + text);
		out.write("\n");
	}

	@Override
	public void appendHoverText(ItemStack p_77624_1_, @Nullable Level p_77624_2_, List<Component> p_77624_3_, TooltipFlag p_77624_4_)
	{
		Component text = Component.literal("Leave me alone!");
		p_77624_3_.add(text);
	}
}
