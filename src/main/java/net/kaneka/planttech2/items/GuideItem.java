package net.kaneka.planttech2.items;

import net.kaneka.planttech2.utilities.ModCreativeTabs;
import net.kaneka.planttech2.utilities.PTClientUtil;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.DistExecutor;

public class GuideItem extends Item
{
    public GuideItem()
    {
	    super(new Item.Properties().defaultDurability(1));
        ModCreativeTabs.putItem(ModCreativeTabs.MAIN, () -> this);
    }

    @Override
    public InteractionResultHolder<ItemStack> use(Level level, Player playerIn, InteractionHand handIn)
    {
        DistExecutor.runWhenOn(Dist.CLIENT, () -> () -> PTClientUtil.openGuideScreen(this));
        return super.use(level, playerIn, handIn);
    }

    @Override
    public boolean isBarVisible(ItemStack p_150899_)
    {
        return false;
    }
}
