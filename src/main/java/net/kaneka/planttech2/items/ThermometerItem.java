package net.kaneka.planttech2.items;

import net.kaneka.planttech2.utilities.ModCreativeTabs;
import net.minecraft.network.chat.Component;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;

public class ThermometerItem extends Item
{

	public ThermometerItem()
	{
		super(new Item.Properties());
		ModCreativeTabs.putItem(ModCreativeTabs.MAIN, () -> this);
	}

	@Override
	public InteractionResultHolder<ItemStack> use(Level level, Player playerIn, InteractionHand handIn)
	{
		ItemStack stack = playerIn.getItemInHand(handIn);
		if (level.isClientSide)
		{
			playerIn.sendSystemMessage(Component.translatable("text.biometemperature").append(": ").append(String.format("%.2f", level.getBiome(playerIn.blockPosition()).value().getModifiedClimateSettings().temperature())));
			return InteractionResultHolder.consume(stack);
		}
		return InteractionResultHolder.pass(stack);
	}
}
