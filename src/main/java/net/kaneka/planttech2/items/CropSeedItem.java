package net.kaneka.planttech2.items;

import net.kaneka.planttech2.blocks.CropBarsBlock;
import net.kaneka.planttech2.blocks.CropBaseBlock;
import net.kaneka.planttech2.blocks.entity.CropsBlockEntity;
import net.kaneka.planttech2.crops.CropTypes;
import net.kaneka.planttech2.crops.TemperatureTolerance;
import net.kaneka.planttech2.enums.CropTraitsTypes;
import net.kaneka.planttech2.hashmaps.TraitsManager;
import net.kaneka.planttech2.utilities.ModCreativeTabs;
import net.minecraft.ChatFormatting;
import net.minecraft.client.color.item.ItemColor;
import net.minecraft.core.BlockPos;
import net.minecraft.core.BlockSource;
import net.minecraft.core.dispenser.OptionalDispenseItemBehavior;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.ComposterBlock;
import net.minecraft.world.level.block.DispenserBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class CropSeedItem extends Item
{
	private final CropTypes cropType;

	public CropSeedItem(CropTypes cropType)
	{
		super(new Item.Properties());
		ModCreativeTabs.putItemStack(ModCreativeTabs.SEEDS, () -> {
			TraitsManager.ItemImpl t = TraitsManager.ItemImpl.of(new ItemStack(this));
			t.type = cropType;
			t.analysed = true;
			t.save();
			return t.stack;
		});
		this.cropType = cropType;
		ComposterBlock.COMPOSTABLES.put(this, 0.3F);
		DispenserBlock.registerBehavior(this, new OptionalDispenseItemBehavior()
		{
			@Override
			protected ItemStack execute(BlockSource source, ItemStack stack)
			{
				Level level = source.getLevel();
				BlockPos target = source.getPos().relative(source.getBlockState().getValue(DispenserBlock.FACING));
				this.setSuccess(plant(level, target, stack));
				if (!level.isClientSide() && this.isSuccess())
					level.levelEvent(2005, target, 0);
				return stack;
			}
		});
	}

	public CropTypes getCropType()
	{
		return cropType;
	}

	public static void addTip(List<Component> tooltip, TraitsManager.ItemImpl manager, String key, CropTraitsTypes trait)
	{
		addTip(tooltip, manager, key, trait, null);
	}

	public static List<Component> getTips(TraitsManager.ItemImpl manager)
	{
		List<Component> tooltip = new ArrayList<>();
		addTip(tooltip, manager, "planttech2.info.grow_speed", CropTraitsTypes.GROW_SPEED);
		addTip(tooltip, manager, "planttech2.info.sensitivity", CropTraitsTypes.SENSITIVITY);
		addTip(tooltip, manager, "planttech2.info.needed_light_level", CropTraitsTypes.LIGHT_SENSITIVITY, (i) -> 14 - i);
		addTip(tooltip, manager, "planttech2.info.water_range", CropTraitsTypes.WATER_SENSITIVITY, (i) -> i + 1);
		addTip(tooltip, manager, "planttech2.info.productivity", CropTraitsTypes.PRODUCTIVITY);
		addTip(tooltip, manager, "planttech2.info.fertility", CropTraitsTypes.FERTILITY);
		addTip(tooltip, manager, "planttech2.info.spreading_speed", CropTraitsTypes.SPREADING_SPEED);
		addTip(tooltip, manager, "planttech2.info.gene_strength", CropTraitsTypes.GENE_STRENGTH);
		addTip(tooltip, manager, "planttech2.info.energy_value", CropTraitsTypes.ENERGY_VALUE, (i) -> i * 20);
		return tooltip;
	}

	public static void addTip(List<Component> tooltip, TraitsManager.ItemImpl manager, String key, CropTraitsTypes trait, Function<Integer, Integer> func)
	{
		if (manager.hasTrait(trait))
		{
			int level = manager.getTrait(trait);
			ChatFormatting colour = ChatFormatting.RESET;
			if (level == trait.getMin())
				colour = ChatFormatting.GRAY;
			if (level == trait.getMax())
				colour = ChatFormatting.GREEN;
			tooltip.add(Component.translatable(key).append(": ").append(Component.literal(String.valueOf(func == null ? level : func.apply(level))).withStyle(colour)));
		}
	}

	@Override
	@OnlyIn(Dist.CLIENT)
	public void appendHoverText(ItemStack stack, @Nullable Level level, List<Component> tooltip, TooltipFlag flagIn)
	{
		TraitsManager.ItemImpl manager = TraitsManager.ItemImpl.of(stack);
		tooltip.add(Component.translatable("planttech2.info.type").append(": ").append(cropType.getDisplayName()));
		if (manager.analysed)
		{
			tooltip.add(Component.translatable("planttech2.info.soil").append(": ").append(cropType.getDisplaySoilName()));
			tooltip.add(Component.translatable("planttech2.info.temperature").append(": ").append(temperatureString(cropType.getConfig().getTemperature(), manager.getTrait(CropTraitsTypes.TEMPERATURE_TOLERANCE))));

			tooltip.addAll(getTips(manager));
		}
		else
		{
			unknown(tooltip, "planttech2.info.soil");
			unknown(tooltip, "planttech2.info.temperature_tolerance");
			unknown(tooltip, "planttech2.info.grow_speed");
			unknown(tooltip, "planttech2.info.sensitivity");
			unknown(tooltip, "planttech2.info.needed_light_level");
			unknown(tooltip, "planttech2.info.water_range");
			unknown(tooltip, "planttech2.info.productivity");
			unknown(tooltip, "planttech2.info.fertility");
			unknown(tooltip, "planttech2.info.spreading_speed");
			unknown(tooltip, "planttech2.info.gene_strength");
			unknown(tooltip, "planttech2.info.energy_value");
		}
	}

	private void unknown(List<Component> tooltip, String key)
	{
		tooltip.add(Component.translatable(key).append(": ").append(Component.translatable("planttech2.info.unknown").withStyle(ChatFormatting.DARK_AQUA)));
	}

	public static Component temperatureString(TemperatureTolerance temp, int tolerance)
	{
		return Component.literal(temp.getDisplayString(tolerance));
	}

	public static class ColorHandler implements ItemColor
	{
		@Override
		public int getColor(ItemStack stack, int color)
		{
			return ((CropSeedItem) stack.getItem()).getCropType().getSeedColor();
		}
	}

	public static boolean plant(Level world, BlockPos pos, ItemStack stack)
	{
		CropTypes entry = CropTypes.getBySeed(stack).orElse(null);
		if (entry == null)
			return false;
		BlockState state = world.getBlockState(pos);
		if (state.getBlock() instanceof CropBarsBlock)
		{
			world.setBlockAndUpdate(pos, entry.getCropBlock().defaultBlockState().setValue(CropBaseBlock.COPPER, state.getValue(CropBaseBlock.COPPER)));
			if (world.getBlockEntity(pos) instanceof CropsBlockEntity crop)
			{
				TraitsManager.ItemImpl traits = TraitsManager.ItemImpl.of(stack);
				traits.analysed = true;
				traits.type = entry;
				crop.setTraits(traits);
				crop.setStartTick();
				return true;
			}
		}
		return false;
	}
}
