package net.kaneka.planttech2.items;

import net.kaneka.planttech2.blocks.entity.CropsBlockEntity;
import net.kaneka.planttech2.enums.CropTraitsTypes;
import net.kaneka.planttech2.hashmaps.TraitsManager;
import net.kaneka.planttech2.utilities.ModCreativeTabs;
import net.kaneka.planttech2.utilities.PTCommonUtil;
import net.kaneka.planttech2.utilities.PlantTechConstants;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.Level;

import java.util.List;

public class AdvancedAnalyserItem extends EnergyStorageItem
{
	public AdvancedAnalyserItem()
	{
		super(new Item.Properties().stacksTo(1), PlantTechConstants.ADVANCED_ANALYSER_ENERGY_CAPACITY);
		ModCreativeTabs.putItem(ModCreativeTabs.MAIN, () -> this);
	}

	@Override
	public InteractionResult useOn(UseOnContext ctx)
	{
		ItemStack stack = ctx.getItemInHand();
		Level level = ctx.getLevel();
		BlockPos pos = ctx.getClickedPos();
		Player player = ctx.getPlayer();
		if (!level.isClientSide && player != null && level.getBlockEntity(pos) instanceof CropsBlockEntity cbe)
		{
			if (cbe.isAnalysed())
			{
				TraitsManager.TypedImpl traits = cbe.getTraits();
				sendTraits(player, traits);
			}
			else
			{
				PTCommonUtil.tryAccessEnergy(stack, (cap) -> {
					if (cap.getEnergyStored() >= PlantTechConstants.ADVANCED_ANALYSER_ENERGY_USE)
					{
						cap.extractEnergy(PlantTechConstants.ADVANCED_ANALYSER_ENERGY_USE, false);
						TraitsManager.TypedImpl traits = cbe.setAnalysedAndGetTraits();
						sendTraits(player, traits);
					}
					else
						player.sendSystemMessage(Component.translatable("planttech2.tip.energy_item.no_energy"));
				});
			}
		}
		return super.useOn(ctx);
	}

	private void sendTraits(Player player, TraitsManager.TypedImpl traits)
	{
		player.sendSystemMessage(Component.literal("--------------------------------------"));
		msg(player, "planttech2.info.type", traits.type.getDisplayName().getString());
		msg(player, "planttech2.info.soil", traits.type.getDisplaySoilName().getString());
		msg(player, "planttech2.info.grow_speed", traits.getTrait(CropTraitsTypes.GROW_SPEED));
		msg(player, "planttech2.info.sensitivity", traits.getTrait(CropTraitsTypes.SENSITIVITY));
		msg(player, "planttech2.info.needed_light_level", String.valueOf(14 - traits.getTrait(CropTraitsTypes.LIGHT_SENSITIVITY)));
		msg(player, "planttech2.info.water_range", String.valueOf(1 + traits.getTrait(CropTraitsTypes.WATER_SENSITIVITY)));
		msg(player, "planttech2.info.temperature", CropSeedItem.temperatureString(traits.type.getConfig().getTemperature(), traits.getTrait(CropTraitsTypes.TEMPERATURE_TOLERANCE)).getString());
		msg(player, "planttech2.info.productivity", traits.getTrait(CropTraitsTypes.PRODUCTIVITY));
		msg(player, "planttech2.info.fertility", traits.getTrait(CropTraitsTypes.FERTILITY));
		msg(player, "planttech2.info.spreading_speed", traits.getTrait(CropTraitsTypes.SPREADING_SPEED));
		msg(player, "planttech2.info.gene_strength", traits.getTrait(CropTraitsTypes.GENE_STRENGTH));
		msg(player, "planttech2.info.energy_value", String.valueOf(traits.getTrait(CropTraitsTypes.ENERGY_VALUE) * 20));
	}

	private void msg(Player player, String key, int value)
	{
		msg(player, key, String.valueOf(value));
	}

	private void msg(Player player, String key, String value)
	{
		player.sendSystemMessage(Component.translatable(key).append(": " + value));
	}

	@Override
	public void appendHoverText(ItemStack stack, Level level, List<Component> tooltip, TooltipFlag flagIn)
	{
		super.appendHoverText(stack, level, tooltip, flagIn);
		tooltip.add(Component.translatable("planttech2.tip.advanced_analyser"));
		tooltip.add(Component.translatable("planttech2.tip.energy_item.energy_use", PlantTechConstants.ADVANCED_ANALYSER_ENERGY_USE));
	}
}
