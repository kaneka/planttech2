package net.kaneka.planttech2.packets;

import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.crops.CropTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkEvent;

import java.util.function.Supplier;

public class CropListSyncMessage
{
    private final CompoundTag cropList;
    private final FriendlyByteBuf buf;

    public CropListSyncMessage()
    {
        this(CropTypes.serializeNBT(), null);
    }

    public CropListSyncMessage(CompoundTag cropList, FriendlyByteBuf buf)
    {
        this.cropList = cropList;
        this.buf = buf;
    }

    public static void encode(CropListSyncMessage pkt, FriendlyByteBuf buf)
    {
        buf.writeNbt(pkt.cropList);
        CropTypes.extraToNetwork(buf);
    }

    public static CropListSyncMessage decode(FriendlyByteBuf buf)
    {
        return new CropListSyncMessage(buf.readNbt(), buf);
    }

    public static void handle(final CropListSyncMessage pkt, Supplier<NetworkEvent.Context> ctx)
    {
        ctx.get().enqueueWork(() -> {
            PlantTechMain.LOGGER.debug("Reading crop configurations sent from server");
            try
            {
                CropTypes.deserializeNBT(pkt.cropList);
                CropTypes.extraFromNetwork(pkt.buf);
            }
            catch (Exception e)
            {
                PlantTechMain.LOGGER.error("An error has occurred during the processing with crop list syncing" +
                        ", report this to the server owner.");
                PlantTechMain.LOGGER.error(e.getMessage());
            }
            PlantTechMain.LOGGER.debug("Finished reading crop configurations sent from server");
        });
        ctx.get().setPacketHandled(true);
    }
}
