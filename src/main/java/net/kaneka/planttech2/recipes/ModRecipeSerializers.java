package net.kaneka.planttech2.recipes;

import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.recipes.recipeclasses.ChipalyzerRecipe;
import net.kaneka.planttech2.recipes.recipeclasses.CompressorRecipe;
import net.kaneka.planttech2.recipes.recipeclasses.InfuserRecipe;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class ModRecipeSerializers
{
    public static final DeferredRegister<RecipeSerializer<?>> RECIPE_SERIALIZERS = DeferredRegister.create(ForgeRegistries.RECIPE_SERIALIZERS, PlantTechMain.MODID);
    public static final RegistryObject<CompressorRecipe.Serializer> COMPRESSOR = RECIPE_SERIALIZERS.register("compressing", CompressorRecipe.Serializer::new);
    public static final RegistryObject<InfuserRecipe.Serializer> INFUSER = RECIPE_SERIALIZERS.register("infusing", InfuserRecipe.Serializer::new);
    public static final RegistryObject<ChipalyzerRecipe.Serializer> CHIPALYZER = RECIPE_SERIALIZERS.register("chipalyzer", ChipalyzerRecipe.Serializer::new);
}
