package net.kaneka.planttech2.recipes;

import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.recipes.recipeclasses.ChipalyzerRecipe;
import net.kaneka.planttech2.recipes.recipeclasses.CompressorRecipe;
import net.kaneka.planttech2.recipes.recipeclasses.InfuserRecipe;
import net.minecraft.world.item.crafting.Recipe;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public final class ModRecipeTypes
{
    public static final DeferredRegister<RecipeType<?>> RECIPE_TYPES = DeferredRegister.create(ForgeRegistries.RECIPE_TYPES, PlantTechMain.MODID);
    public static final RegistryObject<RecipeType<CompressorRecipe>> COMPRESSING = make("compressing");
    public static final RegistryObject<RecipeType<InfuserRecipe>> INFUSING = make("infusing");
    public static final RegistryObject<RecipeType<ChipalyzerRecipe>> CHIPALYZER = make("chipalyzer");

    private static <T extends Recipe<?>> RegistryObject<RecipeType<T>> make(String name)
    {
        return RECIPE_TYPES.register(name, () -> new RecipeType<T>() {});
    }
}
