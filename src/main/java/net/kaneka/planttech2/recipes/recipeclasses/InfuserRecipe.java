package net.kaneka.planttech2.recipes.recipeclasses;

import com.google.gson.JsonObject;
import net.kaneka.planttech2.recipes.ModRecipeSerializers;
import net.kaneka.planttech2.recipes.ModRecipeTypes;
import net.minecraft.core.RegistryAccess;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.Container;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.Recipe;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraft.world.level.Level;
import net.minecraftforge.common.crafting.CraftingHelper;

public class InfuserRecipe implements Recipe<Container>
{
	private final ResourceLocation id; 
	private final Ingredient input;
	private final ItemStack output;
	private final int biomass; 
	
	public InfuserRecipe(ResourceLocation id, Ingredient input, ItemStack output, int biomass)
	{
		this.id = id; 
		this.input = input;
		this.output = output; 
		this.biomass = biomass; 
	}
	
	public Ingredient getInput()
	{
		return input; 
	}
	
	public ItemStack getOutput()
	{
		return output; 
	}
	
	public int getBiomass()
	{
		return biomass; 
	}

	@Override
	public boolean matches(Container inv, Level worldIn)
	{
		return input.test(inv.getItem(0));
	}

	@Override
	public ItemStack assemble(Container p_44001_, RegistryAccess p_267165_)
	{
		return output.copy();
	}

	@Override
	public boolean canCraftInDimensions(int width, int height)
	{
		return width == height && width == 1;
	}

	@Override
	public ItemStack getResultItem(RegistryAccess access)
	{
		return output;
	}

	@Override
	public ResourceLocation getId()
	{
		return id;
	}

	@Override
	public RecipeSerializer<?> getSerializer()
	{
		return ModRecipeSerializers.INFUSER.get();
	}

	@Override
	public RecipeType<?> getType()
	{
		return ModRecipeTypes.INFUSING.get();
	}

	
	public static class Serializer implements RecipeSerializer<InfuserRecipe>
	{
		//private static ResourceLocation NAME = new ResourceLocation(PlantTechMain.MODID, "infusing");

		@Override
		public InfuserRecipe fromJson(ResourceLocation recipeId, JsonObject json)
		{
			return new InfuserRecipe(recipeId, Ingredient.fromJson(json.getAsJsonObject("input")), CraftingHelper.getItemStack(json.getAsJsonObject("result"), true, true), json.get("biomass").getAsInt());
		}

		@Override
		public InfuserRecipe fromNetwork(ResourceLocation recipeId, FriendlyByteBuf buffer)
		{
			return new InfuserRecipe(recipeId, Ingredient.fromNetwork(buffer), buffer.readItem(), buffer.readInt());
		}

		@Override
		public void toNetwork(FriendlyByteBuf buffer, InfuserRecipe recipe)
		{
			recipe.input.toNetwork(buffer);
			buffer.writeItem(recipe.output);
			buffer.writeInt(recipe.biomass);
		}

	}

}
