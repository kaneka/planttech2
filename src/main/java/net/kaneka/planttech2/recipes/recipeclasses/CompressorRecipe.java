package net.kaneka.planttech2.recipes.recipeclasses;

import com.google.gson.JsonObject;
import net.kaneka.planttech2.recipes.ModRecipeSerializers;
import net.kaneka.planttech2.recipes.ModRecipeTypes;
import net.minecraft.core.RegistryAccess;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.Container;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.Recipe;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraft.world.level.Level;
import net.minecraftforge.common.crafting.CraftingHelper;

public class CompressorRecipe implements Recipe<Container>
{
	private final ResourceLocation id;
	private final ItemStack input;
	private final Ingredient output;

	public CompressorRecipe(ResourceLocation id, ItemStack input, Ingredient output)
	{
		this.id = id;
		this.input = input;
		this.output = output;
	}

	public Ingredient getOutput()
	{
		return output;
	}

	@Override
	public boolean matches(Container inv, Level worldIn)
	{
		return Ingredient.of().test(inv.getItem(0));
	}

	@Override
	public ItemStack assemble(Container p_44001_, RegistryAccess p_267165_)
	{
		return ItemStack.EMPTY;
	}

	public ItemStack getInput()
	{
		return input;
	}

	@Override
	public boolean canCraftInDimensions(int width, int height)
	{
		return true;
	}

	@Override
	public ItemStack getResultItem(RegistryAccess p_267052_)
	{
		return ItemStack.EMPTY;
	}

	@Override
	public ResourceLocation getId()
	{
		return id;
	}

	@Override
	public RecipeSerializer<?> getSerializer()
	{
		return ModRecipeSerializers.COMPRESSOR.get();
	}
	
	@Override
	public RecipeType<?> getType()
	{
		return ModRecipeTypes.COMPRESSING.get();
	}

	public static class Serializer implements RecipeSerializer<CompressorRecipe>
	{
		@Override
		public CompressorRecipe fromJson(ResourceLocation recipeId, JsonObject json)
		{
			return new CompressorRecipe(recipeId, CraftingHelper.getItemStack(json.getAsJsonObject("input"), true, true), Ingredient.fromJson(json.getAsJsonObject("result")));
		}

		@Override
		public CompressorRecipe fromNetwork(ResourceLocation recipeId, FriendlyByteBuf buffer)
		{
			return new CompressorRecipe(recipeId, buffer.readItem(), Ingredient.fromNetwork(buffer));
		}

		@Override
		public void toNetwork(FriendlyByteBuf buffer, CompressorRecipe recipe)
		{
			buffer.writeItem(recipe.input);
			recipe.output.toNetwork(buffer);
		}
	}

	@Override
	public String toString()
	{
		return "CompressorRecipe{" +
				"id=" + id +
				", input=" + input +
				", output=" + output +
				'}';
	}
}
