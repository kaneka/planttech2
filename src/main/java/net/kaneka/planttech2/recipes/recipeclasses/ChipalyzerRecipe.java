package net.kaneka.planttech2.recipes.recipeclasses;

import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import net.kaneka.planttech2.recipes.ModRecipeSerializers;
import net.kaneka.planttech2.recipes.ModRecipeTypes;
import net.minecraft.core.RegistryAccess;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.Container;
import net.minecraft.world.item.EnchantedBookItem;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.Recipe;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraft.world.item.enchantment.Enchantment;
import net.minecraft.world.item.enchantment.EnchantmentHelper;
import net.minecraft.world.level.Level;
import net.minecraftforge.common.crafting.CraftingHelper;
import net.minecraftforge.registries.ForgeRegistries;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

public class ChipalyzerRecipe implements Recipe<Container>
{
	private final ResourceLocation id;
	private final ItemStack chip;
	private final Ingredient input;
	@Nullable
	private final Enchantment enchantment;
	private final ItemStack output;

	public ChipalyzerRecipe(ResourceLocation id, ItemStack chip, @Nullable Ingredient input, @Nullable Enchantment enchantment, ItemStack output)
	{
		this.id = id;
		this.chip = chip;
		this.input = input == null ? Ingredient.EMPTY : input;
		this.enchantment = enchantment;
		this.output = output;
	}

	public ItemStack getChip() {return chip;}
	public Ingredient getInput() {return input;}
	@Nullable
	public Enchantment getEnchantment() {return enchantment;}
	public ItemStack getOutput() {return output;}

	public boolean compare(ItemStack chip, ItemStack stack)
	{
		if (!stack.isEmpty() && !chip.isEmpty())
		{
			if (enchantment != null)
			{
				List<Enchantment> stackench = getEnchList(stack);
				if (stackench.contains(enchantment))
					return true;
			}
			
			if (!input.isEmpty())
				return input.test(stack);
		}
		return false;
	}

	public List<Enchantment> getEnchList(ItemStack stack)
	{
		List<Enchantment> list = new ArrayList<Enchantment>();
		if (stack != null)
		{
			if (!stack.isEmpty())
			{
				if (stack.getItem() == Items.ENCHANTED_BOOK)
				{
					for (Tag nbt : EnchantedBookItem.getEnchantments(stack))
					{
						if (nbt instanceof CompoundTag compoundTag)
						{
							if (compoundTag.contains("id"))
							{
								ResourceLocation ench = new ResourceLocation(compoundTag.getString("id"));
								if (ForgeRegistries.ENCHANTMENTS.containsKey(ench))
									list.add(ForgeRegistries.ENCHANTMENTS.getValue(ench));
							}
						}
					}
				} 
				else
					list.addAll(EnchantmentHelper.getEnchantments(stack).keySet());
			}
		}
		return list;
	}

	public List<Ingredient> getComponents()
	{
		List<Ingredient> components = new ArrayList<>();
		components.add(Ingredient.of(chip));
		if (input.isEmpty() && enchantment != null)
		{
			ItemStack book = new ItemStack(Items.ENCHANTED_BOOK);
			book.enchant(enchantment, 1);
			components.add(Ingredient.of(book));
		}
		else
			components.add(input);
		return components;
	}

	@Override
	public boolean matches(Container inv, Level worldIn)
	{
		return input.test(inv.getItem(0));
	}

	@Override
	public ItemStack assemble(Container p_44001_, RegistryAccess p_267165_)
	{
		return output;
	}

	@Override
	public boolean canCraftInDimensions(int width, int height)
	{
		return width == height && width == 1;
	}

	@Override
	public ItemStack getResultItem(RegistryAccess p_267052_)
	{
		return output;
	}

	@Override
	public ResourceLocation getId() {return id;}

	@Override
	public RecipeSerializer<?> getSerializer()
	{
		return ModRecipeSerializers.CHIPALYZER.get();
	}

	@Override
	public RecipeType<?> getType() {return ModRecipeTypes.CHIPALYZER.get();}

	public static class Serializer implements RecipeSerializer<ChipalyzerRecipe>
	{
		@Override
		public ChipalyzerRecipe fromJson(ResourceLocation recipeId, JsonObject json)
		{
			Ingredient input = Ingredient.EMPTY;
			Enchantment enchantment = null; 
			if (json.has("input"))
			{
				JsonObject inputobject = json.getAsJsonObject("input");

				if (inputobject.has("enchantment"))
				{
					ResourceLocation id = new ResourceLocation(inputobject.get("enchantment").getAsString());
					if (ForgeRegistries.ENCHANTMENTS.containsKey(id))
						enchantment = ForgeRegistries.ENCHANTMENTS.getValue(id);
				}
				else input = Ingredient.fromJson(inputobject);
			}
			else throw new JsonParseException("Input cannot be empty!");

			return new ChipalyzerRecipe(recipeId, CraftingHelper.getItemStack(json.getAsJsonObject("chip"), true, true), input, enchantment, CraftingHelper.getItemStack(json.getAsJsonObject("result"), true, true));
		}

		@Override
		public ChipalyzerRecipe fromNetwork(ResourceLocation recipeId, FriendlyByteBuf buffer)
		{
			ItemStack chip = buffer.readItem();
			Ingredient input = Ingredient.fromNetwork(buffer);
			String ench = buffer.readUtf();
			Enchantment enchantment = ench.equals("null") ? null : ForgeRegistries.ENCHANTMENTS.getValue(new ResourceLocation(ench));
			ItemStack result = buffer.readItem();
			return new ChipalyzerRecipe(recipeId, chip, input, enchantment, result);
		}

		@Override
		public void toNetwork(FriendlyByteBuf buffer, ChipalyzerRecipe recipe)
		{
			buffer.writeItem(recipe.chip);
			recipe.input.toNetwork(buffer);
			if (recipe.enchantment != null)
				buffer.writeUtf(ForgeRegistries.ENCHANTMENTS.getKey(recipe.enchantment).toString());
			else
				buffer.writeUtf("null");
			buffer.writeItem(recipe.output);
		}
	}
}
