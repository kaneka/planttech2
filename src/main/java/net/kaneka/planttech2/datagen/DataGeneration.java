package net.kaneka.planttech2.datagen;

import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.crops.CropConfigProvider;
import net.kaneka.planttech2.datagen.blocks.BlockStateGenerator;
import net.kaneka.planttech2.datagen.loot.PT2LootTables;
import net.kaneka.planttech2.datagen.recipes.ChipalyzerRecipesProvider;
import net.kaneka.planttech2.datagen.recipes.CompressorRecipesProvider;
import net.kaneka.planttech2.datagen.recipes.InfuserRecipesProvider;
import net.kaneka.planttech2.datagen.recipes.ItemRecipeProvider;
import net.kaneka.planttech2.datagen.tags.BlockTagGenerator;
import net.kaneka.planttech2.datagen.tags.EntityTagGenerator;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.PackOutput;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.data.event.GatherDataEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = PlantTechMain.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class DataGeneration
{
    @SubscribeEvent
    public static void gatherData(GatherDataEvent event)
    {
        DataGenerator generator = event.getGenerator();
        PackOutput output = generator.getPackOutput();
        ExistingFileHelper existingFileHelper = event.getExistingFileHelper();
        boolean client = event.includeClient();
        boolean server = event.includeServer();
        generator.addProvider(client, new Languages(output));
        generator.addProvider(client, new BlockStateGenerator(output, existingFileHelper));
        generator.addProvider(client, new ItemModels(output, existingFileHelper));
        generator.addProvider(client, new ItemModelGenerator(output, existingFileHelper));
        generator.addProvider(server, new ItemRecipeProvider(output));
        generator.addProvider(server, new PT2LootTables(output));
        generator.addProvider(server, new CropConfigProvider(output));
        generator.addProvider(server, new CompressorRecipesProvider(output));
        generator.addProvider(server, new ChipalyzerRecipesProvider(output));
        generator.addProvider(server, new InfuserRecipesProvider(output));
        generator.addProvider(server, new BlockTagGenerator(output, event.getLookupProvider(), existingFileHelper));
        generator.addProvider(server, new EntityTagGenerator(output, event.getLookupProvider(), existingFileHelper));
    }
}
