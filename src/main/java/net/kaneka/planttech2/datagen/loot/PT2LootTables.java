package net.kaneka.planttech2.datagen.loot;

import com.google.common.collect.ImmutableList;
import net.minecraft.data.PackOutput;
import net.minecraft.data.loot.LootTableProvider;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.storage.loot.LootTable;
import net.minecraft.world.level.storage.loot.ValidationContext;
import net.minecraft.world.level.storage.loot.parameters.LootContextParamSets;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class PT2LootTables extends LootTableProvider
{
    private static final Set<ResourceLocation> SET = new HashSet<>();
    public PT2LootTables(PackOutput dataGeneratorIn)
    {
        super(dataGeneratorIn, SET, ImmutableList.of(new SubProviderEntry(() -> new PT2BlockLoots(SET), LootContextParamSets.BLOCK), new LootTableProvider.SubProviderEntry(() -> new PT2EntityLoots(SET), LootContextParamSets.ENTITY)));
    }

    @Override
    protected void validate(Map<ResourceLocation, LootTable> map, ValidationContext validationcontext)
    {

    }
}
