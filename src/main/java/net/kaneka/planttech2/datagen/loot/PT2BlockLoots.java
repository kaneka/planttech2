package net.kaneka.planttech2.datagen.loot;

import net.kaneka.planttech2.registries.ModBlocks;
import net.minecraft.advancements.critereon.EnchantmentPredicate;
import net.minecraft.advancements.critereon.ItemPredicate;
import net.minecraft.advancements.critereon.MinMaxBounds;
import net.minecraft.data.loot.LootTableSubProvider;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.enchantment.Enchantments;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.storage.loot.LootPool;
import net.minecraft.world.level.storage.loot.LootTable;
import net.minecraft.world.level.storage.loot.entries.AlternativesEntry;
import net.minecraft.world.level.storage.loot.entries.LootItem;
import net.minecraft.world.level.storage.loot.entries.LootPoolSingletonContainer;
import net.minecraft.world.level.storage.loot.functions.ApplyBonusCount;
import net.minecraft.world.level.storage.loot.functions.ApplyExplosionDecay;
import net.minecraft.world.level.storage.loot.predicates.ExplosionCondition;
import net.minecraft.world.level.storage.loot.predicates.MatchTool;
import net.minecraft.world.level.storage.loot.providers.number.ConstantValue;
import net.minecraftforge.registries.RegistryObject;

import java.util.Set;
import java.util.function.BiConsumer;

class PT2BlockLoots implements LootTableSubProvider
{
    private final Set<ResourceLocation> set;
    private BiConsumer<ResourceLocation, LootTable.Builder> consumer;

    public PT2BlockLoots(Set<ResourceLocation> set)
    {
        this.set = set;
    }

    @Override
    public void generate(BiConsumer<ResourceLocation, LootTable.Builder> consumer)
    {
        this.consumer = consumer;
        standardDropTable(ModBlocks.CABLE);
        standardDropTable(ModBlocks.CARVER);
        standardDropTable(ModBlocks.CHIPALYZER);
        standardDropTable(ModBlocks.COMPRESSOR);
        standardDropTable(ModBlocks.CROPBARS);
        standardDropTable(ModBlocks.DANCIUM_BLOCK);
        standardDropTable(ModBlocks.DNA_CLEANER);
        standardDropTable(ModBlocks.DNA_COMBINER);
        standardDropTable(ModBlocks.DNA_EXTRACTOR);
        standardDropTable(ModBlocks.DNA_REMOVER);
        standardDropTable(ModBlocks.ENERGY_SUPPLIER);
        standardDropTable(ModBlocks.IDENTIFIER);
        standardDropTable(ModBlocks.INFUSER);
        standardDropTable(ModBlocks.KANEKIUM_BLOCK);
        standardDropTable(ModBlocks.KINNOIUM_BLOCK);
        standardDropTable(ModBlocks.LENTHURIUM_BLOCK);
        standardDropTable(ModBlocks.MACHINEBULBREPROCESSOR);
        standardDropTable(ModBlocks.MACHINESHELL_IRON);
        standardDropTable(ModBlocks.MACHINESHELL_PLANTIUM);
        standardDropTable(ModBlocks.MEGAFURNACE);
        standardDropTable(ModBlocks.PLANTFARM);
        standardDropTable(ModBlocks.PLANTIUM_BLOCK);
        standardDropTable(ModBlocks.SEEDCONSTRUCTOR);
        standardDropTable(ModBlocks.SEEDSQUEEZER);
        standardDropTable(ModBlocks.SOLARGENERATOR);
        standardDropTable(ModBlocks.UNIVERSAL_SOIL);
        standardDropTable(ModBlocks.UNIVERSAL_SOIL_INFUSED);
        standardDropTable(ModBlocks.BROWN_MUSHROOM_SLAB);
        standardDropTable(ModBlocks.RED_MUSHROOM_SLAB);
        standardDropTable(ModBlocks.CRIMSON_HYPHAE_SLAB);
        standardDropTable(ModBlocks.WARPED_HYPHAE_SLAB);
        standardDropTable(ModBlocks.BROWN_MUSHROOM_STAIRS);
        standardDropTable(ModBlocks.RED_MUSHROOM_STAIRS);
        standardDropTable(ModBlocks.CRIMSON_HYPHAE_STAIRS);
        standardDropTable(ModBlocks.WARPED_HYPHAE_STAIRS);
        standardDropTable(ModBlocks.BROWN_MUSHROOM_TRAPDOOR);
        standardDropTable(ModBlocks.RED_MUSHROOM_TRAPDOOR);
        standardDropTable(ModBlocks.CRIMSON_HYPHAE_TRAPDOOR);
        standardDropTable(ModBlocks.WARPED_HYPHAE_TRAPDOOR);
        standardDropTable(ModBlocks.BROWN_MUSHROOM_CANDLE);
        standardDropTable(ModBlocks.RED_MUSHROOM_CANDLE);
        standardDropTable(ModBlocks.CRIMSON_HYPHAE_CANDLE);
        standardDropTable(ModBlocks.WARPED_HYPHAE_CANDLE);
        standardDropTable(ModBlocks.BROWN_MUSHROOM_FENCE);
        standardDropTable(ModBlocks.RED_MUSHROOM_FENCE);
        standardDropTable(ModBlocks.CRIMSON_HYPHAE_FENCE);
        standardDropTable(ModBlocks.WARPED_HYPHAE_FENCE);
        standardDropTable(ModBlocks.BROWN_MUSHROOM_FENCE_GATE);
        standardDropTable(ModBlocks.RED_MUSHROOM_FENCE_GATE);
        standardDropTable(ModBlocks.CRIMSON_HYPHAE_FENCE_GATE);
        standardDropTable(ModBlocks.WARPED_HYPHAE_FENCE_GATE);
        ModBlocks.HEDGE_BLOCKS.forEach(this::standardDropTable);
    }

    void silkBlockTable(RegistryObject<Block> b)
    {
        LootPool.Builder pool = LootPool.lootPool();
        pool.setRolls(ConstantValue.exactly(1));

        LootPoolSingletonContainer.Builder<?> silk = LootItem.lootTableItem(b.get())
                .when(MatchTool.toolMatches(ItemPredicate.Builder.item().hasEnchantment(new EnchantmentPredicate(
                        Enchantments.SILK_TOUCH, MinMaxBounds.Ints.atLeast(1)))));

        pool.add(AlternativesEntry.alternatives(silk));

        blockTable(b, LootTable.lootTable().withPool(pool));
    }

    void silkFortuneBlockTable(RegistryObject<Block> b, ItemLike item)
    {
        LootPool.Builder pool = LootPool.lootPool();
        pool.setRolls(ConstantValue.exactly(1));

        LootPoolSingletonContainer.Builder<?> silk = LootItem.lootTableItem(b.get())
                .when(MatchTool.toolMatches(ItemPredicate.Builder.item().hasEnchantment(new EnchantmentPredicate(
                        Enchantments.SILK_TOUCH, MinMaxBounds.Ints.atLeast(1)))));
        LootPoolSingletonContainer.Builder<?> fortune = LootItem.lootTableItem(item).apply(ApplyExplosionDecay.explosionDecay())
                .apply(ApplyBonusCount.addOreBonusCount(Enchantments.BLOCK_FORTUNE));

        pool.add(AlternativesEntry.alternatives(silk, fortune));

        blockTable(b, LootTable.lootTable().withPool(pool));
    }

    void standardDropTable(RegistryObject<Block> registryObject)
    {
        blockTable(registryObject, LootTable.lootTable().withPool(createStandardDrops(registryObject.get())));
    }

    void blockTable(RegistryObject<Block> b, LootTable.Builder lootTable)
    {
        ResourceLocation r = b.get().getLootTable();
        consumer.accept(r, lootTable);
        set.add(r);
    }

    LootPool.Builder createStandardDrops(ItemLike itemProvider)
    {
        return LootPool.lootPool().setRolls(ConstantValue.exactly(1)).when(ExplosionCondition.survivesExplosion())
                .add(LootItem.lootTableItem(itemProvider));
    }
}