package net.kaneka.planttech2.datagen.loot;

import net.kaneka.planttech2.entities.passive.snail.SnailEntity;
import net.kaneka.planttech2.registries.ModEntityTypes;
import net.minecraft.data.loot.EntityLootSubProvider;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.flag.FeatureFlags;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.storage.loot.LootPool;
import net.minecraft.world.level.storage.loot.LootTable;
import net.minecraft.world.level.storage.loot.entries.LootItem;
import net.minecraft.world.level.storage.loot.functions.SetItemCountFunction;
import net.minecraft.world.level.storage.loot.providers.number.BinomialDistributionGenerator;
import net.minecraft.world.level.storage.loot.providers.number.ConstantValue;
import net.minecraft.world.level.storage.loot.providers.number.UniformGenerator;

import java.util.Set;
import java.util.function.BiConsumer;

public class PT2EntityLoots extends EntityLootSubProvider
{
    private final Set<ResourceLocation> set;

    protected PT2EntityLoots(Set<ResourceLocation> set)
    {
        super(FeatureFlags.REGISTRY.allFlags());
        this.set = set;
    }

    @Override
    public void generate()
    {

    }

    @Override
    public void generate(BiConsumer<ResourceLocation, LootTable.Builder> consumer)
    {
        for (SnailEntity.SnailType type : SnailEntity.SnailType.values())
        {
            consumer.accept(type.loottable, LootTable.lootTable()
                    .withPool(LootPool.lootPool()
                            .setRolls(ConstantValue.exactly(1.0F))
                            .add(LootItem.lootTableItem(Items.SLIME_BALL).setWeight(1)
                                    .apply(SetItemCountFunction.setCount(UniformGenerator.between(0, 1))))
                            .add(LootItem.lootTableItem(type.crackedShell.get()).setWeight(8)
                                    .apply(SetItemCountFunction.setCount(BinomialDistributionGenerator.binomial(4, 0.1F))))));
        }
        consumer.accept(ModEntityTypes.PLANT_MITE.get().getDefaultLootTable(), LootTable.lootTable()
                .withPool(LootPool.lootPool()
                        .setRolls(ConstantValue.exactly(1.0F))
                        .add(LootItem.lootTableItem(Items.SLIME_BALL).setWeight(1)
                                .apply(SetItemCountFunction.setCount(UniformGenerator.between(0, 1))))
                        .add(LootItem.lootTableItem(Items.SUGAR).setWeight(9)
                                .apply(SetItemCountFunction.setCount(UniformGenerator.between(0, 1))))));
    }

    @Override
    protected void add(EntityType<?> p_252130_, ResourceLocation p_251706_, LootTable.Builder p_249357_)
    {
        super.add(p_252130_, p_251706_, p_249357_);
        set.add(p_251706_);
    }
}
