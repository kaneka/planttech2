package net.kaneka.planttech2.datagen;

import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.crops.CropTypes;
import net.kaneka.planttech2.utilities.PTClientUtil;
import net.minecraft.data.PackOutput;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.ItemLike;
import net.minecraftforge.client.model.generators.ItemModelBuilder;
import net.minecraftforge.client.model.generators.ItemModelProvider;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.registries.ForgeRegistries;

import javax.annotation.Nullable;

import static net.kaneka.planttech2.registries.ModItems.*;

public class ItemModels extends ItemModelProvider
{
	protected final ResourceLocation generated = mcLoc("generated");
	protected final ResourceLocation handheld = mcLoc("handheld");

	public ItemModels(PackOutput generator, ExistingFileHelper existingFileHelper)
	{
		super(generator, PlantTechMain.MODID, existingFileHelper);
	}

	@Override
	protected void registerModels()
	{
		addCrops();
		addBulbs();
		addBiomassContainer();
		addChipsUpgrades();

		singleTexturedItem(RAKE.get());
		CATALYSTS.get().keySet().forEach(this::singleTexturedItem);
		singleTexturedItem(CRACKED_SNAIL_SHELL_BLACK.get());
		singleTexturedItem(CRACKED_SNAIL_SHELL_CYBER.get());
		singleTexturedItem(CRACKED_SNAIL_SHELL_GARDEN.get());
		singleTexturedItem(CRACKED_SNAIL_SHELL_MILK.get());
		singleTexturedItem(CRACKED_SNAIL_SHELL_PINK.get());
		singleTexturedItem(SNAIL_SHELL_DUST_BLACK.get());
		singleTexturedItem(SNAIL_SHELL_DUST_CYBER.get());
		singleTexturedItem(SNAIL_SHELL_DUST_GARDEN.get());
		singleTexturedItem(SNAIL_SHELL_DUST_MILK.get());
		singleTexturedItem(SNAIL_SHELL_DUST_PINK.get());
		singleTexturedItem(ADVANCED_ANALYSER.get());
		singleTexturedItem(ANALYSER.get());
		singleTexturedItem(BIOMASS.get());
		singleTexturedItem(BIOMASS_BUCKET.get());
		singleTexture("cable", generated, "layer0", itemPrefix("cable"));
		singleTexturedItem(COLOR_PARTICLES.get());
		// TODO: crop? crop_seeds?
		singleTexturedItem(CROPREMOVER.get());
		singleTexturedItem(CYBERARMOR_BOOTS.get(), "armor/cyberarmor_boots");
		singleTexturedItem(CYBERARMOR_CHEST.get(), "armor/cyberarmor_chest");
		singleTexturedItem(CYBERARMOR_HELMET.get(), "armor/cyberarmor_helmet");
		singleTexturedItem(CYBERARMOR_LEGGINGS.get(), "armor/cyberarmor_leggings");
		// TODO: cyberbow
		singleTexturedItem(CYBERDAGGER.get(), handheld, "weapons/cyberdagger");
		singleTexturedItem(CYBERKATANA.get(), handheld, "weapons/cyberkatana");
		singleTexturedItem(CYBERRAPIER.get(), handheld, "weapons/cyberrapier");
		singleTexturedItem(DANCIUM_INGOT.get());
		singleTexturedItem(DANCIUM_NUGGET.get());
//		singleTexturedItem(DARK_CRYSTAL.get());
		singleTexturedItem(ENERGYSTORAGE_TIER_1.get());
		singleTexturedItem(ENERGYSTORAGE_TIER_2.get());
		singleTexturedItem(ENERGYSTORAGE_TIER_3.get());
		singleTexturedItem(FERTILIZER_TIER_1.get(), "fert_1");
		singleTexturedItem(FERTILIZER_TIER_2.get(), "fert_2");
		singleTexturedItem(FERTILIZER_TIER_3.get(), "fert_3");
		singleTexturedItem(FERTILIZER_TIER_4.get(), "fert_4");
		singleTexturedItem(FERTILIZER_CREATIVE.get(), "fert_creative");
		// TODO: dna_container ; gears ; guides
		// TODO: more kanekium
		singleTexturedItem(KANEKIUM_INGOT.get());
		singleTexturedItem(KANEKIUM_NUGGET.get());
		singleTexturedItem(KINNOIUM_INGOT.get());
		singleTexturedItem(KINNOIUM_NUGGET.get());
		singleTexturedItem(LENTHURIUM_INGOT.get());
		singleTexturedItem(LENTHURIUM_NUGGET.get());
		// TODO: multitool ; mutated plants
		// TODO: particles?
		// TODO: plant obtainer
//		singleTexturedItem(PLANTCARD.get());
		singleTexturedItem(PLANTIUM_INGOT.get());
		singleTexturedItem(PLANTIUM_NUGGET.get());
		singleTexturedItem(RANGEUPGRADE_TIER_1.get());
		singleTexturedItem(RANGEUPGRADE_TIER_2.get());
		singleTexturedItem(RANGEUPGRADE_TIER_3.get());
		singleTexturedItem(RANGEUPGRADE_TIER_4.get());
		singleTexturedItem(REDSTONE_INFUSED.get());
		singleTexturedItem(TESTITEM.get(), mcLoc(ITEM_FOLDER + "/barrier"));
		for (int i = 0; i < 5; i++)
			singleTexture("thermometer_" + i, mcLoc("item/generated"), "layer0", new ResourceLocation(PlantTechMain.MODID, "item/thermometer_" + i));
	}

	private void addCrops()
	{
		final ResourceLocation seedsTexture = itemPrefix("seeds");
		final ResourceLocation particlesTexture = itemPrefix("particles");
		CropTypes.crops().forEach((entry) -> {
			String name = entry.getName();
			singleTexture(name + "_seeds", generated, "layer0", seedsTexture);
			if (entry.hasParticle())
				singleTexture(name + "_particles", generated, "layer0", particlesTexture);
		});
	}

	private void addBiomassContainer()
	{
		final String basePath = ForgeRegistries.ITEMS.getKey(BIOMASSCONTAINER.get()).getPath();
		ItemModelBuilder baseContainerItem = withExistingParent(basePath, generated);
		ItemModelBuilder empty = withExistingParent(basePath + "_empty", generated);

		baseContainerItem.override().predicate(PTClientUtil.FILLED_PREDICATE, 0).model(empty);
		baseContainerItem.texture("layer0", itemPrefix("biomasscontainer_empty"));
		empty.texture("layer0", itemPrefix("biomasscontainer_empty"));

		int max = 9;
		for (int i = 0; i < max; i++)
		{
			ItemModelBuilder override = withExistingParent(basePath + "_" + i, generated);
			override.texture("layer0", itemPrefix("biomasscontainer_" + i));
			baseContainerItem.override().predicate(PTClientUtil.FILLED_PREDICATE, (float) i + 1).model(override);
		}
	}

	private void addChipsUpgrades()
	{
		singleTexturedItem(AQUA_AFFINITY_CHIP.get(), "chips/enchantment_chip");
		singleTexturedItem(ARMORCHIP_TIER_1.get(), "chips/armor_tier_1");
		singleTexturedItem(ARMORCHIP_TIER_2.get(), "chips/armor_tier_2");
		singleTexturedItem(ARMORCHIP_TIER_3.get(), "chips/armor_tier_3");
		singleTexturedItem(ATTACKCHIP_TIER_1.get(), "chips/attack_tier_1");
		singleTexturedItem(ATTACKCHIP_TIER_2.get(), "chips/attack_tier_2");
		singleTexturedItem(ATTACKCHIP_TIER_3.get(), "chips/attack_tier_3");
		singleTexturedItem(ATTACKSPEEDCHIP_TIER_1.get(), "chips/attackspeed_tier_1");
		singleTexturedItem(ATTACKSPEEDCHIP_TIER_2.get(), "chips/attackspeed_tier_2");
		singleTexturedItem(ATTACKSPEEDCHIP_TIER_3.get(), "chips/attackspeed_tier_3");
		singleTexturedItem(BLAST_PROTECTION_CHIP.get(), "chips/enchantment_chip");
		singleTexturedItem(BANE_OF_ARTHROPODS_CHIP.get(), "chips/enchantment_chip");
		singleTexturedItem(BREAKDOWNRATECHIP_TIER_1.get(), "chips/harvestspeed_tier_1");
		singleTexturedItem(BREAKDOWNRATECHIP_TIER_2.get(), "chips/harvestspeed_tier_2");
		singleTexturedItem(BREAKDOWNRATECHIP_TIER_3.get(), "chips/harvestspeed_tier_3");
		singleTexturedItem(CAPACITYCHIP_TIER_1.get(), "chips/capacity_tier_1");
		singleTexturedItem(CAPACITYCHIP_TIER_2.get(), "chips/capacity_tier_2");
		singleTexturedItem(CAPACITYCHIP_TIER_3.get(), "chips/capacity_tier_3");
		singleTexturedItem(CAPACITYUPGRADE_TIER_1.get());
		singleTexturedItem(CAPACITYUPGRADE_TIER_2.get());
		singleTexturedItem(CAPACITYUPGRADE_TIER_3.get());
		singleTexturedItem(CAPACITYUPGRADE_TIER_4.get());
		singleTexturedItem(CHIP_UPGRADEPACK_CAPACITY_1.get(), "chips/upgradepack/capacity_1");
		singleTexturedItem(CHIP_UPGRADEPACK_CAPACITY_2.get(), "chips/upgradepack/capacity_2");
		singleTexturedItem(CHIP_UPGRADEPACK_HARVESTLEVEL_1.get(), "chips/upgradepack/harvestlevel_1");
		singleTexturedItem(CHIP_UPGRADEPACK_HARVESTLEVEL_2.get(), "chips/upgradepack/harvestlevel_2");
		singleTexturedItem(CHIP_UPGRADEPACK_REACTOR_1.get(), "chips/upgradepack/reactor_1");
		singleTexturedItem(CHIP_UPGRADEPACK_REACTOR_2.get(), "chips/upgradepack/reactor_2");
		singleTexturedItem(DEPTH_STRIDER_CHIP.get(), "chips/enchantment_chip");
		singleTexturedItem(EFFICIENCY_CHIP.get(), "chips/enchantment_chip");
		singleTexturedItem(EMPTY_UPGRADECHIP_TIER_1.get(), "chips/template_tier_1");
		singleTexturedItem(EMPTY_UPGRADECHIP_TIER_2.get(), "chips/template_tier_2");
		singleTexturedItem(EMPTY_UPGRADECHIP_TIER_3.get(), "chips/template_tier_3");
		singleTexturedItem(FEATHER_FALLING_CHIP.get(), "chips/enchantment_chip");
		singleTexturedItem(FIRE_ASPECT_CHIP.get(), "chips/enchantment_chip");
		singleTexturedItem(FIRE_PROTECTION_CHIP.get(), "chips/enchantment_chip");
		singleTexturedItem(FLAME_CHIP.get(), "chips/enchantment_chip");
		singleTexturedItem(FORTUNE_CHIP.get(), "chips/enchantment_chip");
		singleTexturedItem(FROST_WALKER_CHIP.get(), "chips/enchantment_chip");
		singleTexturedItem(HARVESTLEVELCHIP_TIER_1.get(), "chips/harvestlevel_tier_1");
		singleTexturedItem(HARVESTLEVELCHIP_TIER_2.get(), "chips/harvestlevel_tier_2");
		singleTexturedItem(HARVESTLEVELCHIP_TIER_3.get(), "chips/harvestlevel_tier_3");
		singleTexturedItem(INFINITY_CHIP.get(), "chips/enchantment_chip");
		singleTexturedItem(KNOCKBACK_CHIP.get(), "chips/enchantment_chip");
		singleTexturedItem(KNOWLEDGECHIP_TIER_0.get());
		singleTexturedItem(KNOWLEDGECHIP_TIER_1.get());
		singleTexturedItem(KNOWLEDGECHIP_TIER_2.get());
		singleTexturedItem(KNOWLEDGECHIP_TIER_3.get());
		singleTexturedItem(KNOWLEDGECHIP_TIER_4.get());
		singleTexturedItem(KNOWLEDGECHIP_TIER_5.get());
		singleTexturedItem(LOOTING_CHIP.get(), "chips/enchantment_chip");
		singleTexturedItem(POWER_CHIP.get(), "chips/enchantment_chip");
		singleTexturedItem(PROJECTILE_PROTECTION_CHIP.get(), "chips/enchantment_chip");
		singleTexturedItem(PROTECTION_CHIP.get(), "chips/enchantment_chip");
		singleTexturedItem(PUNCH_CHIP.get(), "chips/enchantment_chip");
		singleTexturedItem(REACTORCHIP_TIER_1.get(), "chips/reactor_tier_1");
		singleTexturedItem(REACTORCHIP_TIER_2.get(), "chips/reactor_tier_2");
		singleTexturedItem(REACTORCHIP_TIER_3.get(), "chips/reactor_tier_3");
		singleTexturedItem(RESPIRATION_CHIP.get(), "chips/enchantment_chip");
		singleTexturedItem(SHARPNESS_CHIP.get(), "chips/enchantment_chip");
		singleTexturedItem(SILK_TOUCH_CHIP.get(), "chips/enchantment_chip");
		singleTexturedItem(SMITE_CHIP.get(), "chips/enchantment_chip");
		singleTexturedItem(SOLARFOCUS_TIER_1.get(), "solarfocus_tier_1");
		singleTexturedItem(SOLARFOCUS_TIER_2.get(), "solarfocus_tier_2");
		singleTexturedItem(SOLARFOCUS_TIER_3.get(), "solarfocus_tier_3");
		singleTexturedItem(SOLARFOCUS_TIER_4.get(), "solarfocus_tier_4");
		singleTexturedItem(SPEEDUPGRADE_TIER_1.get());
		singleTexturedItem(SPEEDUPGRADE_TIER_2.get());
		singleTexturedItem(SPEEDUPGRADE_TIER_3.get());
		singleTexturedItem(SPEEDUPGRADE_TIER_4.get());
		singleTexturedItem(SWEEPING_CHIP.get(), "chips/enchantment_chip");
		singleTexturedItem(THORNS_CHIP.get(), "chips/enchantment_chip");
		singleTexturedItem(TOUGHNESSCHIP_TIER_1.get(), "chips/toughness_tier_1");
		singleTexturedItem(TOUGHNESSCHIP_TIER_2.get(), "chips/toughness_tier_2");
		singleTexturedItem(TOUGHNESSCHIP_TIER_3.get(), "chips/toughness_tier_3");
		singleTexturedItem(UNBREAKING_CHIP.get(), "chips/enchantment_chip");
		singleTexturedItem(UNLOCKCHIP_AXE.get(), "chips/axe");
		singleTexturedItem(UNLOCKCHIP_HOE.get(), "chips/hoe");
		singleTexturedItem(UNLOCKCHIP_SHEARS.get(), "chips/shears");
		singleTexturedItem(UNLOCKCHIP_SHOVEL.get(), "chips/shovel");
	}

	private void addBulbs()
	{
		bulbItem("chipalyzer");
		bulbItem("compressor");
		bulbItem("crop_aura_generator");
		bulbItem("dna_cleaner");
		bulbItem("dna_combiner");
		bulbItem("dna_extractor");
		bulbItem("dna_remover");
		bulbItem("energy_supplier");
		bulbItem("identifier");
		bulbItem("infuser");
		bulbItem("machinebulbreprocessor");
		bulbItem("mega_furnace");
		bulbItem("plantfarm");
		bulbItem("seedconstructor");
		bulbItem("seedsqueezer");
		bulbItem("solargenerator");
	}

	private void bulbItem(String item)
	{
		ItemModelBuilder model = withExistingParent(item + "_bulb", generated);
		model.texture("layer0", itemPrefix("machinebulbs/machinebulb_layer_1"));
		model.texture("layer1", itemPrefix("machinebulbs/machinebulb_layer_2_" + item));
	}

	// Creates model for given item
	private void singleTexturedItem(ItemLike item)
	{
		singleTexturedItem(item, (ResourceLocation) null);
	}

	// Creates model for given item, with given texture path
	private void singleTexturedItem(ItemLike item, String texturePath)
	{
		singleTexturedItem(item, generated, texturePath);
	}

	// Creates model for given item, with given parent and texture path
	private void singleTexturedItem(ItemLike item, ResourceLocation parent, String texturePath)
	{
		singleTexturedItem(item, parent, itemPrefix(texturePath));
	}

	// Creates model for given item, with parent `item/generated` and optional texture
	private void singleTexturedItem(ItemLike item, @Nullable ResourceLocation textureLocation)
	{
		singleTexturedItem(item, generated, textureLocation);
	}

	// Creates model for given item, with given parent and optional texture (defaults to "item/" + item name)
	private void singleTexturedItem(ItemLike item, ResourceLocation parent, @Nullable ResourceLocation textureLocation)
	{
		final ResourceLocation loc = ForgeRegistries.ITEMS.getKey(item.asItem());
		if (textureLocation == null)
		{
			textureLocation = itemLoc(loc, "");
		}
		singleTexture(loc.getPath(), parent, "layer0", textureLocation);
	}

	// Helper function to prefix "item/" to a ResourceLocation's path
	private ResourceLocation itemLoc(ResourceLocation loc, String subPath)
	{
		return new ResourceLocation(loc.getNamespace(), "item/" + subPath + loc.getPath());
	}

	private ResourceLocation itemPrefix(String path)
	{
		return new ResourceLocation(PlantTechMain.MODID, "item/" + path);
	}
}
