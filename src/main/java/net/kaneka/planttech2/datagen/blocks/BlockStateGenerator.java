package net.kaneka.planttech2.datagen.blocks;

import net.kaneka.planttech2.PlantTechMain;
import net.minecraft.data.PackOutput;
import net.minecraftforge.client.model.generators.BlockStateProvider;
import net.minecraftforge.common.data.ExistingFileHelper;

public class BlockStateGenerator extends BlockStateProvider
{
	private final CropModels crops;
	private final HedgeModels hedges;
	private final BuildingBlockModels buildingBlocks;
	private final CarverModels carver;
	private final ElectricFenceModels electric_fence;
	private final MachineModels machines;

	public BlockStateGenerator(PackOutput gen, ExistingFileHelper exFileHelper)
	{
		super(gen, PlantTechMain.MODID, exFileHelper);
		crops = new CropModels(this, exFileHelper);
		hedges = new HedgeModels(this, exFileHelper);
		buildingBlocks = new BuildingBlockModels(this, exFileHelper);
		carver = new CarverModels(this, exFileHelper);
		electric_fence = new ElectricFenceModels(this, exFileHelper);
		machines = new MachineModels(this, exFileHelper);
	}

	@Override
	protected void registerStatesAndModels()
	{
		crops.registerStatesAndModels();
		hedges.registerStatesAndModels();
		buildingBlocks.registerStatesAndModels();
		carver.registerStatesAndModels();
		electric_fence.registerStatesAndModels();
		machines.registerStatesAndModels();
	}
}
