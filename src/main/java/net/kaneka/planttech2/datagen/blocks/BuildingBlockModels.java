package net.kaneka.planttech2.datagen.blocks;

import net.kaneka.planttech2.registries.ModBlocks;
import net.minecraft.core.Direction;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.CandleBlock;
import net.minecraft.world.level.block.FenceBlock;
import net.minecraft.world.level.block.FenceGateBlock;
import net.minecraft.world.level.block.RotatedPillarBlock;
import net.minecraft.world.level.block.SlabBlock;
import net.minecraft.world.level.block.StairBlock;
import net.minecraft.world.level.block.TrapDoorBlock;
import net.minecraftforge.client.model.generators.BlockModelBuilder;
import net.minecraftforge.client.model.generators.ConfiguredModel;
import net.minecraftforge.client.model.generators.ModelFile;
import net.minecraftforge.client.model.generators.VariantBlockStateBuilder;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.registries.ForgeRegistries;

public class BuildingBlockModels extends BlockModelBase
{
	BuildingBlockModels(BlockStateGenerator states, ExistingFileHelper fileHelper)
	{
		super(states, fileHelper);
	}

	@Override
	public void registerStatesAndModels() {
		ResourceLocation redShroom = new ResourceLocation("block/red_mushroom_block");
		states.stairsBlock((StairBlock) ModBlocks.RED_MUSHROOM_STAIRS.get(), redShroom);
		states.slabBlock((SlabBlock) ModBlocks.RED_MUSHROOM_SLAB.get(), new ResourceLocation("block/red_mushroom_block_inventory"), redShroom);
		states.trapdoorBlock((TrapDoorBlock) ModBlocks.RED_MUSHROOM_TRAPDOOR.get(), redShroom, true);
		states.fenceGateBlock((FenceGateBlock) ModBlocks.RED_MUSHROOM_FENCE_GATE.get(), redShroom);
		states.fenceBlock((FenceBlock) ModBlocks.RED_MUSHROOM_FENCE.get(), redShroom);
		states.models().withExistingParent("red_mushroom_fence_inventory", "block/fence_inventory")
				.texture("texture", redShroom);
		candle(ModBlocks.RED_MUSHROOM_CANDLE.get());

		ResourceLocation brownShroom = new ResourceLocation("block/brown_mushroom_block");
		states.stairsBlock((StairBlock) ModBlocks.BROWN_MUSHROOM_STAIRS.get(), brownShroom);
		states.slabBlock((SlabBlock) ModBlocks.BROWN_MUSHROOM_SLAB.get(), new ResourceLocation("block/brown_mushroom_block_inventory"), brownShroom);
		states.trapdoorBlock((TrapDoorBlock) ModBlocks.BROWN_MUSHROOM_TRAPDOOR.get(), brownShroom, true);
		states.fenceGateBlock((FenceGateBlock) ModBlocks.BROWN_MUSHROOM_FENCE_GATE.get(), brownShroom);
		states.fenceBlock((FenceBlock) ModBlocks.BROWN_MUSHROOM_FENCE.get(), brownShroom);
		states.models().withExistingParent("brown_mushroom_fence_inventory", "block/fence_inventory")
				.texture("texture", brownShroom);
		candle(ModBlocks.BROWN_MUSHROOM_CANDLE.get());

		ResourceLocation crimsonHyphae = new ResourceLocation("block/crimson_stem");
		states.stairsBlock((StairBlock) ModBlocks.CRIMSON_HYPHAE_STAIRS.get(), crimsonHyphae);
		states.slabBlock((SlabBlock) ModBlocks.CRIMSON_HYPHAE_SLAB.get(), new ResourceLocation("block/crimson_hyphae"), crimsonHyphae);
		states.trapdoorBlock((TrapDoorBlock) ModBlocks.CRIMSON_HYPHAE_TRAPDOOR.get(), crimsonHyphae, true);
		states.fenceGateBlock((FenceGateBlock) ModBlocks.CRIMSON_HYPHAE_FENCE_GATE.get(), crimsonHyphae);
		states.fenceBlock((FenceBlock) ModBlocks.CRIMSON_HYPHAE_FENCE.get(), crimsonHyphae);
		states.models().withExistingParent("crimson_hyphae_fence_inventory", "block/fence_inventory")
				.texture("texture", crimsonHyphae);
		candle(ModBlocks.CRIMSON_HYPHAE_CANDLE.get());

		ResourceLocation warpedHyphae = new ResourceLocation("block/warped_stem");
		states.stairsBlock((StairBlock) ModBlocks.WARPED_HYPHAE_STAIRS.get(), warpedHyphae);
		states.slabBlock((SlabBlock) ModBlocks.WARPED_HYPHAE_SLAB.get(), new ResourceLocation("block/warped_hyphae"), warpedHyphae);
		states.trapdoorBlock((TrapDoorBlock) ModBlocks.WARPED_HYPHAE_TRAPDOOR.get(), warpedHyphae, true);
		states.fenceGateBlock((FenceGateBlock) ModBlocks.WARPED_HYPHAE_FENCE_GATE.get(), warpedHyphae);
		states.fenceBlock((FenceBlock) ModBlocks.WARPED_HYPHAE_FENCE.get(), warpedHyphae);
		states.models().withExistingParent("warped_hyphae_fence_inventory", "block/fence_inventory")
				.texture("texture", warpedHyphae);
		candle(ModBlocks.WARPED_HYPHAE_CANDLE.get());
	}

	private void candle(Block block)
	{
		for (int i=0;i<4;)
		{
			String candles;
			if (i == 0)
				candles = "";
			else if (i == 1)
				candles = "_two";
			else if (i == 2)
				candles = "_three";
			else candles = "_four";
			String name = "planttech2:block/" + ForgeRegistries.BLOCKS.getKey(block).getPath() + candles;
			String template = "minecraft:block/template" + candles + (i > 0 ? "_candles" : "_candle");
			String texture = "planttech2:block/building_blocks/mushrooms/" + ForgeRegistries.BLOCKS.getKey(block).getPath();
			models().withExistingParent(name, template)
					.texture("all", texture)
					.texture("particle", texture);
			models().withExistingParent(name + "_lit", template)
					.texture("all", texture + "_lit")
					.texture("particle", texture + "_lit");
			int count = ++i;
			candleState(candleState(states.getVariantBuilder(block).partialState(), name, count, true).partialState(), name, count, false);
		}
	}

	private VariantBlockStateBuilder.PartialBlockstate candleState(VariantBlockStateBuilder.PartialBlockstate state, String name, int count, boolean lit)
	{
		return state.with(CandleBlock.CANDLES, count).with(CandleBlock.LIT, lit).addModels(new ConfiguredModel(new ModelFile.UncheckedModelFile(name + (lit ? "_lit" : ""))));
	}

	private void simpleBlockItem(Block block, String texturePath)
	{
		simpleBlock(block, texturePath);
		blockItem(block, models().cubeAll("block/building_blocks/" + ForgeRegistries.BLOCKS.getKey(block).getPath(), states.modLoc(texturePath)));
	}

	private void simpleBlock(Block block, String texturePath)
	{
		String name = ForgeRegistries.BLOCKS.getKey(block).getPath();
		states.simpleBlock(block, models().cubeAll("block/building_blocks/" + name, states.modLoc(texturePath)));
	}

	private void pillarBlock(Block block, String textureSide, String textureTopButtom)
	{
		String name = ForgeRegistries.BLOCKS.getKey(block).getPath();
		BlockModelBuilder vertical = models().cubeColumn("block/building_blocks/" + name, states.modLoc(textureSide), states.modLoc(textureTopButtom));
		BlockModelBuilder horizontal = models().cubeColumnHorizontal("block/building_blocks/" + name, states.modLoc(textureSide), states.modLoc(textureTopButtom));

		states.getVariantBuilder(block)
				.partialState().with(RotatedPillarBlock.AXIS, Direction.Axis.Y)
				.modelForState().modelFile(vertical).addModel()
				.partialState().with(RotatedPillarBlock.AXIS, Direction.Axis.Z)
				.modelForState().modelFile(horizontal).rotationX(90).addModel()
				.partialState().with(RotatedPillarBlock.AXIS, Direction.Axis.X)
				.modelForState().modelFile(horizontal).rotationX(90).rotationY(90).addModel();

		blockItem(block, vertical);

	}
}
