package net.kaneka.planttech2.datagen.blocks;

import net.kaneka.planttech2.blocks.CropBaseBlock;
import net.kaneka.planttech2.crops.CropTypes;
import net.minecraftforge.client.model.generators.BlockModelBuilder;
import net.minecraftforge.client.model.generators.ModelFile;
import net.minecraftforge.client.model.generators.VariantBlockStateBuilder;
import net.minecraftforge.common.data.ExistingFileHelper;
import org.apache.commons.lang3.Validate;

public class CropModels extends BlockModelBase
{
	CropModels(BlockStateGenerator states, ExistingFileHelper fileHelper)
	{
		super(states, fileHelper);
	}

	@Override
	public void registerStatesAndModels()
	{
		ModelFile cropBase = models().getExistingFile(models().modLoc("block/basic/crops"));
		for (int i = 0; i < 8; i++)
		{
			String particles = "block/" + (i < 3 ? "empty" : "crop_" + i + "_blooms");
			models().getBuilder("crop_" + i)
					.parent(cropBase)
					.texture("bars", models().modLoc("block/cropbars"))
					.texture("crops", models().modLoc("block/crop_" + i))
					// These particles are not Minecraft block break particles but the plant's texture
					.texture("particles", models().modLoc(particles));
			models().getBuilder("crop_copper_" + i)
					.parent(cropBase)
					.texture("bars", models().modLoc("block/cropbars_copper"))
					.texture("crops", models().modLoc("block/crop_" + i))
					// These particles are not Minecraft block break particles but the plant's texture
					.texture("particles", models().modLoc(particles));
		}
		CropTypes.crops().forEach((crop) -> {
			CropBaseBlock b = (CropBaseBlock) crop.getCropBlock();
			Validate.notNull(b, "Crop list entry %s does not have corresponding block", cropBase);
			VariantBlockStateBuilder.PartialBlockstate state = states.getVariantBuilder(b).partialState();
			for (int i = 0; i < 8; i++)
			{
				state.with(CropBaseBlock.GROWSTATE, i).with(CropBaseBlock.COPPER, true).modelForState().modelFile(new BlockModelBuilder.ExistingModelFile(models().getBuilder("crop_copper_" + i).getLocation(), fileHelper)).addModel();
				state.with(CropBaseBlock.GROWSTATE, i).with(CropBaseBlock.COPPER, false).modelForState().modelFile(new BlockModelBuilder.ExistingModelFile(models().getBuilder("crop_" + i).getLocation(), fileHelper)).addModel();
			}
		});
	}
}
