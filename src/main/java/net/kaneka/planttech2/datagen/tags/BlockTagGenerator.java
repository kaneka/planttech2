package net.kaneka.planttech2.datagen.tags;

import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.crops.CropTypes;
import net.kaneka.planttech2.registries.ModBlocks;
import net.kaneka.planttech2.utilities.PTCommonUtil;
import net.minecraft.core.HolderLookup;
import net.minecraft.data.PackOutput;
import net.minecraft.tags.BlockTags;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.common.data.BlockTagsProvider;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.registries.RegistryObject;

import javax.annotation.Nullable;
import java.util.concurrent.CompletableFuture;

public class BlockTagGenerator extends BlockTagsProvider
{
    public BlockTagGenerator(PackOutput generator, CompletableFuture<HolderLookup.Provider> lookupProvider, @Nullable ExistingFileHelper existingFileHelper)
    {
        super(generator, lookupProvider, PlantTechMain.MODID, existingFileHelper);
    }

    @Override
    protected void addTags(HolderLookup.Provider provider)
    {
        tag(BlockTags.WOODEN_FENCES).add(ModBlocks.RED_MUSHROOM_FENCE.get(), ModBlocks.BROWN_MUSHROOM_FENCE.get(), ModBlocks.CRIMSON_HYPHAE_FENCE.get(), ModBlocks.WARPED_HYPHAE_FENCE.get());
        tag(BlockTags.CANDLES).add(ModBlocks.RED_MUSHROOM_CANDLE.get(), ModBlocks.BROWN_MUSHROOM_CANDLE.get(), ModBlocks.CRIMSON_HYPHAE_CANDLE.get(), ModBlocks.WARPED_HYPHAE_CANDLE.get());
        tag(BlockTags.FLOWERS).add(PTCommonUtil.collect(CropTypes.CROP_BLOCKS.values(), RegistryObject::get).toArray(new Block[0]));
    }
}
