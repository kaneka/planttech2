package net.kaneka.planttech2.datagen.tags;

import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.registries.ModEntityTypes;
import net.minecraft.core.HolderLookup;
import net.minecraft.data.PackOutput;
import net.minecraft.data.tags.EntityTypeTagsProvider;
import net.minecraft.tags.EntityTypeTags;
import net.minecraftforge.common.data.ExistingFileHelper;
import org.jetbrains.annotations.Nullable;

import java.util.concurrent.CompletableFuture;

public class EntityTagGenerator extends EntityTypeTagsProvider
{
    public EntityTagGenerator(PackOutput output, CompletableFuture<HolderLookup.Provider> lookup, @Nullable ExistingFileHelper existingFileHelper)
    {
        super(output, lookup, PlantTechMain.MODID, existingFileHelper);
    }

    @Override
    protected void addTags(HolderLookup.Provider lookup)
    {
        tag(EntityTypeTags.FROG_FOOD).add(ModEntityTypes.SNAIL.get());
    }
}
