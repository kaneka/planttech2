package net.kaneka.planttech2.datagen;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.crops.CropTypes;
import net.kaneka.planttech2.enums.CropTraitsTypes;
import net.kaneka.planttech2.registries.ModBlocks;
import net.kaneka.planttech2.registries.ModEntityTypes;
import net.minecraft.data.PackOutput;
import net.minecraftforge.common.data.LanguageProvider;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

import static net.kaneka.planttech2.PlantTechMain.MODID;
import static net.kaneka.planttech2.registries.ModItems.*;

// Migrate so shares english as default translation
public class Languages extends LanguageProvider
{
    public Languages(PackOutput gen)
    {
        super(gen, MODID, "en_us");
    }

    @Override
    protected void addTranslations()
    {
        addBlocks();
        addHedges(); 
        addCrops();
        addGuides();
        addGUIs();
        addItems();
        addItemGroups();
        addInfo();
        addMisc();
        addEntities();
    }

    private void addEntities()
    {
        addEntityType(ModEntityTypes.TECH_TRADER, "Plant Tech Trader");
        addEntityType(ModEntityTypes.SNAIL, "Snail");
        addEntityType(ModEntityTypes.PLANT_MITE, "Plant Mite");
    }

    protected String forDisplay(String name)
    {
        StringBuilder nameBuilder = new StringBuilder(name.length());
        String[] s = name.split("_");
        for (String n : s)
        {
            n = StringUtils.capitalize(n.toLowerCase());
            if (nameBuilder.length() != 0)
                nameBuilder.append(" ");
            nameBuilder.append(n);
        }
        return nameBuilder.toString();
    }

    private void addMisc()
    {
        add("planttech2.tip.rake", "Efficient tool for killings plant bugs like snails and mites.");
        add("planttech2.tip.catalyst", "The catalyst can be applied on crops to increase the chance of the trait increment if available on the next cross-breeding. Catalysts is consumed despite the success of the cross-breeding.");
        add("planttech2.tip.catalyst_2", "Trait Type - %s");
        add("planttech2.tip.catalyst_3", "Chance Increase - %s");
        add("planttech2.block.crop.catalyst_used", "The plant now has increased %d chance to have better %s upon next cross-breeding.");
        add("planttech2.block.crop.catalyst_use_exists", "The plant was applied with %s catalyst already.");
        add("planttech2.block.crop.catalyst_use_failed", "The plant was applied with maximum catalyst already.");

        add(RAKE.get(), "Rake");

        add(CATALYST_ENERGY_VALUE.get(), "Catalyst - Energy Value I");
        add(CATALYST_ENERGY_VALUE_I.get(), "Catalyst - Energy Value II");
        add(CATALYST_ENERGY_VALUE_II.get(), "Catalyst - Energy Value III");
        add(CATALYST_GENE_STRENGTH.get(), "Catalyst - Genetic Strength I");
        add(CATALYST_GENE_STRENGTH_I.get(), "Catalyst - Genetic Strength II");
        add(CATALYST_GENE_STRENGTH_II.get(), "Catalyst - Genetic Strength III");
        add(CATALYST_GROWTH_RATE.get(), "Catalyst - Growth Rate I");
        add(CATALYST_GROWTH_RATE_I.get(), "Catalyst - Growth Rate II");
        add(CATALYST_GROWTH_RATE_II.get(), "Catalyst - Growth Rate III");
        add(CATALYST_TEMPERATURE_TOLERANCE.get(), "Catalyst - Temperature Tolerance I");
        add(CATALYST_TEMPERATURE_TOLERANCE_I.get(), "Catalyst - Temperature Tolerance II");
        add(CATALYST_TEMPERATURE_TOLERANCE_II.get(), "Catalyst - Temperature Tolerance III");
        add(CATALYST_WATER_SENSITIVITY.get(), "Catalyst - Water Sensitivity I");
        add(CATALYST_WATER_SENSITIVITY_I.get(), "Catalyst - Water Sensitivity II");
        add(CATALYST_WATER_SENSITIVITY_II.get(), "Catalyst - Water Sensitivity III");
        add(CRACKED_SNAIL_SHELL_BLACK.get(), "Cracked Black Snail Shell");
        add(CRACKED_SNAIL_SHELL_CYBER.get(), "Cracked Cyber Snail Shell");
        add(CRACKED_SNAIL_SHELL_GARDEN.get(), "Cracked Garden Snail Shell");
        add(CRACKED_SNAIL_SHELL_MILK.get(), "Cracked Milk Snail Shell");
        add(CRACKED_SNAIL_SHELL_PINK.get(), "Cracked Pink Snail Shell");
        add(SNAIL_SHELL_DUST_BLACK.get(), "Black Snail Shell Dust");
        add(SNAIL_SHELL_DUST_CYBER.get(), "Cyber Snail Shell Dust");
        add(SNAIL_SHELL_DUST_GARDEN.get(), "Garden Snail Shell Dust");
        add(SNAIL_SHELL_DUST_MILK.get(), "Milk Snail Shell Dust");
        add(SNAIL_SHELL_DUST_PINK.get(), "Pink Snail Shell Dust");

        for (CropTraitsTypes type : CropTraitsTypes.values())
            add(type.getTranslationKey(), forDisplay(type.getName()));
        for (CropTypes crop : CropTypes.values())
        {
            String name = crop.getName();
            String displayName = forDisplay(name);
            add("item." + MODID + "." + name + "_seeds", displayName + " Seeds");
            if (crop.hasParticle())
                add("item." + MODID + "." + name + "_particles", displayName + " Particles");
            add(crop.translationKey(), crop == CropTypes.LILY_OF_THE_VALLEY_CROP ? "Lily of the Valley" : displayName);
        }
        add("planttech2.tip.dismantle_by_wrench", "Can be dismantle by wrench.");
        add("planttech2.tip.electric_fence", "Can be dismantled by wrench, connect to a powered energy supplier or electric fence to activate.");
        add("planttech2.tip.hedge.log", "Log: %s");
        add("planttech2.tip.hedge.leaf", "Leaf: %s");
        add("planttech2.tip.hedge.soil", "Can be dismantled by wrench.");
        add("planttech2.tip.energy_item.no_energy", "No enough energy.");
        add("planttech2.tip.energy_item.energy_use", "%d energy per use.");
        add("planttech2.tip.advanced_analyser", "Right-click on a crop to analyse its traits.");
        add("planttech2.tip.crop_remover", "Right-click on the crop bars to remove crop.");
        add("planttech2.tip.analyser.ok", "Everything's well.");
        add("planttech2.tip.crop.no_light", "%s requires more light.");
        add("planttech2.tip.crop.no_water", "%s requires more water.");
        add("planttech2.tip.crop.no_soil", "%s does not like the soil.");
        add("planttech2.tip.crop.no_temperature", "%s does not like the temperature.");

        add(MODID + ".update.available", "An update for PlantTech 2 is available. ");
        add(MODID + ".update.click", "Click here");
        add(MODID + ".update.tooltip", "View CurseForge");
        add(MODID + ".electric_fence.idle", "Electric fence hums");

        add(MODID + ".crossbreeding", "Crossbreeding");
        add(MODID + ".infuser", "Infuser");
        add(MODID + ".compressor", "Compressor");
        add(MODID + ".chipalyzer", "Chipalyzer");
        add(MODID + ".carver", "Carver Crop");
        add(MODID + ".machinebulbreprocessor", "Machinebulb Reprocessor");
        add(MODID + ".machine_growing", "Growing Machine");
        add("config.test", "TEST");
        add("fluid.biomass", "Biomass");

        add("death.attack.radiation_sickness", "%1$s was infected by radiation sickness");
        add("armorinformation.when_worn", "When worn:");
        add("armorinformation.selfrepair", "Self-repair");
        add("armorinformation.autofeed", "Auto-feed");
        add("techvillager.profession.scientist", "Scientist");
        add("techvillager.profession.botanist", "Botanist");
        add("techvillager.profession.headhunter", "Headhunter");
        add("techvillager.profession.engineer", "Engineer");
        add("text.biometemperature", "temperature");
        add("temp.extreme_cold", "Extremely Cold");
        add("temp.cold", "Cold");
        add("temp.normal", "Normal");
        add("temp.warm", "Warm");
        add("temp.extreme_warm", "Extremely Warm");
    }

    private void addItemGroups()
    {
        add("itemGroup.planttech2_main", "General");
        add("itemGroup.planttech2_blocks", "Building Blocks");
        add("itemGroup.planttech2_seeds", "Seeds");
        add("itemGroup.planttech2_machines", "Machines");
        add("itemGroup.planttech2_particles", "Particles");
        add("itemGroup.planttech2_toolsandarmor", "Tools and Armor");
        add("itemGroup.planttech2_chips", "Chips and Upgrades");
    }

    private void addItems()
    {
        add(GUIDE_OVERVIEW.get(), "Tablet: PlantTech 2 Guide");
        add(GUIDE_PLANTS.get(), "Tablet: Plant Encyclopedia");
        add(GUIDE_GENETIC_ENGINEERING.get(), "Tablet: Genetic Engineering (NO FUNKTION!)");
        add(ANALYSER.get(), "Analyzer");
        add(ADVANCED_ANALYSER.get(), "Advanced Analyzer");
        add(CROPREMOVER.get(), "Crop Remover");
        add(WRENCH.get(), "Wrench");
        add(THERMOMETER.get(), "Thermometer");
        add(SOLARFOCUS_TIER_1.get(), "Class 1 Solar Focus");
        add(SOLARFOCUS_TIER_2.get(), "Class 2 Solar Focus");
        add(SOLARFOCUS_TIER_3.get(), "Class 3 Solar Focus");
        add(SOLARFOCUS_TIER_4.get(), "Class 4 Solar Focus");
        add(SPEEDUPGRADE_TIER_1.get(), "Speed Upgrade v1.0");
        add(SPEEDUPGRADE_TIER_2.get(), "Speed Upgrade v2.0");
        add(SPEEDUPGRADE_TIER_3.get(), "Speed Upgrade v3.0");
        add(SPEEDUPGRADE_TIER_4.get(), "Speed Upgrade v4.0");
        add(RANGEUPGRADE_TIER_1.get(), "Range Upgrade v1.0");
        add(RANGEUPGRADE_TIER_2.get(), "Range Upgrade v2.0");
        add(RANGEUPGRADE_TIER_3.get(), "Range Upgrade v3.0");
        add(RANGEUPGRADE_TIER_4.get(), "Range Upgrade v4.0");
        add(CAPACITYUPGRADE_TIER_1.get(), "Capacity Upgrade v1.0");
        add(CAPACITYUPGRADE_TIER_2.get(), "Capacity Upgrade v2.0");
        add(CAPACITYUPGRADE_TIER_3.get(), "Capacity Upgrade v3.0");
        add(CAPACITYUPGRADE_TIER_4.get(), "Capacity Upgrade v4.0");
        add(PLANTIUM_INGOT.get(), "Plantium Ingot");
        add(PLANTIUM_NUGGET.get(), "Plantium Nugget");
        add(DANCIUM_INGOT.get(), "Dancium Ingot");
        add(DANCIUM_NUGGET.get(), "Dancium Nugget");
        add(KANEKIUM_INGOT.get(), "Kanekium Ingot");
        add(KANEKIUM_NUGGET.get(), "Kanekium Nugget");
        add(KINNOIUM_INGOT.get(), "Kinnoium Ingot");
        add(KINNOIUM_NUGGET.get(), "Kinnoium Nugget");
        add(LENTHURIUM_INGOT.get(), "Lenthurium Ingot");
        add(LENTHURIUM_NUGGET.get(), "Lenthurium Nugget");
        add(BIOMASS.get(), "Biomass");
        add(DNA_CONTAINER_EMPTY.get(), "Empty DNA Container");
        add(DNA_CONTAINER.get(), "DNA Container");
        add(FERTILIZER_TIER_1.get(), "G1 Fertilizer");
        add(FERTILIZER_TIER_2.get(), "G2 Fertilizer");
        add(FERTILIZER_TIER_3.get(), "G3 Fertilizer");
        add(FERTILIZER_TIER_4.get(), "G4 Fertilizer");
        add(FERTILIZER_CREATIVE.get(), "Creative Fertilizer");
        add(GEAR_KANEKIUM.get(), "Kanekium Gear");
        add(GEAR_LENTHURIUM.get(), "Lenthurium Gear");
        add(GEAR_KINNOIUM.get(), "Kinnoium Gear");
        add(GEAR_DANCIUM.get(), "Dancium Gear");
        add(GEAR_IRON.get(), "Iron Gear");
        add(GEAR_PLANTIUM.get(), "Plantium Gear");
        add(GEAR_KANEKIUM_INFUSED.get(), "Infused Kanekium Gear");
        add(GEAR_LENTHURIUM_INFUSED.get(), "Infused Lenthurium Gear");
        add(GEAR_KINNOIUM_INFUSED.get(), "Infused Kinnoium Gear");
        add(GEAR_DANCIUM_INFUSED.get(), "Infused Dancium Gear");
        add(GEAR_IRON_INFUSED.get(), "Infused Iron Gear");
        add(GEAR_PLANTIUM_INFUSED.get(), "Infused Plantium Gear");
        add(REDSTONE_INFUSED.get(), "Infused Redstone");
        add(BIOMASSCONTAINER.get(), "Biomass Container");
        add(ENERGYSTORAGE_TIER_1.get(), "Class 1 Battery");
        add(ENERGYSTORAGE_TIER_2.get(), "Class 2 Battery");
        add(ENERGYSTORAGE_TIER_3.get(), "Class 3 Battery");
        add(KNOWLEDGECHIP_TIER_0.get(), "Knowledge Chip v0a");
        add(KNOWLEDGECHIP_TIER_1.get(), "Knowledge Chip v1.0");
        add(KNOWLEDGECHIP_TIER_2.get(), "Knowledge Chip v2.0");
        add(KNOWLEDGECHIP_TIER_3.get(), "Knowledge Chip v3.0");
        add(KNOWLEDGECHIP_TIER_4.get(), "Knowledge Chip v4.0");
        add(KNOWLEDGECHIP_TIER_5.get(), "Knowledge Chip v5.0");
//        add(WHITE_CRYSTAL.get(), "White Crystal");
//        add(DARK_CRYSTAL.get(), "Dark Crystal");
//
//        add(PLANT_OBTAINER.get(), "Plant Obtainer");
//        add(RADIATION_METRE.get(), "Radiation Metre");

        add(BIOMASS_BUCKET.get(), "Biomass Bucket");
        add(MACHINEBULBREPROCESSOR_BULB.get(), "Machinebulb Reprocessor Bulb");
        add(DNA_COMBINER_BULB.get(), "DNA Combiner Bulb");
        add(DNA_EXTRACTOR_BULB.get(), "DNA Extractor Bulb");
        add(DNA_REMOVER_BULB.get(), "DNA Remover Bulb");
        add(DNA_CLEANER_BULB.get(), "DNA Cleaner Bulb");
        add(CHIPALYZER_BULB.get(), "Chipalyzer Bulb");
        add(COMPRESSOR_BULB.get(), "Compressor Bulb");
        add(IDENTIFIER_BULB.get(), "Identifier Bulb");
        add(INFUSER_BULB.get(), "Infuser Bulb");
        add(SEEDSQUEEZER_BULB.get(), "Seed Squeezer Bulb");
        add(SOLARGENERATOR_BULB.get(), "Solar Generator Bulb");
        add(MEGAFURNACE_BULB.get(), "Megafurnace Bulb");
        add(PLANTFARM_BULB.get(), "Plantfarm Bulb");
        add(SEEDCONSTRUCTOR_BULB.get(), "Seed Constructor Bulb");
        add(ENERGY_SUPPLIER_BULB.get(), "Energy Supplier Bulb");

        add(CYBERBOW.get(), "CyberBow");
        add(CYBERDAGGER.get(), "CyberDagger");
        add(CYBERKATANA.get(), "CyberKatana");
        add(CYBERRAPIER.get(), "CyberRapier");
        add(MULTITOOL.get(), "MultiTool");

        add(CYBERARMOR_HELMET.get(), "CyberArmor Helmet");
        add(CYBERARMOR_CHEST.get(), "CyberArmor Chest");
        add(CYBERARMOR_LEGGINGS.get(), "CyberArmor Leggings");
        add(CYBERARMOR_BOOTS.get(), "CyberArmor Boots");

        add(EMPTY_UPGRADECHIP_TIER_1.get(), "Blank Upgrade Chip");
        add(EMPTY_UPGRADECHIP_TIER_2.get(), "v2.0 Chip Upgrade Pack");
        add(EMPTY_UPGRADECHIP_TIER_3.get(), "v3.0 Chip Upgrade Pack");
        add(CHIP_UPGRADEPACK_CAPACITY_1.get(), "v2.0 Capacity Chip Upgrade Pack");
        add(CHIP_UPGRADEPACK_CAPACITY_2.get(), "v3.0 Capacity Chip Upgrade Pack");
        add(CHIP_UPGRADEPACK_HARVESTLEVEL_1.get(), "v2.0 Harvest Level Chip Upgrade Pack");
        add(CHIP_UPGRADEPACK_HARVESTLEVEL_2.get(), "v3.0 Harvest Level Chip Upgrade Pack");
        add(CHIP_UPGRADEPACK_REACTOR_1.get(), "v2.0 Reactor Chip Upgrade Pack");
        add(CHIP_UPGRADEPACK_REACTOR_2.get(), "v3.0 Reactor Chip Upgrade Pack");

        add(CAPACITYCHIP_TIER_1.get(), "Capacity Chip v1.0");
        add(CAPACITYCHIP_TIER_2.get(), "Capacity Chip v2.0");
        add(CAPACITYCHIP_TIER_3.get(), "Capacity Chip v3.0");
        add(HARVESTLEVELCHIP_TIER_1.get(), "Harvest Level Chip v1.0");
        add(HARVESTLEVELCHIP_TIER_2.get(), "Harvest Level Chip v2.0");
        add(HARVESTLEVELCHIP_TIER_3.get(), "Harvest Level Chip v3.0");
        add(ATTACKCHIP_TIER_1.get(), "Attack Chip v1.0");
        add(ATTACKCHIP_TIER_2.get(), "Attack Chip v2.0");
        add(ATTACKCHIP_TIER_3.get(), "Attack Chip v3.0");
        add(BREAKDOWNRATECHIP_TIER_1.get(), "Breakdown Rate Chip v1.0");
        add(BREAKDOWNRATECHIP_TIER_2.get(), "Breakdown Rate Chip v2.0");
        add(BREAKDOWNRATECHIP_TIER_3.get(), "Breakdown Rate Chip v3.0");
        add(ARMORCHIP_TIER_1.get(), "Armor Chip v1.0");
        add(ARMORCHIP_TIER_2.get(), "Armor Chip v2.0");
        add(ARMORCHIP_TIER_3.get(), "Armor Chip v3.0");
        add(TOUGHNESSCHIP_TIER_1.get(), "Toughness Chip v1.0");
        add(TOUGHNESSCHIP_TIER_2.get(), "Toughness Chip v2.0");
        add(TOUGHNESSCHIP_TIER_3.get(), "Toughness Chip v3.0");
        add(ATTACKSPEEDCHIP_TIER_1.get(), "Attack Speed Chip v1.0");
        add(ATTACKSPEEDCHIP_TIER_2.get(), "Attack Speed Chip v2.0");
        add(ATTACKSPEEDCHIP_TIER_3.get(), "Attack Speed Chip v3.0");
        add(REACTORCHIP_TIER_1.get(), "Reactor Chip v1.0");
        add(REACTORCHIP_TIER_2.get(), "Reactor Chip v2.0");
        add(REACTORCHIP_TIER_3.get(), "Reactor Chip v3.0");
        add(UNLOCKCHIP_SHOVEL.get(), "Unlock Chip: Shovel");
        add(UNLOCKCHIP_AXE.get(), "Unlock Chip: Axe");
        add(UNLOCKCHIP_SHEARS.get(), "Unlock Chip: Shears");
        add(UNLOCKCHIP_HOE.get(), "Unlock Chip: Hoe");

        add(PROTECTION_CHIP.get(), "Protection Chip");
        add(FIRE_PROTECTION_CHIP.get(), "Fire Protection Chip");
        add(FEATHER_FALLING_CHIP.get(), "Feather Falling Chip");
        add(BLAST_PROTECTION_CHIP.get(), "Blast Protection Chip");
        add(PROJECTILE_PROTECTION_CHIP.get(), "Projectile Protection Chip");
        add(RESPIRATION_CHIP.get(), "Respiration Chip");
        add(AQUA_AFFINITY_CHIP.get(), "Aqua Affinity Chip");
        add(THORNS_CHIP.get(), "Thorns Chip");
        add(DEPTH_STRIDER_CHIP.get(), "Depth Strider Chip");
        add(FROST_WALKER_CHIP.get(), "Frost Walker Chip");
        add(SHARPNESS_CHIP.get(), "Sharpness Chip");
        add(SMITE_CHIP.get(), "Smite Chip");
        add(BANE_OF_ARTHROPODS_CHIP.get(), "Bane of Arthropods Chip");
        add(KNOCKBACK_CHIP.get(), "Knockback Chip");
        add(FIRE_ASPECT_CHIP.get(), "Fire Aspect Chip");
        add(LOOTING_CHIP.get(), "Looting Chip");
        add(SWEEPING_CHIP.get(), "Sweeping Chip");
        add(EFFICIENCY_CHIP.get(), "Efficiency Chip");
        add(SILK_TOUCH_CHIP.get(), "Silk Touch Chip");
        add(UNBREAKING_CHIP.get(), "Unbreaking Chip");
        add(FORTUNE_CHIP.get(), "Fortune Chip");
        add(POWER_CHIP.get(), "Power Chip");
        add(PUNCH_CHIP.get(), "Punch Chip");
        add(FLAME_CHIP.get(), "Flame Chip");
        add(INFINITY_CHIP.get(), "Infinity Chip");
    }

    private void addBlocks()
    {
        add(ModBlocks.BIOMASSFLUIDBLOCK.get(), "Biomass Fluid");
        add(ModBlocks.CROPBARS.get(), "Cropbars");
        add(ModBlocks.IDENTIFIER.get(), "Identifier");
        add(ModBlocks.MEGAFURNACE.get(), "Mega Furnace");
        add(ModBlocks.SEEDSQUEEZER.get(), "Seed Squeezer");
        add(ModBlocks.SOLARGENERATOR.get(), "Solar Generator");
        add(ModBlocks.PLANTFARM.get(), "Void-Plantfarm");
        add(ModBlocks.PLANTIUM_BLOCK.get(), "Plantium Block");
        add(ModBlocks.DANCIUM_BLOCK.get(), "Dancium Block");
        add(ModBlocks.KANEKIUM_BLOCK.get(), "Kanekium Block");
        add(ModBlocks.KINNOIUM_BLOCK.get(), "Kinnoium Block");
        add(ModBlocks.LENTHURIUM_BLOCK.get(), "Lenthurium Block");
        add(ModBlocks.UNIVERSAL_SOIL.get(), "Experimental Soil Prototype");
        add(ModBlocks.UNIVERSAL_SOIL_INFUSED.get(), "Biomass-Infused OmniSoil");
        add(ModBlocks.CABLE.get(), "Cable");
        add(ModBlocks.DNA_EXTRACTOR.get(), "DNA Extractor");
        add(ModBlocks.DNA_COMBINER.get(), "DNA Combiner");
        add(ModBlocks.DNA_REMOVER.get(), "DNA Remover");
        add(ModBlocks.DNA_CLEANER.get(), "DNA Cleaner");
        add(ModBlocks.SEEDCONSTRUCTOR.get(), "Seed Constructor");
        add(ModBlocks.COMPRESSOR.get(), "Compressor");
        add(ModBlocks.INFUSER.get(), "Infuser");
        //        add(ModBlocks.MACHINESHELL.get(), "Machine Shell"); TODO: verify
        //        add(ModBlocks.MACHINESHELL_INFUSED.get(), "Infused Machine Shell"); TODO: verify
        //        add(ModBlocks.PLANTTOPIA_PORTAL.get(), "PlantTopia Portal");
        add(ModBlocks.CHIPALYZER.get(), "Chipalyzer");
        add(ModBlocks.MACHINEBULBREPROCESSOR.get(), "Machinebulb Reprocessor");
        add(ModBlocks.MACHINESHELL_IRON.get(), "Iron Machine Shell");
        add(ModBlocks.MACHINESHELL_PLANTIUM.get(), "Plantium Machine Shell");
        add(ModBlocks.CARVER.get(), "Carver Crop");
        add(ModBlocks.ENERGY_SUPPLIER.get(), "Energy Supplier");

        add(ModBlocks.ELECTRIC_FENCE.get(), "Electric Fence");
        add(ModBlocks.ELECTRIC_FENCE_TOP.get(), "Electric Fence Top");
        add(ModBlocks.ELECTRIC_FENCE_GATE.get(), "Electric Fence Gate");

        add(ModBlocks.RED_MUSHROOM_STAIRS.get(), "Red Mushroom Stairs");
        add(ModBlocks.RED_MUSHROOM_SLAB.get(), "Red Mushroom Slab");
        add(ModBlocks.RED_MUSHROOM_TRAPDOOR.get(), "Red Mushroom Trapdoor");
        add(ModBlocks.RED_MUSHROOM_FENCE_GATE.get(), "Red Mushroom Fence Gate");
        add(ModBlocks.RED_MUSHROOM_FENCE.get(), "Red Mushroom Fence");
        add(ModBlocks.RED_MUSHROOM_CANDLE.get(), "Red Mushroom Candle");

        add(ModBlocks.BROWN_MUSHROOM_STAIRS.get(), "Brown Mushroom Stairs");
        add(ModBlocks.BROWN_MUSHROOM_SLAB.get(), "Brown Mushroom Slab");
        add(ModBlocks.BROWN_MUSHROOM_TRAPDOOR.get(), "Brown Mushroom Trapdoor");
        add(ModBlocks.BROWN_MUSHROOM_FENCE_GATE.get(), "Brown Mushroom Fence Gate");
        add(ModBlocks.BROWN_MUSHROOM_FENCE.get(), "Brown Mushroom Fence");
        add(ModBlocks.BROWN_MUSHROOM_CANDLE.get(), "Brown Mushroom Candle");

        add(ModBlocks.CRIMSON_HYPHAE_STAIRS.get(), "Crimson Hyphae Stairs");
        add(ModBlocks.CRIMSON_HYPHAE_SLAB.get(), "Crimson Hyphae Slab");
        add(ModBlocks.CRIMSON_HYPHAE_TRAPDOOR.get(), "Crimson Hyphae Trapdoor");
        add(ModBlocks.CRIMSON_HYPHAE_FENCE_GATE.get(), "Crimson Hyphae Fence Gate");
        add(ModBlocks.CRIMSON_HYPHAE_FENCE.get(), "Crimson Hyphae Fence");
        add(ModBlocks.CRIMSON_HYPHAE_CANDLE.get(), "Crimson Hyphae Candle");

        add(ModBlocks.WARPED_HYPHAE_STAIRS.get(), "Warped Hyphae Stairs");
        add(ModBlocks.WARPED_HYPHAE_SLAB.get(), "Warped Hyphae Slab");
        add(ModBlocks.WARPED_HYPHAE_TRAPDOOR.get(), "Warped Hyphae Trapdoor");
        add(ModBlocks.WARPED_HYPHAE_FENCE_GATE.get(), "Warped Hyphae Fence Gate");
        add(ModBlocks.WARPED_HYPHAE_FENCE.get(), "Warped Hyphae Fence");
        add(ModBlocks.WARPED_HYPHAE_CANDLE.get(), "Warped Hyphae Candle");
    }

    public void addCrops()
    {
        add(COLOR_PARTICLES.get(), "Color Particles");
        Map<String, String> langKeys = new HashMap<>();
        // put manual overrides into langKeys

        Map<String, String> crops = defaultCrops();
        crops.forEach((k, v) -> langKeys.putIfAbsent("crop." + k, v));
    }
    
    public void addHedges()
    {
    	String[] types = new String[] {"oak", "spruce", "birch", "jungle", "acacia", "dark_oak"}; 
    	String[] soils = new String[] {"dirt", "grass", "podzol"}; 
     	
        for(String type_1: types)
			for(String type_2: types)
				for(String soil: soils)
					add("block." + PlantTechMain.MODID + ".hedge_" + type_1 + "_" + type_2 + "_" + soil, "Hedge (" + capitalize(type_1) + " Leaves/ " + capitalize(type_2) + " Wood/" + capitalize(soil) + " Soil)");
    }

    private static String capitalize(String str)
    {
        return StringUtils.capitalize(str.toLowerCase());
    }

    public void addGUIs()
    {
        add("container.megafurnace", "Mega Furnace");
        add("container.identifier", "Identifier");
        add("container.solargenerator", "Solar Generator");
        add("container.seedsqueezer", "Seed Squeezer");
        add("container.plantfarm", "Plantfarm");
        add("container.dnaextractor", "DNA Extractor");
        add("container.dnacombiner", "DNA Combiner");
        add("container.dnaremover", "DNA Remover");
        add("container.dnacleaner", "DNA Cleaner");
        add("container.seedconstructor", "Seed Constructor");
        add("container.compressor", "Compressor");
        add("container.infuser", "Infuser");
        add("container.upgradeableitem", "Upgrade Item");
        add("container.chipalyzer", "Chipalyzer");
        add("container.planttopia_teleporter", "PlantTopia Teleporter");
        add("container.machinebulbreprocessor", "Machinebulb Reprocessor");
        add("container.energysupplier", "Energy Supplier");
        add("container.cropauragenerator", "Crop Aura Generator");

        add("gui.soil", "Soil");
        add("gui.seeds", "Seeds");
        add("gui.drops", "Drops");
        add("gui.temperature", "Temperature");
        add("gui.parents", "Parents");
        add("gui.non_selected", "Nothing selected");
        add("gui.next", "Next");
        add("gui.last", "Last");
        add("gui.back", "Back");

        add("slot.chipalyzer.chipinput", "Insert empty chip");
        add("slot.chipalyzer.iteminput", "Insert item to implant in empty chip");
        add("slot.compressor.input", "Insert particle and select result");
        add("slot.compressor.select", "Select result after placing particle in input slot");
        add("slot.dnacleaner.input", "Insert DNA container you want to empty");
        add("slot.dnacombiner.container", "Insert DNA container with traits you want to combine");
        add("slot.dnacombiner.empty_container", "Insert empty DNA container");
        add("slot.dnaextractor.seeds", "Insert seed you want to extract traits from");
        add("slot.dnaextractor.empty_container", "Insert empty DNA container");
        add("slot.dnaremover.container", "Insert DNA container to remove a single trait");
        add("slot.identifier.input", "Insert seeds with unknown traits");
        add("slot.infuser.input", "Insert item/block you want to infuse with biomass");
        add("slot.megafurnace.input", "Insert item/block you want to smelt");
        add("slot.machinebulbreprocessor.input", "Seeds placed here will be reprocessed into a Machinebulb");
        add("slot.plantfarm.storage", "Drops of harvested crops will be inserted here");
        add("slot.seedconstructor.container", "Insert DNA container to construct a new seed with its traits");
        add("slot.seedsqueezer.input", "Insert PlantTech2 seeds that should be squeezed into biomass and energy");
        add("slot.seedsqueezer.squeeze", "No input, will automatically pull from input");
        add("slot.solargenerator.focus", "Insert focus to start energy production");
        add("slot.upgradeableitem.chipslot", "Insert chip to upgrade item");
        add("slot.crop_aura_generator.input_0", "Insert Fertility Aura Core");
        add("slot.crop_aura_generator.input_1", "Insert Light Value Aura Core");
        add("slot.crop_aura_generator.input_2", "Insert Productivity Aura Core");
        add("slot.crop_aura_generator.input_3", "Insert Temperature Aura Core");

        add("slot.util.speedupgrade", "Insert Speed Upgrade to speed up the process");
        add("slot.util.rangeupgrade", "Insert Range Upgrade to increase the production");
        add("slot.util.capacityupgrade", "Insert Capacity Upgrade to increase the energy storage");
//        add("slot.util.squeezerupgrade", "Not implemented yet");
        add("slot.util.knowledgechip", "Insert Knowledge Chip to collect knowledge while machine is active (limited by the tier of the machine)");
        add("slot.util.output", "Output slot");
        add("slot.util.fluidin", "Insert biomass container to insert biomass");
        add("slot.util.fluidout", "Insert biomass container to extract biomass");
        add("slot.util.energyin", "Insert item with energy to charge this machine");
        add("slot.util.energyout", "Insert item with energy to charge the item");
    }

    public void addGuides()
    {
        addGuide("header", "PlantTech 2 Guide");
        addGuide("crossbreeding.menu", "Crossbreeding");
        addGuide("machines.menu", "Machines");
        addGuide("genetic_engineering.menu", "Genetic Engineering");

        addGuide("general_crossbreeding.header", "General");
        addGuide("getting_started.header", "Getting started");
        addGuide("crop_not_growing.header", "Crop is not growing?");
        addGuide("multiply_crops_seeds.header", "How to multiply crops/seeds");
        addGuide("how_to_crossbreed.header", "How to crossbreed");
        addGuide("autoreplanting.header", "Auto-replanting");
        addGuide("croptraits.header", "Crop traits");
        addGuide("identify_traits.header", "Identify traits");
        addGuide("fertilizer.header", "Fertilizer");
        addGuide("genetic_engineering_crops.header", "Genetic engineering crops");

        addGuide("general_genetic_engineering.header", "General");
        addGuide("extracting_genes.header", "Extracting genes");
        addGuide("purifying_genes.header", "Purifying genes");
        addGuide("combining_genes.header", "Combining genes");
        addGuide("cleaning_containers.header", "Cleaning containers");
        addGuide("creating_designerseeds.header", "Creating designer seeds");

        addGuide("general_machines.header", "General");
        addGuide("chipalyzer.header", "Chipalyzer");
        addGuide("compressor.header", "Compressor");
        addGuide("dna_cleaner.header", "DNA Cleaner");
        addGuide("dna_combiner.header", "DNA Combiner");
        addGuide("dna_extractor.header", "DNA Extractor");
        addGuide("dna_remover.header", "DNA Remover");
        addGuide("energy_supplier.header", "Energy Supplier");
        addGuide("identifier.header", "Identifier");
        addGuide("infuser.header", "Infuser");
        addGuide("machinebulb_reprocessor.header", "Machinebulb Reprocessor");
        addGuide("mega_furnace.header", "Mega Furnace");
        addGuide("void_plantfarm.header", "Void Plantfarm");
        addGuide("seedconstructor.header", "Seed Constructor");
        addGuide("seed_squeezer.header", "Seed Squeezer");
        addGuide("solar_generator.header", "Solar Generator");

        addGuide("general_crossbreeding.text",
                "Crossbreeding is the key for renewable resources. But before starting there is some important information: <br> <br> - PlantTech 2" +
                        " crops act different then vanilla crops. They share some behavior, but not all. <br> - The crops have traits that can be " +
                        "improved with crossbreeding, but most of the base traits are considerably worse than vanilla - however, if properly tended" +
                        " they can be much more efficient. <br> <br> Example: In the beginning plants will need a water source to be 1 block away, " +
                        "but through crossbreeding they can potentially thrive with water as far as 9 blocks away. <br> <br> Please refer to the " +
                        "'Crop traits' section of the Guide for detailed information. <br> <br> **Also be advised that Bonemeal does not work on " +
                        "PlantTech 2 crops (see Fertilizer)");

        addGuide("getting_started.text",
                "To start your career as a crossbreeder, you just need cropbars <br> (crafting: 4 sticks in 2x2 square) and a vanilla seed (or " +
                        "plantium nugget). Place the cropbars and right-click with the seeds on the cropbars, and if conditions are right the crop " +
                        "will grow. (If not, have a look at the next chapter.)");

        addGuide("crop_not_growing.text",
                "The easiest way to check, if the conditions are matched, is using the analyser. Just right-click on the crop and you will get a " +
                        "message with every condition that is not matched. <br>  The conditions are: <br> <br> - Water source near enough <br> - " +
                        "Light level high enough <br> - Right soil <br> - Right temperature");

        addGuide("multiply_crops_seeds.text",
                "When fully grown on a cropbar, plants will spread to an empty cropbar that is immediately adjacent. This is the easiest way to " +
                        "multiply your stock of seeds without increasing any traits, and if the original seed was identified the traits of the new " +
                        "plant will also be known (provided the result was not a crossbreed; see the next chapter). Once the Fertility trait " +
                        "increases, extra seeds may begin to drop when a crop is harvested.");

        addGuide("how_to_crossbreed.text",
                "When trying to get better traits - or new crop types - you need to crossbreed. This happens when a fully-grown plant spreads onto " +
                        "an empty cropbar that is next to another plant. The selection of the new traits/type is random with the following logic: " +
                        "<br> <br> Type: If the parent crops are different but have a matching crossbreed result, there is a small chance the new " +
                        "plant will be of that type. <br> <br> Traits: For each trait, the traits of the parents are compared. If they are equal, " +
                        "there is a small chance the trait will increased by one. Otherwise, the traits of the new crop will be in range of the " +
                        "parents' traits. <br> <br> The traits of a crossbred crop are always unknown and will need to be revealed with the " +
                        "Identifier machine.");

        addGuide("autoreplanting.text",
                "All crops in PlantTech 2 are auto-replanted; right-clicking to harvest the crops when fully grown will generate any drops and " +
                        "reset the plant to the seedling state. Until the Fertility trait is increased this will usually result in no extra seed " +
                        "drops, as a seed was needed for replanting.");

        addGuide("croptraits.text",
                " **Growth Rate** <br>     - Minimum level: 0 <br>    - Maximum level: 10 <br>     - Reduce the amount of time needed for the crop " +
                        "to grow. <br>    - Time per Stage = 90s - level * 6 <br> <br> **Sensitivity** <br> <br>    - Minimum level: 0 <br>    - " +
                        "Maximum level 10 <br>    - Not yet implemented. <br> <br>  **Light Sensitivity** <br> <br>    - Minimum level: 0 <br>    -" +
                        " Maximum level: 14 <br>    - Defines the minimum light level needed for the plant to grow. <br>    - Minimum needed " +
                        "lightlevel = 14 - level <br> <br>  **Water Sensitivity** <br> <br>    - Minimum level: 0 <br>    - Maximum level: 8 <br>  " +
                        "  - Defines the max distance from water that that the crop will be able to grow. <br>    - Distance = 1 + level <br> <br> " +
                        " **Temperature Tolerance** <br> <br>    - Minimum level: 0 <br>    - Maximum level: 4 <br>    - Allows the crop to grow in" +
                        " temperatures outside of its native climate. <br>    - Allowed temperature = default temperature +- level <br> <br> <br> " +
                        "<br> <br> <br> **Productivity** <br> <br>    - Minimum level: 0 <br>    - Maximum level: 5 <br>    - Increases the amount " +
                        "of products dropped <br> <br>  **Fertility** <br> <br>    - Minimum level: 0 <br>    - Maximum level: 5 <br>    - " +
                        "Increases the amount of seeds dropped <br> <br>  **Spreading Speed** <br> <br>    - Minimum level: 0 <br>    - Maximum " +
                        "level: 10 <br>    - Not yet implemented <br> <br> <br> **Gene Strength** <br> <br>    - Minimum level: 0 <br>    - Maximum" +
                        " level: 10 <br>    - Not yet implemented <br> <br>  **Energy Value** <br> <br>    - Minimum level: 1 <br>    - Maximum " +
                        "level: 10 <br>    - Increases the amount of energy produced in the seed squeezer <br>    - Energy = level * 20");

        addGuide("identify_traits.text",
                "The identifier is a machine that uses energy to identify the unknown traits of crops. <br> Just put the seeds with the unknown " +
                        "traits in the correct slot and the seeds will be identified in a bit. <br> For more information, have a look at " +
                        "\"Machines: Identifier.\"");

        addGuide("fertilizer.text",
                "Bonemeal does not work on PlantTech 2 crops. Instead, Fertilizer must be created by infusing Bonemeal with biomass (in the Infuser" +
                        " machine). There are 4 tiers of fertilizer, which may be upgraded in the Infuser as well. <br> <br> Fertilizer has a " +
                        "chance of advancing the growth of the crop it is used on by one stage, from 5% at Tier 1 to 75% at Tier 4.");

        addGuide("genetic_engineering_crops.text",
                "To make the life a bit easier, it is possible to extract genes of crops and make seeds with these genes. <br> This process is a " +
                        "bridge between the way of crossbreeding and the way of PlantTechian. <br> You find more information in the main category " +
                        "'Genetic Engineering'");

        addGuide("general_genetic_engineering.text",
                "Genetic engineering is a way to create crops with selected traits. These traits need to be found first but could be used as much " +
                        "as you want. So, start crossbreeding your crops and develop the traits you like to have.");

        addGuide("extracting_genes.text",
                "Once you got traits you want to keep, you need to extract the whole DNA sequence. <br> Therefore, put a seed with traits and an " +
                        "empty DNA container in the DNA extractor. You will obtain a DNA container with the whole DNA sequence; the seed will be " +
                        "destroyed in the process.");

        addGuide("purifying_genes.text",
                "There will be times when you want to remove a gene entirely from a DNA sequence - this can be accomplished by placing the DNA " +
                        "container into the DNA Remover machine. A single gene will be selected randomly and removed from the sequence - it's not " +
                        "possible to select which one, so don't mess around when you only have a single container left. <br> In this way you can " +
                        "isolate a single gene.");

        addGuide("combining_genes.text",
                "DNA containers can be combined in the DNA combiner. Through this process the DNA sequences will be mixed, with the lower values " +
                        "being dominant. (There is no dominant crop type however; that will be selected at random.) The used DNA containers will " +
                        "not be consumed, so this can be an effective way to backup a DNA sequence as you attempt to isolate a gene.");

        addGuide("cleaning_containers.text", "Useless DNA containers can be cleaned up for reuse, by placing them in the DNA cleaner.");

        addGuide("creating_designerseeds.text",
                "Now the last step in getting personalized seeds is to put a DNA container and 500 biomass in the Seed Constructor. <br> Biomass is" +
                        " produced by the Seed Squeezer - the same stuff used in the Machinebulb Reprocessor. Again, the used DNA container will " +
                        "not be consumed. <br> All traits that are not present in the DNA container will be reset to their initial value. The " +
                        "default crop type is Carrot, and for the traits have a look at \"Crossbreeding: Crop traits\"");

        addGuide("test.text", Strings.repeat("XX ", 675));

        addGuide("general_machines.text",
                "Machines in PlantTech 2 aren't just blocks that can be placed, they are complex symbioses of metal and plants. Therefore the " +
                        "process of obtaining the machines is more complex than you may be used to. <br> First of all the machines need a machine " +
                        "shell appropriate for the tier of the machine - lower tier machines need an iron machine shell, with higher tiers " +
                        "requiring a machine shell made from plantium. To obtain the machine shell, you need to place an Iron Block or Plantium " +
                        "Block next to a Carver Crop and wait. <br>  For the plant part you need the machinebulb of the desired machine. A " +
                        "Machinebulb Reprocessor is required for most of these, as well as a Seed Squeezer as a bioenergy source. (These two " +
                        "machinebulbs are the only ones that are obtainable by crafting.) When clicking on the correct machine shell with a bulb " +
                        "the machine will start growing and once finished, the machine is ready. <br> The Machinebulb Reprocessor requires a " +
                        "Knowledge Chip to access higher tiers of machinebulbs. This Chip collects information from machines while they're running," +
                        " as long as the machine is not a lower tier than the chip. (To start, a Knowledge Chip v0.1 may be used in a Seed " +
                        "Squeezer, a Tier 0 machine.) Once the Knowledge Chip has collected enough information, it may be used in the Machinebulb " +
                        "Reprocessor to get better machinebulbs. (And the Knowledge Chip will only gain useful data from the new Tier 1 machines.)");

        addGuide("chipalyzer.text",
                "The Chipalyzer can analyze various items and record the data on Blank Upgrade Chips. The resulting chips can be slotted into " +
                        "PlantTech2 equipment to enhance its capabilities. (Some effects are stackable; refer to each chip's tooltip for more info" +
                        ".) Some of these chips can also be upgraded; the following pages will have more on this. <br> <br> <br> <br> <br> <br> " +
                        "<br> <br> <br> <br> <br> <br> <br> <br> <br> These chips are upgradeable with standard Chip Upgrade Packs. <br> <br> " +
                        "Attack Chip: Iron Sword <br> Attack Speed Chip: Golden Sword <br> <br> Armor Chip: Iron Chestplate <br> Toughness Chip: " +
                        "Diamond Chestplate <br> <br> Breakdown Rate Chip: Golden Pickaxe <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> " +
                        "This set of chips is upgradeable with their own special Upgrade Packs. <br> <br> Capacity Chip: Battery (0.5kBE) <br> " +
                        "Reactor Chip: Solar Focus MkI <br> Harvestlevel Chip: Wooden Pickaxe <br> Harvest Level Chip: Diamond Pickaxe <br> <br> " +
                        "<br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> The following chips are used to unlock additional right-click " +
                        "functions for the Multi-Tool. They are not upgradeable. <br> <br> Unlock Chip (Axe): Iron Axe <br> Unlock Chip (Hoe): Iron" +
                        " Hoe <br> Unlock Chip (Shears): Shears <br> Unlock Chip (Shovel): Iron Shovel <br> <br> <br> It is also possible to " +
                        "program Upgrade Chips with Enchantment data. These do not have multiple levels (so don't waste a Fortune III - you'll get " +
                        "a Fortune I chip), but the effects are stackable with multiple chips.");

        addGuide("compressor.text", "The Compressor converts crop particles into items. Some particles have more multiple options.");
        addGuide("dna_cleaner.text", "The DNA Cleaner sanitizes used DNA containers and prepares them for reuse.");
        addGuide("dna_combiner.text",
                "The DNA Combiner combines two sequences of DNA (lower values are dominant). The used sample sequences are not lost.");
        addGuide("dna_extractor.text",
                "The DNA Extractor stores the DNA sequence of a crop seed into an empty DNA container. This process destroys the seed.");
        addGuide("dna_remover.text",
                "The DNA Remover removes a single trait, selected randomly. Should be used to reduce the sequence to only good traits.");
        addGuide("energy_supplier.text", "The Energy Supplier does what the names says, it supplies energy for blocks like electric fences.");
        addGuide("identifier.text",
                "The Identifier is used to identify crop traits. It can also be used to convert vanilla seeds into PlantTech 2 seeds.");
        addGuide("infuser.text", "The Infuser infuses biomass into things like bonemeal to obtain fertilizer.");
        addGuide("machinebulb_reprocessor.text",
                "Takes energy and biomass to produce Machinebulbs. Available Machinebulbs are limited by the Knowledge Chip installed. Energy " +
                        "needed for each bulb: 1000.        Biomass needed: Compressor, Identifier, Energy Supplier -> 100; Infuser, Chipalyzer, " +
                        "Mega Furnace -> 1000; DNA Cleaner, DNA Combiner, DNA Extractor, DNA Remover, Seed Constructor, Void-Plantfarm, Solar " +
                        "Generator -> 2000;");
        addGuide("mega_furnace.text", "Powered with BE and has 6 slots for smelting.");
        addGuide("void_plantfarm.text",
                "You want to have cropproducts but don't want to have a farm consume your space? The void plantfarm takes a seed and will plant and" +
                        " farm the crops in the void dimension! It consumes energy to create a portal once it can harvest a crop and the production" +
                        " can be increased by putting a rangeupgrade in it! We can't access the dimension in person, but the machine can.");
        addGuide("seedconstructor.text",
                "The seedconstructor takes a sequence of dna, energy and biomass to creates seeds from it. The sequence will not be consumed. If a " +
                        "trait is not set in the sequence, it will be set to the default value.");
        addGuide("seed_squeezer.text",
                "The seed squeezer is a power generator fuelled by PlantTech2 seeds. The amount of power generated is determined by the seed's " +
                        "energy trait. Biomass is also produced, albeit at a fixed rate.");
        addGuide("solar_generator.text",
                "The solar generator needs a solar focus to produce energy. The higher the tier of the focus, the higher the production.");
    }

    private void addInfo()
    {

        addInfo("knowledge", "Knowledge");
        addInfo("tier", "Tier");

        addInfo("type", "Crop Type");
        addInfo("soil", "Soil");
        addInfo("grow_speed", "Growth Rate");
        addInfo("sensitivity", "Sensitivity");
        addInfo("needed_light_level", "Light Level Required");
        addInfo("water_range", "Water Range");
        addInfo("temperature", "Temperature");
        addInfo("temperature_tolerance", "Temperature Tolerance");
        addInfo("productivity", "Productivity");
        addInfo("fertility", "Fertility");
        addInfo("spreading_speed", "Spreading Speed");
        addInfo("gene_strength", "Gene Strength");
        addInfo("energy_value", "Energy Value");

        addInfo("unknown", "Unknown");
        addInfo("only_creative", "Only for Creative Mode.");
        addInfo("only_seeds", "Only for PlantTech 2 seeds.");
        addInfo("no_space", "Not enough space in your inventory.");
        addInfo("not_a_trait", "Not a trait.");

        addInfo("fertilizer", "Chance");
        addInfo("fertilizer_creative", "Ignores requirements");

        addInfo("energy", "energy: %s");
        addInfo("energycosts", "energycosts per action: %s");
        addInfo("openwithshift", "Shift-rightclick to open inventory");

        addInfo("upgradechip.increasecapacity", "+%s energy capacity per chip");
        addInfo("upgradechip.increasecapacitymax", "Max energy capacity: %s");
        addInfo("upgradechip.energyproduction", "+%s energy production per chip");
        addInfo("upgradechip.increaseharvestlevel", "Max energy production: %s");
        addInfo("upgradechip.increasearmor", "+%s armor per chip");
        addInfo("upgradechip.increaseattack", "+%s attack per chip");
        addInfo("upgradechip.increaseattackmax", "Max attack: %s ");
        addInfo("upgradechip.increaseattackspeed", "+%s attack speed per chip");
        addInfo("upgradechip.increaseattackspeedmax", "Max attack speed: %s");
        addInfo("upgradechip.increasebreakdownrate", "+%s breakdown rate per chip");
        addInfo("upgradechip.increasebreakdownratemax", "Max breakdown rate: %s");
        addInfo("upgradechip.increasetougness", "+%s toughness per chip");
        addInfo("upgradechip.increasetougnessmax", "Max toughness: %s");
        addInfo("upgradechip.unlockaxefeat", "Unlocks axe features");
        addInfo("upgradechip.unlockshovelfeat", "Unlocks shovel features");
        addInfo("upgradechip.unlockshearsfeat", "Unlocks shears features");
        addInfo("upgradechip.unlockhoefeat", "Unlocks hoe features");
        addInfo("upgradechip.energycosts", "+%s energy costs per chip");
        addInfo("upgradechip.add", "Add");
        addInfo("upgradechip.stackable", "Enchantment can be stacked");

        addInfo("any_enchanted_item", "Any item with this enchantment. Level doesn`t matter");

        addInfo("wrench_cable", "Right-click cable connections to toggle");
        addInfo("wrench_dismantle", "Right-click while sneaking to dismantle PlantTech2 machines and cables");
    }

    public static Map<String, String> defaultCrops()
    {
        ImmutableMap.Builder<String, String> map = ImmutableMap.builder();

        map.put("abyssalnite", "Abyssalnite");
        map.put("adamantine", "Adamantine");
        map.put("allium", "Allium");
        map.put("aluminum", "Aluminum");
        map.put("aluminum_brass", "Aluminum Brass");
        map.put("alumite", "Alumite");
        map.put("amber", "Amber");
        map.put("apatite", "Apatite");
        map.put("aquamarine", "Aquamarine");
        map.put("ardite", "Ardite");
        map.put("awakened_draconium", "Awakened Draconium");
        map.put("azure_bluet", "Azure Bluet");
        map.put("bamboo", "Bamboo");
        map.put("basalt", "Basalt");
        map.put("beast", "Ravager");
        map.put("beetroots", "Beetroots");
        map.put("black_quartz", "Black Quartz");
        map.put("blaze", "Blaze");
        map.put("blitz", "Blitz");
        map.put("blizz", "Blizz");
        map.put("blue_topaz", "Blue Topaz");
        map.put("blue_orchid", "Blue Orchid");
        map.put("brass", "Brass");
        map.put("bronze", "Bronze");
        map.put("cactus", "Cactus");
        map.put("carrot", "Carrot");
        map.put("certus_quartz", "Certus Quartz");
        map.put("chicken", "Chicken");
        map.put("chimerite", "Chimerite");
        map.put("chorus", "Chorus");
        map.put("chrome", "Chrome");
        map.put("coal", "Coal");
        map.put("cobalt", "Cobalt");
        map.put("cocoa_bean", "Cocoa Bean");
        map.put("cold_iron", "Cold Iron");
        map.put("compressed_iron", "Compressed Iron");
        map.put("conductive_iron", "Conductive Iron");
        map.put("constantan", "Constantan");
        map.put("copper", "Copper");
        map.put("coralium", "Coralium");
        map.put("cornflower", "Cornflower");
        map.put("cow", "Cow");
        map.put("creeper", "Creeper");
        map.put("dancium", "Dancium");
        map.put("dandelion", "Dandelion");
        map.put("dark_gem", "Dark Gem");
        map.put("dark_steel", "Dark Steel");
        map.put("desh", "Desh");
        map.put("diamond", "Diamond");
        map.put("dirt", "Dirt");
        map.put("draconium", "Draconium");
        map.put("dreadium", "Dreadium");
        map.put("drowned", "Drowned");
        map.put("electrical_steel", "Electrical Steel");
        map.put("electrotine", "Electrotine");
        map.put("electrum", "Electrum");
        map.put("elementium", "Elementium");
        map.put("emerald", "Emerald");
        map.put("end_steel", "End Steel");
        map.put("ender_amethyst", "Ender Amethyst");
        map.put("ender_biotite", "Ender Biotite");
        map.put("enderdragon", "Enderdragon");
        map.put("enderium", "Enderium");
        map.put("enderman", "Enderman");
        map.put("endstone", "Endstone");
        map.put("energetic_alloy", "Energetic Alloy");
        map.put("fish", "Fish");
        map.put("fluix_crystal", "Fluix Crystal");
        map.put("fluxed_electrum", "Fluxed Electrum");
        map.put("ghast", "Ghast");
        map.put("glowstone", "Glowstone");
        map.put("glowstone_ingot", "Glowstone Ingot");
        map.put("gold", "Gold");
        map.put("graphite", "Graphite");
        map.put("guardian", "Guardian");
        map.put("husk", "Husk");
        map.put("illager", "Illager");
        map.put("invar", "Invar");
        map.put("iridium", "Iridium");
        map.put("iron", "Iron");
        map.put("kanekium", "Kanekium");
        map.put("kelp", "Kelp");
        map.put("kinnoium", "Kinnoium");
        map.put("knightslime", "Knightslime");
        map.put("lapis", "Lapis");
        map.put("lava", "Lava");
        map.put("lead", "Lead");
        map.put("lenthurium", "Lenthurium");
        map.put("lilly_of_the_valley", "Lily Of The Valley");
        map.put("lithium", "Lithium");
        map.put("lumium", "Lumium");
        map.put("magma_cube", "Magma Cube");
        map.put("magnesium", "Magnesium");
        map.put("malachite", "Malachite");
        map.put("manasteel", "Manasteel");
        map.put("manyullyn", "Manyullyn");
        map.put("melon", "Melon");
        map.put("meteoric_iron", "Meteoric Iron");
        map.put("mithril", "Mithril");
        map.put("moonstone", "Moonstone");
        map.put("mooshroom", "Mooshroom");
        map.put("mushroom", "Mushroom");
        map.put("mycelium", "Mycelium");
        map.put("nether_wart", "Nether Wart");
        map.put("netherrack", "Netherrack");
        map.put("neutronium", "Neutronium");
        map.put("nickel", "Nickel");
        map.put("octine", "Octine");
        map.put("orange_tulip", "Orange Tulip");
        map.put("osmium", "Osmium");
        map.put("oxeye_daisy", "Oxeye Daisy");
        map.put("panda", "Panda");
        map.put("parrot", "Parrot");
        map.put("peridot", "Peridot");
        map.put("pig", "Pig");
        map.put("pink_tulip", "Pink Tulip");
        map.put("plantium", "Plantium");
        map.put("platinum", "Platinum");
        map.put("polarbear", "Polarbear");
        map.put("poppy", "Poppy");
        map.put("potato", "Potato");
        map.put("prismarine", "Prismarine");
        map.put("pulsating_iron", "Pulsating Iron");
        map.put("pumpkin", "Pumpkin");
        map.put("quartz", "Quartz");
        map.put("quicksilver", "Quicksilver");
        map.put("red_tulip", "Red Tulip");
        map.put("redstone", "Redstone");
        map.put("redstone_alloy", "Redstone Alloy");
        map.put("refined_obsidian", "Refined Obsidian");
        map.put("rock_crystal", "Rock Crystal");
        map.put("rubber", "Rubber");
        map.put("ruby", "Ruby");
        map.put("saltpeter", "Saltpeter");
        map.put("sand", "Sand");
        map.put("sapphire", "Sapphire");
        map.put("sheep", "Sheep");
        map.put("shulker", "Shulker");
        map.put("signalum", "Signalum");
        map.put("silicon", "Silicon");
        map.put("silver", "Silver");
        map.put("skeleton", "Skeleton");
        map.put("sky_stone", "Sky Stone");
        map.put("slate", "Slate");
        map.put("slime", "Slime");
        map.put("slimy_bone", "Slimy Bone");
        map.put("snow", "Snow");
        map.put("soularium", "Soularium");
        map.put("soulsand", "Soulsand");
        map.put("spider", "Spider");
        map.put("sponge", "Sponge");
        map.put("squid", "Squid");
        map.put("star_steel", "Star Steel");
        map.put("starmetal", "Starmetal");
        map.put("steel", "Steel");
        map.put("stone", "Stone");
        map.put("stray", "Stray");
        map.put("sugarcane", "Sugarcane");
        map.put("sulfur", "Sulfur");
        map.put("sunstone", "Sunstone");
        map.put("syrmorite", "Syrmorite");
        map.put("tanzanite", "Tanzanite");
        map.put("terrasteel", "Terrasteel");
        map.put("thaumium", "Thaumium");
        map.put("tin", "Tin");
        map.put("titanium", "Titanium");
        map.put("topaz", "Topaz");
        map.put("tungsten", "Tungsten");
        map.put("turtle", "Turtle");
        map.put("uranium", "Uranium");
        map.put("valonite", "Valonite");
        map.put("vibrant_alloy", "Vibrant Alloy");
        map.put("villager", "Villager");
        map.put("vine", "Vine");
        map.put("vinteum", "Vinteum");
        map.put("void_metal", "Void Metal");
        map.put("water", "Water");
        map.put("wheat", "Wheat");
        map.put("white_tulip", "White Tulip");
        map.put("witch", "Witch");
        map.put("wither", "Wither");
        map.put("wither_rose", "Wither Rose");
        map.put("wither_skeleton", "Wither Skeleton");
        map.put("wood", "Wood");
        map.put("yellorium", "Yellorium");
        map.put("zinc", "Zinc");
        map.put("zombie", "Zombie");
        map.put("zombie_pigman", "Zombie Pigman");
        map.put("zombie_villager", "Zombie Villager");

        return map.build();
    }

    public void addInfo(String key, String value)
    {
        add("planttech2.info." + key, value);
    }

    public void addGuide(String key, String value)
    {
        add("guide", key, value);
    }

    public void add(String category, String subKey, String value)
    {
        add(category + "." + MODID + (!subKey.isEmpty() ? "." + subKey : ""), value);
    }
}
