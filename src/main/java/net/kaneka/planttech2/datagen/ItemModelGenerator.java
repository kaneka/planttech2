package net.kaneka.planttech2.datagen;

import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.registries.ModBlocks;
import net.minecraft.data.PackOutput;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.client.model.generators.ItemModelProvider;
import net.minecraftforge.client.model.generators.ModelFile;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class ItemModelGenerator extends ItemModelProvider
{
	public ItemModelGenerator(PackOutput generator, ExistingFileHelper existingFileHelper)
	{
		super(generator, PlantTechMain.MODID, existingFileHelper);
	}

	@Override
	protected void registerModels()
	{
		block(ModBlocks.RED_MUSHROOM_STAIRS.get());
		block(ModBlocks.RED_MUSHROOM_SLAB.get());
		trapdoorItem(ModBlocks.RED_MUSHROOM_TRAPDOOR.get());
		block(ModBlocks.RED_MUSHROOM_FENCE_GATE.get());
		fenceItem(ModBlocks.RED_MUSHROOM_FENCE.get());
		generated(ModBlocks.RED_MUSHROOM_CANDLE.get());

		block(ModBlocks.BROWN_MUSHROOM_STAIRS.get());
		block(ModBlocks.BROWN_MUSHROOM_SLAB.get());
		trapdoorItem(ModBlocks.BROWN_MUSHROOM_TRAPDOOR.get());
		block(ModBlocks.BROWN_MUSHROOM_FENCE_GATE.get());
		fenceItem(ModBlocks.BROWN_MUSHROOM_FENCE.get());
		generated(ModBlocks.BROWN_MUSHROOM_CANDLE.get());

		block(ModBlocks.CRIMSON_HYPHAE_STAIRS.get());
		block(ModBlocks.CRIMSON_HYPHAE_SLAB.get());
		trapdoorItem(ModBlocks.CRIMSON_HYPHAE_TRAPDOOR.get());
		block(ModBlocks.CRIMSON_HYPHAE_FENCE_GATE.get());
		fenceItem(ModBlocks.CRIMSON_HYPHAE_FENCE.get());
		generated(ModBlocks.CRIMSON_HYPHAE_CANDLE.get());

		block(ModBlocks.WARPED_HYPHAE_STAIRS.get());
		block(ModBlocks.WARPED_HYPHAE_SLAB.get());
		trapdoorItem(ModBlocks.WARPED_HYPHAE_TRAPDOOR.get());
		block(ModBlocks.WARPED_HYPHAE_FENCE_GATE.get());
		fenceItem(ModBlocks.WARPED_HYPHAE_FENCE.get());
		generated(ModBlocks.WARPED_HYPHAE_CANDLE.get());
	}

	private void trapdoorItem(Block block)
	{
		String name = ForgeRegistries.BLOCKS.getKey(block).getPath();
		withExistingParent(name, modLoc("block/" + name) + "_bottom");
	}

	private void fenceItem(Block block)
	{
		String name = ForgeRegistries.BLOCKS.getKey(block).getPath();
		withExistingParent(name, modLoc("block/" + name) + "_inventory");
	}

	private void registerBlockModels()
	{
		for (RegistryObject<Block> registryObject: ModBlocks.HEDGE_BLOCKS)
		{
			blockHedge(registryObject.get());
		}
	}

	private void generatedItemModel(String name)
	{
		getBuilder(name)
				.parent(new ModelFile.UncheckedModelFile("item/generated"))
				.texture("layer0", modLoc("item/" + name));
	}

	private void generated(Block b)
	{
		generatedItemModel(ForgeRegistries.BLOCKS.getKey(b).getPath());
	}

	private void block(Block b)
	{
		block(b, modLoc("block/" + ForgeRegistries.BLOCKS.getKey(b).getPath()));

	}

	private void block(Block b, ResourceLocation parent)
	{
		withExistingParent(ForgeRegistries.BLOCKS.getKey(b).getPath(), parent);
	}

	private void buildingBlock(Block b)
	{
		withExistingParent(ForgeRegistries.BLOCKS.getKey(b).getPath(), modLoc("block/building_blocks/" + ForgeRegistries.BLOCKS.getKey(b).getPath()));
	}

	private void blockHedge(Block b)
	{
		withExistingParent(ForgeRegistries.BLOCKS.getKey(b).getPath(), modLoc("block/hedge/" + ForgeRegistries.BLOCKS.getKey(b).getPath().replace("hedge_", "") + "_base"));
	}
	
	@Override
    public String getName() 
	{
        return " planttech2 item model generator";
    }

}
