package net.kaneka.planttech2.datagen.recipes;

import net.kaneka.planttech2.blocks.Hedge;
import net.kaneka.planttech2.registries.ModBlocks;
import net.kaneka.planttech2.registries.ModItems;
import net.minecraft.advancements.critereon.InventoryChangeTrigger;
import net.minecraft.data.PackOutput;
import net.minecraft.data.recipes.FinishedRecipe;
import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.data.recipes.RecipeProvider;
import net.minecraft.data.recipes.ShapedRecipeBuilder;
import net.minecraft.data.recipes.ShapelessRecipeBuilder;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraftforge.common.Tags;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

import java.util.function.Consumer;

import static net.kaneka.planttech2.registries.ModItems.PLANTIUM_INGOT;
import static net.kaneka.planttech2.registries.ModItems.RAKE;
import static net.minecraft.world.item.Items.CANDLE;

public class ItemRecipeProvider extends RecipeProvider
{
	public ItemRecipeProvider(PackOutput gen)
	{
		super(gen);
	}

	private Consumer<FinishedRecipe> cons;

	@Override
	protected void buildRecipes(Consumer<FinishedRecipe> cons)
	{
		this.cons = cons;
		registerHedgeRecipes(cons);
		slab(ModBlocks.BROWN_MUSHROOM_SLAB.get(), Blocks.BROWN_MUSHROOM_BLOCK, cons);
		slab(ModBlocks.RED_MUSHROOM_SLAB.get(), Blocks.RED_MUSHROOM_BLOCK, cons);
		slab(ModBlocks.CRIMSON_HYPHAE_SLAB.get(), Blocks.CRIMSON_HYPHAE, cons);
		slab(ModBlocks.WARPED_HYPHAE_SLAB.get(), Blocks.WARPED_HYPHAE, cons);
		stairs(ModBlocks.BROWN_MUSHROOM_STAIRS.get(), Blocks.BROWN_MUSHROOM_BLOCK, cons);
		stairs(ModBlocks.RED_MUSHROOM_STAIRS.get(), Blocks.RED_MUSHROOM_BLOCK, cons);
		stairs(ModBlocks.CRIMSON_HYPHAE_STAIRS.get(), Blocks.CRIMSON_HYPHAE, cons);
		stairs(ModBlocks.WARPED_HYPHAE_STAIRS.get(), Blocks.WARPED_HYPHAE, cons);
		trapdoors(ModBlocks.BROWN_MUSHROOM_TRAPDOOR.get(), Blocks.BROWN_MUSHROOM_BLOCK, cons);
		trapdoors(ModBlocks.RED_MUSHROOM_TRAPDOOR.get(), Blocks.RED_MUSHROOM_BLOCK, cons);
		trapdoors(ModBlocks.CRIMSON_HYPHAE_TRAPDOOR.get(), Blocks.CRIMSON_HYPHAE, cons);
		trapdoors(ModBlocks.WARPED_HYPHAE_TRAPDOOR.get(), Blocks.WARPED_HYPHAE, cons);
		candles(ModBlocks.BROWN_MUSHROOM_CANDLE.get(), Blocks.BROWN_MUSHROOM_BLOCK, cons);
		candles(ModBlocks.RED_MUSHROOM_CANDLE.get(), Blocks.RED_MUSHROOM_BLOCK, cons);
		candles(ModBlocks.CRIMSON_HYPHAE_CANDLE.get(), Blocks.CRIMSON_HYPHAE, cons);
		candles(ModBlocks.WARPED_HYPHAE_CANDLE.get(), Blocks.WARPED_HYPHAE, cons);
		fence(ModBlocks.BROWN_MUSHROOM_FENCE.get(), Blocks.BROWN_MUSHROOM_BLOCK, cons);
		fence(ModBlocks.RED_MUSHROOM_FENCE.get(), Blocks.RED_MUSHROOM_BLOCK, cons);
		fence(ModBlocks.CRIMSON_HYPHAE_FENCE.get(), Blocks.CRIMSON_HYPHAE, cons);
		fence(ModBlocks.WARPED_HYPHAE_FENCE.get(), Blocks.WARPED_HYPHAE, cons);
		fenceGate(ModBlocks.BROWN_MUSHROOM_FENCE_GATE.get(), Blocks.BROWN_MUSHROOM_BLOCK, cons);
		fenceGate(ModBlocks.RED_MUSHROOM_FENCE_GATE.get(), Blocks.RED_MUSHROOM_BLOCK, cons);
		fenceGate(ModBlocks.CRIMSON_HYPHAE_FENCE_GATE.get(), Blocks.CRIMSON_HYPHAE, cons);
		fenceGate(ModBlocks.WARPED_HYPHAE_FENCE_GATE.get(), Blocks.WARPED_HYPHAE, cons);

		tiered("ABA", "BCB", "ABA", ModItems.SOLARFOCUS_TIER_1, Blocks.GLASS_PANE, ModItems.PLANTIUM_INGOT.get(), Items.DIAMOND);
		tiered("ABA", "BCB", "ABA", ModItems.SOLARFOCUS_TIER_2, Blocks.GLASS_PANE, ModItems.SOLARFOCUS_TIER_1.get(), Items.DIAMOND);
		tiered("ABA", "BCB", "ABA", ModItems.SOLARFOCUS_TIER_3, Blocks.GLASS_PANE, ModItems.SOLARFOCUS_TIER_2.get(), Items.DIAMOND);
		tiered("ABA", "BCB", "ABA", ModItems.SOLARFOCUS_TIER_4, Blocks.GLASS_PANE, ModItems.SOLARFOCUS_TIER_3.get(), Items.DIAMOND);
		tiered("ABA", "BCB", "ABA", ModItems.SPEEDUPGRADE_TIER_1, Blocks.GLASS_PANE, ModItems.PLANTIUM_INGOT.get(), Items.EMERALD);
		tiered("ABA", "BCB", "ABA", ModItems.SPEEDUPGRADE_TIER_2, Blocks.GLASS_PANE, ModItems.SPEEDUPGRADE_TIER_1.get(), Items.EMERALD);
		tiered("ABA", "BCB", "ABA", ModItems.SPEEDUPGRADE_TIER_3, Blocks.GLASS_PANE, ModItems.SPEEDUPGRADE_TIER_2.get(), Items.EMERALD);
		tiered("ABA", "BCB", "ABA", ModItems.SPEEDUPGRADE_TIER_4, Blocks.GLASS_PANE, ModItems.SPEEDUPGRADE_TIER_3.get(), Items.EMERALD);
		tiered("ABA", "BCB", "ABA", ModItems.CAPACITYUPGRADE_TIER_1, Blocks.GLASS_PANE, ModItems.PLANTIUM_INGOT.get(), Items.REDSTONE_BLOCK);
		tiered("ABA", "BCB", "ABA", ModItems.CAPACITYUPGRADE_TIER_2, Blocks.GLASS_PANE, ModItems.CAPACITYUPGRADE_TIER_1.get(), Items.DIAMOND);
		tiered("ABA", "BCB", "ABA", ModItems.CAPACITYUPGRADE_TIER_3, Blocks.GLASS_PANE, ModItems.CAPACITYUPGRADE_TIER_2.get(), Items.DIAMOND);
		tiered("ABA", "BCB", "ABA", ModItems.CAPACITYUPGRADE_TIER_4, Blocks.GLASS_PANE, ModItems.CAPACITYUPGRADE_TIER_3.get(), Items.DIAMOND);
		tiered("ABA", "BCB", "ABA", ModItems.RANGEUPGRADE_TIER_1, Blocks.GLASS_PANE, ModItems.PLANTIUM_INGOT.get(), Items.WOODEN_HOE);
		tiered("ABA", "BCB", "ABA", ModItems.RANGEUPGRADE_TIER_2, Blocks.GLASS_PANE, ModItems.RANGEUPGRADE_TIER_1.get(), Items.IRON_HOE);
		tiered("ABA", "BCB", "ABA", ModItems.RANGEUPGRADE_TIER_3, Blocks.GLASS_PANE, ModItems.RANGEUPGRADE_TIER_2.get(), Items.GOLDEN_HOE);
		tiered("ABA", "BCB", "ABA", ModItems.RANGEUPGRADE_TIER_4, Blocks.GLASS_PANE, ModItems.RANGEUPGRADE_TIER_3.get(), Items.DIAMOND_HOE);

		ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, RAKE.get())
				.pattern("III")
				.pattern(" P ")
				.pattern(" P ").define('I', Tags.Items.INGOTS_IRON).define('P', PLANTIUM_INGOT.get()).unlockedBy(getHasName(PLANTIUM_INGOT.get()), has(PLANTIUM_INGOT.get())).save(cons);
	}

	private void tiered(String r, String r2, String r3, RegistryObject<Item> item, ItemLike a, ItemLike b, ItemLike c)
	{
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, item.get()).pattern(r).pattern(r2).pattern(r3).define('A', a).define('B', b).define('C', c).unlockedBy(getHasName(a), has(a)).save(cons);
	}

	private void slab(ItemLike item, ItemLike material, Consumer<FinishedRecipe> cons)
	{
		simple(item, 6, material, (builder) -> builder.pattern("mmm"), cons);
	}

	private void stairs(ItemLike item, ItemLike material, Consumer<FinishedRecipe> cons)
	{
		simple(item, 4, material, (builder) -> builder.pattern("m  ").pattern("mm ").pattern("mmm"), cons);
	}

	private void trapdoors(ItemLike item, ItemLike material, Consumer<FinishedRecipe> cons)
	{
		simple(item, 2, material, (builder) -> builder.pattern("mmm").pattern("mmm"), cons);
	}

	private void candles(ItemLike item, ItemLike material, Consumer<FinishedRecipe> cons)
	{
		shapeless(RecipeCategory.DECORATIONS, item, cons, material, CANDLE);
	}

	private void fence(ItemLike item, ItemLike material, Consumer<FinishedRecipe> cons)
	{
		ShapedRecipeBuilder.shaped(RecipeCategory.BUILDING_BLOCKS, item, 3).pattern("wsw").pattern("wsw").define('w', material).define('s', Tags.Items.RODS_WOODEN).unlockedBy(ForgeRegistries.ITEMS.getKey(material.asItem()).getPath(), InventoryChangeTrigger.TriggerInstance.hasItems(material)).save(cons);
	}

	private void fenceGate(ItemLike item, ItemLike material, Consumer<FinishedRecipe> cons)
	{
		ShapedRecipeBuilder.shaped(RecipeCategory.BUILDING_BLOCKS, item, 1).pattern("wsw").pattern("wsw").define('s', material).define('w', Tags.Items.RODS_WOODEN).unlockedBy(ForgeRegistries.ITEMS.getKey(material.asItem()).getPath(), InventoryChangeTrigger.TriggerInstance.hasItems(material)).save(cons);
	}

	private void simple(ItemLike item, int count, ItemLike material, Consumer<ShapedRecipeBuilder> pattern, Consumer<FinishedRecipe> cons)
	{
		ShapedRecipeBuilder builder = ShapedRecipeBuilder.shaped(RecipeCategory.BUILDING_BLOCKS, item, count);
		pattern.accept(builder);
		builder.define('m', material).unlockedBy(ForgeRegistries.ITEMS.getKey(material.asItem()).getPath(), InventoryChangeTrigger.TriggerInstance.hasItems(material)).save(cons);
	}

	private void shapeless(RecipeCategory category, ItemLike item, Consumer<FinishedRecipe> cons, ItemLike... materials)
	{
		ShapelessRecipeBuilder builder = ShapelessRecipeBuilder.shapeless(category, item);
		for (ItemLike material : materials)
			builder.requires(material);
		ItemLike unlock = materials[0];
		builder.unlockedBy(ForgeRegistries.ITEMS.getKey(unlock.asItem()).getPath(), InventoryChangeTrigger.TriggerInstance.hasItems(unlock)).save(cons);
	}


	private void registerHedgeRecipes(Consumer<FinishedRecipe> cons)
	{
		for(RegistryObject<Block> registryObject: ModBlocks.HEDGE_BLOCKS)
		{
			Hedge block = (Hedge) registryObject.get();
			ShapedRecipeBuilder.shaped(RecipeCategory.BUILDING_BLOCKS, block, 6)
			.pattern(" A ")
			.pattern(" A ")
			.pattern("BCB")
			.define('A', block.getLeaves())
			.define('B', block.getWood())
			.define('C', block.getSoil())
			.unlockedBy("leaves", InventoryChangeTrigger.TriggerInstance.hasItems(block.getLeaves()))
			.save(cons);
		}
	}

	private void makeTieredAuraCore(ItemLike ingredient, ItemLike... cores)
	{
		makeAuraCore(cores[0], ingredient, PLANTIUM_INGOT.get(), PLANTIUM_INGOT.get());
		for (int i = 1; i < cores.length; i++)
			makeAuraCore(cores[i], cores[i - 1], ingredient, cores[i - 1]);
	}

	private void makeAuraCore(ItemLike product, ItemLike middle, ItemLike side)
	{
		makeAuraCore(product, middle, side, middle);
	}

	private void makeAuraCore(ItemLike product, ItemLike middle, ItemLike side, ItemLike criterion)
	{
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, product)
				.pattern(" S ")
				.pattern("SMS")
				.pattern(" S ")
				.define('S', side)
				.define('M', middle)
				.unlockedBy("plantium", has(criterion))
				.save(cons);
	}
}
