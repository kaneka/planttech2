package net.kaneka.planttech2.datagen.recipes;

import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.kaneka.planttech2.PlantTechMain;
import net.minecraft.data.CachedOutput;
import net.minecraft.data.DataProvider;
import net.minecraft.data.PackOutput;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.Container;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.Recipe;
import net.minecraftforge.registries.ForgeRegistries;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public abstract class MachineRecipeProvider<R extends Recipe<Container>> implements DataProvider
{
    protected static final Gson GSON = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();
    private final PackOutput generator;
    protected final String folder;
    protected HashMap<ResourceLocation, R> recipes = new HashMap<>();

    protected MachineRecipeProvider(PackOutput generator, String folder)
    {
        this.generator = generator;
        this.folder = "recipes/" + folder;
    }

    @Override
    public CompletableFuture<?> run(CachedOutput cache)
    {
        Path resourceRoot = generator.getOutputFolder();

        putRecipes();

        List<CompletableFuture<?>> completables = new ArrayList<>();
        recipes.forEach((key, recipe) -> {
            Path target = getPath(resourceRoot, key);

            completables.add(DataProvider.saveStable(cache, write(recipe), target));

            //            try
//            {
//                if (!Files.exists(target.getParent()))
//                    Files.createDirectory(target.getParent());
//            }
//            catch (IOException e)
//            {
//                e.printStackTrace();
//            }
//            String data = GSON.toJson(write(recipe));
//            ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
//            HashingOutputStream hashingoutputstream = new HashingOutputStream(Hashing.sha1(), bytearrayoutputstream);
//            try (Writer writer = new OutputStreamWriter(hashingoutputstream, StandardCharsets.UTF_8))
//            {
//                writer.write(data);
//                cache.writeIfNeeded(target, bytearrayoutputstream.toByteArray(), hashingoutputstream.hash());
//                bytearrayoutputstream.close();
//                hashingoutputstream.close();
//            }
//            catch (IOException e)
//            {
//                e.printStackTrace();
//            }
        });
        return CompletableFuture.allOf(completables.toArray(new CompletableFuture[0]));
    }

    private Path getPath(Path pathIn, ResourceLocation id)
    {
        return pathIn.resolve("data/" + id.getNamespace() + "/" + folder + "/" + id.getPath() + ".json");
    }

    protected JsonObject write(R recipe)
    {
        JsonObject json = new JsonObject();
        json.addProperty("type", PlantTechMain.MODID + ":" + getName());
        return json;
    }

    protected abstract void putRecipes();

    protected void putAll(R... recipes)
    {
        for (R r : recipes)
            put(r);
    }

    protected void put(R recipe)
    {
        recipes.put(recipe.getId(), recipe);
    }

    protected void addItem(JsonObject json, String key, Ingredient ingredient)
    {
        json.add(key, ingredient.toJson());
    }

    protected void addItem(JsonObject json, String key, Item item)
    {
        add(json, key, "item", ForgeRegistries.ITEMS.getKey(item).toString());
    }

    protected void addItem(JsonObject json, String key, ItemStack stack)
    {
        json.add(key, JsonParser.parseString(GSON.toJson(ImmutableMap.of(
                "item", ForgeRegistries.ITEMS.getKey(stack.getItem()).toString(),
                "count", stack.getCount()
        ))));
    }

    protected void addWithBiomass(JsonObject json, String key, Item stack, int biomass)
    {
        json.add(key, JsonParser.parseString(GSON.toJson(ImmutableMap.of(
                "item", ForgeRegistries.ITEMS.getKey(stack).toString(),
                "biomass", biomass
        ))));
    }

    protected void addWithBiomass(JsonObject json, String key, Ingredient ingredient, int biomass)
    {
        json.addProperty("biomass", biomass);
        json.add(key, ingredient.toJson());
    }

    protected void add(JsonObject json, String key, String subKey, String subValue)
    {
        json.add(key, JsonParser.parseString(GSON.toJson(ImmutableMap.of(
                subKey, subValue
        ))));
    }
}
