package net.kaneka.planttech2.datagen.recipes;

import com.google.gson.JsonObject;
import net.kaneka.planttech2.recipes.recipeclasses.InfuserRecipe;
import net.minecraft.data.PackOutput;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraftforge.registries.ForgeRegistries;

import static net.kaneka.planttech2.datagen.recipes.InfuserRecipesProvider.Builder.create;
import static net.kaneka.planttech2.registries.ModItems.*;
import static net.minecraft.world.item.Items.BONE_MEAL;
import static net.minecraft.world.item.Items.REDSTONE;

public class InfuserRecipesProvider extends MachineRecipeProvider<InfuserRecipe>
{
    public InfuserRecipesProvider(PackOutput generator)
    {
        super(generator, "infuser");
    }

    @Override
    protected void putRecipes()
    {
        putAll(
                create("fertilizer_tier_1", 100).in(BONE_MEAL).out("planttech2:fertilizer_tier_1").build(),
                create("fertilizer_tier_2", 200).in(FERTILIZER_TIER_1.get()).out("planttech2:fertilizer_tier_2").build(),
                create("fertilizer_tier_3", 300).in(FERTILIZER_TIER_2.get()).out("planttech2:fertilizer_tier_3").build(),
                create("fertilizer_tier_4", 400).in(FERTILIZER_TIER_3.get()).out("planttech2:fertilizer_tier_4").build(),
                create("gear_dancium_infused", 200).in(GEAR_DANCIUM.get()).out("planttech2:gear_dancium_infused").build(),
                create("gear_iron_infused", 200).in(GEAR_IRON.get()).out("planttech2:gear_iron_infused").build(),
                create("gear_kanekium_infused", 200).in(GEAR_KANEKIUM.get()).out("planttech2:gear_kanekium_infused").build(),
                create("gear_kinnoium_infused", 200).in(GEAR_KINNOIUM.get()).out("planttech2:gear_kinnoium_infused").build(),
                create("gear_lenthurium_infused", 200).in(GEAR_LENTHURIUM.get()).out("planttech2:gear_lenthurium_infused").build(),
                create("gear_plantium_infused", 200).in(GEAR_PLANTIUM.get()).out("planttech2:gear_plantium_infused").build(),
                create("redstone_infused", 250).in(REDSTONE).out("planttech2:redstone_infused").build()
        );

        CATALYSTS.get().forEach((item, pair) -> {
            Item ingredient = pair.getA();
            int level = pair.getB().getB();
            put(create(item, 100 * (level + 1)).in(ingredient).out(item).build());
        });
    }

    @Override
    protected JsonObject write(InfuserRecipe recipe)
    {
        JsonObject json = super.write(recipe);
        addWithBiomass(json, "input", recipe.getInput(), recipe.getBiomass());
        addItem(json, "result", recipe.getResultItem(null));
        return json;
    }

    @Override
    public String getName()
    {
        return "infusing";
    }

    static class Builder extends RecipeBuilder
    {
        private Ingredient input;
        private final int biomass;
        private ItemStack output;

        public static Builder create(String name, int biomass)
        {
            return new Builder(name, biomass);
        }

        public static Builder create(Item item, int biomass)
        {
            return new Builder(item, biomass);
        }

        private Builder(Item item, int biomass)
        {
            this(ForgeRegistries.ITEMS.getKey(item).getPath(), biomass);
        }

        private Builder(String name, int biomass)
        {
            super(name);
            this.biomass = biomass;
        }

        public Builder in(String input)
        {
            return in(getItemFromString(input));
        }

        public Builder in(Item input)
        {
            this.input = Ingredient.of(input);
            return this;
        }

        public Builder out(String output)
        {
            return out(getItemFromString(output));
        }

        public Builder out(Item output)
        {
            this.output = new ItemStack(output);
            return this;
        }

        public InfuserRecipe build()
        {
            return new InfuserRecipe(id, input, output, biomass);
        }
    }
}
