package net.kaneka.planttech2.datagen.recipes;

import com.google.gson.JsonObject;
import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.addons.Pair;
import net.kaneka.planttech2.crops.CropTypes;
import net.kaneka.planttech2.recipes.recipeclasses.CompressorRecipe;
import net.minecraft.data.PackOutput;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Blocks;
import net.minecraftforge.common.Tags;
import net.minecraftforge.registries.ForgeRegistries;

import java.util.ArrayList;
import java.util.List;

import static net.kaneka.planttech2.crops.CropTypes.*;
import static net.kaneka.planttech2.datagen.recipes.CompressorRecipesProvider.Builder.USED;
import static net.kaneka.planttech2.datagen.recipes.CompressorRecipesProvider.Builder.create;
import static net.kaneka.planttech2.registries.ModItems.*;
import static net.minecraft.world.item.Items.*;

public class CompressorRecipesProvider extends MachineRecipeProvider<CompressorRecipe>
{
    private static CompressorRecipesProvider INSTANCE = null;

    public CompressorRecipesProvider(PackOutput generator)
    {
        super(generator, "compressor");
        INSTANCE = this;
    }

    @Override
    protected void putRecipes()
    {
        create().in(ALLAY_CROP, 8).out(AMETHYST_SHARD).build();
        create().in(AMETHYST_CROP, 8).out(AMETHYST_SHARD).build();
        create().in(AMETHYST_CROP, 64).out(AMETHYST_CLUSTER).build();
        create().in(BEE_CROP, 12).out(HONEY_BOTTLE).out(ItemTags.FLOWERS).build();
        create().in(BLAZE_CROP, 8).out(Tags.Items.RODS_BLAZE).build();
        create().in(CHICKEN_CROP, 8).out(CHICKEN).out(Tags.Items.FEATHERS).build();
        create().in(COAL_CROP, 8).out(ItemTags.COALS).build();
        create().in(COAL_CROP, 12).out(Tags.Items.ORES_COAL).build();
        create().in(COLOR_PARTICLES.get(), 8).out(Tags.Items.DYES).build();
        create().in(COPPER_CROP, 8).out(Tags.Items.RAW_MATERIALS_COPPER).build();
        create().in(COPPER_CROP, 12).out(Tags.Items.ORES_COPPER).build();
        create().in(COW_CROP, 8).out(BEEF).out(Tags.Items.LEATHER).build();
        create().in(CREEPER_CROP, 8).out(Tags.Items.GUNPOWDER).build();
        create().in(DANCIUM_CROP, 8).out(DANCIUM_INGOT.get()).build();
        create().in(DIAMOND_CROP, 8).out(Tags.Items.GEMS_DIAMOND).build();
        create().in(DIAMOND_CROP, 12).out(Tags.Items.ORES_DIAMOND).build();
        create().in(DIRT_CROP, 8).out(ItemTags.DIRT).out(GRASS_BLOCK).build();
        create().in(DROWNED_CROP, 8).out(ROTTEN_FLESH).build();
        create().in(DROWNED_CROP, 32).out(TRIDENT).build();
        create().in(EMERALD_CROP, 8).out(Tags.Items.GEMS_EMERALD).build();
        create().in(EMERALD_CROP, 12).out(Tags.Items.ORES_EMERALD).build();
        create().in(ENDERDRAGON_CROP, 8).out(DRAGON_BREATH, DRAGON_EGG).build();
        create().in(ENDERMAN_CROP, 8).out(Tags.Items.ENDER_PEARLS).build();
        create().in(ENDSTONE_CROP, 8).out(Tags.Items.END_STONES).build();
        create().in(FISH_CROP, 8).out(ItemTags.FISHES).build();
        create().in(FROG_CROP, 64).out(Blocks.OCHRE_FROGLIGHT, Blocks.PEARLESCENT_FROGLIGHT, Blocks.VERDANT_FROGLIGHT).build();
        create().in(GHAST_CROP, 8).out(GHAST_TEAR).build();
        create().in(GLOWSTONE_CROP, 8).out(GLOWSTONE).build();
        create().in(GLOWSTONE_CROP, 2).out(Tags.Items.DUSTS_GLOWSTONE).build();
        create().in(GOAT_CROP, 32).out(GOAT_HORN).build();
        create().in(GOLD_CROP, 8).out(Tags.Items.RAW_MATERIALS_GOLD).build();
        create().in(GOLD_CROP, 12).out(Tags.Items.ORES_GOLD).build();
        create().in(GUARDIAN_CROP, 8).out(EXPERIENCE_BOTTLE).build();
        create().in(HUSK_CROP, 8).out(Tags.Items.INGOTS_IRON).build();
        create().in(ILLAGER_CROP, 24).out(IRON_AXE).build();
        create().in(ILLAGER_CROP, 32).out(IRON_BOOTS).build();
        create().in(ILLAGER_CROP, 64).out(IRON_CHESTPLATE).build();
        create().in(ILLAGER_CROP, 40).out(IRON_HELMET).build();
        create().in(ILLAGER_CROP, 56).out(IRON_LEGGINGS).build();
        create().in(ILLAGER_CROP, 24).out(IRON_PICKAXE).build();
        create().in(ILLAGER_CROP, 8).out(IRON_SHOVEL).build();
        create().in(ILLAGER_CROP, 16).out(IRON_SWORD).build();
        create().in(IRON_CROP, 8).out(Tags.Items.RAW_MATERIALS_IRON).build();
        create().in(IRON_CROP, 12).out(Tags.Items.ORES_IRON).build();
        create().in(KANEKIUM_CROP, 8).out(KANEKIUM_INGOT.get()).build();
        create().in(KINNOIUM_CROP, 8).out(KINNOIUM_INGOT.get()).build();
        create().in(LAPIS_CROP, 8).out(Tags.Items.GEMS_LAPIS).build();
        create().in(LAPIS_CROP, 12).out(Tags.Items.ORES_LAPIS).build();
        create().in(LAVA_CROP, 8).out(LAVA_BUCKET).build();
        create().in(LENTHURIUM_CROP, 8).out(LENTHURIUM_INGOT.get()).build();
        create().in(MAGMA_CUBE_CROP, 8).out(MAGMA_CREAM).build();
        create().in(MYCELIUM_CROP, 8).out(MYCELIUM).build();
        create().in(NETHERRACK_CROP, 8).out(Tags.Items.NETHERRACK).build();
        create().in(NETHERITE_CROP, 8).out(Tags.Items.INGOTS_NETHERITE).build();
        create().in(NETHERITE_CROP, 12).out(Tags.Items.ORES_NETHERITE_SCRAP).build();
        create().in(PARROT_CROP, 8).out(COLOR_PARTICLES.get(), 8).out(Tags.Items.FEATHERS).build();
        create().in(PIG_CROP, 8).out(PORKCHOP).build();
        create().in(PLANTIUM_CROP, 8).out(PLANTIUM_INGOT.get()).build();
        create().in(PRISMARINE_CROP, 8).out(Tags.Items.GEMS_PRISMARINE, Tags.Items.DUSTS_PRISMARINE).build();
        create().in(QUARTZ_CROP, 8).out(Tags.Items.GEMS_QUARTZ).build();
        create().in(QUARTZ_CROP, 12).out(Tags.Items.ORES_QUARTZ).build();
        create().in(RABBIT_CROP, 12).out(RABBIT_HIDE, RABBIT_FOOT).build();
        create().in(REDSTONE_CROP, 8).out(Tags.Items.DUSTS_REDSTONE).build();
        create().in(REDSTONE_CROP, 12).out(Tags.Items.ORES_REDSTONE).build();
        create().in(SAND_CROP, 8).out(Tags.Items.SAND).build();
        create().in(SAND_CROP, 32).out(Tags.Items.SANDSTONE).build();
        create().in(SHEEP_CROP, 8).out(MUTTON).out(ItemTags.WOOL).build();
        create().in(SHULKER_CROP, 8).out(SHULKER_SHELL).build();
        create().in(SKELETON_CROP, 8).out(ARROW).out(Tags.Items.BONES).build();
        create().in(SLIME_CROP, 8).out(Tags.Items.SLIMEBALLS).build();
        create().in(SNOW_CROP, 8).out(ICE).build();
        create().in(SNOW_CROP, 8).out(SNOWBALL, 4).build();
        create().in(SOULSAND_CROP, 8).out(SOUL_SAND).build();
        create().in(SPIDER_CROP, 8).out(SPIDER_EYE).out(Tags.Items.STRING).build();
        create().in(SPONGE_CROP, 8).out(SPONGE).build();
        create().in(SQUID_CROP, 8).out(INK_SAC).build();
        create().in(STONE_CROP, 8).out(Tags.Items.STONE, Tags.Items.COBBLESTONE).build();
        create().in(STRAY_CROP, 8).out(ARROW).out(Tags.Items.BONES).build();
        create().in(SWEET_BERRIES_CROP, 8).out(SWEET_BERRIES).build();
        create().in(TURTLE_CROP, 64).out(TURTLE_HELMET).build();
        create().in(TURTLE_CROP, 8).out(SCUTE).build();
        create().in(VILLAGER_CROP, 8).out(Tags.Items.GEMS_EMERALD).build();
        create().in(WARDEN_CROP, 8).out(SCULK, SCULK_VEIN).build();
        create().in(WARDEN_CROP, 64).out(SCULK_CATALYST).build();
        create().in(WATER_CROP, 8).out(WATER_BUCKET).build();
        create().in(WITCH_CROP, 8).out(GLASS_BOTTLE).out(Tags.Items.DUSTS_REDSTONE, Tags.Items.DUSTS_GLOWSTONE).build();
        create().in(WITHER_CROP, 8).out(Tags.Items.NETHER_STARS).build();
        create().in(WITHER_SKELETON_CROP, 8).out(WITHER_SKELETON_SKULL).build();
        create().in(WOOD_CROP, 8).out(ItemTags.LOGS).build();
        create().in(ZOMBIE_CROP, 8).out(ROTTEN_FLESH).build();
        create().in(ZOMBIE_PIGMAN_CROP, 8).out(Tags.Items.NUGGETS_GOLD).build();
        create().in(ZOMBIE_VILLAGER_CROP, 8).out(Tags.Items.INGOTS_IRON).build();

        // Non-particles
        create().in(CRACKED_SNAIL_SHELL_GARDEN.get(), 64).out(NAUTILUS_SHELL).build();
        create().in(CRACKED_SNAIL_SHELL_BLACK.get(), 16).out(NAUTILUS_SHELL).build();
        create().in(CRACKED_SNAIL_SHELL_MILK.get(), 12).out(NAUTILUS_SHELL).build();
        create().in(CRACKED_SNAIL_SHELL_PINK.get(), 6).out(NAUTILUS_SHELL).build();
        create().in(CRACKED_SNAIL_SHELL_CYBER.get(), 4).out(NAUTILUS_SHELL).build();

        create().in(CRACKED_SNAIL_SHELL_GARDEN.get(), 1).out(SNAIL_SHELL_DUST_GARDEN.get()).build();
        create().in(CRACKED_SNAIL_SHELL_BLACK.get(), 1).out(SNAIL_SHELL_DUST_BLACK.get()).build();
        create().in(CRACKED_SNAIL_SHELL_MILK.get(), 1).out(SNAIL_SHELL_DUST_MILK.get()).build();
        create().in(CRACKED_SNAIL_SHELL_PINK.get(), 1).out(SNAIL_SHELL_DUST_PINK.get()).build();
        create().in(CRACKED_SNAIL_SHELL_CYBER.get(), 1).out(SNAIL_SHELL_DUST_CYBER.get()).build();

        for (CropTypes crop : crops())
            if (crop.hasParticle() && !USED.contains(crop) && crop.getType().hasDefaultProduct())
                create().in(crop, 8).build();
    }

    @Override
    protected JsonObject write(CompressorRecipe recipe)
    {
        JsonObject json = super.write(recipe);
        addItem(json, "input", recipe.getInput());
        addItem(json, "result", recipe.getOutput());
        return json;
    }

    @Override
    public String getName()
    {
        return "compressing";
    }

    static class Builder extends RecipeBuilder
    {
        static List<CropTypes> USED = new ArrayList<>();
        private CropTypes crop;
        private ItemStack input;
        private final List<Pair<String, Ingredient>> outputs = new ArrayList<>();

        public static Builder create()
        {
            return new Builder();
        }

        public static Builder create(String name)
        {
            return new Builder(name);
        }

        private Builder()
        {
            super("");
        }

        private Builder(String name)
        {
            super(name);
        }

        public Builder in(CropTypes crop)
        {
            return in(crop, 1);
        }

        public Builder in(CropTypes crop, int count)
        {
            this.crop = crop;
            return in(crop.getParticle(), count);
        }

        public Builder in(Item input, int count)
        {
            return in(new ItemStack(input, count));
        }

        private Builder in(ItemStack input)
        {
            this.input = input;
            return this;
        }

        @SafeVarargs
        public final Builder out(TagKey<Item>... outputs)
        {
            for (TagKey<Item> output : outputs)
                this.outputs.add(Pair.of(output.location().getPath().replace("/", "_"), Ingredient.of(output)));
            return this;
        }

        public Builder out(ItemLike... outputs)
        {
            for (ItemLike output : outputs)
                out(output, 1);
            return this;
        }

        public Builder out(ItemLike output, int count)
        {
            return out(new ItemStack(output, count));
        }

        private Builder out(ItemStack output)
        {
            this.outputs.add(Pair.of(ForgeRegistries.ITEMS.getKey(output.getItem()).getPath(), Ingredient.of(output)));
            return this;
        }

        public void build()
        {
            String path = path(input);
            String inputName = path.replace("_particles", "");
            // No Particles
            if (inputName.equals("air"))
                return;
            if (crop != null && !USED.contains(crop))
            {
                USED.add(crop);
                ItemType type = crop.getType();
                type.tagKeys(inputName).forEach(this::out);
            }
            for (Pair<String, Ingredient> output : outputs)
                INSTANCE.put(new CompressorRecipe(new ResourceLocation(PlantTechMain.MODID, inputName + "-" + output.getA()), input, output.getB()));
        }

        private String path(ItemStack stack)
        {
            return ForgeRegistries.ITEMS.getKey(stack.getItem()).getPath();
        }
    }
}
