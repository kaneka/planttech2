package net.kaneka.planttech2.energy;

import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.energy.IEnergyStorage;

public class BioEnergyProvider implements ICapabilitySerializable<CompoundTag>
{
	protected final BioEnergyStorage bioEnergyStorage;
	private final LazyOptional<IEnergyStorage> energyCapability;
	
	public BioEnergyProvider(int storage)
	{
		bioEnergyStorage = new BioEnergyStorage(storage);
		energyCapability = LazyOptional.of(() -> bioEnergyStorage);
	}

	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side)
	{
		return cap == ForgeCapabilities.ENERGY ? energyCapability.cast() : LazyOptional.empty();
	}

	@Override
	public CompoundTag serializeNBT()
	{
		CompoundTag compound = new CompoundTag();
		compound.put("energy", bioEnergyStorage.serializeNBT());
		return compound;
	}

	@Override
	public void deserializeNBT(CompoundTag nbt)
	{
		bioEnergyStorage.deserializeNBT(nbt.getCompound("energy"));
	}
}
