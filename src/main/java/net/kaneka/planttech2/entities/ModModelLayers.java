package net.kaneka.planttech2.entities;

import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.entities.passive.snail.SnailModel;
import net.minecraft.client.model.VillagerModel;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.client.event.EntityRenderersEvent;

public class ModModelLayers
{
    public static final ModelLayerLocation TECH_TRADER = new ModelLayerLocation(new ResourceLocation(PlantTechMain.MODID, "tech_trader_model"), "main");
    public static final ModelLayerLocation SNAIL = new ModelLayerLocation(new ResourceLocation(PlantTechMain.MODID, "snail_model"), "main");

    public static void registerAll(EntityRenderersEvent.RegisterLayerDefinitions event)
    {
        event.registerLayerDefinition(TECH_TRADER, () -> LayerDefinition.create(VillagerModel.createBodyModel(), 64, 64));
        event.registerLayerDefinition(SNAIL, SnailModel::createBodyLayer);
    }
}
