package net.kaneka.planttech2.entities.passive.tech_trader;

import net.kaneka.planttech2.configuration.PlantTech2Configuration;
import net.kaneka.planttech2.registries.ModEntityTypes;
import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.tags.BiomeTags;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.entity.SpawnPlacements;
import net.minecraft.world.entity.ai.village.poi.PoiManager;
import net.minecraft.world.entity.ai.village.poi.PoiTypes;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.CustomSpawner;
import net.minecraft.world.level.GameRules;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.NaturalSpawner;
import net.minecraft.world.level.levelgen.Heightmap;
import net.minecraft.world.level.storage.ServerLevelData;
import net.minecraftforge.event.level.LevelEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Optional;

@Mod.EventBusSubscriber
public class TechTraderSpawner implements CustomSpawner {
    private final RandomSource random = RandomSource.create();
    private final ServerLevelData serverLevelData;
    private int tickDelay;
    private int spawnDelay;
    private final float spawnChance;
    @Deprecated
    public static final int TEST_CD = 100;
    public static final int ACTUAL_CD = 36000;
    public static final int CD = ACTUAL_CD;

    @SubscribeEvent
    public static void registerSpawn(LevelEvent.Load event)
    {
        LevelAccessor accessor = event.getLevel();
        if (accessor instanceof ServerLevel world)
        {
//            world.customSpawners.add(new TechTraderSpawner((ServerLevelData) world.getLevelData()));
        }
    }

    public TechTraderSpawner(ServerLevelData p_35914_) {
        this.serverLevelData = p_35914_;
        this.tickDelay = 12000;
        this.spawnDelay = CD;
        this.spawnChance = 0.075F;
    }

    @Override
    public int tick(ServerLevel p_35922_, boolean p_35923_, boolean p_35924_) {
        if (--this.tickDelay > 0) {
            return 0;
        } else {
            this.tickDelay = CD;
            this.spawnDelay -= CD;
            if (this.spawnDelay > 0) {
                return 0;
            } else {
                this.spawnDelay = CD;
                if (!p_35922_.getGameRules().getBoolean(GameRules.RULE_DOMOBSPAWNING)) {
                    return 0;
                } else {
                    if (this.random.nextFloat() <= this.spawnChance * PlantTech2Configuration.TECHTRADER_SPAWN_CHANCE.get()) {
                        return 0;
                    } else if (this.spawn(p_35922_)) {
                        return 1;
                    } else {
                        return 0;
                    }
                }
            }
        }
    }

    private boolean spawn(ServerLevel p_35916_) {
        Player player = p_35916_.getRandomPlayer();
        if (player == null) {
            return true;
        } else {
            BlockPos blockpos = player.blockPosition();
            int i = 48;
            PoiManager poimanager = p_35916_.getPoiManager();
            Optional<BlockPos> optional = poimanager.find((p_219713_) -> {
                return p_219713_.is(PoiTypes.MEETING);
            }, (p_219711_) -> {
                return true;
            }, blockpos, 48, PoiManager.Occupancy.ANY);
            BlockPos blockpos1 = optional.orElse(blockpos);
            BlockPos blockpos2 = this.findSpawnPositionNear(p_35916_, blockpos1, 48);
            if (blockpos2 != null && this.hasEnoughSpace(p_35916_, blockpos2)) {
                if (p_35916_.getBiome(blockpos2).is(BiomeTags.WITHOUT_WANDERING_TRADER_SPAWNS)) {
                    return false;
                }

                TechTrader techTrader = ModEntityTypes.TECH_TRADER.get().spawn(p_35916_, blockpos2, MobSpawnType.EVENT);
                if (techTrader != null) {

                    this.serverLevelData.setWanderingTraderId(techTrader.getUUID());
                    techTrader.setDespawnDelay(48000);
                    techTrader.setWanderTarget(blockpos1);
                    techTrader.restrictTo(blockpos1, 16);
                    return true;
                }
            }

            return false;
        }
    }

    @Nullable
    private BlockPos findSpawnPositionNear(LevelReader p_35929_, BlockPos p_35930_, int p_35931_) {
        BlockPos blockpos = null;

        for(int i = 0; i < 10; ++i) {
            int j = p_35930_.getX() + this.random.nextInt(p_35931_ * 2) - p_35931_;
            int k = p_35930_.getZ() + this.random.nextInt(p_35931_ * 2) - p_35931_;
            int l = p_35929_.getHeight(Heightmap.Types.WORLD_SURFACE, j, k);
            BlockPos blockpos1 = new BlockPos(j, l, k);
            if (NaturalSpawner.isSpawnPositionOk(SpawnPlacements.Type.ON_GROUND, p_35929_, blockpos1, ModEntityTypes.TECH_TRADER.get())) {
                blockpos = blockpos1;
                break;
            }
        }

        return blockpos;
    }

    private boolean hasEnoughSpace(BlockGetter p_35926_, BlockPos p_35927_) {
        for(BlockPos blockpos : BlockPos.betweenClosed(p_35927_, p_35927_.offset(1, 2, 1))) {
            if (!p_35926_.getBlockState(blockpos).getCollisionShape(p_35926_, blockpos).isEmpty()) {
                return false;
            }
        }

        return true;
    }
}