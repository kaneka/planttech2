package net.kaneka.planttech2.entities.passive.snail;// Made with Blockbench 4.7.4
// Exported for Minecraft version 1.17 or later with Mojang mappings
// Paste this class into your mod and generate all required imports


import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.model.EntityModel;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;

import java.util.function.Function;

public class SnailModel extends EntityModel<SnailEntity>
{
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation("modid", "custom_model"), "main");
	private final ModelPart bone;
	private final ModelPart eye;
	private final ModelPart eye2;

	public SnailModel(ModelPart root)
	{
		this.bone = root.getChild("bone");
		this.eye = root.getChild("eye");
		this.eye2 = root.getChild("eye2");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition bone = partdefinition.addOrReplaceChild("bone", CubeListBuilder.create().texOffs(1, 1).addBox(-0.5F, 2.6F, -1.25F, 4.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
				.texOffs(0, 7).addBox(0.0F, -1.4F, 0.0F, 3.0F, 4.0F, 4.0F, new CubeDeformation(0.0F))
				.texOffs(10, 7).addBox(0.0F, 1.6F, -2.0F, 3.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(-1.5F, 20.4F, -1.0F));

		PartDefinition eye = partdefinition.addOrReplaceChild("eye", CubeListBuilder.create(), PartPose.offset(-1.5F, 20.4F, -1.0F));

		PartDefinition cube_r1 = eye.addOrReplaceChild("cube_r1", CubeListBuilder.create().texOffs(0, 7).addBox(-0.4194F, -2.5315F, -0.3485F, 1.0F, 3.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.75F, 1.5F, -1.75F, 0.2618F, 0.0F, -0.2182F));

		PartDefinition eye2 = partdefinition.addOrReplaceChild("eye2", CubeListBuilder.create(), PartPose.offset(0.0F, 24.0F, 0.0F));

		PartDefinition cube_r2 = eye2.addOrReplaceChild("cube_r2", CubeListBuilder.create().texOffs(0, 0).addBox(-0.5806F, -2.5315F, -0.3485F, 1.0F, 3.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.75F, -2.1F, -2.75F, 0.2618F, 0.0F, 0.2182F));

		return LayerDefinition.create(meshdefinition, 32, 32);
	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		poseStack.pushPose();
		if (young)
		{
			float s = 0.5F;
			poseStack.scale(s, s, s);
			poseStack.translate(0, 1.5F, 0);
		}
		bone.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		eye.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		eye2.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		poseStack.popPose();
	}

	@Override
	public void setupAnim(SnailEntity entity, float p_102619_, float partialTicks, float p_102621_, float p_102622_, float p_102623_)
	{
		eye.xRot = (float) animEye(Math::sin, entity.tickCount, 5.0D, 0.15F, partialTicks);
		eye2.xRot = (float) animEye(Math::cos, entity.tickCount, 6.5D, 0.125F, partialTicks);
	}

	private double animEye(Function<Double, Double> func, int tick, double speed, double scale, float partialTicks)
	{
		return Mth.rotLerp(partialTicks, func.apply(Math.toRadians((tick - 1) * speed)).floatValue(), func.apply(Math.toRadians((tick) * speed)).floatValue()) * scale;
	}
}