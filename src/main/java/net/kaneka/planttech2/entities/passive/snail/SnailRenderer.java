package net.kaneka.planttech2.entities.passive.snail;

import net.kaneka.planttech2.entities.ModModelLayers;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.resources.ResourceLocation;

public class SnailRenderer extends MobRenderer<SnailEntity, SnailModel>
{
    public SnailRenderer(EntityRendererProvider.Context context)
    {
        super(context, new SnailModel(context.bakeLayer(ModModelLayers.SNAIL)), 0.15F);
    }

    @Override
    public ResourceLocation getTextureLocation(SnailEntity entity)
    {
        return entity.getVariant().texture;
    }
}
