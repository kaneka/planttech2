package net.kaneka.planttech2.entities.passive.snail;

import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.blocks.CropBaseBlock;
import net.kaneka.planttech2.registries.ModEntityTypes;
import net.kaneka.planttech2.registries.ModItems;
import net.kaneka.planttech2.utilities.PTCommonUtil;
import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.BlockParticleOption;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundSource;
import net.minecraft.tags.ItemTags;
import net.minecraft.util.RandomSource;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.entity.AgeableMob;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.entity.SpawnGroupData;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.goal.FloatGoal;
import net.minecraft.world.entity.ai.goal.MoveToBlockGoal;
import net.minecraft.world.entity.ai.goal.PanicGoal;
import net.minecraft.world.entity.ai.goal.TemptGoal;
import net.minecraft.world.entity.ai.goal.WaterAvoidingRandomStrollGoal;
import net.minecraft.world.entity.animal.Animal;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.CropBlock;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.registries.RegistryObject;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.function.Predicate;

public class SnailEntity extends Animal
{
    private static final EntityDataAccessor<Integer> VARIANT = SynchedEntityData.defineId(SnailEntity.class, EntityDataSerializers.INT);
    private static final int EAT_CD = 1200;
    protected int ticksLastAte;

    public SnailEntity(EntityType<SnailEntity> type, Level world)
    {
        super(ModEntityTypes.SNAIL.get(), world);
    }

    @Override
    public void tick()
    {
        super.tick();
        if (ticksLastAte > 0)
            ticksLastAte--;
    }

    @Override
    protected void defineSynchedData()
    {
        super.defineSynchedData();
        entityData.define(VARIANT, 0);
    }

    private final RandomSource rand = RandomSource.create();
    public void setRandomVariant()
    {
        setVariant(PTCommonUtil.randomByWeight(Arrays.stream(SnailType.values()).toList(), SnailType::getChance, rand).ordinal());
    }

    @Override
    public SpawnGroupData finalizeSpawn(ServerLevelAccessor p_146746_, DifficultyInstance p_146747_, MobSpawnType p_146748_, @Nullable SpawnGroupData p_146749_, @Nullable CompoundTag p_146750_)
    {
        SpawnGroupData d = super.finalizeSpawn(p_146746_, p_146747_, p_146748_, p_146749_, p_146750_);
        setRandomVariant();
        return d;
    }

    @Override
    protected void registerGoals()
    {
        this.goalSelector.addGoal(0, new FloatGoal(this));
        this.goalSelector.addGoal(1, new PanicGoal(this, 1.35D));
        this.goalSelector.addGoal(2, new TemptGoal(this, 1.25D, Ingredient.of(ItemTags.LEAVES), false));
        this.goalSelector.addGoal(3, new EatCropsGoal(this, 0.85F, 20, 3));
        this.goalSelector.addGoal(5, new WaterAvoidingRandomStrollGoal(this, 1.0D, 0.0F));
    }

    @Override
    public boolean canMate(Animal partner)
    {
        return false;
    }

    public SnailType getVariant()
    {
        return SnailType.values()[entityData.get(VARIANT)];
    }

    public void setVariant(SnailType variant)
    {
        setVariant(variant.ordinal());
    }

    public void setVariant(int variant)
    {
        entityData.set(VARIANT, variant);
    }

    @Override
    public void addAdditionalSaveData(CompoundTag compound)
    {
        super.addAdditionalSaveData(compound);
        compound.putInt("snailVariant", getVariant().ordinal());
        compound.putInt("lastAte", ticksLastAte);
    }

    @Override
    public void readAdditionalSaveData(CompoundTag compound)
    {
        super.readAdditionalSaveData(compound);
        setVariant(compound.getInt("snailVariant"));
        ticksLastAte = compound.getInt("lastAte");
    }

    @Override
    public boolean removeWhenFarAway(double p_27598_)
    {
        return !isPersistenceRequired();
    }

    @Override
    public float getScale()
    {
        return isBaby() ? 0.575F : 1.0F;
    }

    @Override
    public boolean isFood(ItemStack stack)
    {
        return stack.is(ItemTags.LEAVES);
    }

    public static AttributeSupplier.Builder createSnailAttributes()
    {
        return Mob.createMobAttributes().add(Attributes.MAX_HEALTH, 6.0D).add(Attributes.MOVEMENT_SPEED, 0.115F);
    }

    @Nullable
    @Override
    public SnailEntity getBreedOffspring(ServerLevel world, AgeableMob mob)
    {
        SnailEntity entity = ModEntityTypes.SNAIL.get().create(world);
        if (entity != null)
            entity.setVariant(getVariant());
        return entity;
    }

    @Override
    protected ResourceLocation getDefaultLootTable()
    {
        return getVariant().loottable;
    }

    public enum SnailType implements Predicate<SnailType>
    {
        BLACK(0.1F, ModItems.CRACKED_SNAIL_SHELL_BLACK),
        CYBER(0.01F, ModItems.CRACKED_SNAIL_SHELL_CYBER),
        GARDEN(0.8F, ModItems.CRACKED_SNAIL_SHELL_GARDEN),
        MILK(0.07F, ModItems.CRACKED_SNAIL_SHELL_MILK),
        PINK(0.02F, ModItems.CRACKED_SNAIL_SHELL_PINK),
        ;
//        public static final Map<SnailType, Set<SnailType>> PREDATORS = ImmutableMap.of(
//                BLACK, ImmutableSet.of(GARDEN, MILK, PINK),
//                GARDEN, ImmutableSet.of(),
//                MILK, ImmutableSet.of(GARDEN),
//                PINK, ImmutableSet.of(GARDEN),
//                TECH, ImmutableSet.of(GARDEN, MILK, PINK, BLACK)
//        );
        public final float chance;
        public final RegistryObject<Item> crackedShell;
        public final ResourceLocation texture;
        public final ResourceLocation loottable;

        SnailType(float chance, RegistryObject<Item> crackedShell)
        {
            this.chance = chance;
            this.crackedShell = crackedShell;
            this.texture = new ResourceLocation(PlantTechMain.MODID, "textures/entities/snail/snail_" + name().toLowerCase() + ".png");
            this.loottable = new ResourceLocation(PlantTechMain.MODID, "entities/snail_" + name().toLowerCase());
        }

        public float getChance()
        {
            return chance;
        }

        @Override
        public boolean test(SnailType snailType)
        {
//            return PREDATORS.get(this).contains(snailType);
            return false;
        }
    }

    public static class EatCropsGoal extends MoveToBlockGoal
    {
        private static final int WAIT_TICKS = 60;
        protected int ticksWaited;
        private final SnailEntity snail;

        public EatCropsGoal(SnailEntity entity, double speed, int hRange, int vRange)
        {
            super(entity, speed, hRange, vRange);
            this.snail = entity;
        }

        @Override
        protected BlockPos getMoveToTarget()
        {
            return blockPos;
        }

        @Override
        public double acceptedDistance()
        {
            return 1.3F;
        }

        @Override
        public boolean canUse()
        {
            return super.canUse() && snail.ticksLastAte <= 0;
        }

        @Override
        public boolean canContinueToUse()
        {
            return super.canContinueToUse() && snail.ticksLastAte <= 0;
        }

        @Override
        public void stop()
        {
            super.stop();
            snail.ticksLastAte = EAT_CD + snail.level().random.nextInt(1200);
        }

        protected boolean isValidTarget(LevelReader world, BlockPos pos)
        {
            BlockState state = world.getBlockState(pos);
            return (state.getBlock() instanceof CropBlock crop && crop.getAge(state) > 0) || (state.getBlock() instanceof CropBaseBlock && state.getValue(CropBaseBlock.GROWSTATE) > 0 && !state.getValue(CropBaseBlock.COPPER));
        }

        @Override
        public void tick()
        {
            super.tick();
            if (this.isReachedTarget())
            {
                if (mob.position().distanceTo(Vec3.atBottomCenterOf(getMoveToTarget())) > 0.3F && mob.level().random.nextBoolean())
                    mob.setDeltaMovement(mob.getDeltaMovement().add(mob.getLookAngle().multiply(0.005F, 0F, 0.005F)).add(0, 0.082F, 0));
                if (this.ticksWaited >= WAIT_TICKS)
                {
                    if (net.minecraftforge.event.ForgeEventFactory.getMobGriefingEvent(mob.level(), mob))
                    {
                        Level world = mob.level();
                        BlockState state = mob.level().getBlockState(this.blockPos);
                        boolean changed = false;
                        if (state.getBlock() instanceof CropBlock crop)
                        {
                            int age = crop.getAge(state);
                            if (age > 0)
                            {
                                world.setBlockAndUpdate(blockPos, crop.getStateForAge(age - 1));
                                changed = true;
                            }
                        }
                        else if (state.getBlock() instanceof CropBaseBlock)
                        {
                            if (state.getValue(CropBaseBlock.GROWSTATE) > 0)
                            {
                                world.setBlockAndUpdate(blockPos, state.setValue(CropBaseBlock.GROWSTATE, state.getValue(CropBaseBlock.GROWSTATE) - 1));
                                changed = true;
                            }
                        }
                        if (changed && world instanceof ServerLevel server)
                        {
                            server.sendParticles(new BlockParticleOption(ParticleTypes.BLOCK, Blocks.BIRCH_LEAVES.defaultBlockState()), mob.getX() - 0.2F + world.random.nextFloat() * 0.4F, mob.getY() + 0.15F, mob.getZ() + world.random.nextFloat() * 0.4F, 16, 0.1F, 0.1F, 0.1F, 0.5F);
                            server.playSound(null, snail, SoundType.GRASS.getBreakSound(), SoundSource.NEUTRAL, 1.0F, 1.0F);
                        }
                    }
                    stop();
                }
                else
                    this.ticksWaited++;
            }
        }

        @Override
        public void start()
        {
            this.ticksWaited = 0;
            super.start();
        }
    }
}
