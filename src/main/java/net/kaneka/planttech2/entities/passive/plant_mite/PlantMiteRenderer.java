package net.kaneka.planttech2.entities.passive.plant_mite;

import com.mojang.blaze3d.vertex.PoseStack;
import net.kaneka.planttech2.PlantTechMain;
import net.minecraft.client.model.EndermiteModel;
import net.minecraft.client.model.geom.ModelLayers;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.resources.ResourceLocation;
import org.jetbrains.annotations.Nullable;

public class PlantMiteRenderer extends MobRenderer<PlantMiteEntity, EndermiteModel<PlantMiteEntity>>
{
    private static final ResourceLocation TEXTURE = new ResourceLocation(PlantTechMain.MODID, "textures/entities/plant_mite/plant_mite.png");
    public PlantMiteRenderer(EntityRendererProvider.Context context)
    {
        super(context, new EndermiteModel<>(context.bakeLayer(ModelLayers.ENDERMITE)), 0.15F);
    }

    @Nullable
    @Override
    protected RenderType getRenderType(PlantMiteEntity p_115322_, boolean p_115323_, boolean p_115324_, boolean p_115325_)
    {
        return RenderType.entityTranslucentCull(TEXTURE);
    }

    @Override
    public void render(PlantMiteEntity p_115455_, float p_115456_, float p_115457_, PoseStack stack, MultiBufferSource p_115459_, int p_115460_)
    {
        stack.pushPose();
        float f = 0.5F;
        stack.scale(f, f, f);
        super.render(p_115455_, p_115456_, p_115457_, stack, p_115459_, p_115460_);
        stack.popPose();
    }

    @Override
    protected float getFlipDegrees(PlantMiteEntity p_114352_) {
        return 180.0F;
    }

    @Override
    public ResourceLocation getTextureLocation(PlantMiteEntity p_114482_)
    {
        return TEXTURE;
    }
}
