package net.kaneka.planttech2.entities.passive.tech_trader;

import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.entities.ModModelLayers;
import net.minecraft.client.model.EntityModel;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.resources.ResourceLocation;

public class TechTraderRenderer extends MobRenderer<TechTrader, EntityModel<TechTrader>>
{
    private static final ResourceLocation TEXTURE = new ResourceLocation(PlantTechMain.MODID, "textures/entities/tech_trader/tech_trader.png");
    public TechTraderRenderer(EntityRendererProvider.Context context)
    {
        super(context, new TechTraderModel2(context.bakeLayer(ModModelLayers.TECH_TRADER)), 0.5F);
    }

    @Override
    public ResourceLocation getTextureLocation(TechTrader entity)
    {
        return TEXTURE;
    }
}
