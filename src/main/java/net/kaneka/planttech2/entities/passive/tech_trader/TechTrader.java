package net.kaneka.planttech2.entities.passive.tech_trader;

import net.kaneka.planttech2.crops.CropTypes;
import net.kaneka.planttech2.crops.ParentPair;
import net.kaneka.planttech2.registries.ModItems;
import net.minecraft.Util;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.NbtUtils;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.util.RandomSource;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.AgeableMob;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.ExperienceOrb;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.ai.goal.FloatGoal;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.entity.ai.goal.InteractGoal;
import net.minecraft.world.entity.ai.goal.LookAtPlayerGoal;
import net.minecraft.world.entity.ai.goal.LookAtTradingPlayerGoal;
import net.minecraft.world.entity.ai.goal.MoveTowardsRestrictionGoal;
import net.minecraft.world.entity.ai.goal.PanicGoal;
import net.minecraft.world.entity.ai.goal.TradeWithPlayerGoal;
import net.minecraft.world.entity.ai.goal.WaterAvoidingRandomStrollGoal;
import net.minecraft.world.entity.npc.AbstractVillager;
import net.minecraft.world.entity.npc.VillagerTrades;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.trading.MerchantOffer;
import net.minecraft.world.item.trading.MerchantOffers;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.common.util.Lazy;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

public class TechTrader extends AbstractVillager
{
    private static final int NUMBER_OF_TRADE_OFFERS = 4;
    private static final int NUMBER_OF_RARE_TRADE_OFFERS = 1;
    @Nullable
    private BlockPos wanderTarget;
    private int despawnDelay;

    private static final Lazy<VillagerTrades.ItemListing[]> TRADES = Lazy.of(() -> Util.make(new ArrayList<VillagerTrades.ItemListing>(), (list) -> {
        list.add(normalTrade(ModItems.PLANTIUM_INGOT.get(), 3, ModItems.THERMOMETER.get(), 1));

        list.add(normalTrade(ModItems.PLANTIUM_INGOT.get(), (rand) -> 33 + rand.nextInt(25), ModItems.CYBERBOW, (rand) -> 1, (rand) -> 10 + rand.nextInt(10)));
        list.add(normalTrade(ModItems.PLANTIUM_INGOT.get(), (rand) -> 31 + rand.nextInt(25), ModItems.CYBERDAGGER, (rand) -> 1, (rand) -> 10 + rand.nextInt(10)));
        list.add(normalTrade(ModItems.PLANTIUM_INGOT.get(), (rand) -> 32 + rand.nextInt(25), ModItems.CYBERKATANA, (rand) -> 1, (rand) -> 10 + rand.nextInt(10)));
        list.add(normalTrade(ModItems.PLANTIUM_INGOT.get(), (rand) -> 32 + rand.nextInt(25), ModItems.CYBERRAPIER, (rand) -> 1, (rand) -> 10 + rand.nextInt(10)));
        list.add(normalTrade(ModItems.PLANTIUM_INGOT.get(), (rand) -> 34 + rand.nextInt(25), ModItems.CYBERARMOR_BOOTS, (rand) -> 1, (rand) -> 10 + rand.nextInt(10)));
        list.add(normalTrade(ModItems.PLANTIUM_INGOT.get(), (rand) -> 37 + rand.nextInt(25), ModItems.CYBERARMOR_LEGGINGS, (rand) -> 1, (rand) -> 10 + rand.nextInt(10)));
        list.add(normalTrade(ModItems.PLANTIUM_INGOT.get(), (rand) -> 38 + rand.nextInt(25), ModItems.CYBERARMOR_CHEST, (rand) -> 1, (rand) -> 10 + rand.nextInt(10)));
        list.add(normalTrade(ModItems.PLANTIUM_INGOT.get(), (rand) -> 35 + rand.nextInt(25), ModItems.CYBERARMOR_HELMET, (rand) -> 1, (rand) -> 10 + rand.nextInt(10)));

        list.add(normalTrade(Items.BONE_MEAL, 16, ModItems.FERTILIZER_TIER_1.get(), 1));
        list.add(normalTrade(Items.BONE_MEAL, 32, ModItems.FERTILIZER_TIER_2.get(), 1));
        list.add(normalTrade(Items.BONE_MEAL, 48, ModItems.FERTILIZER_TIER_3.get(), 1));
        list.add(normalTrade(Items.BONE_MEAL, 64, ModItems.FERTILIZER_TIER_4.get(), 1));

        list.add(particle(Items.COAL, 2, CropTypes.COAL_CROP, 1));
        list.add(particle(Items.IRON_INGOT, 2, CropTypes.IRON_CROP, 1));
        list.add(particle(Items.COPPER_INGOT, 2, CropTypes.COPPER_CROP, 1));
        list.add(particle(Items.GOLD_INGOT, 2, CropTypes.GOLD_CROP, 1));
        list.add(particle(Items.REDSTONE, 2, CropTypes.REDSTONE_CROP, 1));
        list.add(particle(Items.LAPIS_LAZULI, 2, CropTypes.LAPIS_CROP, 1));
        list.add(particle(Items.DIAMOND, 2, CropTypes.DIAMOND_CROP, 1));
        list.add(particle(Items.NETHERITE_INGOT, 2, CropTypes.NETHERITE_CROP, 1));

    }).toArray(new VillagerTrades.ItemListing[0]));

    private static final Lazy<VillagerTrades.ItemListing[]> RARE_TRADES = Lazy.of(() -> Util.make(new ArrayList<VillagerTrades.ItemListing>(), (list) -> {
        for (int i = 0; i < CropTypes.crops().size(); i++)
        {
            CropTypes crop = CropTypes.crops().get(i);
            List<ParentPair> parents = crop.getConfig().getParents();
            for (ParentPair parent : parents)
            {
                list.add(new VillagerTrades.ItemListing()
                {
                    @org.jetbrains.annotations.Nullable
                    @Override
                    public MerchantOffer getOffer(Entity entity, RandomSource random)
                    {
                        return crop.shouldBeInGame() && crop.hasParticle() ? new MerchantOffer(
                                new ItemStack(parent.getFirstParent().getConfig().getPrimarySeed().getItem().get(), 16 + random.nextInt(48)),
                                new ItemStack(parent.getSecondParent().getConfig().getPrimarySeed().getItem().get(), 16 + random.nextInt(48)),
                                new ItemStack(crop.getConfig().getPrimarySeed().getItem().get()),
                                10, 2, 0.0F): null;
                    }
                });
            }
        };
    }).toArray(new VillagerTrades.ItemListing[0]));

    private static VillagerTrades.ItemListing particle(Item input, int count, CropTypes output, int count2)
    {
        return normalTrade(input, (rand) -> count, () -> !output.shouldBeInGame() || !output.hasParticle() ? null : output.getParticle(), (rand) -> count2, (rand) -> 0);
    }

    private static VillagerTrades.ItemListing normalTrade(Item input, int count, @Nullable Item output, int count2)
    {
        return normalTrade(input, (rand) -> count, () -> output, (rand) -> count2, (rand) -> 0);
    }

    private static VillagerTrades.ItemListing normalTrade(Item input, Function<RandomSource, Integer> count, Supplier<Item> output, Function<RandomSource, Integer> count2, Function<RandomSource, Integer> inch)
    {
        return new VillagerTrades.ItemListing()
        {
            @org.jetbrains.annotations.Nullable
            @Override
            public MerchantOffer getOffer(Entity p_219693_, RandomSource rand)
            {
                return output.get() == null ? null : new MerchantOffer(new ItemStack(input, count.apply(rand)), new ItemStack(output.get(), count2.apply(rand)), 10, 1, 0.0F);
            }
        };
    }
    
    public TechTrader(EntityType<? extends TechTrader> type, Level world)
    {
        super(type, world);
    }

    @Override
    protected void registerGoals()
    {
        this.goalSelector.addGoal(0, new FloatGoal(this));
        this.goalSelector.addGoal(1, new TradeWithPlayerGoal(this));
        this.goalSelector.addGoal(1, new PanicGoal(this, 0.5D));
        this.goalSelector.addGoal(1, new LookAtTradingPlayerGoal(this));
        this.goalSelector.addGoal(2, new WanderToPositionGoal(this, 2.0D, 0.35D));
        this.goalSelector.addGoal(4, new MoveTowardsRestrictionGoal(this, 0.35D));
        this.goalSelector.addGoal(8, new WaterAvoidingRandomStrollGoal(this, 0.35D));
        this.goalSelector.addGoal(9, new InteractGoal(this, Player.class, 3.0F, 1.0F));
        this.goalSelector.addGoal(10, new LookAtPlayerGoal(this, Mob.class, 8.0F));
    }

    @Nullable
    @Override
    public AgeableMob getBreedOffspring(ServerLevel world, AgeableMob parent)
    {
        return null;
    }

    @Override
    public boolean showProgressBar()
    {
        return false;
    }

    @Override
    public InteractionResult mobInteract(Player player, InteractionHand hand)
    {
        if (this.isAlive() && !this.isTrading() && !this.isBaby())
        {
            if (!this.getOffers().isEmpty())
            {
                if (!this.level().isClientSide)
                {
                    this.setTradingPlayer(player);
                    this.openTradingScreen(player, this.getDisplayName(), 1);
                }
            }
            return InteractionResult.sidedSuccess(this.level().isClientSide);
        }
        return super.mobInteract(player, hand);
    }

    @Override
    protected void updateTrades()
    {
        MerchantOffers offers = this.getOffers();
        addOffersFromItemListings(offers, TRADES.get(), NUMBER_OF_TRADE_OFFERS);
        addOffersFromItemListings(offers, RARE_TRADES.get(), NUMBER_OF_RARE_TRADE_OFFERS);
    }

    @Override
    public void addAdditionalSaveData(CompoundTag compound)
    {
        super.addAdditionalSaveData(compound);
        compound.putInt("DespawnDelay", this.despawnDelay);
        if (this.wanderTarget != null)
            compound.put("WanderTarget", NbtUtils.writeBlockPos(this.wanderTarget));
    }

    @Override
    public void readAdditionalSaveData(CompoundTag p_35852_)
    {
        super.readAdditionalSaveData(p_35852_);
        if (p_35852_.contains("DespawnDelay", 99))
            this.despawnDelay = p_35852_.getInt("DespawnDelay");
        if (p_35852_.contains("WanderTarget"))
            this.wanderTarget = NbtUtils.readBlockPos(p_35852_.getCompound("WanderTarget"));
        this.setAge(Math.max(0, this.getAge()));
    }

    @Override
    public boolean removeWhenFarAway(double p_35886_)
    {
        return false;
    }

    @Override
    protected void rewardTradeXp(MerchantOffer p_35859_)
    {
        if (p_35859_.shouldRewardExp())
        {
            int i = 3 + this.random.nextInt(4);
            this.level().addFreshEntity(new ExperienceOrb(this.level(), this.getX(), this.getY() + 0.5D, this.getZ(), i));
        }
    }

    @Override
    protected SoundEvent getAmbientSound()
    {
        return this.isTrading() ? SoundEvents.WANDERING_TRADER_TRADE : SoundEvents.WANDERING_TRADER_AMBIENT;
    }

    @Override
    protected SoundEvent getHurtSound(DamageSource p_35870_)
    {
        return SoundEvents.WANDERING_TRADER_HURT;
    }

    @Override
    protected SoundEvent getDeathSound()
    {
        return SoundEvents.WANDERING_TRADER_DEATH;
    }

    @Override
    protected SoundEvent getDrinkingSound(ItemStack stack)
    {
        return stack.is(Items.MILK_BUCKET) ? SoundEvents.WANDERING_TRADER_DRINK_MILK : SoundEvents.WANDERING_TRADER_DRINK_POTION;
    }

    @Override
    protected SoundEvent getTradeUpdatedSound(boolean happy)
    {
        return happy ? SoundEvents.WANDERING_TRADER_YES : SoundEvents.WANDERING_TRADER_NO;
    }

    @Override
    public SoundEvent getNotifyTradeSound()
    {
        return SoundEvents.WANDERING_TRADER_YES;
    }

    public void setDespawnDelay(int p_35892_)
    {
        this.despawnDelay = p_35892_;
    }

    public int getDespawnDelay()
    {
        return this.despawnDelay;
    }

    @Override
    public void aiStep()
    {
        super.aiStep();
        if (!this.level().isClientSide)
            this.maybeDespawn();
    }

    private void maybeDespawn()
    {
        if (this.despawnDelay > 0 && !this.isTrading() && --this.despawnDelay == 0)
            this.discard();
    }

    public void setWanderTarget(@Nullable BlockPos p_35884_)
    {
        this.wanderTarget = p_35884_;
    }

    @Nullable
    BlockPos getWanderTarget()
    {
        return this.wanderTarget;
    }

    static class WanderToPositionGoal extends Goal
    {
        final TechTrader trader;
        final double stopDistance;
        final double speedModifier;

        WanderToPositionGoal(TechTrader tarder, double maxDistance, double speed)
        {
            trader = tarder;
            this.stopDistance = maxDistance;
            this.speedModifier = speed;
            this.setFlags(EnumSet.of(Goal.Flag.MOVE));
        }

        @Override
        public void stop()
        {
            trader.setWanderTarget((BlockPos)null);
            trader.navigation.stop();
        }

        @Override
        public boolean canUse()
        {
            BlockPos blockpos = trader.getWanderTarget();
            return blockpos != null && this.isTooFarAway(blockpos, this.stopDistance);
        }

        @Override
        public void tick()
        {
            BlockPos pos = trader.getWanderTarget();
            if (pos != null && trader.navigation.isDone())
            {
                if (this.isTooFarAway(pos, 10.0D))
                {
                    Vec3 vec3 = (new Vec3((double) pos.getX() - trader.getX(), (double) pos.getY() - trader.getY(), (double) pos.getZ() - trader.getZ())).normalize();
                    Vec3 vec31 = vec3.scale(10.0D).add(trader.getX(), trader.getY(), trader.getZ());
                    trader.navigation.moveTo(vec31.x, vec31.y, vec31.z, this.speedModifier);
                }
                else
                    trader.navigation.moveTo(pos.getX(), pos.getY(), pos.getZ(), this.speedModifier);
            }
        }

        private boolean isTooFarAway(BlockPos pos, double d)
        {
            return !pos.closerToCenterThan(trader.position(), d);
        }
    }
}