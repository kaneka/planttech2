package net.kaneka.planttech2.entities.passive.plant_mite;

import net.kaneka.planttech2.blocks.CropBaseBlock;
import net.kaneka.planttech2.blocks.entity.CropsBlockEntity;
import net.kaneka.planttech2.entities.passive.snail.SnailEntity;
import net.kaneka.planttech2.registries.ModEntityTypes;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.tags.ItemTags;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.AgeableMob;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.goal.FloatGoal;
import net.minecraft.world.entity.ai.goal.MoveToBlockGoal;
import net.minecraft.world.entity.ai.goal.PanicGoal;
import net.minecraft.world.entity.ai.goal.WaterAvoidingRandomStrollGoal;
import net.minecraft.world.entity.animal.Animal;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.phys.Vec3;
import org.jetbrains.annotations.Nullable;

public class PlantMiteEntity extends Animal
{
    private int sugar = 0;
    private static final int EAT_CD = 1000;
    protected int ticksLastAte;

    public PlantMiteEntity(EntityType<PlantMiteEntity> type, Level world)
    {
        super(ModEntityTypes.PLANT_MITE.get(), world);
    }

    @Override
    public void tick()
    {
        super.tick();
        if (ticksLastAte > 0)
            ticksLastAte--;
    }

    @Override
    public InteractionResult mobInteract(Player player, InteractionHand hand)
    {
        if (sugar > 0)
        {
            spawnAtLocation(Items.SUGAR, sugar);
            sugar = 0;
        }
        return super.mobInteract(player, hand);
    }

    @Override
    protected void registerGoals()
    {
        this.goalSelector.addGoal(0, new FloatGoal(this));
        this.goalSelector.addGoal(1, new PanicGoal(this, 1.35D));
        this.goalSelector.addGoal(2, new InfectCropGoal(this, 1.0F, 10, 3));
        this.goalSelector.addGoal(3, new WaterAvoidingRandomStrollGoal(this, 1.0D, 0.0F));
    }

    @Override
    public boolean removeWhenFarAway(double p_27598_)
    {
        return !isPersistenceRequired();
    }

    @Override
    public boolean isFood(ItemStack stack)
    {
        return stack.is(ItemTags.LEAVES);
    }

    public static AttributeSupplier.Builder createPlantMiteAttributes()
    {
        return Mob.createMobAttributes().add(Attributes.MAX_HEALTH, 2.0D).add(Attributes.MOVEMENT_SPEED, 0.1F);
    }

    @Nullable
    @Override
    public SnailEntity getBreedOffspring(ServerLevel world, AgeableMob mob)
    {
        return null;
    }

    @Override
    public void addAdditionalSaveData(CompoundTag compound)
    {
        super.addAdditionalSaveData(compound);
        compound.putInt("lastAte", ticksLastAte);
    }

    @Override
    public void readAdditionalSaveData(CompoundTag compound)
    {
        super.readAdditionalSaveData(compound);
        ticksLastAte = compound.getInt("lastAte");
    }

    public static class InfectCropGoal extends MoveToBlockGoal
    {
        private static final int WAIT_TICKS = 100;
        protected int ticksWaited;
        private final PlantMiteEntity mite;

        public InfectCropGoal(PlantMiteEntity entity, double speed, int hRange, int vRange)
        {
            super(entity, speed, hRange, vRange);
            this.mite = entity;
        }

        @Override
        public boolean canUse()
        {
            return super.canUse() && mite.ticksLastAte <= 0 && mite.level().random.nextFloat() <= 0.75F;
        }

        @Override
        public boolean canContinueToUse()
        {
            return super.canContinueToUse() && mite.ticksLastAte <= 0;
        }

        @Override
        protected boolean isValidTarget(LevelReader world, BlockPos pos)
        {
            return world.getBlockState(pos).getBlock() instanceof CropBaseBlock;
        }

        @Override
        protected BlockPos getMoveToTarget()
        {
            return blockPos;
        }

        @Override
        public void stop()
        {
            super.stop();
            mite.ticksLastAte = EAT_CD + mite.level().random.nextInt(1000);
        }

        @Override
        public void tick()
        {
            super.tick();
            if (this.isReachedTarget())
            {
                if (mob.position().distanceTo(Vec3.atBottomCenterOf(getMoveToTarget())) > 0.3F && mob.level().random.nextBoolean())
                    mob.setDeltaMovement(mob.getDeltaMovement().add(mob.getLookAngle().multiply(0.005F, 0F, 0.005F)).add(0, 0.082F, 0));
                if (this.ticksWaited >= WAIT_TICKS)
                {
                    if (mob.level().getBlockEntity(getMoveToTarget()) instanceof CropsBlockEntity crop)
                    {
                        if (mob.level().random.nextFloat() <= 0.1F)
                            crop.setSick(true);
                        else
                            mite.sugar = Math.min(mite.sugar + 1, 4);
                    }
                    stop();
                }
                else
                    this.ticksWaited++;
            }
        }

        @Override
        public void start()
        {
            this.ticksWaited = 0;
            super.start();
        }
    }
}
