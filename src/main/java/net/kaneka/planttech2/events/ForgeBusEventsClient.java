package net.kaneka.planttech2.events;

import com.mojang.brigadier.CommandDispatcher;
import net.kaneka.planttech2.commands.GuideCommand;
import net.minecraft.commands.CommandSourceStack;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.RegisterClientCommandsEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(value = Dist.CLIENT)
public class ForgeBusEventsClient
{
    @SubscribeEvent
    public static void onClientCommandsRegister(RegisterClientCommandsEvent event)
    {
        CommandDispatcher<CommandSourceStack> dispatcher = event.getDispatcher();
        GuideCommand.register(dispatcher);
    }
}
