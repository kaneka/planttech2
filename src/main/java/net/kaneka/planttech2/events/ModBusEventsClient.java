package net.kaneka.planttech2.events;

import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.blocks.CropBaseBlock;
import net.kaneka.planttech2.blocks.Hedge;
import net.kaneka.planttech2.crops.CropTypes;
import net.kaneka.planttech2.entities.ModModelLayers;
import net.kaneka.planttech2.entities.passive.plant_mite.PlantMiteRenderer;
import net.kaneka.planttech2.entities.passive.snail.SnailRenderer;
import net.kaneka.planttech2.entities.passive.tech_trader.TechTraderRenderer;
import net.kaneka.planttech2.items.CropSeedItem;
import net.kaneka.planttech2.items.ParticleItem;
import net.kaneka.planttech2.registries.ModBlocks;
import net.kaneka.planttech2.registries.ModEntityTypes;
import net.kaneka.planttech2.registries.ModItems;
import net.kaneka.planttech2.registries.ModScreens;
import net.kaneka.planttech2.utilities.PTClientUtil;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.EntityRenderersEvent;
import net.minecraftforge.client.event.RegisterColorHandlersEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.registries.RegistryObject;

@Mod.EventBusSubscriber(modid = PlantTechMain.MODID, bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
public class ModBusEventsClient
{
	@SubscribeEvent
	static void clientSetup(final FMLClientSetupEvent event)
	{
		event.enqueueWork(PTClientUtil::addAllItemModelsOverrides);
		ModScreens.registerGUI();
	}

	@SubscribeEvent
	public static void addLayerDefs(EntityRenderersEvent.RegisterLayerDefinitions event)
	{
		ModModelLayers.registerAll(event);
	}

	@SubscribeEvent
	public static void onRenderersRegistered(EntityRenderersEvent.RegisterRenderers event)
	{
		event.registerEntityRenderer(ModEntityTypes.TECH_TRADER.get(), TechTraderRenderer::new);
		event.registerEntityRenderer(ModEntityTypes.SNAIL.get(), SnailRenderer::new);
		event.registerEntityRenderer(ModEntityTypes.PLANT_MITE.get(), PlantMiteRenderer::new);
	}

	@SubscribeEvent
	public static void registerColorItem(RegisterColorHandlersEvent.Item event)
	{
		for (RegistryObject<Item> registryObject : ModItems.PARTICLES.values())
			event.getItemColors().register(new ParticleItem.ColorHandler(), registryObject.get());
		for (RegistryObject<Item> registryObject : CropTypes.SEEDS.values())
			event.getItemColors().register(new CropSeedItem.ColorHandler(), registryObject.get());
		for (RegistryObject<Block> registryObject: ModBlocks.HEDGE_BLOCKS)
		{
			Hedge block = (Hedge) registryObject.get();
			event.getItemColors().register(new Hedge.ColorHandlerItem(block.getLeaves()), block);
		}
	}

	@SubscribeEvent
	public static void registerColorBlock(RegisterColorHandlersEvent.Block event)
	{
		for (RegistryObject<Block> registryObject : CropTypes.CROP_BLOCKS.values())
		{
			CropBaseBlock block = (CropBaseBlock) registryObject.get();
			event.getBlockColors().register(new CropBaseBlock.ColorHandler(), block);
		}
		for(RegistryObject<Block> registryObject: ModBlocks.HEDGE_BLOCKS)
		{
			Hedge block = (Hedge) registryObject.get();
			event.getBlockColors().register(new Hedge.ColorHandler(block.getLeaves(), block.getSoil()), block);
		}
	}
}
