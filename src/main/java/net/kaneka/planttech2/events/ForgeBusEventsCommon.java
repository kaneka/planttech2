package net.kaneka.planttech2.events;

import net.kaneka.planttech2.crops.CropListReloadListener;
import net.kaneka.planttech2.crops.CropTypes;
import net.minecraft.world.item.Item;
import net.minecraftforge.event.AddReloadListenerEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.MissingMappingsEvent;

@Mod.EventBusSubscriber
public class ForgeBusEventsCommon
{
    @SubscribeEvent
    public static void onReloadListenerAddedEvent(AddReloadListenerEvent event)
    {
        event.addListener(new CropListReloadListener());
    }

    @SubscribeEvent
    public static void onMissingMappings(MissingMappingsEvent event)
    {
        if (event.getKey() != ForgeRegistries.Keys.ITEMS)
            return;
        // Migrates `prismarin_*` to new `prismarine_*`
        for (MissingMappingsEvent.Mapping<Item> mapping : event.getAllMappings(ForgeRegistries.Keys.ITEMS))
        {
            String path = mapping.getKey().getPath();
            switch (path)
            {
                case "prismarin_seeds" -> mapping.remap(CropTypes.PRISMARINE_CROP.getSeed());
                case "prismarin_particles" -> mapping.remap(CropTypes.PRISMARINE_CROP.getParticle());
                case "lilly_of_the_valley_seeds" -> mapping.remap(CropTypes.LILY_OF_THE_VALLEY_CROP.getSeed());
                case "lilly_of_the_valley_particles" -> mapping.remap(CropTypes.LILY_OF_THE_VALLEY_CROP.getParticle());
            }
        }
    }
}
