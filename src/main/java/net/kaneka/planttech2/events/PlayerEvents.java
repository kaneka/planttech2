package net.kaneka.planttech2.events;

import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.packets.CropListSyncMessage;
import net.kaneka.planttech2.packets.PlantTech2PacketHandler;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = PlantTechMain.MODID)
public class PlayerEvents
{
	@SubscribeEvent
	public static void playerConnect(PlayerEvent.PlayerLoggedInEvent event)
	{
		Player player = event.getEntity();
		if (!player.level().isClientSide())
		{
			PlantTechMain.LOGGER.info(event.getEntity().getDisplayName().getString() + " has logged in, syncing crop list");
			PlantTech2PacketHandler.sendTo(new CropListSyncMessage(), (ServerPlayer) player);
		}
	}
}
