package net.kaneka.planttech2.events;

import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.fluids.BiomassContainerBiomassHandler;
import net.kaneka.planttech2.items.BiomassContainerItem;
import net.kaneka.planttech2.registries.ModReferences;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = PlantTechMain.MODID)
public class AttachCapabilityEvents
{
    @SubscribeEvent
    public static void attachItemStackCapability(final AttachCapabilitiesEvent<ItemStack> event)
    {
        if (event.getObject() instanceof ItemStack)
            if (event.getObject().getItem() instanceof BiomassContainerItem)
                if (!event.getCapabilities().containsKey(ModReferences.BIOMASSFLUIDENERGYCAP))
                    event.addCapability(ModReferences.BIOMASSFLUIDENERGYCAP, new BiomassContainerBiomassHandler(event.getObject()));
    }
}
