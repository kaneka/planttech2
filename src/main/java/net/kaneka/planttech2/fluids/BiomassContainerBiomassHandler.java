package net.kaneka.planttech2.fluids;

import net.kaneka.planttech2.items.BiomassContainerItem;
import net.kaneka.planttech2.utilities.PlantTechConstants;
import net.minecraft.core.Direction;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.fluids.capability.IFluidHandlerItem;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class BiomassContainerBiomassHandler implements PTBiomassFluidHandler, IFluidHandlerItem, ICapabilityProvider
{
    private final LazyOptional<IFluidHandlerItem> holder = LazyOptional.of(() -> this);
    protected ItemStack stack;

    public BiomassContainerBiomassHandler(ItemStack stack)
    {
        this.stack = stack;
    }

    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side)
    {
        return cap == ForgeCapabilities.FLUID_HANDLER_ITEM ? holder.cast() : LazyOptional.empty();
    }

    @Nonnull
    @Override
    public ItemStack getContainer()
    {
        return stack;
    }

    @Override
    public int getTankCapacity(int tank)
    {
        return PlantTechConstants.BIOMASS_CONTAINER_CAPACITY;
    }

    @Override
    public void forceSetStorage(int amount)
    {
        getContainer().getOrCreateTag().putInt(PlantTechConstants.BIOMASS_TAG, amount);
    }

    @Override
    public boolean validCOntainer()
    {
        return getContainer().getItem() instanceof BiomassContainerItem && getContainer().getCount() == 1;
    }

    @Override
    public int currentBiomass()
    {
        return getContainer().getOrCreateTag().getInt(PlantTechConstants.BIOMASS_TAG);
    }
}
