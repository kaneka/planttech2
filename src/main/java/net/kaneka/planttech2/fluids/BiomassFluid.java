package net.kaneka.planttech2.fluids;

import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.registries.ModBlocks;
import net.kaneka.planttech2.registries.ModFluids;
import net.kaneka.planttech2.registries.ModItems;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.FluidTags;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.LiquidBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.material.FlowingFluid;
import net.minecraft.world.level.material.Fluid;
import net.minecraft.world.level.material.FluidState;
import net.minecraftforge.client.extensions.common.IClientFluidTypeExtensions;
import net.minecraftforge.fluids.FluidType;

import java.util.function.Consumer;

public abstract class BiomassFluid extends FlowingFluid
{
	public static final ResourceLocation BIOMASS_STILL = new ResourceLocation(PlantTechMain.MODID, "block/fluid/biomass_still");
	public static final ResourceLocation BIOMASS_FLOWING = new ResourceLocation(PlantTechMain.MODID, "block/fluid/biomass_flow");

	@Override
	public Fluid getFlowing()
	{
		return ModFluids.BIOMASS_FLOWING.get();
	}

	@Override
	public Fluid getSource()
	{
		return ModFluids.BIOMASS.get();
	}

	@Override
	protected boolean canConvertToSource(Level p_256009_)
	{
		return false;
	}

	@Override
	protected void beforeDestroyingBlock(LevelAccessor level, BlockPos pos, BlockState state)
	{
		Block.dropResources(state, level , pos, level.getBlockEntity(pos));
	}

	@Override
	protected int getSlopeFindDistance(LevelReader worldIn)
	{
		return 4;
	}

	@Override
	protected int getDropOff(LevelReader worldIn)
	{
		return 1;
	}

	@Override
	public Item getBucket()
	{
		return ModItems.BIOMASS_BUCKET.get();
	}

	@Override
	protected boolean canBeReplacedWith(FluidState fluidState, BlockGetter blockReader, BlockPos pos, Fluid fluid, Direction direction)
	{
		return direction == Direction.DOWN && !fluid.is(FluidTags.WATER);
	}

	@Override
	public int getTickDelay(LevelReader p_205569_1_)
	{
		return 5;
	}

	@Override
	protected float getExplosionResistance()
	{
		return 100.0F;
	}

	@Override
	protected BlockState createLegacyBlock(FluidState state)
	{
		return ModBlocks.BIOMASSFLUIDBLOCK.get().defaultBlockState().setValue(LiquidBlock.LEVEL, getLegacyLevel(state));
	}

	@Override
	public boolean isSame(Fluid fluidIn)
	{
		return fluidIn == ModFluids.BIOMASS.get() || fluidIn == ModFluids.BIOMASS_FLOWING.get();
	}

	public static class Flowing extends BiomassFluid
	{
		public Flowing()
		{
			registerDefaultState(getStateDefinition().any().setValue(LEVEL, 7));
		}

		protected void createFluidStateDefinition(StateDefinition.Builder<Fluid, FluidState> builder)
		{
			super.createFluidStateDefinition(builder);
			builder.add(LEVEL);
		}

		@Override
		public int getAmount(FluidState state)
		{
			return state.getValue(LEVEL);
		}

		@Override
		public boolean isSource(FluidState state)
		{
			return false;
		}

		@Override
		public FluidType getFluidType()
		{
			return new FluidType(FluidType.Properties.create().canConvertToSource(false)) {
				@Override
				public void initializeClient(Consumer<IClientFluidTypeExtensions> consumer)
				{
					consumer.accept(new IClientFluidTypeExtensions() {
						@Override
						public ResourceLocation getStillTexture() {
							return BIOMASS_STILL;
						}
						@Override
						public ResourceLocation getFlowingTexture() {
							return BIOMASS_FLOWING;
						}
					});
				}
			};
		}
	}

	public static class Source extends BiomassFluid
	{
		public int getAmount(FluidState state)
		{
		 return 8;
		}

		public boolean isSource(FluidState state)
         {
             return true;
         }

		@Override
		public FluidType getFluidType()
		{
			return new FluidType(FluidType.Properties.create().canConvertToSource(true)) {
				@Override
				public void initializeClient(Consumer<IClientFluidTypeExtensions> consumer)
				{
					consumer.accept(new IClientFluidTypeExtensions() {
						@Override
						public ResourceLocation getStillTexture() {
							return BIOMASS_STILL;
						}
						@Override
						public ResourceLocation getFlowingTexture() {
							return BIOMASS_FLOWING;
						}
					});
				}
			};
		}
	}
}
