package net.kaneka.planttech2.fluids;

import net.kaneka.planttech2.registries.ModFluids;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.IFluidHandler;

import javax.annotation.Nonnull;

public interface PTBiomassFluidHandler extends IFluidHandler
{
    @Override
    default int getTanks()
    {
        return 1;
    }

    @Nonnull
    @Override
    default FluidStack getFluidInTank(int tank)
    {
        return validCOntainer() ? new FluidStack(ModFluids.BIOMASS.get(), currentBiomass()) : FluidStack.EMPTY;
    }

    @Override
    default boolean isFluidValid(int tank, @Nonnull FluidStack stack)
    {
        return stack.getFluid() == ModFluids.BIOMASS.get();
    }

    @Override
    default int fill(FluidStack resource, FluidAction action)
    {
        int space = space();
        if (!validCOntainer() || (!isFluidValid(1, resource) && resource != FluidStack.EMPTY) || space <= 0)
            return 0;

        int amount = Math.min(resource.getAmount(), space);
        if (action.execute())
            forceChangeStorage(amount);
        return amount;
    }

    @Nonnull
    @Override
    default FluidStack drain(FluidStack resource, FluidAction action)
    {
        if (!isFluidValid(1, resource))
            return FluidStack.EMPTY;
        return drain(resource.getAmount(), action);
    }

    @Nonnull
    @Override
    default FluidStack drain(int maxDrain, FluidAction action)
    {
        int current = currentBiomass();
        if (!validCOntainer() || current <= 0)
            return FluidStack.EMPTY;
        int amount = Math.min(maxDrain, current);
        if (action.execute())
            forceChangeStorage(-amount);
        return new FluidStack(ModFluids.BIOMASS.get(), amount);
    }

    default void forceChangeStorage(int amount)
    {
        forceSetStorage(currentBiomass() + amount);
    }

    void forceSetStorage(int amount);

    boolean validCOntainer();

    int currentBiomass();

    default int space()
    {
        return getTankCapacity(0) - currentBiomass();
    }
}
