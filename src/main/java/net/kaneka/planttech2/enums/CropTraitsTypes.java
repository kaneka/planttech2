package net.kaneka.planttech2.enums;


import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;

import java.util.Arrays;

public enum CropTraitsTypes
{
    GROW_SPEED("growspeed",            0, 10, 0.04F),
    SENSITIVITY         ("sensitivity",          0, 10, 0.04F),
    LIGHT_SENSITIVITY("lightsensitivity",     0, 14, 0.06F),
    WATER_SENSITIVITY("watersensitivity",     0, 8,  0.06F),
    TEMPERATURE_TOLERANCE("temperaturetolerance", 0, 6,  0.03F),
    PRODUCTIVITY        ("productivity",         0, 5,  0.04F),
    FERTILITY           ("fertility",            0, 5,  0.04F),
    SPREADING_SPEED("spreadingspeed",       0, 10, 0.04F),
    GENE_STRENGTH("genestrength",         0, 10, 0.04F),
    ENERGY_VALUE("energyvalue",          1, 10, 0.04F);

    private final int min, max;
    private final float transitionpossibility;
    private final String name;

    CropTraitsTypes(String name, int min, int max, float transitionpossibility)
    {
        this.name = name;
        this.min = min;
        this.max = max;
        this.transitionpossibility = transitionpossibility;
    }

    public MutableComponent getDisplayName()
    {
        return Component.translatable(getTranslationKey());
    }

    public String getTranslationKey()
    {
        return "planttech2.traits." + this.name;
    }

    public static CropTraitsTypes getByName(String name)
    {
        return Arrays.stream(values()).filter((trait) -> trait.getName().equalsIgnoreCase(name)).findFirst().orElse(null);
    }

    public String getName()
    {
	    return this.name;
    }

    public int getMin()
    {
	return this.min;
    }

    public int getMax()
    {
	return this.max;
    }

    public float getTransitionPossibility()
    {
	return this.transitionpossibility;
    }
}
