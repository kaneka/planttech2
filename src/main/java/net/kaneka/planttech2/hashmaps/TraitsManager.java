package net.kaneka.planttech2.hashmaps;

import net.kaneka.planttech2.crops.CropTypes;
import net.kaneka.planttech2.enums.CropTraitsTypes;
import net.kaneka.planttech2.utilities.PTCommonUtil;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.RandomSource;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.common.util.INBTSerializable;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class TraitsManager implements INBTSerializable<CompoundTag>
{
	public boolean analysed = false;
	protected final Map<CropTraitsTypes, Integer> traits = new HashMap<>();

	public TraitsManager(CompoundTag nbt)
	{
		this();
		deserializeNBT(nbt);
	}

	public TraitsManager()
	{
		defaultStats();
	}

	public void defaultStats()
	{
		for (CropTraitsTypes trait : CropTraitsTypes.values())
			traits.put(trait, trait.getMin());
	}

	public TraitsManager copy()
	{
		TraitsManager copy = new TraitsManager();
		copy.analysed = analysed;
		traits.keySet().forEach((trait) -> copy.traits.put(trait, traits.get(trait)));
		return copy;
	}

	@Override
	public CompoundTag serializeNBT()
	{
		CompoundTag compound = new CompoundTag();
		compound.putBoolean("analysed", analysed);
		traits.forEach((trait, level) -> {
			if (level != null)
				compound.putInt(trait.getName(), level);
		});
		return compound;
	}

	@Override
	public void deserializeNBT(CompoundTag nbt)
	{
		if (nbt != null)
		{
			analysed = nbt.getBoolean("analysed");
			traits.keySet().forEach((trait) ->
			{
				traits.put(trait, nbt.contains(trait.getName()) ? nbt.getInt(trait.getName()) : null);
			});
		}
	}

	public boolean hasTrait(CropTraitsTypes trait)
	{
		return traits.get(trait) != null;
	}

	public int getTrait(CropTraitsTypes trait)
	{
		Integer v = traits.get(trait);
		return v == null ? 0 : v;
	}

	public void setTrait(CropTraitsTypes trait, int value)
	{
		traits.put(trait, value);
	}

	@Override
	public String toString()
	{
		return "TraitsManager{" +
				"analysed=" + analysed +
				", traits=" + traits +
				'}';
	}

	public static class TypedImpl extends TraitsManager
	{
		@Nullable
		public CropTypes type;
		public TypedImpl()
		{
			super();
		}

		public TypedImpl(@org.jetbrains.annotations.Nullable CropTypes type)
		{
			super();
			this.type = type;
		}

		public TypedImpl(CompoundTag nbt)
		{
			super(nbt);
		}


		public TypedImpl(CompoundTag nbt, @org.jetbrains.annotations.Nullable CropTypes type)
		{
			super(nbt);
			this.type = type;
		}

		public static Set<String> mutableTraitKeys()
		{
			TreeSet<String> set = new TreeSet<>(PTCommonUtil.collect(Arrays.stream(CropTraitsTypes.values()).toList(), CropTraitsTypes::getName));
			set.add("type");
			return set;
		}

		@Override
		public CompoundTag serializeNBT()
		{
			CompoundTag compound = super.serializeNBT();
			if (type != null)
				compound.putString("type", type.getName());
			return compound;
		}

		@Override
		public void deserializeNBT(CompoundTag nbt)
		{
			super.deserializeNBT(nbt);
			if (nbt != null)
				type = CropTypes.fromNameNullable(nbt.getString("type"));
		}

		@Override
		public TypedImpl copy()
		{
			TypedImpl copy = new TypedImpl(type);
			copy.analysed = analysed;
			traits.keySet().forEach((trait) -> copy.traits.put(trait, traits.get(trait)));
			return copy;
		}

		public void setType(CropTypes type)
		{
			this.type = type;
		}

		private static final RandomSource RANDOM = RandomSource.create();
		public TypedImpl calculateNewTraits(TypedImpl otherTrait, CropTraitsTypes boost, int boostLevel)
		{
			CropTypes newType = RANDOM.nextBoolean() ? type : otherTrait.type;
			if (type != otherTrait.type && type != null && otherTrait.type != null)
			{
				List<CropTypes> children = CropTypes.getByParents(type, otherTrait.type);
				if (!children.isEmpty())
				{
					Map<CropTypes, Float> mutationChances = new TreeMap<>();
					for (CropTypes child : children)
						mutationChances.put(child, child.getConfig().getMutateChanceForParents(type, otherTrait.type));
					float mutateChance = (float) mutationChances.values().stream().mapToDouble(Float::doubleValue).sum();
					boolean mutate = RANDOM.nextFloat() <= mutateChance;
					if (mutate)
					{
						double minInterval = 0;
						float rand = RANDOM.nextFloat();
						for (CropTypes crop : mutationChances.keySet())
						{
							double maxInterval = minInterval + mutationChances.get(crop) / mutateChance;
							if (rand >= minInterval && rand < maxInterval)
							{
								newType = crop;
								break;
							}
							minInterval = maxInterval;
						}
					}
				}
			}

			TypedImpl newTraits = new TypedImpl(newType);
			for (CropTraitsTypes trait : CropTraitsTypes.values())
			{
				int lv = getTrait(trait);
				int lv2 = otherTrait.getTrait(trait);
				if (lv == lv2)
					newTraits.traits.put(trait, lv + (RANDOM.nextFloat() <= (trait.getTransitionPossibility() + (trait == boost ? boostLevel * 0.01F : 0.0F)) && lv < trait.getMax() ? 1 : 0));
				else
				{
					int min = Math.min(lv, lv2);
					int max = Math.max(lv, lv2);
					newTraits.traits.put(trait, RANDOM.nextInt(max - min) + min);
				}
			}
			return newTraits;
		}

		@Override
		public String toString()
		{
			return "TypedImpl{" +
					super.toString() + ", " +
					"type=" + type +
					'}';
		}
	}

	public static class ItemImpl extends TypedImpl
	{
		public final ItemStack stack;
		private ItemImpl(ItemStack stack, CompoundTag compound)
		{
			super(compound);
			this.stack = stack;
			save();
		}

		private ItemImpl(ItemStack stack)
		{
			super(stack.hasTag() ? stack.getTag() : null);
			this.stack = stack;
		}

		@Override
		public ItemImpl copy()
		{
			return of(stack.copy(), serializeNBT());
		}

		public static ItemImpl of(ItemStack stack)
		{
			return new ItemImpl(stack);
		}

		public static ItemImpl of(Item item, CompoundTag compound)
		{
			return of(new ItemStack(item), compound);
		}

		public static ItemImpl of(ItemStack stack, CompoundTag compound)
		{
			return new ItemImpl(stack, compound);
		}

		public void save()
		{
			stack.getOrCreateTag().merge(serializeNBT());
		}

		@Override
		public String toString()
		{
			return "ItemImpl{" +
					super.toString() + ", " +
					"ItemStack=" + stack +
					'}';
		}
	}
}
