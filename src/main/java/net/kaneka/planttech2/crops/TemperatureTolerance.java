package net.kaneka.planttech2.crops;

import net.kaneka.planttech2.addons.Pair;
import net.kaneka.planttech2.utilities.ISerializable;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;

//	minecraft:frozen_peaks -0.7
//	minecraft:jagged_peaks -0.7
//	minecraft:snowy_taiga -0.5
//	minecraft:snowy_slopes -0.3
//	minecraft:grove -0.2
//	minecraft:frozen_ocean 0.0
//	minecraft:frozen_river 0.0
//	minecraft:ice_spikes 0.0
//	minecraft:snowy_plains 0.0
//	minecraft:snowy_beach 0.05
//	minecraft:stony_shore 0.2
//	minecraft:windswept_forest 0.2
//	minecraft:windswept_gravelly_hills 0.2
//	minecraft:windswept_hills 0.2
//	minecraft:old_growth_spruce_taiga 0.25
//	minecraft:taiga 0.25
//	minecraft:old_growth_pine_taiga 0.3
//	minecraft:cherry_grove 0.5
//	minecraft:cold_ocean 0.5
//	minecraft:deep_cold_ocean 0.5
//	minecraft:deep_frozen_ocean 0.5
//	minecraft:deep_lukewarm_ocean 0.5
//	minecraft:deep_ocean 0.5
//	minecraft:end_barrens 0.5
//	minecraft:end_highlands 0.5
//	minecraft:end_midlands 0.5
//	minecraft:lukewarm_ocean 0.5
//	minecraft:lush_caves 0.5
//	minecraft:meadow 0.5
//	minecraft:ocean 0.5
//	minecraft:river 0.5
//	minecraft:small_end_islands 0.5
//	minecraft:the_end 0.5
//	minecraft:the_void 0.5
//	minecraft:warm_ocean 0.5
//	minecraft:birch_forest 0.6
//	minecraft:old_growth_birch_forest 0.6
//	minecraft:dark_forest 0.7
//	minecraft:flower_forest 0.7
//	minecraft:forest 0.7
//	minecraft:beach 0.8
//	minecraft:deep_dark 0.8
//	minecraft:dripstone_caves 0.8
//	minecraft:mangrove_swamp 0.8
//	minecraft:plains 0.8
//	minecraft:sunflower_plains 0.8
//	minecraft:swamp 0.8
//	minecraft:mushroom_fields 0.9
//	minecraft:bamboo_jungle 0.95
//	minecraft:jungle 0.95
//	minecraft:sparse_jungle 0.95
//	minecraft:stony_peaks 1.0
//	minecraft:badlands 2.0
//	minecraft:basalt_deltas 2.0
//	minecraft:crimson_forest 2.0
//	minecraft:desert 2.0
//	minecraft:eroded_badlands 2.0
//	minecraft:nether_wastes 2.0
//	minecraft:savanna 2.0
//	minecraft:savanna_plateau 2.0
//	minecraft:soul_sand_valley 2.0
//	minecraft:warped_forest 2.0
//	minecraft:windswept_savanna 2.0
//	minecraft:wooded_badlands 2.0

public class TemperatureTolerance extends Pair<Float, Float> implements ISerializable
{
    public static final float MIN = -0.7F;
    public static final float MAX = 2.0F;
    public static final float RANGE = MAX - MIN;
    public static final TemperatureTolerance DEFAULT = of(0.0F, 0.0F);
    public static final TemperatureTolerance EXTREME_COLD = of(-0.7F, 0.1F);
    public static final TemperatureTolerance COLD = of(0.1F, 0.41F);
    public static final TemperatureTolerance NORMAL = of(0.41F, 0.65F);
    public static final TemperatureTolerance WARM = of(0.65F, 1.5F);
    public static final TemperatureTolerance EXTREME_WARM = of(1.5F, 2.0F);

    public static TemperatureTolerance of(float min, float max)
    {
        return new TemperatureTolerance(min, max);
    }

    public static TemperatureTolerance fromNBT(CompoundTag compound)
    {
        TemperatureTolerance t = new TemperatureTolerance();
        t.deserializeNBT(compound);
        return t;
    }

    public TemperatureTolerance()
    {
        this(0.0F, 0.0F);
    }

    protected TemperatureTolerance(Float aFloat, Float aFloat2)
    {
        super(aFloat, aFloat2);
    }

    public boolean inRange(float temp)
    {
        return inRange(temp, 0);
    }

    public boolean inRange(float temp, float extraTolerance)
    {
        return temp >= getA() - extraTolerance && temp <= getB() + extraTolerance;
    }

    public float getMinTempTolerancePercent()
    {
        return getAbsMinTempTolerance() / RANGE;
    }

    public float getMaxTempTolerancePercent()
    {
        return getAbsMaxTempTolerance() / RANGE;
    }

    public float getMinTempTolerance()
    {
        return getA();
    }

    public float getMaxTempTolerance()
    {
        return getB();
    }

    public float getAbsMinTempTolerance()
    {
        return getA() - MIN;
    }

    public float getAbsMaxTempTolerance()
    {
        return getB() - MIN;
    }

    @Override
    public TemperatureTolerance copy()
    {
        return of(getMinTempTolerance(), getMaxTempTolerance());
    }

    @Override
    public CompoundTag serializeNBT()
    {
        CompoundTag compound = new CompoundTag();
        compound.putFloat("minTempTolerance", getA());
        compound.putFloat("maxTempTolerance", getB());
        return compound;
    }

    @Override
    public void deserializeNBT(CompoundTag nbt)
    {
        setA(nbt.getFloat("minTempTolerance"));
        setB(nbt.getFloat("maxTempTolerance"));
    }

    @Override
    public void extraToNetwork(FriendlyByteBuf buf)
    {
        ISerializable.super.extraToNetwork(buf);
    }

    @Override
    public void extraFromNetwork(FriendlyByteBuf buf)
    {
        ISerializable.super.extraFromNetwork(buf);
    }

    public String getDisplayString(int extraTolerence)
    {
        return getDisplayString() + (extraTolerence == 0 ? "" : " (+-" + String.format("%.2f", extraTolerence / 4.0F) + ")");
    }

    public String getDisplayString()
    {
        return String.format("%.2f ~ %.2f", getA(), getB());
    }
}
