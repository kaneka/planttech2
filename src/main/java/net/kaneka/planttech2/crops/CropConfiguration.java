package net.kaneka.planttech2.crops;

import net.kaneka.planttech2.utilities.ISerializable;
import net.kaneka.planttech2.utilities.NBTHelper;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraftforge.registries.ForgeRegistries;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

import static net.minecraftforge.registries.ForgeRegistries.ITEMS;

public class CropConfiguration implements ISerializable
{
	private boolean enabled;
	private TemperatureTolerance temperature;
	private DropEntry primarySeed;
	private List<Supplier<Ingredient>> seeds;
	private List<DropEntry> drops;
	private List<ParentPair> parents;
	private Supplier<Block> soil;

	public CropConfiguration(TemperatureTolerance temperature, DropEntry primarySeed, List<Supplier<Ingredient>> seeds,
	                  List<DropEntry> drops, List<ParentPair> parents, Supplier<Block> soil)
	{
		update(temperature, primarySeed, seeds, drops, parents, soil);
	}

	public void setEnabled(boolean enabled)
	{
		this.enabled = enabled;
	}

	public void update(CropConfiguration config)
	{
		update(config.temperature, config.primarySeed, config.seeds, config.drops, config.parents, config.soil);
	}

	public void update(TemperatureTolerance temperature, DropEntry primarySeed, List<Supplier<Ingredient>> seeds,
					   List<DropEntry> drops, List<ParentPair> parents, Supplier<Block> soil)
	{
		this.temperature = temperature;
		this.primarySeed = primarySeed;
		this.seeds = seeds;
		this.drops = drops;
		this.parents = parents;
		this.soil = soil;
	}

	@Override
	public void extraToNetwork(FriendlyByteBuf buf)
	{
		buf.writeInt(seeds.size());
		for (Supplier<Ingredient> seed : seeds)
			seed.get().toNetwork(buf);
	}

	@Override
	public CompoundTag serializeNBT()
	{
		CompoundTag compound = new CompoundTag();
		compound.putBoolean("enabled", enabled);
		compound.put("temperature", temperature.serializeNBT());
		compound.put("primaryseed", primarySeed.serializeNBT());
		NBTHelper.putSerilizableList(compound, "drops", drops);
		NBTHelper.putSerilizableList(compound, "parents", parents);
		compound.putString("soil", ForgeRegistries.BLOCKS.getKey(soil.get()).toString());
		return compound;
	}

	@Override
	public void extraFromNetwork(FriendlyByteBuf buf)
	{
		seeds.clear();
		int s = buf.readInt();
		for (int i = 0; i < s; i++)
		{
			Ingredient ingredient = Ingredient.fromNetwork(buf);
			seeds.add(() -> ingredient);
		}
	}

	@Override
	public void deserializeNBT(CompoundTag compound)
	{
		enabled = compound.getBoolean("enabled");
		temperature = TemperatureTolerance.fromNBT(compound.getCompound("temperature"));
		primarySeed = DropEntry.of(compound.getCompound("primaryseed"));
		drops = NBTHelper.constructListFromCompound(compound, "drops", DropEntry::of);
		parents = NBTHelper.constructListFromCompound(compound, "parents", ParentPair::of);
		soil = () -> ForgeRegistries.BLOCKS.getValue(new ResourceLocation(compound.getString("soil")));
	}

	public boolean isEnabled()
	{
		return enabled;
	}

	public boolean isEmpty()
	{
//		return primarySeed.isEmpty() || seeds.isEmpty() || seeds.stream().allMatch((e) -> e.get().isEmpty());
		return primarySeed.isEmpty();
	}

	public boolean shouldBeInGame()
	{
		return isEnabled() && !isEmpty();
	}

	public TemperatureTolerance getTemperature()
	{
		return temperature;
	}

	public DropEntry getPrimarySeed()
	{
		return primarySeed;
	}

	public List<Supplier<Ingredient>> getSeeds()
	{
		return seeds;
	}

	public List<DropEntry> getDrops()
	{
		return drops;
	}

	public List<ParentPair> getParents()
	{
		return parents;
	}

	public Supplier<Block> getSoil()
	{
		return soil;
	}

	public float getMutateChanceForParents(CropTypes parent1, CropTypes parent2)
	{
		return parents.stream().filter(parentPair -> parentPair.test(parent1, parent2)).map(ParentPair::getMutationChance).findFirst().orElse(0F);
	}

	@Override
	public String toString()
	{
		return "CropConfiguration{" +
				"enabled=" + enabled +
				", temperature=" + temperature +
				", primarySeed=" + primarySeed +
				", seeds=" + seeds +
				", drops=" + drops +
				", parents=" + parents +
				", soil=" + soil +
				'}';
	}

	public static Builder builder(CropConfiguration configuration)
	{
		return new Builder(configuration);
	}

	public static Builder builder(DropEntry primarySeed)
	{
		return new Builder(primarySeed);
	}

	public static class Builder
	{
		final TemperatureTolerance temperature = TemperatureTolerance.NORMAL.copy();
		final DropEntry primarySeed;
		final List<Supplier<Ingredient>> seeds = new ArrayList<>();
		final List<DropEntry> drops = new ArrayList<>();
		final List<ParentPair> parents = new ArrayList<>();
		Supplier<Block> soil = () -> Blocks.DIRT;

		private Builder(CropConfiguration configuration)
		{
			temperature(configuration.getTemperature());
			this.primarySeed = configuration.getPrimarySeed();
			this.seeds.addAll(configuration.getSeeds());
			this.drops.addAll(configuration.getDrops());
			this.parents.addAll(configuration.getParents());
			this.soil = configuration.getSoil();
		}

		private Builder(DropEntry primarySeed)
		{
			this.primarySeed = primarySeed;
		}

		public CropConfiguration build()
		{
			return new CropConfiguration(temperature, primarySeed, seeds, drops, parents, soil);
		}

		public Builder temperature(float min, float max)
		{
			this.temperature.set(min, max);
			return this;
		}

		public Builder temperature(TemperatureTolerance temperature)
		{
			this.temperature.set(temperature.getA(), temperature.getB());
			return this;
		}

		public Builder seed(Supplier<Item> item)
		{
			this.seeds.add(() -> Ingredient.of(item.get()));
			return this;
		}

		public Builder seed(TagKey<Item> tag)
		{
			this.seeds.add(() -> Ingredient.of(tag));
			return this;
		}


		public Builder seed(ResourceLocation item)
		{
			return this.seed(LazyRegistryEntry.of(item, ITEMS));
		}

		public Builder seed(String item)
		{
			return this.seed(new ResourceLocation(item));
		}

		public Builder parents(ParentPair entry)
		{
			this.parents.add(entry);
			return this;
		}

		public Builder parents(CropTypes parent1, CropTypes parent2, float chance)
		{
			return parents(parent1.getName(), parent2.getName(), chance);
		}


		public Builder parents(String parent1, String parent2, float chance)
		{
			return this.parents(ParentPair.of(parent1, parent2, chance));
		}

		public Builder parents(ParentPair... entries)
		{
			this.parents.addAll(Arrays.asList(entries));
			return this;
		}

		public Builder drop(DropEntry entry)
		{
			this.drops.add(entry);
			return this;
		}

		public Builder drop(Supplier<Item> item, int min, int max)
		{
			return this.drop(DropEntry.of(item, min, max));
		}

		public Builder drop(ResourceLocation item, int min, int max)
		{
			return this.drop(LazyRegistryEntry.of(item, ITEMS), min, max);
		}

		public Builder drop(String item, int min, int max)
		{
			return this.drop(new ResourceLocation(item), min, max);
		}

		public Builder drop(DropEntry... entries)
		{
			this.drops.addAll(Arrays.asList(entries));
			return this;
		}

		public Builder soil(Supplier<Block> newSoil)
		{
			this.soil = newSoil;
			return this;
		}
	}
}
