package net.kaneka.planttech2.crops;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.kaneka.planttech2.packets.CropListSyncMessage;
import net.kaneka.planttech2.packets.PlantTech2PacketHandler;
import net.kaneka.planttech2.utilities.BaseReloadListener;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.network.PacketDistributor;
import net.minecraftforge.server.ServerLifecycleHooks;

import java.util.Map;
import java.util.function.Supplier;

import static net.kaneka.planttech2.PlantTechMain.LOGGER;

public class CropListReloadListener extends BaseReloadListener<CropTypes>
{
	public static String FOLDER = "pt2_crops";
	public static Gson GSON = new GsonBuilder().registerTypeAdapter(CropTypes.class, CropTypes.Serializer.INSTANCE)
			.registerTypeAdapter(DropEntry.class, DropEntry.Serializer.INSTANCE)
			.registerTypeAdapter(ParentPair.class, ParentPair.Serializer.INSTANCE)
			.create();

	public CropListReloadListener()
	{
		super("crop configuration", GSON, FOLDER);
	}

	@Override
	protected CropTypes fromJson(JsonElement element)
	{
		return gson.fromJson(element, CropTypes.class);
	}

	@Override
	protected CropTypes loadIndex(JsonElement element)
	{
		JsonObject obj = element.getAsJsonObject();
		for (String key : obj.keySet())
		{
			CropTypes crop = CropTypes.fromNameNullable(key);
			if (crop != null)
				crop.getConfig().setEnabled(obj.get(key).getAsBoolean());
		}
		return null;
	}

	@Override
	protected void postReload(Map<ResourceLocation, Supplier<CropTypes>> map)
	{
		for (Supplier<CropTypes> value : map.values())
			value.get();
		if (ServerLifecycleHooks.getCurrentServer() != null)
		{
			LOGGER.info("syncing crop list to clients");
			PlantTech2PacketHandler.INSTANCE.send(PacketDistributor.ALL.noArg(), new CropListSyncMessage());
		}
		else LOGGER.warn("Server is not up yet, will not send the changes to clients. Might need to reload manually afterwards");
	}

	public static JsonElement toJson(CropTypes crop)
	{
		return GSON.toJsonTree(crop);
	}
}
