package net.kaneka.planttech2.crops;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.registries.ModBlocks;
import net.kaneka.planttech2.registries.ModItems;
import net.kaneka.planttech2.utilities.BaseDataProvider;
import net.minecraft.data.CachedOutput;
import net.minecraft.data.DataProvider;
import net.minecraft.data.PackOutput;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.block.Blocks;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

import static net.kaneka.planttech2.crops.CropTypes.*;
import static net.minecraft.world.item.Items.*;

public class CropConfigProvider extends BaseDataProvider<CropTypes>
{
	public CropConfigProvider(PackOutput generator)
	{
		super(generator, (entry) -> modLoc(entry.getName()));
	}

	@Override
	public CompletableFuture<?> run(CachedOutput cache)
	{
		return super.run(cache);
	}

	@Override
	protected JsonObject writeIndex()
	{
		JsonObject index = new JsonObject();
		data.forEach((crop) -> index.addProperty(crop.getName(), crop.getConfig().isEnabled()));
		return index;
	}

	@Override
	protected void validate(List<CropTypes> dataMap)
	{
		final AtomicBoolean errored = new AtomicBoolean(false);
		dataMap.forEach((crop) -> {
			String name = crop.getName();
			ResourceLocation loc = new ResourceLocation(PlantTechMain.MODID, name);
			crop.getConfig().getParents().forEach(parentPair -> {
				if (parentPair.getFirstParent() == null)
				{
					LOGGER.fatal("Unknown first parent crop entry {} in {}: {}", parentPair.getFirstParent(), loc, parentPair);
					errored.set(true);
				}
				if (parentPair.getSecondParent() == null)
				{
					LOGGER.fatal("Unknown second parent crop entry {} in {}: {}", parentPair.getSecondParent(), loc, parentPair);
					errored.set(true);
				}
			});
		});
		if (errored.get())
			throw new RuntimeException("Unknown crop entries found during validation. Check the log for details");
	}

	@Override
	protected JsonElement toJson(CropTypes data)
	{
		return CropListReloadListener.toJson(data);
	}

	protected Path getPath(Path pathIn, ResourceLocation id)
	{
		return pathIn.resolve("data/" + id.getNamespace() + "/" + CropListReloadListener.FOLDER + "/" + id.getPath() + ".json");
	}

	@Override
	public String getName()
	{
		return "Crop Entry Configurations";
	}

	@Override
	public List<CropTypes> getData()
	{
		data.clear();
		addEntryWithSeeds(ALLAY_CROP,
				b -> b.parents(CHICKEN_CROP, SQUID_CROP, 0.1F));
		addEntryWithSeeds(ALLIUM_CROP,
				b -> b.seed(() -> ALLIUM)
						.drop(() -> ModItems.COLOR_PARTICLES.get(), 1, 4)
						.drop(() -> ALLIUM, 0, 4));
		addEntryWithSeeds(AMETHYST_CROP,
				b -> b.parents(DIAMOND_CROP, LAPIS_CROP, 0.1F)
						.soil(() -> Blocks.CALCITE));
		addEntryWithSeeds(AZURE_BLUET_CROP,
				b -> b.seed(() -> AZURE_BLUET)
						.drop(() -> ModItems.COLOR_PARTICLES.get(), 1, 4)
						.drop(() -> AZURE_BLUET, 0, 4));
		addEntryWithSeeds(BAMBOO_CROP,
				b -> b.seed(() -> BAMBOO)
						.drop(() -> BAMBOO, 0, 4));
		addEntryWithSeeds(BEAST_CROP,
				b -> b.parents(ILLAGER_CROP, WITCH_CROP, 0.1F));
		addEntryWithSeeds(BEE_CROP,
				b -> b.parents(DANDELION_CROP, ALLIUM_CROP, 0.1F));
		addEntryWithSeeds(BEETROOTS_CROP,
				b -> b.seed(() -> BEETROOT_SEEDS)
						.drop(() -> BEETROOT, 0, 4));
		addEntryWithSeeds(BLAZE_CROP,
				b -> b.parents(CREEPER_CROP, LAVA_CROP, 0.1F)
						.temperature(TemperatureTolerance.EXTREME_WARM));
		addEntryWithSeeds(BLUE_ORCHID_CROP,
				b -> b.seed(() -> BLUE_ORCHID)
						.drop(() -> ModItems.COLOR_PARTICLES.get(), 1, 4)
						.drop(() -> BLUE_ORCHID, 0, 4));
		addEntryWithSeeds(CACTUS_CROP,
				b -> b.seed(() -> CACTUS)
						.drop(() -> CACTUS, 0, 4)
						.temperature(TemperatureTolerance.WARM)
						.soil(() -> Blocks.SAND));
		addEntryWithSeeds(CARROT_CROP,
				b -> b.seed(() -> CARROT)
						.drop(() -> CARROT, 0, 4));
		addEntryWithSeeds(CHICKEN_CROP,
				b -> b.parents(BEETROOTS_CROP, CACTUS_CROP, 0.1F));
		addEntryWithSeeds(CHORUS_CROP,
				b -> b.seed(() -> CHORUS_FLOWER)
						.drop(() -> CHORUS_FRUIT, 0, 4)
						.temperature(TemperatureTolerance.COLD)
						.soil(() -> Blocks.END_STONE));
		addEntryWithSeeds(COAL_CROP,
				b -> b.parents(NETHER_WART_CROP, CHORUS_CROP, 0.1F)
						.soil(() -> Blocks.COAL_ORE));
		addEntryWithSeeds(COCOA_BEAN_CROP,
				b -> b.seed(() -> COCOA_BEANS)
						.drop(() -> ModItems.COLOR_PARTICLES.get(), 1, 4)
						.drop(() -> COCOA_BEANS, 0, 4)
						.temperature(TemperatureTolerance.WARM));
		addEntryWithSeeds(COPPER_CROP,
				b -> b.parents(IRON_CROP, COAL_CROP, 0.1F)
						.soil(() -> Blocks.COPPER_ORE)
		);
		addEntryWithSeeds(CORNFLOWER_CROP,
				b -> b.seed(() -> CORNFLOWER)
						.drop(() -> ModItems.COLOR_PARTICLES.get(), 1, 4)
						.drop(() -> CORNFLOWER, 0, 4));
		addEntryWithSeeds(COW_CROP,
				b -> b.parents(CARROT_CROP, POTATO_CROP, 0.1F));
		addEntryWithSeeds(CREEPER_CROP,
				b -> b.parents(PIG_CROP, COAL_CROP, 0.1F));
		addEntryWithSeeds(DANCIUM_CROP,
				b -> b.parents(PLANTIUM_CROP, FISH_CROP, 0.1F));
		addEntryWithSeeds(DANDELION_CROP,
				b -> b.seed(() -> DANDELION)
						.drop(() -> ModItems.COLOR_PARTICLES.get(), 1, 4)
						.drop(() -> DANDELION, 0, 4));
		addEntryWithSeeds(DIAMOND_CROP,
				b -> b.parents(QUARTZ_CROP, ENDERDRAGON_CROP, 0.1F)
						.soil(() -> Blocks.DIAMOND_ORE));
		addEntryWithSeeds(DIRT_CROP,
				b -> b.parents(MELON_CROP, PUMPKIN_CROP, 0.1F));
		addEntryWithSeeds(DROWNED_CROP,
				b -> b.parents(STRAY_CROP, HUSK_CROP, 0.1F)
						.temperature(TemperatureTolerance.COLD));
		addEntryWithSeeds(EMERALD_CROP,
				b -> b.parents(DIAMOND_CROP, WITHER_CROP, 0.1F)
						.soil(() -> Blocks.DIAMOND_ORE));
		addEntryWithSeeds(ENDERDRAGON_CROP,
				b -> b.parents(ENDERMAN_CROP, SHULKER_CROP, 0.1F)
						.temperature(TemperatureTolerance.EXTREME_WARM)
						.soil(() -> Blocks.BEDROCK));
		addEntryWithSeeds(ENDERMAN_CROP,
				b -> b.parents(WITHER_SKELETON_CROP, NETHERRACK_CROP, 0.1F));
		addEntryWithSeeds(ENDSTONE_CROP,
				b -> b.parents(COAL_CROP, CHORUS_CROP, 0.1F)
						.temperature(TemperatureTolerance.COLD));
		addEntryWithSeeds(FISH_CROP,
				b -> b.parents(CACTUS_CROP, SUGARCANE_CROP, 0.1F)
						.temperature(TemperatureTolerance.COLD));
		addEntryWithSeeds(FROG_CROP,
				b -> b.parents(SLIME_CROP, WATER_CROP, 0.1F)
						.temperature(TemperatureTolerance.WARM));
		addEntryWithSeeds(GHAST_CROP,
				b -> b.parents(MAGMA_CUBE_CROP, BLAZE_CROP, 0.1F)
						.temperature(TemperatureTolerance.EXTREME_WARM));
		addEntryWithSeeds(GLOWING_SQUID_CROP,
				b -> b.parents(SQUID_CROP, GLOWSTONE_CROP, 0.1F)
						.temperature(TemperatureTolerance.COLD)
						.soil(() -> Blocks.SAND));
		addEntryWithSeeds(GLOWSTONE_CROP,
				b -> b.parents(SOULSAND_CROP, NETHERRACK_CROP, 0.1F)
						.temperature(TemperatureTolerance.EXTREME_WARM));
		addEntryWithSeeds(GLOW_BERRIES_CROP,
				b -> b.parents(SWEET_BERRIES_CROP, GLOWSTONE_CROP, 0.1F)
						.seed(() -> GLOW_BERRIES)
						.drop(() -> GLOW_BERRIES, 0, 4));
		addEntryWithSeeds(GOAT_CROP,
				b -> b.parents(SHEEP_CROP, COW_CROP, 0.1F)
						.soil(() -> Blocks.STONE)
						.temperature(TemperatureTolerance.COLD));
		addEntryWithSeeds(GOLD_CROP,
				b -> b.parents(COAL_CROP, LAPIS_CROP, 0.1F)
						.soil(() -> Blocks.GOLD_ORE));
		addEntryWithSeeds(GUARDIAN_CROP,
				b -> b.parents(GHAST_CROP, WITCH_CROP, 0.1F)
						.temperature(TemperatureTolerance.COLD));
		addEntryWithSeeds(HUSK_CROP,
				b -> b.parents(ZOMBIE_CROP, LAVA_CROP, 0.1F)
						.temperature(TemperatureTolerance.WARM));
		addEntryWithSeeds(ILLAGER_CROP,
				b -> b.parents(VILLAGER_CROP, WITCH_CROP, 0.1F));
		addEntryWithSeeds(IRON_CROP,
				b -> b.parents(REDSTONE_CROP, GOLD_CROP, 0.1F)
						.soil(() -> Blocks.IRON_ORE));
		addEntryWithSeeds(KANEKIUM_CROP,
				b -> b.parents(PLANTIUM_CROP, SQUID_CROP, 0.1F));
		addEntryWithSeeds(KELP_CROP,
				b -> b.seed(() -> KELP)
						.drop(() -> KELP, 0, 4));
		addEntryWithSeeds(KINNOIUM_CROP,
				b -> b.parents(PLANTIUM_CROP, PIG_CROP, 0.1F));
		addEntryWithSeeds(LAPIS_CROP,
				b -> b.parents(STONE_CROP, COAL_CROP, 0.1F)
						.soil(() -> Blocks.LAPIS_ORE));
		addEntryWithSeeds(LAVA_CROP,
				b -> b.parents(STONE_CROP, WATER_CROP, 0.1F)
						.temperature(TemperatureTolerance.EXTREME_WARM));
		addEntryWithSeeds(LENTHURIUM_CROP,
				b -> b.parents(PLANTIUM_CROP, CHICKEN_CROP, 0.1F));
		addEntryWithSeeds(
                LILY_OF_THE_VALLEY_CROP,
				b -> b.seed(() -> LILY_OF_THE_VALLEY)
						.drop(() -> ModItems.COLOR_PARTICLES.get(), 1, 4)
						.drop(() -> LILY_OF_THE_VALLEY, 0, 4));
		addEntryWithSeeds(MAGMA_CUBE_CROP,
				b -> b.parents(SLIME_CROP, LAVA_CROP, 0.1F)
						.temperature(TemperatureTolerance.EXTREME_WARM));
		addEntryWithSeeds(MELON_CROP,
				b -> b.seed(() -> MELON_SEEDS)
						.drop(() -> MELON, 0, 4));
		addEntryWithSeeds(MOOSHROOM_CROP,
				b -> b.parents(COW_CROP, MUSHROOM_CROP, 0.1F));
		addEntryWithSeeds(MUSHROOM_CROP,
				b -> b.seed(() -> BROWN_MUSHROOM)
						.seed(() -> RED_MUSHROOM)
						.drop(() -> BROWN_MUSHROOM, 0, 4)
						.drop(() -> RED_MUSHROOM, 0, 4));
		addEntryWithSeeds(MYCELIUM_CROP,
				b -> b.parents(NETHERRACK_CROP, MOOSHROOM_CROP, 0.1F));
		addEntryWithSeeds(NETHER_WART_CROP,
				b -> b.seed(() -> NETHER_WART)
						.drop(() -> NETHER_WART, 0, 4)
						.temperature(TemperatureTolerance.EXTREME_WARM));
		addEntryWithSeeds(NETHERRACK_CROP,
				b -> b.parents(ENDSTONE_CROP, NETHER_WART_CROP, 0.1F)
						.temperature(TemperatureTolerance.EXTREME_WARM));
		addEntryWithSeeds(NETHERITE_CROP,
				b -> b.parents(GHAST_CROP, ENDERDRAGON_CROP, 0.1F)
						.temperature(TemperatureTolerance.EXTREME_WARM));
		addEntryWithSeeds(ORANGE_TULIP_CROP,
				b -> b.seed(() -> ORANGE_TULIP)
						.drop(() -> ModItems.COLOR_PARTICLES.get(), 1, 4)
						.drop(() -> ORANGE_TULIP, 0, 4));
		addEntryWithSeeds(OXEYE_DAISY_CROP,
				b -> b.seed(() -> OXEYE_DAISY)
						.drop(() -> ModItems.COLOR_PARTICLES.get(), 1, 4)
						.drop(() -> OXEYE_DAISY, 0, 4));
		addEntryWithSeeds(PINK_TULIP_CROP,
				b -> b.seed(() -> PINK_TULIP)
						.drop(() -> ModItems.COLOR_PARTICLES.get(), 1, 4)
						.drop(() -> PINK_TULIP, 0, 4));
		addEntryWithSeeds(PLANTIUM_CROP,
				b -> b.seed(() -> ModItems.PLANTIUM_NUGGET.get())
						.soil(() -> ModBlocks.PLANTIUM_BLOCK.get()));
		addEntryWithSeeds(POPPY_CROP,
				b -> b.seed(() -> POPPY)
						.drop(() -> ModItems.COLOR_PARTICLES.get(), 1, 4)
						.drop(() -> POPPY, 0, 4));
		addEntryWithSeeds(POTATO_CROP,
				b -> b.seed(() -> POTATO)
						.drop(() -> POTATO, 0, 4)
						.drop(() -> POISONOUS_POTATO, 0, 2));
		addEntryWithSeeds(PUMPKIN_CROP,
				b -> b.seed(() -> PUMPKIN_SEEDS)
						.drop(() -> PUMPKIN, 0, 3));
		addEntryWithSeeds(PANDA_CROP,
				b -> b.parents(BEETROOTS_CROP, MUSHROOM_CROP, 0.1F));
		addEntryWithSeeds(PARROT_CROP,
				b -> b.parents(WHEAT_CROP, COCOA_BEAN_CROP, 0.1F)
						.temperature(TemperatureTolerance.WARM));
		addEntryWithSeeds(PIG_CROP,
				b -> b.parents(BEETROOTS_CROP, CARROT_CROP, 0.1F));
		addEntryWithSeeds(POLARBEAR_CROP,
				b -> b.parents(COW_CROP, PANDA_CROP, 0.1F)
						.temperature(TemperatureTolerance.EXTREME_COLD));
		addEntryWithSeeds(PRISMARINE_CROP,
				b -> b.parents(SOULSAND_CROP, WATER_CROP, 0.1F)
						.temperature(TemperatureTolerance.COLD));
		addEntryWithSeeds(QUARTZ_CROP,
				b -> b.parents(NETHERRACK_CROP, IRON_CROP, 0.1F)
						.temperature(TemperatureTolerance.EXTREME_WARM));
		addEntryWithSeeds(RABBIT_CROP,
				b -> b.parents(CARROT_CROP, CHICKEN_CROP, 0.1F));
		addEntryWithSeeds(REDSTONE_CROP,
				b -> b.parents(GOLD_CROP, GLOWSTONE_CROP, 0.1F)
						.soil(() -> Blocks.REDSTONE_ORE));
		addEntryWithSeeds(RED_TULIP_CROP,
				b -> b.seed(() -> RED_TULIP)
						.drop(() -> ModItems.COLOR_PARTICLES.get(), 1, 4)
						.drop(() -> RED_TULIP, 0, 4));
		addEntryWithSeeds(SAND_CROP,
				b -> b.parents(MUSHROOM_CROP, CACTUS_CROP, 0.1F));
		addEntryWithSeeds(SHEEP_CROP,
				b -> b.parents(POTATO_CROP, WHEAT_CROP, 0.1F));
		addEntryWithSeeds(SHULKER_CROP,
				b -> b.parents(BLAZE_CROP, ENDSTONE_CROP, 0.1F)
						.temperature(TemperatureTolerance.COLD));
		addEntryWithSeeds(SKELETON_CROP,
				b -> b.parents(PIG_CROP, COW_CROP, 0.1F));
		addEntryWithSeeds(SLIME_CROP,
				b -> b.parents(CREEPER_CROP, ZOMBIE_CROP, 0.1F));
		addEntryWithSeeds(SNOW_CROP,
				b -> b.parents(POLARBEAR_CROP, WATER_CROP, 0.1F)
						.temperature(TemperatureTolerance.EXTREME_COLD));
		addEntryWithSeeds(SOULSAND_CROP,
				b -> b.parents(SAND_CROP, NETHERRACK_CROP, 0.1F)
						.temperature(TemperatureTolerance.EXTREME_WARM));
		addEntryWithSeeds(SPIDER_CROP,
				b -> b.parents(SHEEP_CROP, SQUID_CROP, 0.1F));
		addEntryWithSeeds(SPONGE_CROP,
				b -> b.parents(MYCELIUM_CROP, GUARDIAN_CROP, 0.1F)
						.temperature(TemperatureTolerance.COLD));
		addEntryWithSeeds(SQUID_CROP,
				b -> b.parents(NETHER_WART_CROP, VINE_CROP, 0.1F)
						.temperature(TemperatureTolerance.COLD));
		addEntryWithSeeds(STONE_CROP,
				b -> b.parents(WHEAT_CROP, MUSHROOM_CROP, 0.1F));
		addEntryWithSeeds(STRAY_CROP,
				b -> b.parents(SKELETON_CROP, SNOW_CROP, 0.1F)
						.temperature(TemperatureTolerance.COLD));
		addEntryWithSeeds(SUGARCANE_CROP,
				b -> b.seed(() -> SUGAR_CANE)
						.drop(() -> SUGAR_CANE, 0, 4));
		addEntryWithSeeds(SWEET_BERRIES_CROP,
				b -> b.seed(() -> SWEET_BERRIES)
						.drop(() -> SWEET_BERRIES, 0, 4));
		addEntryWithSeeds(TURTLE_CROP,
				b -> b.parents(SQUID_CROP, FISH_CROP, 0.1F));
		addEntryWithSeeds(VILLAGER_CROP,
				b -> b.parents(POLARBEAR_CROP, TURTLE_CROP, 0.1F));
		addEntryWithSeeds(VINE_CROP,
				b -> b.seed(() -> VINE)
						.drop(() -> VINE, 0, 4));
		addEntryWithSeeds(WATER_CROP,
				b -> b.parents(VINE_CROP, SUGARCANE_CROP, 0.1F));
		addEntryWithSeeds(WARDEN_CROP,
				b -> b.parents(BEAST_CROP, WITHER_CROP, 0.1F)
						.temperature(TemperatureTolerance.COLD)
						.soil(() -> Blocks.SCULK));
		addEntryWithSeeds(WHEAT_CROP,
				b -> b.seed(() -> WHEAT_SEEDS)
						.drop(() -> WHEAT, 0, 4));
		addEntryWithSeeds(WHITE_TULIP_CROP,
				b -> b.seed(() -> WHITE_TULIP)
						.drop(() -> ModItems.COLOR_PARTICLES.get(), 1, 4)
						.drop(() -> WHITE_TULIP, 0, 4));
		addEntryWithSeeds(WITCH_CROP,
				b -> b.parents(VILLAGER_CROP, SOULSAND_CROP, 0.1F));
		addEntryWithSeeds(WITHER_CROP,
				b -> b.parents(GHAST_CROP, ZOMBIE_VILLAGER_CROP, 0.1F)
						.soil(() -> Blocks.BEDROCK));
		addEntryWithSeeds(WITHER_ROSE_CROP,
				b -> b.seed(() -> WITHER_ROSE)
						.drop(() -> WITHER_ROSE, 0, 1));
		addEntryWithSeeds(WITHER_SKELETON_CROP,
				b -> b.parents(SKELETON_CROP, NETHERRACK_CROP, 0.1F)
						.temperature(TemperatureTolerance.EXTREME_WARM));
		addEntryWithSeeds(WOOD_CROP,
				b -> b.parents(DIRT_CROP, SAND_CROP, 0.1F));
		addEntryWithSeeds(ZOMBIE_CROP,
				b -> b.parents(CHICKEN_CROP, SHEEP_CROP, 0.1F));
		addEntryWithSeeds(ZOMBIE_PIGMAN_CROP,
				b -> b.parents(ZOMBIE_CROP, NETHER_WART_CROP, 0.1F)
						.temperature(TemperatureTolerance.EXTREME_WARM));
		addEntryWithSeeds(ZOMBIE_VILLAGER_CROP,
				b -> b.parents(ZOMBIE_CROP, VILLAGER_CROP, 0.1F));

		empty(ABYSSALNITE_CROP);
		empty(ADAMANTINE_CROP);
		empty(ALUMINUM_CROP);
		empty(ALUMINUM_BRASS_CROP);
		empty(ALUMITE_CROP);
		empty(AMBER_CROP);
		empty(APATITE_CROP);
		empty(AQUAMARINE_CROP);
		empty(ARDITE_CROP);
		empty(AWAKENED_DRACONIUM_CROP);
		empty(BASALT_CROP);
		empty(BLACK_QUARTZ_CROP);
		empty(BLITZ_CROP);
		empty(BLIZZ_CROP);
		empty(BLUE_TOPAZ_CROP);
		empty(BRASS_CROP);
		empty(BRONZE_CROP);
		empty(CERTUS_QUARTZ_CROP);
		empty(CHIMERITE_CROP);
		empty(CHROME_CROP);
		empty(COBALT_CROP);
		empty(COLD_IRON_CROP);
		empty(COMPRESSED_IRON_CROP);
		empty(CONDUCTIVE_IRON_CROP);
		empty(CONSTANTAN_CROP);
		//		addDisabledEntry(COPPER_CROP);
		empty(CORALIUM_CROP);
		empty(DARK_GEM_CROP);
		empty(DARK_STEEL_CROP);
		empty(DESH_CROP);
		empty(DRACONIUM_CROP);
		empty(DREADIUM_CROP);
		empty(ELECTRICAL_STEEL_CROP);
		empty(ELECTROTINE_CROP);
		empty(ELECTRUM_CROP);
		empty(ELEMENTIUM_CROP);
		empty(END_STEEL_CROP);
		empty(ENDER_AMETHYST_CROP);
		empty(ENDER_BIOTITE_CROP);
		empty(ENDERIUM_CROP);
		empty(ENERGETIC_ALLOY_CROP);
		empty(FLUIX_CRYSTAL_CROP);
		empty(FLUXED_ELECTRUM_CROP);
		empty(GLOWSTONE_INGOT_CROP);
		empty(GRAPHITE_CROP);
		empty(INVAR_CROP);
		empty(IRIDIUM_CROP);
		empty(KNIGHTSLIME_CROP);
		empty(LEAD_CROP);
		empty(LITHIUM_CROP);
		empty(LUMIUM_CROP);
		empty(MAGNESIUM_CROP);
		empty(MALACHITE_CROP);
		empty(MANASTEEL_CROP);
		empty(MANYULLYN_CROP);
		empty(METEORIC_IRON_CROP);
		empty(MITHRIL_CROP);
		empty(MOONSTONE_CROP);
		empty(NEUTRONIUM_CROP);
		empty(NICKEL_CROP);
		empty(OCTINE_CROP);
		empty(OSMIUM_CROP);
		empty(PERIDOT_CROP);
		empty(PLATINUM_CROP);
		empty(PULSATING_IRON_CROP);
		empty(QUICKSILVER_CROP);
		empty(REDSTONE_ALLOY_CROP);
		empty(REFINED_OBSIDIAN_CROP);
		empty(ROCK_CRYSTAL_CROP);
		empty(RUBBER_CROP);
		empty(RUBY_CROP);
		empty(SALTPETER_CROP);
		empty(SIGNALUM_CROP);
		empty(SILICON_CROP);
		empty(SILVER_CROP);
		empty(SKY_STONE_CROP);
		empty(SLATE_CROP);
		empty(SLIMY_BONE_CROP);
		empty(SOULARIUM_CROP);
		empty(SAPPHIRE_CROP);
		empty(STAR_STEEL_CROP);
		empty(STARMETAL_CROP);
		empty(STEEL_CROP);
		empty(SULFUR_CROP);
		empty(SUNSTONE_CROP);
		empty(SYRMORITE_CROP);
		empty(TANZANITE_CROP);
		empty(TERRASTEEL_CROP);
		empty(THAUMIUM_CROP);
		empty(TIN_CROP);
		empty(TITANIUM_CROP);
		empty(TOPAZ_CROP);
		empty(TUNGSTEN_CROP);
		empty(URANIUM_CROP);
		empty(VALONITE_CROP);
		empty(VIBRANT_ALLOY_CROP);
		empty(VINTEUM_CROP);
		empty(VOID_METAL_CROP);
		empty(YELLORIUM_CROP);
		empty(ZINC_CROP);

		return data;
	}

	private void empty(CropTypes crop)
	{
		addEntryWithSeeds(crop, (builder) -> crop.getType().tagKeys(crop.getName()).forEach(builder::seed));
		crop.getConfig().setEnabled(false);
	}

	private void addEntryWithSeeds(CropTypes crop, Consumer<CropConfiguration.Builder> modifier)
	{
		crop.getConfig().setEnabled(true);
		CropConfiguration.Builder config = CropConfiguration.builder(crop.getConfig());
		modifier.accept(config);
		crop.getConfig().update(config.build());
		data.add(crop);
	}
}
