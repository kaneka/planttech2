package net.kaneka.planttech2.crops;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.JsonSyntaxException;
import net.kaneka.planttech2.PlantTechMain;
import net.kaneka.planttech2.enums.CropTraitsTypes;
import net.kaneka.planttech2.hashmaps.TraitsManager;
import net.kaneka.planttech2.registries.ModItems;
import net.kaneka.planttech2.utilities.PlantTechConstants;
import net.minecraft.Util;
import net.minecraft.core.HolderSet;
import net.minecraft.core.NonNullList;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.Tag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.Style;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.TagKey;
import net.minecraft.util.GsonHelper;
import net.minecraft.util.RandomSource;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.material.MapColor;
import net.minecraftforge.registries.RegistryObject;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.Nullable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static net.kaneka.planttech2.crops.CropTypes.ItemType.*;
import static net.minecraftforge.registries.ForgeRegistries.BLOCKS;

public enum CropTypes
{
	ABYSSALNITE_CROP("abyssalnite", false, INGOTS, 0x45005f),
	ADAMANTINE_CROP("adamantine", false, INGOTS, 0xd53c00),
	ALLIUM_CROP("allium", true, OTHER, 0xa65ee1, false),
	ALLAY_CROP("allay", true, OTHER, 0x09f2ff),
	ALUMINUM_CROP("aluminum", false, INGOTS, 0xb4b4b4),
	ALUMINUM_BRASS_CROP("aluminum_brass", false, INGOTS, 0xc8b300),
	ALUMITE_CROP("alumite", false, INGOTS, 0xe789ff),
	AMBER_CROP("amber", false, GEMS, 0xe09e00),
	AMETHYST_CROP("amethyst", true, GEMS, 0xc055ff),
	APATITE_CROP("apatite", false, GEMS, 0x00b3e0),
	AQUAMARINE_CROP("aquamarine", false, GEMS, 0x00c3d4),
	ARDITE_CROP("ardite", false, INGOTS, 0x88471b),
	AWAKENED_DRACONIUM_CROP("awakened_draconium", false, INGOTS, 0xbf4c00),
	AZURE_BLUET_CROP("azure_bluet", true, OTHER, 0xd6e8e8, false),
	BAMBOO_CROP("bamboo", true, OTHER, 0x5d8824, false),
	BASALT_CROP("basalt", false, INGOTS, 0x424242),
	BEAST_CROP("beast", true, OTHER, 0x6a6965),
	BEE_CROP("bee", true, OTHER, 0xffd700),
	BEETROOTS_CROP("beetroots", true, OTHER, 0xbf2529, false),
	BLACK_QUARTZ_CROP("black_quartz", false, GEMS, 0x202020),
	BLAZE_CROP("blaze", true, OTHER, 0xfc9600),
	BLITZ_CROP("blitz", false, OTHER, 0xfffdcc),
	BLIZZ_CROP("blizz", false, OTHER, 0xc3d3ff),
	BLUE_TOPAZ_CROP("blue_topaz", false, GEMS, 0x6089ff),
	BLUE_ORCHID_CROP("blue_orchid", true, OTHER, 0x3066ff, false),
	BRASS_CROP("brass", false, INGOTS, 0xeaeaea),
	BRONZE_CROP("bronze", false, INGOTS, 0x804500),
	CACTUS_CROP("cactus", true, OTHER, 0x527d26, false),
	CARROT_CROP("carrot", true, OTHER, 0xe38a1d, false),
	CERTUS_QUARTZ_CROP("certus_quartz", false, GEMS, 0x9fc3ff),
	CHICKEN_CROP("chicken", true, OTHER, 0xe2e2e2),
	CHIMERITE_CROP("chimerite", false, GEMS, 0xaeffa6),
	CHORUS_CROP("chorus", true, OTHER, 0x8f718f, false),
	CHROME_CROP("chrome", false, INGOTS, 0xFFFFFF),
	COAL_CROP("coal", true, GEMS, 0x3f3f3f),
	COBALT_CROP("cobalt", false, INGOTS, 0x1d5791),
	COCOA_BEAN_CROP("cocoa_bean", true, OTHER, 0xb97335, false),
	COLD_IRON_CROP("cold_iron", false, INGOTS, 0x72a7ff),
	COMPRESSED_IRON_CROP("compressed_iron", false, INGOTS, 0xbdbdbd),
	CONDUCTIVE_IRON_CROP("conductive_iron", false, INGOTS, 0x629dff),
	CONSTANTAN_CROP("constantan", false, INGOTS, 0xb1ab00),
	COPPER_CROP("copper", true, INGOTS, 0xb47800),
	CORALIUM_CROP("coralium", false, GEMS, 0x00646a),
	CORNFLOWER_CROP("cornflower", true, OTHER, 0x466aeb, false),
	COW_CROP("cow", true, OTHER, 0x443626),
	CREEPER_CROP("creeper", true, OTHER, 0x41b736),
	DANCIUM_CROP("dancium", true, INGOTS, 0xeb8c14),
	DANDELION_CROP("dandelion", true, OTHER, 0xfed639, false),
	DARK_GEM_CROP("dark_gem", false, GEMS, 0x4e4e4e),
	DARK_STEEL_CROP("dark_steel", false, INGOTS, 0x8d7aff),
	DESH_CROP("desh", false, INGOTS, 0x535353),
	DIAMOND_CROP("diamond", true, GEMS, 0x77cefb),
	DIRT_CROP("dirt", true, VANILLA_BLOCKS, 0x593d29),
	DRACONIUM_CROP("draconium", false, INGOTS, 0x7600ba),
	DREADIUM_CROP("dreadium", false, INGOTS, 0xba000d),
	DROWNED_CROP("drowned", true, OTHER, 0x8ff1d7),
	ELECTRICAL_STEEL_CROP("electrical_steel", false, INGOTS, 0xb8b8b8),
	ELECTROTINE_CROP("electrotine", false, DUSTS, 0x005093),
	ELECTRUM_CROP("electrum", false, INGOTS, 0xfff158),
	ELEMENTIUM_CROP("elementium", false, INGOTS, 0xeb11ff),
	EMERALD_CROP("emerald", true, GEMS, 0x17dd62),
	END_STEEL_CROP("end_steel", false, INGOTS, 0xfeffd6),
	ENDER_AMETHYST_CROP("ender_amethyst", false, GEMS, 0xfd35ff),
	ENDER_BIOTITE_CROP("ender_biotite", false, GEMS, 0x000000),
	ENDERDRAGON_CROP("enderdragon", false, OTHER, 0x181818),
	ENDERIUM_CROP("enderium", false, INGOTS, 0x007b77),
	ENDERMAN_CROP("enderman", true, OTHER, 0x181818),
	ENDSTONE_CROP("endstone", true, VANILLA_BLOCKS, 0xf6fabd),
	ENERGETIC_ALLOY_CROP("energetic_alloy", false, INGOTS, 0x9cff32),
	FISH_CROP("fish", true, OTHER, 0xbf841b),
	FLUIX_CRYSTAL_CROP("fluix_crystal", false, GEMS, 0x6f0098),
	FLUXED_ELECTRUM_CROP("fluxed_electrum", false, INGOTS, 0xfffb87),
	FROG_CROP("frog", true, OTHER, 0x4c834c),
	GHAST_CROP("ghast", true, OTHER, 0xf0f0f0),
	GLOWING_SQUID_CROP("glowing_squid", true, OTHER, 0x027859),
	GLOWSTONE_CROP("glowstone", true, VANILLA_BLOCKS, 0xfbda74),
	GLOWSTONE_INGOT_CROP("glowstone_ingot", false, INGOTS, 0xf6ed00),
	GLOW_BERRIES_CROP("glow_berries", false, OTHER, 0xffce43, false),
	GOAT_CROP("goat", true, OTHER, 0xebebeb),
	GOLD_CROP("gold", true, INGOTS, 0xf8af2b),
	GRAPHITE_CROP("graphite", false, INGOTS, 0x444444),
	GUARDIAN_CROP("guardian", true, OTHER, 0x668980),
	HUSK_CROP("husk", true, OTHER, 0x6a5d4a),
	ILLAGER_CROP("illager", true, OTHER, 0x939999),
	INVAR_CROP("invar", false, INGOTS, 0xc3bc00),
	IRIDIUM_CROP("iridium", false, INGOTS, 0xcfcfcf),
	IRON_CROP("iron", true, INGOTS, 0xbc9980),
	KANEKIUM_CROP("kanekium", true, INGOTS, 0x572e8a),
	KELP_CROP("kelp", true, OTHER, 0x5b8131, false),
	KINNOIUM_CROP("kinnoium", true, INGOTS, 0x246b2d),
	KNIGHTSLIME_CROP("knightslime", false, INGOTS, 0xfd5fff),
	LAPIS_CROP("lapis", true, GEMS, 0x1044ac),
	LAVA_CROP("lava", true, VANILLA_FLUIDS, 0xd14f0c),
	LEAD_CROP("lead", false, INGOTS, 0x326e99),
	LENTHURIUM_CROP("lenthurium", true, INGOTS, 0x2c8585),
	LILY_OF_THE_VALLEY_CROP("lily_of_the_valley", true, OTHER, 0xe7e7e7, false),
	LITHIUM_CROP("lithium", false, INGOTS, 0xfffac4),
	LUMIUM_CROP("lumium", false, INGOTS, 0xfff282),
	MAGMA_CUBE_CROP("magma_cube", true, VANILLA_BLOCKS, 0x330000),
	MAGNESIUM_CROP("magnesium", false, INGOTS, 0x615900),
	MALACHITE_CROP("malachite", false, GEMS, 0x36bf53),
	MANASTEEL_CROP("manasteel", false, INGOTS, 0x3d8fff),
	MANYULLYN_CROP("manyullyn", false, INGOTS, 0x793393),
	MELON_CROP("melon", true, OTHER, 0xa7ac1d, false),
	METEORIC_IRON_CROP("meteoric_iron", false, INGOTS, 0x8f845e),
	MITHRIL_CROP("mithril", false, INGOTS, 0xb7d7ff),
	MOONSTONE_CROP("moonstone", false, GEMS, 0xeef6ff),
	MOOSHROOM_CROP("mooshroom", true, OTHER, 0xa81012),
	MUSHROOM_CROP("mushroom", true, OTHER, 0xe21212, false),
	MYCELIUM_CROP("mycelium", false, VANILLA_BLOCKS, 0x736162),
	NETHER_WART_CROP("nether_wart", true, OTHER, 0x831c20, false),
	NETHERRACK_CROP("netherrack", true, VANILLA_BLOCKS, 0x652828),
	NETHERITE_CROP("netherite", true, INGOTS, 0x320000),
	NEUTRONIUM_CROP("neutronium", false, INGOTS, 0x585858),
	NICKEL_CROP("nickel", false, INGOTS, 0x9f998c),
	OCTINE_CROP("octine", false, INGOTS, 0xffb400),
	ORANGE_TULIP_CROP("orange_tulip", true, OTHER, 0xbd6a22, false),
	OSMIUM_CROP("osmium", false, INGOTS, 0xc6edff),
	OXEYE_DAISY_CROP("oxeye_daisy", true, OTHER, 0xf5ba27, false),
	PANDA_CROP("panda", true, OTHER, 0xf5ba27),
	PARROT_CROP("parrot", true, OTHER, 0x18bdff),
	PERIDOT_CROP("peridot", false, GEMS, 0x5fc859),
	PIG_CROP("pig", true, OTHER, 0xf19e98),
	PINK_TULIP_CROP("pink_tulip", true, OTHER, 0xe4aff4, false),
	PLANTIUM_CROP("plantium", true, OTHER, 0x35a048),
	PLATINUM_CROP("platinum", false, INGOTS, 0xa2a2a2),
	POLARBEAR_CROP("polarbear", true, GEMS, 0xf6f6f6),
	POPPY_CROP("poppy", true, OTHER, 0xed302c, false),
	POTATO_CROP("potato", true, OTHER, 0xc8a24b, false),
	PRISMARINE_CROP("prismarine", true, GEMS, 0x5ea496),
	PULSATING_IRON_CROP("pulsating_iron", false, INGOTS, 0x75d970),
	PUMPKIN_CROP("pumpkin", true, OTHER, 0xe38a1d, false),
	QUARTZ_CROP("quartz", true, GEMS, 0xd4caba),
	QUICKSILVER_CROP("quicksilver", false, GEMS, 0xd6ffff),
	RABBIT_CROP("rabbit", true, OTHER, 0x000000),
	RED_TULIP_CROP("red_tulip", true, OTHER, 0xed302c, false),
	REDSTONE_CROP("redstone", true, DUSTS, 0xff0000),
	REDSTONE_ALLOY_CROP("redstone_alloy", false, INGOTS, 0xff0000),
	REFINED_OBSIDIAN_CROP("refined_obsidian", false, INGOTS, 0x62316d),
	ROCK_CRYSTAL_CROP("rock_crystal", false, GEMS, 0xa6a6a6),
	RUBBER_CROP("rubber", false, OTHER, 0x444444),
	RUBY_CROP("ruby", false, GEMS, 0x980000),
	SALTPETER_CROP("saltpeter", false, DUSTS, 0x969696),
	SAND_CROP("sand", false, VANILLA_BLOCKS, 0xdacfa3),
	SAPPHIRE_CROP("sapphire", false, GEMS, 0x000a8e),
	SHEEP_CROP("sheep", true, OTHER, 0xc09e86),
	SHULKER_CROP("shulker", true, OTHER, 0x8e608e),
	SIGNALUM_CROP("signalum", false, INGOTS, 0x8e5700),
	SILICON_CROP("silicon", false, INGOTS, 0x777777),
	SILVER_CROP("silver", false, INGOTS, 0xdadada),
	SKELETON_CROP("skeleton", true, OTHER, 0xbcbcbc),
	SKY_STONE_CROP("sky_stone", false, MOD_BLOCKS, 0x383838),
	SLATE_CROP("slate", false, MOD_BLOCKS, 0xFFFFFF),
	SLIME_CROP("slime", true, OTHER, 0x59bd45),
	SLIMY_BONE_CROP("slimy_bone", false, MOD_ITEMS, 0x7b7b7b),
	SNOW_CROP("snow", true, VANILLA_BLOCKS, 0xffffff),
	SOULARIUM_CROP("soularium", false, INGOTS, 0x443824),
	SOULSAND_CROP("soulsand", true, VANILLA_BLOCKS, 0x49372c),
	SPIDER_CROP("spider", true, OTHER, 0x605448),
	SPONGE_CROP("sponge", true, VANILLA_BLOCKS, 0xcdce4a),
	SQUID_CROP("squid", true, OTHER, 0xcdce4a),
	STAR_STEEL_CROP("star_steel", false, INGOTS, 0x171717),
	STARMETAL_CROP("starmetal", false, INGOTS, 0x22002f),
	STEEL_CROP("steel", false, INGOTS, 0x686868),
	STONE_CROP("stone", true, VANILLA_BLOCKS, 0x616161),
	STRAY_CROP("stray", true, OTHER, 0xacbabd),
	SUGARCANE_CROP("sugarcane", true, OTHER, 0x82a859, false),
	SULFUR_CROP("sulfur", false, DUSTS, 0xb1ac27),
	SUNSTONE_CROP("sunstone", false, MOD_BLOCKS, 0xc13b00),
	SWEET_BERRIES_CROP("sweet_berries", true, OTHER, 0xe00000, false),
	SYRMORITE_CROP("syrmorite", false, INGOTS, 0xc71eff),
	TANZANITE_CROP("tanzanite", false, GEMS, 0xa951c6),
	TERRASTEEL_CROP("terrasteel", false, INGOTS, 0x32b100),
	THAUMIUM_CROP("thaumium", false, INGOTS, 0x8a00ff),
	TIN_CROP("tin", false, INGOTS, 0xaba78c),
	TITANIUM_CROP("titanium", false, INGOTS, 0xc6c6c6),
	TOPAZ_CROP("topaz", false, GEMS, 0xffde29),
	TUNGSTEN_CROP("tungsten", false, INGOTS, 0x005a40),
	TURTLE_CROP("turtle", true, OTHER, 0x388d3a),
	URANIUM_CROP("uranium", false, INGOTS, 0x3abd22),
	VALONITE_CROP("valonite", false, GEMS, 0xcfa5d5),
	VIBRANT_ALLOY_CROP("vibrant_alloy", false, INGOTS, 0xbf7e00),
	VILLAGER_CROP("villager", true, OTHER, 0xb57b67),
	VINE_CROP("vine", true, VANILLA_BLOCKS, 0x1b5011, false),
	VINTEUM_CROP("vinteum", false, DUSTS, 0x5a81ff),
	VOID_METAL_CROP("void_metal", false, INGOTS, 0x000000),
	WARDEN_CROP("warden", true, OTHER, 0x394454),
	WATER_CROP("water", true, VANILLA_FLUIDS, 0x2b5fff),
	WHEAT_CROP("wheat", true, OTHER, 0xae19, false),
	WHITE_TULIP_CROP("white_tulip", true, OTHER, 0xf7f7f7, false),
	WITCH_CROP("witch", true, OTHER, 0xa39483),
	WITHER_CROP("wither", true, OTHER, 0x343434),
	WITHER_ROSE_CROP("wither_rose", true, OTHER, 0x000000, false),
	WITHER_SKELETON_CROP("wither_skeleton", true, OTHER, 0x515353),
	WOOD_CROP("wood", true, VANILLA_BLOCKS, 0x605e54),
	YELLORIUM_CROP("yellorium", false, INGOTS, 0xfffc00),
	ZINC_CROP("zinc", false, INGOTS, 0xb8b78b),
	ZOMBIE_CROP("zombie", true, OTHER, 0x71955b),
	ZOMBIE_PIGMAN_CROP("zombie_pigman", true, OTHER, 0xeea5a4),
	ZOMBIE_VILLAGER_CROP("zombie_villager", true, OTHER, 0x3b622f);

	private final String name;
	private final boolean vanilla;
	private final ItemType type;
	private final String displayName;
	private final int seedColor;
	private final Style style;
	private final boolean hasParticle;
	private final Supplier<Item> particle;
	private final CropConfiguration config;
	private RegistryObject<Item> seed;
	private RegistryObject<Block> cropBlock;

	public static final Logger LOGGER = LogManager.getLogger();
	public static final Map<CropTypes, RegistryObject<Item>> SEEDS = new HashMap<>();
	public static final Map<CropTypes, RegistryObject<Block>> CROP_BLOCKS = new HashMap<>();
	public static CropTypes DEFAULT;

	CropTypes(String name, boolean vanilla, ItemType type, int seedColor)
	{
		this(name, vanilla, type, seedColor, true);
	}

	CropTypes(String name, boolean vanilla, ItemType type, int seedColor, boolean particle)
	{
		this.name = name;
		this.vanilla = vanilla;
		this.type = type;
		StringBuilder nameBuilder = new StringBuilder(name.length());
		String[] s = name.split("_");
		for (String n : s)
		{
			n = StringUtils.capitalize(n.toLowerCase());
			if (nameBuilder.length() != 0)
				nameBuilder.append(" ");
			nameBuilder.append(n);
		}
		displayName = nameBuilder.toString();
		this.seedColor = seedColor;
		this.style = Style.EMPTY.withColor(seedColor);
		this.hasParticle = particle;
		this.particle = () -> (particle ? ModItems.PARTICLES.get(name).get() : Items.AIR);
		CropConfiguration.Builder builder = CropConfiguration.builder(DropEntry.of(this::getSeed, 1, 4));
		if (particle)
			builder.drop(() -> ModItems.PARTICLES.get(name).get(), 0, 8);
		config = builder.build();
	}

	public void setSeed(RegistryObject<Item> seed)
	{
		SEEDS.put(this, seed);
		this.seed = seed;
	}

	public void setCropBlock(RegistryObject<Block> cropBlock)
	{
		CROP_BLOCKS.put(this, cropBlock);
		this.cropBlock = cropBlock;
	}

	public Item getSeed()
	{
		return seed.get();
	}

	public Component getDisplaySoilName()
	{
		Block block = getConfig().getSoil().get();
		return block.getName().copy().withStyle(Style.EMPTY.withColor(block.defaultMapColor().calculateRGBColor(MapColor.Brightness.NORMAL)));
	}

	public Block getCropBlock()
	{
		return cropBlock.get();
	}

	public ItemType getType()
	{
		return type;
	}


	public boolean isSeed(final ItemStack stack)
	{
		CropConfiguration config = getConfig();
		return config.getPrimarySeed().getItem().get() == stack.getItem() || config.getSeeds().stream().anyMatch((sup) -> sup.get().test(stack));
	}

	public List<ItemStack> calculateDrops(List<ItemStack> drops, TraitsManager cropTrait, int growstate, RandomSource rand)
	{
		CropConfiguration config = getConfig();
		ItemStack seed = config.getPrimarySeed().getDroppedStack(cropTrait.getTrait(CropTraitsTypes.FERTILITY), CropTraitsTypes.FERTILITY.getMax(), rand);
		if (!seed.isEmpty())
		{
			if (growstate < 7)
				seed.setCount(1);
			drops.add(TraitsManager.ItemImpl.of(seed, cropTrait.serializeNBT()).stack);
		}
		if (growstate > 6)
		{
			List<DropEntry> dropEntries = config.getDrops();
			for (int i = 1; i < dropEntries.size(); i++)
			{
				DropEntry drop = dropEntries.get(i);
				ItemStack addDrop = drop.getDroppedStack(cropTrait.getTrait(CropTraitsTypes.PRODUCTIVITY), CropTraitsTypes.PRODUCTIVITY.getMax(), rand);
				if (!addDrop.isEmpty())
					drops.add(addDrop);
			}
		}
		return drops;
	}

	public NonNullList<ItemStack> calculateDropsReduced(NonNullList<ItemStack> drops, TraitsManager cropTrait, int growstate, RandomSource rand)
	{
		CropConfiguration config = getConfig();
		List<DropEntry> dropEntries = config.getDrops();
		if (growstate > 6)
		{
			ItemStack seed = config.getPrimarySeed().getDroppedStack(cropTrait.getTrait(CropTraitsTypes.FERTILITY), CropTraitsTypes.FERTILITY.getMax(), rand);
			if (!seed.isEmpty() && seed.getCount() > 1)
			{
				seed.shrink(1);
				drops.add(TraitsManager.ItemImpl.of(seed, cropTrait.serializeNBT()).stack);
			}
			for (DropEntry drop : dropEntries)
			{
				ItemStack stack = drop.getDroppedStack(cropTrait.getTrait(CropTraitsTypes.PRODUCTIVITY), CropTraitsTypes.PRODUCTIVITY.getMax(), rand);
				if (!stack.isEmpty())
					drops.add(stack);
			}
		}
		return drops;
	}

	public boolean isChild(CropTypes parent1, CropTypes parent2)
	{
		return getConfig().getParents().stream().anyMatch((parent) -> parent.test(parent1, parent2));
	}

	public Item getParticle()
	{
		return particle.get();
	}

	public int getSeedColor()
	{
		return seedColor;
	}

	public boolean hasParticle()
	{
		return hasParticle;
	}

	public CropConfiguration getConfig()
	{
		return config;
	}

	public String getName()
	{
		return name;
	}

	public Component getDisplayName()
	{
		return Component.translatable(translationKey()).withStyle(style);
	}

	public String translationKey()
	{
		return "planttech2.crop." + name;
	}

	public static CropTypes fromName(String name)
	{
		return Arrays.stream(values()).filter((crop) -> crop.name.equals(name)).findFirst().orElse(DEFAULT);
	}

	@Nullable
	public static CropTypes fromNameNullable(String name)
	{
		return Arrays.stream(values()).filter((crop) -> crop.name.equals(name)).findFirst().orElse(null);
	}

	public static CropTypes getDefault()
	{
		return DEFAULT;
	}

	public static void extraToNetwork(FriendlyByteBuf buf)
	{
		try
		{
			for (CropTypes crop : values())
				crop.getConfig().extraToNetwork(buf);
		}
		catch (Exception e)
		{
			PlantTechMain.LOGGER.error("Crop list is broken, cannot write the data");
			e.printStackTrace();
		}
	}

	public static CompoundTag serializeNBT()
	{
		CompoundTag compound = new CompoundTag();
		try
		{
			ListTag list = new ListTag();
			for (CropTypes crop : values())
			{
				CompoundTag tag = crop.getConfig().serializeNBT();
				tag.putString("name", crop.name);
				list.add(tag);
			}
			compound.put("crops", list);
		}
		catch (Exception e)
		{
			PlantTechMain.LOGGER.error("Crop list is broken, cannot write the data");
			e.printStackTrace();
			return new CompoundTag();
		}
		return compound;
	}

	public static void extraFromNetwork(FriendlyByteBuf buf)
	{
		try
		{
			for (CropTypes crop : values())
				crop.getConfig().extraFromNetwork(buf);
		}
		catch (Exception e)
		{
			PlantTechMain.LOGGER.error("Crop list is broken, cannot write the data");
			e.printStackTrace();
		}
	}

	public static void deserializeNBT(CompoundTag compound)
	{
		try
		{
			ListTag crops = compound.getList("crops", Tag.TAG_COMPOUND);
			for (int i = 0; i < crops.size(); i++)
			{
				CompoundTag data = crops.getCompound(i);
				fromName(data.getString("name")).getConfig().deserializeNBT(data);
			}
		}
		catch (Exception e)
		{
			PlantTechMain.LOGGER.error("Received incomplete crop list, cannot deserialize the data");
			e.printStackTrace();
		}
	}

	public static int getEnabledLength()
	{
		return crops(false).size();
	}

	public static Optional<CropTypes> getBySeed(ItemStack item)
	{
		return crops().stream().filter((crop) -> crop.isSeed(item)).findFirst();
	}

	public static List<CropTypes> getByParents(CropTypes parent, CropTypes parent2)
	{
		return crops().stream().filter((crop) -> crop.isChild(parent, parent2)).collect(Collectors.toList());
	}

	public static List<CropTypes> crops(boolean includeDisabled)
	{
		return crops().stream().filter((entry) -> includeDisabled || entry.shouldBeInGame()).sorted().collect(Collectors.toList());
	}

	public static List<CropTypes> crops()
	{
		return Arrays.stream(CropTypes.values()).sorted().toList();
	}

	public static List<CropConfiguration> configs()
	{
		return crops().stream().map(CropTypes::getConfig).collect(Collectors.toList());
	}

	private static final HashSet<String> WARNED_DEAD_END = new HashSet<>();

	public static List<Map<Integer, CropTypes>> getFamilyTree(CropTypes crop)
	{
		ArrayList<Map<Integer, CropTypes>> tree = new ArrayList<>();
		Map<Integer, CropTypes> map = new HashMap<>();
		Map<Integer, CropTypes> mapNext = new HashMap<>();
		map.put(0, crop);
		tree.add(map);

		boolean next = true;
		int counter = 0;
		while (next)
		{
			next = false;
			for (Map.Entry<Integer, CropTypes> mapEntry: map.entrySet())
			{
				int position = mapEntry.getKey();
				CropTypes currentEntry = mapEntry.getValue();
				List<ParentPair> parents = currentEntry.getConfig().getParents();
				if (parents.size() > 0)
				{
					mapNext.put(2 * position, parents.get(0).getFirstParent());
					mapNext.put(2 * position + 1, parents.get(0).getSecondParent());
					next = true;
				}
				else if(currentEntry.getConfig().getSeeds().size() <= 1)
				{
					String name = currentEntry.getName();
					if (!WARNED_DEAD_END.contains(name))
					{
						LOGGER.info("Dead end in " + name);
						WARNED_DEAD_END.add(name);
					}
				}
			}
			counter++;
			if (next && counter < 20)
			{
				tree.add(mapNext);
				map = mapNext;
				mapNext = new HashMap<>();
			}
			else
			{
				if (counter >= 20)
					LOGGER.info("Loophole in croplist");
				break;
			}
		}
		return tree;
	}

	public static class Serializer implements JsonSerializer<CropTypes>, JsonDeserializer<CropTypes>
	{
		public static final Serializer INSTANCE = new Serializer();

		private Serializer()
		{
		}

		@Override
		public JsonElement serialize(CropTypes crop, Type typeOfSrc, JsonSerializationContext context)
		{
			JsonObject obj = new JsonObject();
			CropConfiguration config = crop.getConfig();

			obj.addProperty("crop", crop.getName());

			// Moved from individual crop to global index list
//			obj.addProperty("enabled", config.isEnabled());

			JsonObject temp = new JsonObject();
			temp.addProperty("minTemperatureTolerance", config.getTemperature().getMinTempTolerance());
			temp.addProperty("maxTemperatureTolerance", config.getTemperature().getMaxTempTolerance());
			obj.add("temperature", temp);

			obj.add("primary_seed", context.serialize(config.getPrimarySeed(), DropEntry.class));

			JsonArray seeds = new JsonArray();
			config.getSeeds().stream()
					.map(Supplier::get)
					.map(Ingredient::toJson)
					.sorted(Comparator.comparing(JsonElement::toString))
					.forEach(seeds::add);
//
			obj.add("seeds", seeds);

			JsonArray drops = new JsonArray();
			config.getDrops().stream()
					.map(entry -> context.serialize(entry, DropEntry.class))
					.forEach(drops::add);
			obj.add("drops", drops);

			JsonArray parents = new JsonArray();
			config.getParents().stream()
					.map(pair -> context.serialize(pair, ParentPair.class))
					.forEach(parents::add);
			obj.add("parents", parents);

			JsonObject soil = new JsonObject();
			soil.addProperty("block", BLOCKS.getKey(config.getSoil().get()).toString());
			obj.add("soil", soil);

			return obj;
		}

		@Override
		public CropTypes deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException
		{
			JsonObject obj = json.getAsJsonObject();

			CropTypes crop = CropTypes.fromName(obj.get("crop").getAsString());
			crop.getConfig().update(
					// Moved from individual crop to global index list
//					obj.get("enabled").getAsBoolean(),
					getTemperature(obj.getAsJsonObject("temperature")),
					context.deserialize(obj.get("primary_seed"), DropEntry.class),
					getSeeds(obj.get("seeds")),
					getDropEntries(obj.get("drops"), context),
					getParentPairs(obj.get("parents"), context),
					getSoil(obj.get("soil"))
			);
			return crop;
		}

		@Deprecated
		public void write(CropTypes crop, FriendlyByteBuf buf)
		{
			CropConfiguration config = crop.getConfig();
			buf.writeUtf(crop.getName(), 64);
			buf.writeBoolean(config.isEnabled());
			buf.writeNbt(config.getTemperature().serializeNBT());

			DropEntry.Serializer.INSTANCE.write(config.getPrimarySeed(), buf);
			buf.writeShort(config.getSeeds().size());
			config.getSeeds().stream().map(Supplier::get).filter(Objects::nonNull)
					.forEach((e) -> e.toNetwork(buf));

			buf.writeShort(config.getParents().size());
			config.getDrops().forEach(drop -> DropEntry.Serializer.INSTANCE.write(drop, buf));

			buf.writeShort(config.getParents().size());
			config.getParents().forEach(pair -> ParentPair.Serializer.INSTANCE.write(pair, buf));

			buf.writeResourceLocation(BLOCKS.getKey(config.getSoil().get()));
		}

		@Deprecated
		public CropTypes read(FriendlyByteBuf buf)
		{
			String name = buf.readUtf(64);
			boolean enabled = buf.readBoolean();
			TemperatureTolerance temperature = TemperatureTolerance.fromNBT(buf.readNbt());

			DropEntry primarySeed = DropEntry.Serializer.INSTANCE.read(buf);

			List<Supplier<Ingredient>> seeds = new LinkedList<>();
			short seedsAmount = buf.readShort();
			for (int i = 0; i < seedsAmount; i++)
				seeds.add(() -> Ingredient.fromNetwork(buf));

			short dropsAmount = buf.readShort();
			List<DropEntry> drops = new LinkedList<>();
			for (int i = 0; i < dropsAmount; i++)
				drops.add(DropEntry.Serializer.INSTANCE.read(buf));

			List<ParentPair> parentPairs = new LinkedList<>();
			short parentsAmount = buf.readShort();
			for (int i = 0; i < parentsAmount; i++)
				parentPairs.add(ParentPair.Serializer.INSTANCE.read(buf));

			Supplier<Block> soil = LazyRegistryEntry.of(buf.readResourceLocation(), BLOCKS);

			CropTypes crop = CropTypes.fromName(name);
			crop.getConfig().setEnabled(enabled);
			crop.getConfig().update(
//					enabled,
					temperature,
					primarySeed,
					seeds,
					drops,
					parentPairs,
					soil
			);
			return crop;
		}

		private TemperatureTolerance getTemperature(JsonObject obj)
		{
			return TemperatureTolerance.of(GsonHelper.convertToFloat(obj.get("minTemperatureTolerance"), "minTemperatureTolerance"), GsonHelper.convertToFloat(obj.get("maxTemperatureTolerance"), "maxTemperatureTolerance"));
		}

		private List<DropEntry> getDropEntries(JsonElement element, JsonDeserializationContext context)
		{
			return getEntries(element, context, DropEntry.class);
		}

		private List<ParentPair> getParentPairs(JsonElement element, JsonDeserializationContext context)
		{
			return getEntries(element, context, ParentPair.class);
		}

		private <T> List<T> getEntries(JsonElement element, JsonDeserializationContext context, Class<T> clazz)
		{
			List<T> list = new ArrayList<>();
			if (element.isJsonPrimitive() || element.isJsonObject())
				list.add(context.deserialize(element, clazz));
			else if (element.isJsonArray())
				element.getAsJsonArray().forEach((el) -> list.add(context.deserialize(el, clazz)));
			else throw new JsonSyntaxException("Expected " + clazz.getName() + " to be a JsonObject or JsonArray, was " + GsonHelper.getType(element));
			return list;
		}

		private List<Supplier<Ingredient>> getSeeds(JsonElement json)
		{
			List<JsonElement> elements = StreamSupport.stream(GsonHelper.convertToJsonArray(json, "seeds").spliterator(), false).collect(Collectors.toList());
			return Util.make(new ArrayList<>(), (list) -> {
				for (JsonElement element : elements)
					list.add(() -> {
						try
						{
							return Ingredient.fromJson(element);
						}
						catch (Exception e)
						{
							return Ingredient.EMPTY;
						}
					});
			});
//			return StreamSupport.stream(GsonHelper.convertToJsonArray(element, "seeds").spliterator(), false)
//					.map(JsonElement::getAsString)
//					.map(ResourceLocation::new)
//					.map(loc -> RegistryEntrySupplier.of(loc, ITEMS))
//					.collect(Collectors.toList());
		}

		private Supplier<Block> getSoil(JsonElement element)
		{
			if (element.isJsonPrimitive())
				return () -> BLOCKS.getValue(new ResourceLocation(GsonHelper.convertToString(element, "soil")));
			if (element.isJsonObject())
				return () -> BLOCKS.getValue(new ResourceLocation(GsonHelper.getAsString(element.getAsJsonObject(), "block")));
			throw new JsonSyntaxException("Expected soil to be a string or JsonObject, was " + GsonHelper.getType(element));
		}
	}

	public boolean shouldBeInGame()
	{
		return getConfig().shouldBeInGame() && (vanilla || getType().tagKeys(getName()).stream().anyMatch((key) -> {
			Optional<HolderSet.Named<Item>> tag = BuiltInRegistries.ITEM.getTag(key);
			return tag.isPresent() && tag.get().size() > 0;
		}));
	}

	public enum ItemType
	{
		ORES(0, 1, "ores", true),
		DUSTS(1, 1, "dusts", true),
		NUGGETS(2, 1, "nuggets", true),
		GEMS(3, 1, "gems", true) {
			@Override
			public TagKey<Item> alternateTagKey(String item)
			{
				return ItemTags.create(new ResourceLocation(INGOTS.getRel() + item));
			}
		},
		INGOTS(4, 1, "ingots", true) {
			@Override
			public TagKey<Item> alternateTagKey(String item)
			{
				return ItemTags.create(new ResourceLocation(GEMS.getRel() + item));
			}
		},
		STORAGE_BLOCKS(5, 1, "storage_blocks", true),
		VANILLA_BLOCKS(6, 0, null, false),
		MOD_BLOCKS(7, 1, null, false),
		VANILLA_ITEMS(8, 0, null, false),
		MOD_ITEMS(9, 1, null, false),
		VANILLA_FLUIDS(10, 0, null, false),
		MOD_FLUIDS(11, 1, null, false),
		OTHER(12, 0, null, false),
		;
		private final int index;
		@Nullable
		private final String id;
		@Nullable
		private final String name;
		@Nullable
		private final String rel;
		private final boolean hasDefaultProduct;
		ItemType(int index, int mod, @Nullable String name, boolean hasDefaultProduct)
		{
			this.index = index;
			this.id = mod == 0 ? PlantTechConstants.MINECRAFT_ID : PlantTechConstants.FORGE_ID;
			this.name = name;
			this.rel = name == null ? id + ":" : id + ":" + name + "/";
			this.hasDefaultProduct = hasDefaultProduct;
		}

		public boolean hasDefaultProduct()
		{
			return hasDefaultProduct;
		}

		@Nullable
		public String getRel()
		{
			return rel;
		}

		public int getIndex()
		{
			return index;
		}

		public static ItemType fromIndex(int index)
		{
			return Arrays.stream(values()).filter((type) -> type.getIndex() == index).findFirst().orElse(OTHER);
		}

		public Set<TagKey<Item>> tagKeys(String item)
		{
			TagKey<Item> tag = ItemTags.create(new ResourceLocation(rel + item));
			return alternates() ? Set.of(tag, alternateTagKey(item)) : Collections.singleton(tag);
		}

		public TagKey<Item> alternateTagKey(String item)
		{
			throw new NullPointerException();
		}

		public boolean alternates()
		{
			return this == INGOTS || this == GEMS;
		}
	}
}