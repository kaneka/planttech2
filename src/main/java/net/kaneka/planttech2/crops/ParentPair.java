package net.kaneka.planttech2.crops;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import net.kaneka.planttech2.addons.Pair;
import net.kaneka.planttech2.utilities.ISerializable;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.util.GsonHelper;

import java.lang.reflect.Type;
import java.util.function.BiPredicate;

public class ParentPair extends Pair<String, String> implements BiPredicate<CropTypes, CropTypes>, ISerializable
{
    private final float mutationChance;

    public static ParentPair of(CompoundTag compound)
    {
        return new ParentPair(compound.getString("parent1"), compound.getString("parent2"), compound.getFloat("mutation"));
    }

    public static ParentPair of(String firstParent, String secondParent, float mutationChance)
    {
        return new ParentPair(firstParent, secondParent, mutationChance);
    }

    private ParentPair(String parent1, String parent2, float mutationChance)
    {
        super(parent1, parent2);
        this.mutationChance = mutationChance;
    }

    @Override
    public CompoundTag serializeNBT()
    {
        CompoundTag compound = new CompoundTag();
        compound.putString("parent1", getA());
        compound.putString("parent2", getB());
        compound.putFloat("mutation", mutationChance);
        return compound;
    }

    @Override
    public boolean test(CropTypes parent1, CropTypes parent2)
    {
        return testExact(parent1, parent2) || testExact(parent2, parent1);
    }

    private boolean testExact(CropTypes parent1, CropTypes parent2)
    {
        return getFirstParent() == parent1 && getSecondParent() == parent2;
    }

    public CropTypes getFirstParent()
    {
        return CropTypes.fromName(getA());
    }

    public CropTypes getSecondParent()
    {
        return CropTypes.fromName(getB());
    }

    public float getMutationChance()
    {
        return mutationChance;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        if (!super.equals(o))
        {
            return false;
        }

        ParentPair that = (ParentPair) o;

        return super.equals(o) && Float.compare(that.mutationChance, mutationChance) == 0;
    }

    @Override
    public String toString()
    {
        return "ParentPair{" +
                super.toString() + ", " +
                "mutationChance=" + mutationChance +
                '}';
    }

    public static class Serializer implements JsonSerializer<ParentPair>, JsonDeserializer<ParentPair>
    {
        public static final Serializer INSTANCE = new Serializer();

        private Serializer() {}

        @Override
        public JsonElement serialize(ParentPair src, Type typeOfSrc, JsonSerializationContext context)
        {
            JsonObject obj = new JsonObject();
            obj.addProperty("parent_1", src.getA());
            obj.addProperty("parent_2", src.getB());
            obj.addProperty("chance", src.getMutationChance());
            return obj;
        }

        @Override
        public ParentPair deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException
        {
            JsonObject obj = json.getAsJsonObject();
            String parent1 = GsonHelper.getAsString(obj, "parent_1");
            String parent2 = GsonHelper.getAsString(obj, "parent_2");
            float mutateChance = GsonHelper.getAsFloat(obj, "chance");
            return new ParentPair(parent1, parent2, mutateChance);
        }

        public ParentPair read(FriendlyByteBuf buf)
        {
            String parent1 = buf.readUtf();
            String parent2 = buf.readUtf();
            float chance = buf.readFloat();
            return ParentPair.of(parent1, parent2, chance);
        }

        public void write(ParentPair pair, FriendlyByteBuf buf)
        {
            buf.writeUtf(pair.getA());
            buf.writeUtf(pair.getB());
            buf.writeFloat(pair.getMutationChance());
        }
    }
}
