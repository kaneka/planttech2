package net.kaneka.planttech2.crops;

import net.kaneka.planttech2.PlantTechMain;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.common.util.Lazy;
import net.minecraftforge.registries.IForgeRegistry;

import javax.annotation.Nullable;
import java.util.function.Supplier;

public class LazyRegistryEntry  <T> implements Supplier<T>
{
    private final Lazy<T> obj;

    public static <R> LazyRegistryEntry<R> of(String name, IForgeRegistry<R> registry)
    {
        return of(new ResourceLocation(PlantTechMain.MODID, name), registry);
    }

    public static <R> LazyRegistryEntry<R> of(ResourceLocation name, IForgeRegistry<R> registry)
    {
        return new LazyRegistryEntry<>(name, registry);
    }

    private LazyRegistryEntry(ResourceLocation name, IForgeRegistry<T> registry)
    {
        this.obj = Lazy.of(() -> registry.getValue(name));
    }

    @Nullable
    @Override
    public T get()
    {
        return obj.get();
    }
}
