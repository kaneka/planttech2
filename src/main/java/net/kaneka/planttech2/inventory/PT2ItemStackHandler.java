package net.kaneka.planttech2.inventory;

import net.kaneka.planttech2.items.TierItem;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.IItemHandlerModifiable;
import net.minecraftforge.items.ItemStackHandler;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * Used for PT2 Container - Vanilla Hopper interactions
 */
public class PT2ItemStackHandler implements IItemHandler, IItemHandlerModifiable, INBTSerializable<CompoundTag>
{
    private final ItemStackHandler handler;
    private final Handler[] handlers;

    public PT2ItemStackHandler(ItemStackHandler handler, int size)
    {
        this(handler, size, (handler2) -> {});
    }

    public PT2ItemStackHandler(ItemStackHandler handler, int from, int to)
    {
        this(handler, from, to, (handler2) -> {});
    }

    public PT2ItemStackHandler(ItemStackHandler handler, int size, Consumer<Handler> extra)
    {
        this(handler, 0, size, extra);
    }

    public PT2ItemStackHandler(ItemStackHandler handler, int from, int to, Consumer<Handler> extra)
    {
        this(handler, new ArrayList<>() {{
            for (int i = from; i < to; i++)
            {
                Handler handler2 = Handler.of(i);
                extra.accept(handler2);
                add(handler2);
            }
        }});
    }

    public PT2ItemStackHandler(ItemStackHandler handler, List<Handler> handlers)
    {
        this.handler = handler;
        this.handlers = handlers.toArray(Handler[]::new);
    }

    public PT2ItemStackHandler(ItemStackHandler handler, Handler... handlers)
    {
        this.handler = handler;
        this.handlers = handlers;
    }

    public boolean isEmpty()
    {
        return handlers.length == 0;
    }

    @Override
    public CompoundTag serializeNBT()
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public void deserializeNBT(CompoundTag nbt)
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setStackInSlot(int slot, @NotNull ItemStack stack)
    {
        handler.setStackInSlot(getIndex(slot), stack);
    }

    @Override
    public int getSlots()
    {
        return handlers.length;
    }

    @NotNull
    @Override
    public ItemStack getStackInSlot(int slot)
    {
        return handler.getStackInSlot(getIndex(slot));
    }

    @NotNull
    @Override
    public ItemStack insertItem(int slot, @NotNull ItemStack stack, boolean simulate)
    {
        if (!isItemValid(slot, stack))
            return stack;
        return handler.insertItem(getIndex(slot), stack, simulate);
    }

    @NotNull
    @Override
    public ItemStack extractItem(int slot, int amount, boolean simulate)
    {
        return handler.extractItem(getIndex(slot), amount, simulate);
    }

    @Override
    public int getSlotLimit(int slot)
    {
        return getHandler(slot).maxCount;
    }

    @Override
    public boolean isItemValid(int slot, @NotNull ItemStack stack)
    {
        return getHandler(slot).conditions.test(stack);
    }

    private int getIndex(int slot)
    {
        return getHandler(slot).index;
    }

    private Handler getHandler(int index)
    {
        return this.handlers[index];
    }

    public static class Handler
    {
        private final int index;
        private Predicate<ItemStack> conditions = (stack) -> true;
        private int maxCount = 64;

        private Handler(int index)
        {
            this.index = index;
        }

        public static Handler of(int index)
        {
            return new Handler(index);
        }

        public Handler setConditions(boolean enabled)
        {
            return setConditions((stack) -> enabled);
        }

        public Handler setConditions(Item... acceptableItems)
        {
            return setConditions((stack) -> acceptableItems.length == 0 || Arrays.stream(acceptableItems).anyMatch((item) -> stack.getItem() == item));
        }

        public Handler setConditions(Predicate<ItemStack> conditions)
        {
            this.conditions = conditions;
            return this;
        }

        public Handler setConditions(TierItem.ItemType type)
        {
            this.conditions = (stack) -> stack.getItem() instanceof TierItem && ((TierItem) stack.getItem()).getItemType() == type;
            return setMaxCount(1);
        }

        public Handler setMaxCount(int maxCount)
        {
            this.maxCount = maxCount;
            return this;
        }
    }
}
