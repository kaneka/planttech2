package net.kaneka.planttech2.inventory;

import com.google.common.collect.ImmutableList;
import net.kaneka.planttech2.addons.Pair;
import net.kaneka.planttech2.blocks.entity.machine.SeedConstructorBlockEntity;
import net.kaneka.planttech2.registries.ModContainers;
import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.player.Inventory;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.items.IItemHandler;

import java.util.List;

public class SeedConstructorMenu extends BlockBaseMenu
{
	public SeedConstructorMenu(int id, Inventory player, SeedConstructorBlockEntity tileentity)
	{
		super(id, ModContainers.SEEDCONSTRUCTOR.get(), player, tileentity, 7);
		IItemHandler handler = tileentity.getCapability(ForgeCapabilities.ITEM_HANDLER).orElseThrow(NullPointerException::new);
		
		this.addSlot(createDNAContainerSlot(handler, 0, 95, 29, "slot.seedconstructor.container", false));
		this.addSlot(createOutoutSlot(handler, 1, 95, 66));
		this.addSlot(createSpeedUpgradeSlot(handler, 2, 71, 43));
		this.addSlot(createFluidInSlot(handler, 23, 38));
		this.addSlot(createFluidOutSlot(handler, 23, 57));
		this.addSlot(createEnergyInSlot(handler, 167, 38));
		this.addSlot(createEnergyOutSlot(handler, 167, 57));
		this.addSlot(createKnowledgeChipSlot(handler, 12, 9));
		
	}

    public SeedConstructorMenu(int i, Inventory inventory, BlockPos blockPos)
    {
		this(i, inventory, (SeedConstructorBlockEntity) inventory.player.level().getBlockEntity(blockPos));
	}

	@Override
	protected List<Pair<Integer, Integer>> getSlotsPrioritized()
	{
		return ImmutableList.of(
				// knowledge
				Pair.of(7, null),
				// upgrade
				Pair.of(2, null),
				// energy io
				Pair.of(5, 6),
				// fluid io
				Pair.of(3, 4),
				// inputs
				Pair.of(0, null));
	}
}
