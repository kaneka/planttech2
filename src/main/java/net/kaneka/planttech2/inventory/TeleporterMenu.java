package net.kaneka.planttech2.inventory;

import net.kaneka.planttech2.addons.Pair;
import net.kaneka.planttech2.registries.ModContainers;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.items.IItemHandler;

import java.util.Collections;
import java.util.List;

public class TeleporterMenu extends BaseMenu
{
	private final ItemStack stack;
	
	public TeleporterMenu(int id, Inventory inventory, ItemStack stack)
	{ 
		super(id, ModContainers.TELEPORTERITEM.get(), inventory);
		this.stack = stack;
		IItemHandler handler = stack.getCapability(ForgeCapabilities.ITEM_HANDLER).orElseThrow(NullPointerException::new);
		this.addSlot(createEnergyInSlot(handler, 0, 167, 38));
		this.addSlot(createEnergyOutSlot(handler, 1, 167, 57));
	}

    @Override
	public boolean stillValid(Player playerIn)
	{
		return !stack.isEmpty();
	}

	@Override
	protected List<Pair<Integer, Integer>> getSlotsPrioritized()
	{
		return Collections.emptyList();
	}

	@Override
	public ItemStack quickMoveStack(Player playerIn, int index)
	{
		return ItemStack.EMPTY;
	}

	public ItemStack getStack()
	{
		return stack;
	}
}
