package net.kaneka.planttech2.inventory;

import net.kaneka.planttech2.items.upgradeable.IUpgradeable;
import net.kaneka.planttech2.items.upgradeable.UpgradeChipItem;
import net.kaneka.planttech2.registries.ModContainers;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.Map;

public class ItemUpgradeableMenu extends AbstractContainerMenu {
	public final static Map<Integer, Integer[]> settings = new HashMap<Integer, Integer[]>() {
		private static final long serialVersionUID = 1L;
		{
			// invsize            rowcount, 	columncount, 	startx, starty
			put(10, new Integer[]{2, 5, 20, 20});
		}
	};
	private final ItemStack stack;

	public ItemUpgradeableMenu(int id, Inventory playerInv, ItemStack itemInv)
	{
		super(ModContainers.UPGRADEABLEITEM.get(), id);
		this.stack = itemInv;
		LazyOptional<IItemHandler> provider = itemInv.getCapability(ForgeCapabilities.ITEM_HANDLER);
		IItemHandler handler = provider.orElseThrow(NullPointerException::new);
		int invsize = handler.getSlots();
		Integer[] setting = settings.get(invsize);
		if (setting != null)
			for (int y = 0; y < setting[0]; y++)
				for (int x = 0; x < setting[1]; x++)
					addSlot(new ChangeCheckSlot(itemInv, handler, x + y * setting[1], 46 + x * 18, 38 + y * 18, "slot.upgradeableitem.chipslot"));
		for (int y = 0; y < 3; y++)
			for (int x = 0; x < 9; x++)
				addSlot(new Slot(playerInv, x + y * 9 + 9, 23 + x * 18, 106 + y * 18));
		for (int x = 0; x < 9; x++)
			addSlot(new Slot(playerInv, x, 23 + x * 18, 164));
	}

    @Override
	public boolean stillValid(Player playerIn)
	{
		return true;
	}

	@Override
	public ItemStack quickMoveStack(Player playerIn, int index)
	{
		ItemStack stack = ItemStack.EMPTY;
		Slot slot = slots.get(index);
		if(slot.hasItem())
		{
			ItemStack stack1 = slot.getItem();
			stack = stack1.copy();
			if (index > 35)
			{
				if (!this.moveItemStackTo(stack1, 0, 34, true))
					return ItemStack.EMPTY;
			}
			else
			{
				if (!this.moveItemStackTo(stack1, 36, 37, false))
					return ItemStack.EMPTY;
				else if (index >= 0 && index < 27)
				{
					if(!this.moveItemStackTo(stack1, 27, 35, false))
						return ItemStack.EMPTY;
				}
				else if (index >= 27 && !this.moveItemStackTo(stack1, 0, 26, false))
					return ItemStack.EMPTY;
			}
			if (stack1.isEmpty())
				slot.set(ItemStack.EMPTY);
			else
				slot.setChanged();
			if (stack1.getCount() == stack.getCount())
				return ItemStack.EMPTY;
			slot.onTake(playerIn, stack1);
		}
		return stack;
	}

	private class ChangeCheckSlot extends SlotItemHandlerWithInfo {
		private ItemStack stack;

		public ChangeCheckSlot(ItemStack stack, IItemHandler itemHandler, int index, int xPosition, int yPosition, String usage) {
			super(itemHandler, index, xPosition, yPosition, usage);
			this.stack = stack;
		}

		@Override
		public boolean mayPlace(@Nonnull ItemStack stack)
		{
			return stack.getItem() instanceof UpgradeChipItem;
		}

		@Override
		public int getMaxStackSize() {
			return 1;
		}

		@Override
		public void setChanged() {
			if (stack.getItem() instanceof IUpgradeable) ((IUpgradeable) stack.getItem()).updateNBTValues(stack);
			super.setChanged();
		}
	}

	private class SlotItemHandlerWithInfo extends SlotItemHandler
	{
		private final String usage;

		public SlotItemHandlerWithInfo(IItemHandler itemHandler, int index, int xPosition, int yPosition, String usage)
		{
			super(itemHandler, index, xPosition, yPosition);
			this.usage = usage;
		}

		public String getUsageString()
		{
			return usage;
		}
	}

	public ItemStack getStack() {
		return stack;
	}

}