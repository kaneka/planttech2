package net.kaneka.planttech2.inventory;

import com.google.common.collect.ImmutableList;
import net.kaneka.planttech2.addons.Pair;
import net.kaneka.planttech2.blocks.entity.machine.MachineBulbReprocessorBlockEntity;
import net.kaneka.planttech2.items.CropSeedItem;
import net.kaneka.planttech2.registries.ModContainers;
import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.player.Inventory;
import net.minecraftforge.common.Tags;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.items.IItemHandler;

import java.util.List;

public class MachineBulbReprocessorMenu extends BlockBaseMenu
{
	public MachineBulbReprocessorMenu(int id, Inventory player, MachineBulbReprocessorBlockEntity tileentity)
	{
		super(id, ModContainers.MACHINEBULBREPROCESSOR.get(), player, tileentity, 7);
		IItemHandler handler = tileentity.getCapability(ForgeCapabilities.ITEM_HANDLER).orElseThrow(NullPointerException::new);
		
		this.addSlot(new LimitedItemInfoSlot(handler, 0, 77, 85, "slot.machinebulbreprocessor.input").setConditions((stack) -> stack.is(Tags.Items.SEEDS) || stack.getItem() instanceof CropSeedItem).setShouldListen());
		this.addSlot(createOutoutSlot(handler, 1, 131, 85));

		this.addSlot(createFluidInSlot(handler, 23, 38));
		this.addSlot(createFluidOutSlot(handler, 23, 57));
		
		this.addSlot(createEnergyInSlot(handler, 167, 38));
		this.addSlot(createEnergyOutSlot(handler, 167, 57));
		
		this.addSlot(createKnowledgeChipSlot(handler, 12, 9).setShouldListen());
	}

    public MachineBulbReprocessorMenu(int i, Inventory inventory, BlockPos blockPos)
    {
		this(i, inventory, (MachineBulbReprocessorBlockEntity) inventory.player.level().getBlockEntity(blockPos));
	}

	@Override
	protected List<Pair<Integer, Integer>> getSlotsPrioritized()
	{
		return ImmutableList.of(
				// knowledge
				Pair.of(6, null),
				// energy io
				Pair.of(4, 5),
				// fluid io
				Pair.of(2, 3),
				// inputs
				Pair.of(0, null));
	}
}
