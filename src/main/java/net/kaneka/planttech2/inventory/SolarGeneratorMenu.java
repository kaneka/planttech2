package net.kaneka.planttech2.inventory;

import com.google.common.collect.ImmutableList;
import net.kaneka.planttech2.addons.Pair;
import net.kaneka.planttech2.blocks.entity.machine.SolarGeneratorBlockEntity;
import net.kaneka.planttech2.items.TierItem;
import net.kaneka.planttech2.registries.ModContainers;
import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.player.Inventory;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.items.IItemHandler;

import java.util.List;

public class SolarGeneratorMenu extends BlockBaseMenu
{
	public SolarGeneratorMenu(int id, Inventory player, SolarGeneratorBlockEntity tileentity)
	{
		super(id, ModContainers.SOLARGENERATOR.get(), player, tileentity, 4);
		IItemHandler handler = tileentity.getCapability(ForgeCapabilities.ITEM_HANDLER).orElseThrow(NullPointerException::new);
		
		this.addSlot(new LimitedItemInfoSlot(handler, 0, 95, 60, "slot.solargenerator.focus").setLimited().setConditions(TierItem.ItemType.SOLAR_FOCUS));
		this.addSlot(createSpeedUpgradeSlot(handler, 1, 95, 31));
		this.addSlot(createEnergyInSlot(handler, 167, 38));
		this.addSlot(createEnergyOutSlot(handler, 167, 57));
		this.addSlot(createKnowledgeChipSlot(handler, 12, 9));
	}

	public SolarGeneratorMenu(int i, Inventory inventory, BlockPos blockPos)
	{
		this(i, inventory, (SolarGeneratorBlockEntity) inventory.player.level().getBlockEntity(blockPos));
	}

	@Override
	protected List<Pair<Integer, Integer>> getSlotsPrioritized()
	{
		return ImmutableList.of(
				// knowledge
				Pair.of(4, null),
				// upgrade
				Pair.of(1, null),
				// energy io
				Pair.of(2, 3),
				// inputs
				Pair.of(0, null));
	}
}
