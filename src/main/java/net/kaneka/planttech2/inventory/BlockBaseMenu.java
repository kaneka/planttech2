package net.kaneka.planttech2.inventory;

import net.kaneka.planttech2.blocks.entity.machine.baseclasses.EnergyBlockEntity;
import net.kaneka.planttech2.blocks.entity.machine.baseclasses.EnergyInventoryBlockEntity;
import net.kaneka.planttech2.blocks.entity.machine.baseclasses.EnergyInventoryFluidBlockEntity;
import net.kaneka.planttech2.items.KnowledgeChip;
import net.kaneka.planttech2.utilities.PTCommonUtil;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.inventory.MenuType;
import net.minecraftforge.items.IItemHandler;

public abstract class BlockBaseMenu extends BaseMenu
{
    protected final EnergyInventoryBlockEntity blockEntity;
    protected final ContainerData data;

    public BlockBaseMenu(int id, MenuType<?> type, Inventory player, EnergyInventoryBlockEntity BlockEntity, int slots)
    {
        super(id, type, player);
        this.blockEntity = BlockEntity;
        data = BlockEntity.getContainerData();
        addDataSlots(data);
        prioritizedSlots = getSlotsPrioritized();
    }

    protected LimitedItemInfoSlot createEnergyInSlot(IItemHandler itemHandler, int xPosition, int yPosition)
    {
        return createEnergyInSlot(itemHandler, blockEntity.getEnergyInSlot(), xPosition, yPosition);
    }

    protected LimitedItemInfoSlot createEnergyOutSlot(IItemHandler itemHandler, int xPosition, int yPosition)
    {
        return createEnergyInSlot(itemHandler, blockEntity.getEnergyOutSlot(), xPosition, yPosition);
    }

    protected LimitedItemInfoSlot createKnowledgeChipSlot(IItemHandler itemHandler, int xPosition, int yPosition)
    {
        return new LimitedItemInfoSlot(itemHandler, blockEntity.getKnowledgeChipSlot(), xPosition, yPosition, "slot.util.knowledgechip").setConditions((stack) -> stack.getItem() instanceof KnowledgeChip);
    }

    protected LimitedItemInfoSlot createFluidInSlot(IItemHandler itemHandler, int xPosition, int yPosition)
    {
        return new LimitedItemInfoSlot(itemHandler, ((EnergyInventoryFluidBlockEntity) blockEntity).getFluidInSlot(), xPosition, yPosition, "slot.util.fluidin").setConditions((stack) -> PTCommonUtil.getBiomassCap(stack).getA().isPresent()).setLimited();
    }

    protected LimitedItemInfoSlot createFluidOutSlot(IItemHandler itemHandler, int xPosition, int yPosition)
    {
        return new LimitedItemInfoSlot(itemHandler, ((EnergyInventoryFluidBlockEntity) blockEntity).getFluidOutSlot(), xPosition, yPosition, "slot.util.fluidout").setConditions((stack) -> PTCommonUtil.getBiomassCap(stack).getA().isPresent()).setLimited();
    }

    @Override
    public boolean stillValid(Player playerIn)
    {
        return blockEntity.isUsableByPlayer(playerIn);
    }

    public EnergyBlockEntity getTE()
    {
        return blockEntity;
    }

    public int getValue(int id)
    {
        return data.get(id);
    }
}
