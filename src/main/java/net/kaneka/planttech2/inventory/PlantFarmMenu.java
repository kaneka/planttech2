package net.kaneka.planttech2.inventory;

import com.google.common.collect.ImmutableList;
import net.kaneka.planttech2.addons.Pair;
import net.kaneka.planttech2.blocks.entity.machine.PlantFarmBlockEntity;
import net.kaneka.planttech2.registries.ModContainers;
import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.player.Inventory;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.items.IItemHandler;

import java.util.List;

public class PlantFarmMenu extends BlockBaseMenu
{
	public PlantFarmMenu(int id, Inventory player, PlantFarmBlockEntity tileentity)
	{
		super(id, ModContainers.PLANTFARM.get(), player, tileentity, 17);
		IItemHandler handler = tileentity.getCapability(ForgeCapabilities.ITEM_HANDLER).orElseThrow(NullPointerException::new);
		this.addSlot(new LimitedItemInfoSlot(handler, 0, 84, 41, "slot.plantfarm.seed").setConditions(PlantFarmBlockEntity::isSeed));
		for(int y = 0; y < 2; y++)
			for(int x = 0; x < 5; x++)
				this.addSlot(new LimitedItemInfoSlot(handler, 1 + x + y * 5, 59 + x * 18, 67 + y * 18, "slot.plantfarm.storage").setConditions(false));
		this.addSlot(createSpeedUpgradeSlot(handler, 11, 59, 41));
		this.addSlot(createRangeUpgradeSlot(handler, 12, 131, 41));
		this.addSlot(createFluidInSlot(handler, 23, 38));
		this.addSlot(createFluidOutSlot(handler, 23, 57));
		this.addSlot(createEnergyInSlot(handler, 167, 38));
		this.addSlot(createEnergyOutSlot(handler, 167, 57));
		this.addSlot(createKnowledgeChipSlot(handler, 12, 9));
	}

    public PlantFarmMenu(int i, Inventory inventory, BlockPos blockPos)
    {
		this(i, inventory, (PlantFarmBlockEntity) inventory.player.level().getBlockEntity(blockPos));
	}

	@Override
	protected List<Pair<Integer, Integer>> getSlotsPrioritized()
	{
		return ImmutableList.of(
				// knowledge
				Pair.of(16, null),
				// upgrade
				Pair.of(10, 11),
				// energy io
				Pair.of(14, 15),
				// fluid io
				Pair.of(12, 13),
				// inputs
				Pair.of(0, null));
	}
}
