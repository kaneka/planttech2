package net.kaneka.planttech2.inventory;

import com.google.common.collect.ImmutableList;
import net.kaneka.planttech2.addons.Pair;
import net.kaneka.planttech2.blocks.entity.machine.CompressorBlockEntity;
import net.kaneka.planttech2.registries.ModContainers;
import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.player.Inventory;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.items.IItemHandler;

import java.util.List;

public class CompressorMenu extends BlockBaseMenu
{
	public CompressorMenu(int id, Inventory player, CompressorBlockEntity tileentity)
	{
		super(id, ModContainers.COMPRESSOR.get(), player, tileentity, 25);
		IItemHandler handler = tileentity.getCapability(ForgeCapabilities.ITEM_HANDLER).orElseThrow(NullPointerException::new);

		this.addSlot(new LimitedItemInfoSlot(handler, 0, 34, 83, "slot.compressor.input").setShouldListen());
		this.addSlot(createOutoutSlot(handler, tileentity.getOutputSlotIndex(), 126, 83));
		this.addSlot(createSpeedUpgradeSlot(handler, 2, 78, 87));
		for (int y = 0; y < 3; y++)
			for (int x = 0; x < 6; x++)
				addSlot(createFakeSlot(handler, x + y * 6 + 3, 35 + x * 18, 26 + y * 18, "slot.compressor.select"));
		this.addSlot(createEnergyInSlot(handler, 167, 38));
		this.addSlot(createEnergyOutSlot(handler, 167, 57));
		this.addSlot(createKnowledgeChipSlot(handler, 12, 9));
	}

    public CompressorMenu(int i, Inventory inventory, BlockPos blockPos)
    {
    	this(i, inventory, (CompressorBlockEntity) inventory.player.level().getBlockEntity(blockPos));
    }

	@Override
	protected List<Pair<Integer, Integer>> getSlotsPrioritized()
	{
		return ImmutableList.of(
				// knowledge
				Pair.of(23, null),
				// upgrade
				Pair.of(2, null),
				// energy io
				Pair.of(21, 22),
				// inputs
				Pair.of(0, null));
	}
}