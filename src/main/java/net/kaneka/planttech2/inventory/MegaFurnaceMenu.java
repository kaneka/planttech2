package net.kaneka.planttech2.inventory;

import com.google.common.collect.ImmutableList;
import net.kaneka.planttech2.addons.Pair;
import net.kaneka.planttech2.blocks.entity.machine.MegaFurnaceBlockEntity;
import net.kaneka.planttech2.registries.ModContainers;
import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.player.Inventory;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.items.IItemHandler;

import java.util.List;

public class MegaFurnaceMenu extends BlockBaseMenu
{
	public MegaFurnaceMenu(int id, Inventory player, MegaFurnaceBlockEntity tileentity)
	{
		super(id, ModContainers.MEGAFURNACE.get(), player, tileentity, 15);
		IItemHandler handler = tileentity.getCapability(ForgeCapabilities.ITEM_HANDLER).orElseThrow(NullPointerException::new);
		for(int x = 0; x < 6; x++)
			this.addSlot(new SlotItemHandlerWithInfo(handler, x, 21 + x * 22 , 27, "slot.megafurnace.input"));
		for(int x = 0; x < 6; x++)
			this.addSlot(createOutoutSlot(handler, x + 6, 21 + x * 22 , 64));
		this.addSlot(createSpeedUpgradeSlot(handler, 12, 109, 85));
		this.addSlot(createEnergyInSlot(handler, 167, 38));
		this.addSlot(createEnergyOutSlot(handler, 167, 57));
		this.addSlot(createKnowledgeChipSlot(handler, 12, 9));
	}

    public MegaFurnaceMenu(int i, Inventory inventory, BlockPos blockPos)
    {
		this(i, inventory, (MegaFurnaceBlockEntity) inventory.player.level().getBlockEntity(blockPos));
	}

	@Override
	protected List<Pair<Integer, Integer>> getSlotsPrioritized()
	{
		return ImmutableList.of(
				// knowledge
				Pair.of(15, null),
				// upgrade
				Pair.of(12, null),
				// energy io
				Pair.of(13, 14),
				// inputs
				Pair.of(0, 5));
	}
}
