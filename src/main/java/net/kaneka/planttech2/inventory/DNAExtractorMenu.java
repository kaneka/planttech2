package net.kaneka.planttech2.inventory;

import com.google.common.collect.ImmutableList;
import net.kaneka.planttech2.addons.Pair;
import net.kaneka.planttech2.blocks.entity.machine.DNAExtractorBlockEntity;
import net.kaneka.planttech2.items.CropSeedItem;
import net.kaneka.planttech2.registries.ModContainers;
import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.player.Inventory;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.items.IItemHandler;

import java.util.List;

public class DNAExtractorMenu extends BlockBaseMenu
{
	public DNAExtractorMenu(int id, Inventory player, DNAExtractorBlockEntity tileentity)
	{
		super(id, ModContainers.DNAEXTRACTOR.get(), player, tileentity, 6);
		IItemHandler handler = tileentity.getCapability(ForgeCapabilities.ITEM_HANDLER).orElseThrow(NullPointerException::new);
		
		this.addSlot(new LimitedItemInfoSlot(handler, 0, 58, 40, "slot.dnaextractor.seeds").setConditions((stack) -> stack.getItem() instanceof CropSeedItem));
		this.addSlot(createDNAContainerSlot(handler, 1, 130, 40, "slot.dnaextractor.empty_container", true));
		this.addSlot(createOutoutSlot(handler, 2, 130, 67));
		this.addSlot(createSpeedUpgradeSlot(handler, 3, 94, 66));
		this.addSlot(createEnergyInSlot(handler, 167, 38));
		this.addSlot(createEnergyOutSlot(handler, 167, 57));
		this.addSlot(createKnowledgeChipSlot(handler, 12, 9));
		
	}

    public DNAExtractorMenu(int i, Inventory inventory, BlockPos blockPos)
    {
		this(i, inventory, (DNAExtractorBlockEntity) inventory.player.level().getBlockEntity(blockPos));
	}

	@Override
	protected List<Pair<Integer, Integer>> getSlotsPrioritized()
	{
		return ImmutableList.of(
				// knowledge
				Pair.of(6, null),
				// upgrade
				Pair.of(3, null),
				// energy io
				Pair.of(4, 5),
				// inputs
				Pair.of(0, 1));
	}
}
