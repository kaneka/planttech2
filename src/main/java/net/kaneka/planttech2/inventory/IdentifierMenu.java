package net.kaneka.planttech2.inventory;

import com.google.common.collect.ImmutableList;
import net.kaneka.planttech2.addons.Pair;
import net.kaneka.planttech2.blocks.entity.machine.IdentifierBlockEntity;
import net.kaneka.planttech2.crops.CropTypes;
import net.kaneka.planttech2.registries.ModContainers;
import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.player.Inventory;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.items.IItemHandler;

import java.util.List;

public class IdentifierMenu extends BlockBaseMenu
{
	public IdentifierMenu(int id, Inventory player, IdentifierBlockEntity tileentity)
	{
		super(id, ModContainers.IDENTIFIER.get(), player, tileentity, 21);
		IItemHandler handler = tileentity.getCapability(ForgeCapabilities.ITEM_HANDLER).orElseThrow(NullPointerException::new);
		for(int x = 0; x < 3; x++)
			for(int y = 0; y < 3; y++)
				this.addSlot(new LimitedItemInfoSlot(handler, x + y * 3, 19 + x * 18, 27 + y * 18, "slot.identifier.input").setConditions((stack) -> CropTypes.getBySeed(stack).isPresent()));
		for(int x = 0; x < 3; x++)
			for(int y = 0; y < 3; y++)
				this.addSlot(createOutoutSlot(handler, x + y * 3 + 9, 95 + x * 18, 27 + y * 18));
		this.addSlot(createSpeedUpgradeSlot(handler, 18, 75, 85));
		this.addSlot(createEnergyInSlot(handler, 167, 38));
		this.addSlot(createEnergyOutSlot(handler, 167, 57));
		this.addSlot(createKnowledgeChipSlot(handler, 12, 9));
		
	}

    public IdentifierMenu(int i, Inventory inventory, BlockPos blockPos)
    {
		this(i, inventory, (IdentifierBlockEntity) inventory.player.level().getBlockEntity(blockPos));
	}

	@Override
	protected List<Pair<Integer, Integer>> getSlotsPrioritized()
	{
		return ImmutableList.of(
				// knowledge
				Pair.of(21, null),
				// upgrade
				Pair.of(18, null),
				// energy io
				Pair.of(19, 20));
	}
}
