package net.kaneka.planttech2.inventory;

import com.google.common.collect.ImmutableList;
import net.kaneka.planttech2.addons.Pair;
import net.kaneka.planttech2.blocks.entity.machine.DNACombinerBlockEntity;
import net.kaneka.planttech2.registries.ModContainers;
import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.player.Inventory;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.items.IItemHandler;

import java.util.List;

public class DNACombinerMenu extends BlockBaseMenu
{
	public DNACombinerMenu(int id, Inventory player, DNACombinerBlockEntity tileentity)
	{
		super(id, ModContainers.DNACOMBINER.get(), player, tileentity, 7);
		IItemHandler handler = tileentity.getCapability(ForgeCapabilities.ITEM_HANDLER).orElseThrow(NullPointerException::new);
		
		this.addSlot(createDNAContainerSlot(handler, 0, 77, 37, "slot.dnacombiner.container", false));
		this.addSlot(createDNAContainerSlot(handler, 1, 113, 37, "slot.dnacombiner.container", false));
		this.addSlot(createDNAContainerSlot(handler, 2, 121, 56, "slot.dnacombiner.empty_container", true));
		this.addSlot(createOutoutSlot(handler, 3, 95, 73));
		this.addSlot(createSpeedUpgradeSlot(handler, 4, 54, 50));
		this.addSlot(createEnergyInSlot(handler, 167, 38));
		this.addSlot(createEnergyOutSlot(handler,167, 57));
		this.addSlot(createKnowledgeChipSlot(handler, 12, 9));
	}

    public DNACombinerMenu(int i, Inventory inventory, BlockPos blockPos)
    {
		this(i, inventory, (DNACombinerBlockEntity) inventory.player.level().getBlockEntity(blockPos));
	}

	@Override
	protected List<Pair<Integer, Integer>> getSlotsPrioritized()
	{
		return ImmutableList.of(
				// knowledge
				Pair.of(7, null),
				// upgrade
				Pair.of(4, null),
				// energy io
				Pair.of(5, 6),
				// inputs
				Pair.of(0, 2));
	}
}
