package net.kaneka.planttech2.inventory;

import com.google.common.collect.ImmutableList;
import net.kaneka.planttech2.addons.Pair;
import net.kaneka.planttech2.blocks.entity.machine.DNACleanerBlockEntity;
import net.kaneka.planttech2.registries.ModContainers;
import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.player.Inventory;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.items.IItemHandler;

import java.util.List;

public class DNACleanerMenu extends BlockBaseMenu
{
    public DNACleanerMenu(int id, Inventory player, DNACleanerBlockEntity tileentity)
    {
		super(id, ModContainers.DNACLEANER.get(), player, tileentity, 5);
		IItemHandler handler = tileentity.getCapability(ForgeCapabilities.ITEM_HANDLER).orElseThrow(NullPointerException::new);

		this.addSlot(createDNAContainerSlot(handler, 0, 41, 47, "slot.dnacleaner.input", false));
		this.addSlot(createOutoutSlot(handler, 1, 95, 47));
		this.addSlot(createSpeedUpgradeSlot(handler, 2, 68, 69));
		this.addSlot(createEnergyInSlot(handler, 167, 38));
		this.addSlot(createEnergyOutSlot(handler, 167, 57));
		this.addSlot(createKnowledgeChipSlot(handler, 12, 9));
    }

    public DNACleanerMenu(int i, Inventory inventory, BlockPos blockPos)
    {
		this(i, inventory, (DNACleanerBlockEntity) inventory.player.level().getBlockEntity(blockPos));
	}

	@Override
	protected List<Pair<Integer, Integer>> getSlotsPrioritized()
	{
		return ImmutableList.of(
				// knowledge
				Pair.of(5, null),
				// upgrade
				Pair.of(2, null),
				// energy io
				Pair.of(3, 4),
				// inputs
				Pair.of(0, null));
	}
}
