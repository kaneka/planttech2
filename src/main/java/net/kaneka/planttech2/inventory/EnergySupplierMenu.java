package net.kaneka.planttech2.inventory;

import com.google.common.collect.ImmutableList;
import net.kaneka.planttech2.addons.Pair;
import net.kaneka.planttech2.blocks.entity.machine.EnergySupplierBlockEntity;
import net.kaneka.planttech2.registries.ModContainers;
import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.player.Inventory;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.items.IItemHandler;

import java.util.List;

public class EnergySupplierMenu extends BlockBaseMenu
{
	public EnergySupplierMenu(int id, Inventory player, EnergySupplierBlockEntity tileentity)
	{
		super(id, ModContainers.ENERGYSUPPLIER.get(), player, tileentity, 2);
		IItemHandler handler = tileentity.getCapability(ForgeCapabilities.ITEM_HANDLER).orElseThrow(NullPointerException::new);
		this.addSlot(createEnergyInSlot(handler, 167, 38));
		this.addSlot(createEnergyOutSlot(handler, 167, 57));
		this.addSlot(createCapacityChipSlot(handler, tileentity.getUpgradeSlot(), 123, 65));
	}

    public EnergySupplierMenu(int i, Inventory inventory, BlockPos blockPos)
    {
		this(i, inventory, (EnergySupplierBlockEntity) inventory.player.level().getBlockEntity(blockPos));
	}

	@Override
	protected List<Pair<Integer, Integer>> getSlotsPrioritized()
	{
		return ImmutableList.of(
				// upgrade
				Pair.of(2, null),
				// energy io
				Pair.of(0, 1));
	}
}
