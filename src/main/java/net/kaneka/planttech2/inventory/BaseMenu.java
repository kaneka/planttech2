package net.kaneka.planttech2.inventory;

import net.kaneka.planttech2.addons.Pair;
import net.kaneka.planttech2.items.TierItem;
import net.kaneka.planttech2.registries.ModItems;
import net.kaneka.planttech2.utilities.PTCommonUtil;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.MenuType;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public abstract class BaseMenu extends AbstractContainerMenu
{
	public BaseMenu(int id, MenuType<?> type, Inventory player)
	{
		super(type, id);
		for (int y = 0; y < 3; y++)
			for (int x = 0; x < 9; x++)
				addSlot(new Slot(player, x + y * 9 + 9, 23 + x * 18, 106 + y * 18));
		for (int x = 0; x < 9; x++)
			addSlot(new Slot(player, x, 23 + x * 18, 164));
	}

	protected LimitedItemInfoSlot createSpeedUpgradeSlot(IItemHandler itemHandler, int index, int xPosition, int yPosition)
	{
		return new LimitedItemInfoSlot(itemHandler, index, xPosition, yPosition, "slot.util.speedupgrade").setConditions(TierItem.ItemType.SPEED_UPGRADE).setLimited();
	}

	protected LimitedItemInfoSlot createRangeUpgradeSlot(IItemHandler itemHandler, int index, int xPosition, int yPosition)
	{
		return new LimitedItemInfoSlot(itemHandler, index, xPosition, yPosition, "slot.util.rangeupgrade").setConditions(TierItem.ItemType.RANGE_UPGRADE).setLimited();
	}

	protected LimitedItemInfoSlot createEnergyInSlot(IItemHandler itemHandler, int index, int xPosition, int yPosition)
	{
		return new LimitedItemInfoSlot(itemHandler, index, xPosition, yPosition, "slot.util.energyin").setConditions((stack) -> PTCommonUtil.getEnergyCap(stack).isPresent()).setLimited();
	}

	protected LimitedItemInfoSlot createEnergyOutSlot(IItemHandler itemHandler, int index, int xPosition, int yPosition)
	{
		return new LimitedItemInfoSlot(itemHandler, index, xPosition, yPosition, "slot.util.energyout").setConditions((stack) -> PTCommonUtil.getEnergyCap(stack).isPresent()).setLimited();
	}

	protected LimitedItemInfoSlot createCapacityChipSlot(IItemHandler itemHandler, int index, int xPosition, int yPosition)
	{
		return (LimitedItemInfoSlot) new LimitedItemInfoSlot(itemHandler, index, xPosition, yPosition, "slot.util.capacityupgrade").setConditions(TierItem.ItemType.CAPACITY_UPGRADE).setLimited().setShouldListen();
	}

	protected LimitedItemInfoSlot createFakeSlot(IItemHandler itemHandler, int index, int xPosition, int yPosition, String usage)
	{
		return new LimitedItemInfoSlot(itemHandler, index, xPosition, yPosition, usage).setConditions(false).setCanTake(false);
	}

	protected LimitedItemInfoSlot createOutoutSlot(IItemHandler itemHandler, int index, int xPosition, int yPosition)
	{
		return new LimitedItemInfoSlot(itemHandler, index, xPosition, yPosition, "slot.util.output").setConditions(false);
	}

	protected LimitedItemInfoSlot createDNAContainerSlot(IItemHandler handler, int index, int xPosition, int yPosition, String usage, boolean isEmpty)
	{
		return new LimitedItemInfoSlot(handler, index, xPosition, yPosition, usage).setConditions(
				(stack) -> (isEmpty && stack.getItem() == ModItems.DNA_CONTAINER_EMPTY.get()) || (!isEmpty && stack.getItem() == ModItems.DNA_CONTAINER.get() && stack.hasTag()));
	}

	protected abstract List<Pair<Integer, Integer>> getSlotsPrioritized();
	protected List<Pair<Integer, Integer>> prioritizedSlots;

	@Override
	public ItemStack quickMoveStack(Player playerIn, int index)
	{
		ItemStack stack = ItemStack.EMPTY;
		Slot slot = slots.get(index);
		if(slot.hasItem())
		{
			ItemStack stack1 = slot.getItem();
			stack = stack1.copy();
			//To Inventory
			final int inventorySlots = 36;
			if (index > inventorySlots - 1)
			{
				if (!this.moveItemStackTo(stack1, 0, 34, true))
					return ItemStack.EMPTY;
			}
			//From Inventory
			else
			{
				boolean moved = false;
				for (Pair<Integer, Integer> prioritizedSlot : prioritizedSlots)
				{
					moved = moveItemStackTo(stack1, prioritizedSlot.getA() + inventorySlots, (prioritizedSlot.getB() == null ? prioritizedSlot.getA() : prioritizedSlot.getB()) + inventorySlots + 1, false);
					if (moved)
						break;
				}
				if (!moved)
					return ItemStack.EMPTY;
			}
			if (stack1.isEmpty())
				slot.set(ItemStack.EMPTY);
			else
				slot.setChanged();
			if (stack1.getCount() == stack.getCount())
				return ItemStack.EMPTY;
			slot.onTake(playerIn, stack1);
		}
		return stack;
	}

	public class SlotItemHandlerWithInfo extends SlotItemHandler
	{
		private final String usage;
		protected boolean listening = false;

		public SlotItemHandlerWithInfo(IItemHandler itemHandler, int index, int xPosition, int yPosition, String usage)
		{
			super(itemHandler, index, xPosition, yPosition);
			this.usage = usage;
		}

		public String getUsageString()
		{
			return usage;
		}

		@Override
		public void setChanged()
		{
			super.setChanged();
			if (listening)
			{
				BaseMenu.this.broadcastChanges();
				if (BaseMenu.this instanceof BlockBaseMenu blockMenu)
					blockMenu.blockEntity.onContainerUpdated(getSlotIndex());
			}
			if (BaseMenu.this instanceof BlockBaseMenu menu)
				menu.blockEntity.setChanged();
		}

		public SlotItemHandlerWithInfo setShouldListen()
		{
			listening = true;
			return this;
		}

		@Override
		public String toString()
		{
			return "SlotItemHandlerWithInfo{" +
					"usage='" + usage + '\'' +
					", listening=" + listening +
					'}';
		}
	}

	public class LimitedItemInfoSlot extends SlotItemHandlerWithInfo
	{
		private Predicate<ItemStack> conditions = (stack) -> true;
		private Predicate<Player> canTake = (stack) -> true;

		private boolean limited = false;

		public LimitedItemInfoSlot(IItemHandler itemHandler, int index, int xPosition, int yPosition, String usage)
		{
			super(itemHandler, index, xPosition, yPosition, usage);
		}

		@Override
		public boolean mayPlace(ItemStack stack)
		{
			return conditions.test(stack);
		}

		@Override
		public boolean mayPickup(Player playerIn)
		{
			return canTake.test(playerIn);
		}

		@Override
		public int getMaxStackSize()
		{
			return limited ? 1 : super.getMaxStackSize();
		}

		@Override
		public int getMaxStackSize(ItemStack stack)
		{
			return limited ? 1 : super.getMaxStackSize(stack);
		}

		public LimitedItemInfoSlot setLimited()
		{
			limited = true;
			return this;
		}

		public LimitedItemInfoSlot setConditions(boolean enabled)
		{
			return setConditions((stack) -> enabled);
		}

		public LimitedItemInfoSlot setConditions(Item... acceptableItems)
		{
			return setConditions((stack) -> acceptableItems.length == 0 || Arrays.stream(acceptableItems).anyMatch((item) -> stack.getItem() == item));
		}

		public LimitedItemInfoSlot setConditions(Predicate<ItemStack> conditions)
		{
			this.conditions = conditions;
			return this;
		}

		public LimitedItemInfoSlot setConditions(TierItem.ItemType type)
		{
			this.conditions = (stack) -> stack.getItem() instanceof TierItem && ((TierItem) stack.getItem()).getItemType() == type;
			return this;
		}

		public LimitedItemInfoSlot setCanTake(boolean enabled)
		{
			return setCanTake((stack) -> enabled);
		}

		public LimitedItemInfoSlot setCanTake(Predicate<Player> conditions)
		{
			this.canTake = conditions;
			return this;
		}
	}
}