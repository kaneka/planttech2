package net.kaneka.planttech2.blocks;

import com.google.common.collect.Lists;
import net.kaneka.planttech2.addons.Pair;
import net.kaneka.planttech2.blocks.entity.CropsBlockEntity;
import net.kaneka.planttech2.crops.CropTypes;
import net.kaneka.planttech2.enums.CropTraitsTypes;
import net.kaneka.planttech2.hashmaps.TraitsManager;
import net.kaneka.planttech2.items.AdvancedAnalyserItem;
import net.kaneka.planttech2.items.AnalyserItem;
import net.kaneka.planttech2.items.CropRemover;
import net.kaneka.planttech2.registries.ModBlocks;
import net.kaneka.planttech2.registries.ModItems;
import net.minecraft.client.color.block.BlockColor;
import net.minecraft.core.BlockPos;
import net.minecraft.core.NonNullList;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.network.chat.Component;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.RandomSource;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.animal.Bee;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.BlockAndTintGetter;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.level.block.state.properties.IntegerProperty;
import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.level.storage.loot.LootParams;
import net.minecraft.world.level.storage.loot.parameters.LootContextParams;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.Vec3;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CropBaseBlock extends ModBlockEntityBlock
{
	public static final IntegerProperty GROWSTATE = IntegerProperty.create("growstate", 0, 7);
	public static final BooleanProperty COPPER = BooleanProperty.create("copper");
	private final CropTypes cropType;

	public CropBaseBlock(CropTypes cropType)
	{
		super(BlockBehaviour.Properties.of().sound(SoundType.WOOD).noCollission().strength(0.5F));
		this.cropType = cropType;
		registerDefaultState(defaultBlockState().setValue(COPPER, false));
	}

	@Override
	public VoxelShape getShape(BlockState p_60555_, BlockGetter p_60556_, BlockPos p_60557_, CollisionContext p_60558_)
	{
		return p_60558_.isHoldingItem(ModItems.RAKE.get()) ? Shapes.empty() : super.getShape(p_60555_, p_60556_, p_60557_, p_60558_);
	}

	@Override
	public void animateTick(BlockState state, Level world, BlockPos pos, RandomSource rand)
	{
		if (world.getBlockEntity(pos) instanceof CropsBlockEntity crop && crop.isSick())
		{
			for (int i = 0; i < 4; ++i)
				if (rand.nextFloat() <= 0.05F)
					world.addParticle(ParticleTypes.SMOKE, pos.getX() + rand.nextFloat(), pos.getY() + rand.nextFloat(), pos.getZ() + rand.nextFloat(), 0.0D, 0.0D, 0.0D);
		}
	}

	public void updateCrop(Level level, BlockPos pos)
	{
		BlockState state = level.getBlockState(pos);
		int growth = state.getValue(GROWSTATE);
		if (level.getBlockEntity(pos) instanceof CropsBlockEntity crop)
		{
			if (growth < 7 && canGrow(level, pos, crop.getTraits()))
				level.setBlockAndUpdate(pos, state.setValue(GROWSTATE, growth + 1));
			else
			{
				for (BlockPos neighbor : getNeighborBlockPosRandom(pos))
				{
					BlockState neightborState = level.getBlockState(neighbor);
					if (neightborState.getBlock() == ModBlocks.CROPBARS.get())
					{
						for (BlockPos neightbor2 : getNeighborBlockPosRandom(neighbor))
						{
							if (neightbor2.equals(pos))
								continue;
							if (level.getBlockEntity(neightbor2) instanceof CropsBlockEntity partner)
							{
								TraitsManager.TypedImpl newTrait = crop.getTraits().calculateNewTraits(partner.getTraits(), crop.chanceBoosts.getA(), crop.chanceBoosts.getB());
								crop.chanceBoosts.set(null, 0);
								level.setBlockAndUpdate(neighbor, newTrait.type.getCropBlock().defaultBlockState().setValue(COPPER, neightborState.getValue(COPPER)));
								if (level.getBlockEntity(neighbor) instanceof CropsBlockEntity crop2)
								{
									crop2.setTraits(newTrait);
									return;
								}
							}
						}
						if (level.getBlockState(neighbor).getBlock() == ModBlocks.CROPBARS.get())
						{
							level.setBlockAndUpdate(neighbor, crop.getTraits().type.getCropBlock().defaultBlockState().setValue(COPPER, neightborState.getValue(COPPER)));
							if (level.getBlockEntity(neighbor) instanceof CropsBlockEntity crop2)
							{
								crop2.setTraits(crop.getTraits().copy());
								return;
							}
						}
					}
				}
			}
		}
	}

	public void updateCreative(Level level, BlockPos pos)
	{
		BlockState state = level.getBlockState(pos);
		int growstate = state.getValue(GROWSTATE);
		if (growstate < 7)
			level.setBlockAndUpdate(pos, state.setValue(GROWSTATE, 7));
	}

	private List<BlockPos> getNeighborBlockPosRandom(BlockPos pos)
	{
		List<BlockPos> neighbors = new ArrayList<BlockPos>();
		neighbors.add(pos.north());
		neighbors.add(pos.east());
		neighbors.add(pos.south());
		neighbors.add(pos.west());
		Collections.shuffle(neighbors);
		return neighbors;
	}

	private boolean canGrow(Level level, BlockPos pos, TraitsManager.TypedImpl traits)
	{
		if (!enoughLight(level, pos, traits.getTrait(CropTraitsTypes.LIGHT_SENSITIVITY)))
			return false;
		if (!enoughWater(level, pos, traits.getTrait(CropTraitsTypes.WATER_SENSITIVITY)))
			return false;
		if (!rightSoil(level, pos, traits.type))
			return false;
		return rightTemperature(level, pos, traits.type, traits.getTrait(CropTraitsTypes.TEMPERATURE_TOLERANCE));
	}

	public String[] canGrowString(Level level, BlockPos pos)
	{
		String[] messages = new String[5];
		if (level.getBlockEntity(pos) instanceof CropsBlockEntity cbe)
		{
			TraitsManager.TypedImpl traits = cbe.getTraits();
			if (!enoughLight(level, pos, traits.getTrait(CropTraitsTypes.LIGHT_SENSITIVITY)))
				messages[1] = "planttech2.tip.crop.no_light";
			if (!enoughWater(level, pos, traits.getTrait(CropTraitsTypes.WATER_SENSITIVITY)))
				messages[2] = "planttech2.tip.crop.no_water";
			if (!rightSoil(level, pos, traits.type))
				messages[3] = "planttech2.tip.crop.no_soil";
			if (!rightTemperature(level, pos, traits.type, traits.getTrait(CropTraitsTypes.TEMPERATURE_TOLERANCE)))
				messages[4] = "planttech2.tip.crop.no_temperature";
		}
		else
			messages[0] = "error";
		return messages;
	}

	public boolean enoughLight(Level level, BlockPos pos, int lightsensitivity)
	{
		if (!level.isAreaLoaded(pos, 1)) // prevent loading unloaded chunks
			return false;
		return level.getMaxLocalRawBrightness(pos, 0) >= 14 - lightsensitivity;
	}

	public static boolean enoughWater(Level level, BlockPos pos, int waterSensitivity)
	{
		waterSensitivity++;
		for (BlockPos blockpos$mutableblockpos : BlockPos.betweenClosed(pos.offset(-waterSensitivity, -1, -waterSensitivity), pos.offset(waterSensitivity, -1, waterSensitivity)))
		{
			BlockState state = level.getBlockState(blockpos$mutableblockpos);
			if (state.getFluidState().is(FluidTags.WATER) || (state.hasProperty(BlockStateProperties.WATERLOGGED) && state.getValue(BlockStateProperties.WATERLOGGED)))
				return true;
		}
		return false;
	}

	public static boolean rightSoil(Level level, BlockPos pos, CropTypes type)
	{
		Block block = type.getConfig().getSoil().get();
		Block soil = level.getBlockState(pos.below()).getBlock();
		return soil == block || soil == ModBlocks.UNIVERSAL_SOIL_INFUSED.get() || (soil == Blocks.GRASS_BLOCK && block == Blocks.DIRT);
	}

	public static boolean rightTemperature(Level level, BlockPos pos, CropTypes type, int tolerance)
	{
		return type.getConfig().getTemperature().inRange(level.getBiome(pos).value().getModifiedClimateSettings().temperature(), tolerance * 0.45F);
	}

	@Override
	protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder)
	{
		builder.add(GROWSTATE).add(COPPER);
	}

	@Override
	public boolean onDestroyedByPlayer(BlockState state, Level world, BlockPos pos, Player player, boolean willHarvest, FluidState fluid)
	{
		return willHarvest && !player.isCreative() || super.onDestroyedByPlayer(state, world, pos, player, willHarvest, fluid);
	}

	@Override
	public void playerDestroy(Level level, Player player, BlockPos pos, BlockState state, BlockEntity te, ItemStack stack)
	{
		super.playerDestroy(level, player, pos, state, te, stack);
		level.destroyBlock(pos, false);
	}

	@SuppressWarnings("deprecation")
	@Override
	public InteractionResult use(BlockState state, Level levelIn, BlockPos pos, Player player, InteractionHand hand, BlockHitResult ray)
	{
		if (hand.equals(InteractionHand.MAIN_HAND) && !levelIn.isClientSide)
		{
			ItemStack stack = player.getItemInHand(hand);
			if (ModItems.CATALYSTS.get().containsKey(stack.getItem()))
			{
				if (levelIn.getBlockEntity(pos) instanceof CropsBlockEntity te)
				{
					Pair<CropTraitsTypes, Integer> pair = te.chanceBoosts;
					Pair<Item, Pair<CropTraitsTypes, Integer>> type = ModItems.CATALYSTS.get().get(stack.getItem());
					if (pair.getA() == null || pair.getA() == type.getB().getA())
					{
						pair.setA(type.getB().getA());
						if (pair.getB() < 20)
						{
							pair.mapB((b) -> Math.min(20, b + type.getB().getB() * 2 + 1));
							if (!player.isCreative())
								stack.shrink(1);
							player.sendSystemMessage(Component.translatable("planttech2.block.crop.catalyst_used", pair.getB() + "%", pair.getA().getDisplayName()));
							return InteractionResult.SUCCESS;
						}
						else
						{
							player.sendSystemMessage(Component.translatable("planttech2.block.crop.catalyst_use_failed"));
							return InteractionResult.FAIL;
						}
					}
					else
					{
						player.sendSystemMessage(Component.translatable("planttech2.block.crop.catalyst_use_exists", pair.getA().getDisplayName()));
						return InteractionResult.FAIL;
					}
				}
			}
			else
			{
				int growstate = state.getValue(GROWSTATE);
				if (growstate > 6)
				{
					ItemStack holdItem = player.getItemInHand(InteractionHand.MAIN_HAND);
					if (!holdItem.isEmpty())
					{
						if (holdItem.getItem() instanceof AnalyserItem || holdItem.getItem() instanceof AdvancedAnalyserItem || holdItem.getItem() instanceof CropRemover)
							return InteractionResult.CONSUME;
					}
					NonNullList<ItemStack> drops = NonNullList.create();
					if (levelIn.getBlockEntity(pos) instanceof CropsBlockEntity cbe)
					{
						cbe.dropsRemoveOneSeed(drops, growstate);
						for (ItemStack stack2 : drops)
							popResource(levelIn, pos, stack2);
						levelIn.setBlockAndUpdate(pos, state.setValue(GROWSTATE, 0));
						return InteractionResult.SUCCESS;
					}
				}
			}
		}
		return super.use(state, levelIn, pos, player, hand, ray);
	}

	@Override
	public List<ItemStack> getDrops(BlockState state, LootParams.Builder builder)
	{
		List<ItemStack> drops = Lists.newArrayList();
		int age = state.getValue(GROWSTATE);
		Vec3 vec3d = builder.getOptionalParameter(LootContextParams.ORIGIN);
		if (vec3d != null)
		{
			BlockPos pos = BlockPos.containing(vec3d);
			if (builder.getLevel().getBlockEntity(pos) instanceof CropsBlockEntity cbe)
			{
				cbe.addDrops(drops, age);
				drops.add(new ItemStack(ModBlocks.CROPBARS.get()));
			}
		}
		return drops; 
	}

	@Override
	public void entityInside(BlockState state, Level world, BlockPos pos, Entity entity)
	{
		if (entity instanceof Bee)
			if (world.random.nextFloat() <= 0.005F)
				updateCrop(world, pos);
	}

	/*----------------------RENDERING------------------*/

	@Override
	public RenderShape getRenderShape(BlockState state)
	{
		return RenderShape.MODEL;
	}

	public CropTypes getCropType()
	{
		return cropType;
	}

	@Nullable
	@Override
	public BlockEntity newBlockEntity(BlockPos blockPos, BlockState blockState)
	{
		return new CropsBlockEntity(blockPos, blockState);
	}

	@Nullable
	@Override
	public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level level, BlockState p_153213_, BlockEntityType<T> p_153214_)
	{
		return level.isClientSide() ? null : CropsBlockEntity::tick;
	}

	public static class ColorHandler implements BlockColor
	{
		@Override
		public int getColor(BlockState state, BlockAndTintGetter blockDisplayReader, BlockPos pos, int tintindex)
		{
			if (tintindex == 0)
				return ((CropBaseBlock) state.getBlock()).getCropType().getSeedColor();
			return 0xFFFFFFFF;
		}
	}
}
