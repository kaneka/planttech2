package net.kaneka.planttech2.blocks;

import net.kaneka.planttech2.items.CropSeedItem;
import net.kaneka.planttech2.registries.ModItems;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraftforge.common.Tags;


public class CropBarsBlock extends Block
{
	public CropBarsBlock()
	{
		super(Block.Properties.of().sound(SoundType.WOOD).noCollission().noOcclusion());
		registerDefaultState(defaultBlockState().setValue(CropBaseBlock.COPPER, false));
	}


	@Override
	public VoxelShape getShape(BlockState p_60555_, BlockGetter p_60556_, BlockPos p_60557_, CollisionContext p_60558_)
	{
		return p_60558_.isHoldingItem(ModItems.RAKE.get()) ? Shapes.empty() : super.getShape(p_60555_, p_60556_, p_60557_, p_60558_);
	}

	@Override
	protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder)
	{
		builder.add(CropBaseBlock.COPPER);
	}

	@Override
	public InteractionResult use(BlockState state, Level level, BlockPos pos, Player player, InteractionHand hand, BlockHitResult hit) 
    {
    	ItemStack stack = player.getMainHandItem();
    	if (!level.isClientSide)
		{
			if (CropSeedItem.plant(level, pos, stack))
			{
				if (!player.getAbilities().instabuild)
					stack.shrink(1);
				return InteractionResult.SUCCESS;
			}
			else if (stack.is(Tags.Items.INGOTS_COPPER) && !state.getValue(CropBaseBlock.COPPER))
			{
				level.setBlockAndUpdate(pos, state.setValue(CropBaseBlock.COPPER, true));
				if (!player.isCreative())
					stack.shrink(1);
				return InteractionResult.SUCCESS;
			}
		}
    	return InteractionResult.PASS;
    }
	
	@Override
	public boolean propagatesSkylightDown(BlockState state, BlockGetter reader, BlockPos pos)
	{
		return true; 
	}

	@Override
	public boolean canSurvive(BlockState state, LevelReader level, BlockPos pos)
	{
		return level.getBlockState(pos.below()).isFaceSturdy(level, pos, Direction.UP);
	}
}
