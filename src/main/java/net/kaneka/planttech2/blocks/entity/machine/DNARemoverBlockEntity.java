package net.kaneka.planttech2.blocks.entity.machine;

import net.kaneka.planttech2.blocks.entity.machine.baseclasses.ConvertEnergyInventoryBlockEntity;
import net.kaneka.planttech2.hashmaps.TraitsManager;
import net.kaneka.planttech2.inventory.DNARemoverMenu;
import net.kaneka.planttech2.inventory.PT2ItemStackHandler;
import net.kaneka.planttech2.registries.ModBlockEntities;
import net.kaneka.planttech2.registries.ModBlocks;
import net.kaneka.planttech2.registries.ModItems;
import net.kaneka.planttech2.utilities.PlantTechConstants;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.state.BlockState;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.IntConsumer;
import java.util.function.IntSupplier;

public class DNARemoverBlockEntity extends ConvertEnergyInventoryBlockEntity
{
	protected final ContainerData data = createContainerData(new IntSupplier[]{
			() -> energyStorage.getEnergyStored(),
			() -> energyStorage.getMaxEnergyStored(),
			() -> ticksPassed }, new IntConsumer[] {
			(value) -> energyStorage.setEnergyStored(value),
			(value) -> energyStorage.setEnergyMaxStored(value),
			(value) -> ticksPassed = value,
	});

	public DNARemoverBlockEntity()
	{
		this(BlockPos.ZERO, ModBlocks.DNA_REMOVER.get().defaultBlockState());
	}

	public DNARemoverBlockEntity(BlockPos pos, BlockState state)
	{
		super(ModBlockEntities.DNAREMOVER_TE.get(), pos, state, 1000, 6, PlantTechConstants.MACHINETIER_DNA_REMOVER);
	}

	@Override
	protected PT2ItemStackHandler createInsertables()
	{
		return new PT2ItemStackHandler(itemhandler, ofDNAContainer(getInputSlotIndex(), false));
	}

	@Override
	protected PT2ItemStackHandler createExtractables()
	{
		return new PT2ItemStackHandler(itemhandler, PT2ItemStackHandler.Handler.of(getOutputSlotIndex()));
	}

	@Override
	protected boolean canProceed(ItemStack input, ItemStack output)
	{
		return !input.isEmpty() && input.hasTag() && !getRemovableTraits(input).isEmpty();
	}

	@Override
	protected ItemStack getResult(ItemStack input, ItemStack output)
	{
		List<String> traitsList = getRemovableTraits(input);
		Collections.shuffle(traitsList);
		CompoundTag nbt = input.getOrCreateTag().copy();
		nbt.remove(traitsList.get(0));
		ItemStack stack = new ItemStack(ModItems.DNA_CONTAINER.get());
		stack.setTag(nbt);
		return stack;
	}

	@Override
	public ContainerData getContainerData()
	{
		return data;
	}

	private List<String> getRemovableTraits(ItemStack stack)
	{
		List<String> list = new ArrayList<>();
		CompoundTag nbt = stack.getOrCreateTag();
		for (String key : TraitsManager.TypedImpl.mutableTraitKeys())
			if (nbt.contains(key))
				list.add(key);
		return list;
	}

	@Override
	public String getNameString()
	{
		return "dnaremover";
	}

	@Override
	public AbstractContainerMenu createMenu(int id, Inventory inv, Player player)
	{
		return new DNARemoverMenu(id, inv, this);
	}

	@Override
	public int getEnergyInSlot()
	{
		return 3;
	}

	@Override
	public int getEnergyOutSlot()
	{
		return 4;
	}

	@Override
	public int getKnowledgeChipSlot()
	{
		return 5;
	}

	@Override
	public int getKnowledgePerAction()
	{
		return 50;
	}

	@Override
	public int getUpgradeSlot()
	{
		return 2;
	}
}
