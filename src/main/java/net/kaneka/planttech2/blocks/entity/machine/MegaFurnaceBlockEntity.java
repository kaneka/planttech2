package net.kaneka.planttech2.blocks.entity.machine;


import net.kaneka.planttech2.blocks.entity.machine.baseclasses.EnergyInventoryBlockEntity;
import net.kaneka.planttech2.inventory.MegaFurnaceMenu;
import net.kaneka.planttech2.inventory.PT2ItemStackHandler;
import net.kaneka.planttech2.registries.ModBlockEntities;
import net.kaneka.planttech2.registries.ModBlocks;
import net.kaneka.planttech2.utilities.PlantTechConstants;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraft.world.item.crafting.SmeltingRecipe;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.items.ItemStackHandler;
import net.minecraftforge.items.wrapper.RecipeWrapper;

import java.util.Optional;
import java.util.function.IntConsumer;
import java.util.function.IntSupplier;

public class MegaFurnaceBlockEntity extends EnergyInventoryBlockEntity
{
	public int[] ticksPassed = new int[6];
	boolean isSmelting;
	protected ItemStackHandler dummyitemhandler = new ItemStackHandler();
	
	protected final ContainerData data = createContainerData(new IntSupplier[]{
			() -> energyStorage.getEnergyStored(),
			() -> energyStorage.getMaxEnergyStored(),
			() -> ticksPassed[0],
			() -> ticksPassed[1],
			() -> ticksPassed[2],
			() -> ticksPassed[3],
			() -> ticksPassed[4],
			() -> ticksPassed[5], }, new IntConsumer[] {
			(value) -> energyStorage.setEnergyStored(value),
			(value) -> energyStorage.setEnergyMaxStored(value),
			(value) -> ticksPassed[0] = value,
			(value) -> ticksPassed[1] = value,
			(value) -> ticksPassed[2] = value,
			(value) -> ticksPassed[3] = value,
			(value) -> ticksPassed[4] = value,
			(value) -> ticksPassed[5] = value,
	});

	public MegaFurnaceBlockEntity()
	{
		this(BlockPos.ZERO, ModBlocks.MEGAFURNACE.get().defaultBlockState());
	}

	public MegaFurnaceBlockEntity(BlockPos pos, BlockState state)
	{
		super(ModBlockEntities.MEGAFURNACE_TE.get(), pos, state, 10000, 16, PlantTechConstants.MACHINETIER_MEGAFURNACE);
	}

	@Override
	protected PT2ItemStackHandler createInsertables()
	{
		return new PT2ItemStackHandler(itemhandler, 6);
	}

	@Override
	protected PT2ItemStackHandler createExtractables()
	{
		return new PT2ItemStackHandler(itemhandler, 6, 12);
	}

	@Override
	public void doUpdate()
	{
		super.doUpdate();
		isSmelting = false;
		for (int i = 0; i < 6; i++)
		{
			if (this.energyStorage.getEnergyStored() > this.energyPerAction())
			{
				if (this.canSmelt(i))
				{
					isSmelting = true;
					ticksPassed[i]++;
					if (ticksPassed[i] >= this.ticksPerItem())
					{
						this.smeltItem(i);
						ticksPassed[i] = 0;
						addKnowledge();
					}
				}
				else if (ticksPassed[i] > 0)
					ticksPassed[i] = 0;
			}
			else
			{
				if (!this.canSmelt(i) && ticksPassed[i] > 0)
					ticksPassed[i] = 0;
				break;
			}
		}
		if (isSmelting)
			this.energyStorage.extractEnergy(energyPerAction(), false);
	}
	
	@Override
	public ContainerData getContainerData()
	{
		return data;
	}

	private boolean canSmelt(int slot)
	{
		ItemStack itemstack = itemhandler.getStackInSlot(slot);
		if (itemstack.isEmpty())
			return false;
		else
		{
			
			ItemStack output = getOutput(slot);
			if (output.isEmpty())
				return false;
			else
			{
				ItemStack outputslot = itemhandler.getStackInSlot(slot + 6);
				if (outputslot.isEmpty())
					return true;
				else if (output.getItem() != outputslot.getItem())
					return false;
				else if (outputslot.getCount() + output.getCount() <= 64 && outputslot.getCount() + output.getCount() <= outputslot.getMaxStackSize())
					return true;
				else
					return outputslot.getCount() + output.getCount() <= output.getMaxStackSize();

			}
		}
	}
	
	public ItemStack getOutput(int slot)
	{
		if (level == null)
			return ItemStack.EMPTY;
		dummyitemhandler.setStackInSlot(0, itemhandler.getStackInSlot(slot));
		RecipeWrapper wrapper = new RecipeWrapper(dummyitemhandler);
		Optional<SmeltingRecipe> recipeopt = level.getRecipeManager().getRecipeFor(RecipeType.SMELTING, wrapper, level);
		SmeltingRecipe recipe = recipeopt.orElse(null);
		return recipe == null ? ItemStack.EMPTY : recipe.getResultItem(level.registryAccess());
	}

	public void smeltItem(int slot)
	{
		if (this.canSmelt(slot))
		{
			ItemStack itemstack = this.itemhandler.getStackInSlot(slot);
			ItemStack itemstack1 = getOutput(slot); 
			ItemStack itemstack2 = this.itemhandler.getStackInSlot(slot + 6);
			if (itemstack2.isEmpty())
				this.itemhandler.setStackInSlot(slot + 6, itemstack1.copy());
			else if (itemstack2.getItem() == itemstack1.getItem())
				itemstack2.grow(itemstack1.getCount());
			itemstack.shrink(1);
		}
	}

	@Override
	protected void saveAdditional(CompoundTag compound)
	{
	 	super.saveAdditional(compound);
		compound.putIntArray("cooktime", ticksPassed);
	}

	@Override
	public void load(CompoundTag compound)
	{
		super.load(compound);
		ticksPassed = compound.getIntArray("cooktime");
		if (ticksPassed.length == 0)
			ticksPassed = new int[6];
	}

	@Override
	public String getNameString()
	{
		return "megafurnace";
	}

	@Override
	public AbstractContainerMenu createMenu(int id, Inventory inv, Player player)
	{
		return new MegaFurnaceMenu(id, inv, this);
	}

	@Override
	public int getEnergyInSlot()
	{
		return 13;
	}

	@Override
	public int getEnergyOutSlot()
	{
		return 14;
	}

	@Override
	public int getKnowledgeChipSlot()
	{
		return 15;
	}

	@Override
	public int getKnowledgePerAction()
	{
		return 50;
	}

	@Override
	public int getUpgradeSlot()
	{
		return 12;
	}
}
