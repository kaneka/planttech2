package net.kaneka.planttech2.blocks.entity.machine;

import net.kaneka.planttech2.blocks.entity.machine.baseclasses.ConvertEnergyInventoryFluidBlockEntity;
import net.kaneka.planttech2.inventory.InfuserMenu;
import net.kaneka.planttech2.inventory.PT2ItemStackHandler;
import net.kaneka.planttech2.recipes.ModRecipeTypes;
import net.kaneka.planttech2.recipes.recipeclasses.InfuserRecipe;
import net.kaneka.planttech2.registries.ModBlockEntities;
import net.kaneka.planttech2.registries.ModBlocks;
import net.minecraft.core.BlockPos;
import net.minecraft.world.SimpleContainer;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.state.BlockState;

import javax.annotation.Nullable;
import java.util.function.IntConsumer;
import java.util.function.IntSupplier;

import static net.kaneka.planttech2.utilities.PlantTechConstants.MACHINETIER_INFUSER;

public class InfuserBlockEntity extends ConvertEnergyInventoryFluidBlockEntity
{
	private int fluidTotal = 0;
	private ItemStack output = null;

	protected final ContainerData data = createContainerData(new IntSupplier[]{
			() -> energyStorage.getEnergyStored(),
			() -> energyStorage.getMaxEnergyStored(),
			this::currentBiomass,
			this::getTankCapacity,
			() -> ticksPassed,
			() -> fluidTotal }, new IntConsumer[] {
			(value) -> energyStorage.setEnergyStored(value),
			(value) -> energyStorage.setEnergyMaxStored(value),
			this::forceSetStorage,
			this::setTankCapacity,
			(value) -> ticksPassed = value,
			(value) -> fluidTotal = value,
	});

	public InfuserBlockEntity()
	{
		this(BlockPos.ZERO, ModBlocks.INFUSER.get().defaultBlockState());
	}

	public InfuserBlockEntity(BlockPos pos, BlockState state)
	{
		super(ModBlockEntities.INFUSER_TE.get(), pos, state, 1000, 8, 5000, MACHINETIER_INFUSER);
	}

	@Override
	protected PT2ItemStackHandler createInsertables()
	{
		return new PT2ItemStackHandler(itemhandler, PT2ItemStackHandler.Handler.of(getInputSlotIndex()));
	}

	@Override
	protected PT2ItemStackHandler createExtractables()
	{
		return new PT2ItemStackHandler(itemhandler, PT2ItemStackHandler.Handler.of(getOutputSlotIndex()));
	}

	@Override
	protected boolean canProceed(ItemStack input, ItemStack output)
	{
		if (input.isEmpty())
			return false;
		InfuserRecipe recipe = getOutputRecipe();
		if (recipe == null)
			return false;
		if (this.output == null || this.output.isEmpty())
		{
			this.output = recipe.getOutput().copy();
			fluidTotal = recipe.getBiomass();
		}
		return ItemStack.isSameItem(recipe.getOutput(), this.output);
	}

	@Override
	protected void resetProgress(boolean forced)
	{
		super.resetProgress(forced);
		this.output = null;
		fluidTotal = 0;
	}

	@Override
	public int ticksPerItem()
	{
		return fluidTotal > 0 ? fluidTotal: 100;
	}

	@Override
	protected void increaseProgress()
	{
		ticksPassed += fluidPerAction();
	}
	
	@Override
	protected int remainingFluid()
	{
		return fluidTotal - ticksPassed;
	}

	@Override
	protected ItemStack getResult(ItemStack input, ItemStack output)
	{
		return getOutputRecipe() == null ? ItemStack.EMPTY : getOutputRecipe().getResultItem(level.registryAccess()).copy();
	}

	@Override
	protected boolean shouldResetProgressIfNotProcessing()
	{
		return false;
	}

	@Override
	public ContainerData getContainerData()
	{
		return data;
	}

	@Nullable
	private InfuserRecipe getOutputRecipe()
	{
		if (level == null)
			return null;
		return level.getRecipeManager().getRecipeFor(ModRecipeTypes.INFUSING.get(), new SimpleContainer(getInput()), level).orElse(null);
	}

	@Override
	public String getNameString()
	{
		return "infuser";
	}

	@Override
	public int getFluidInSlot()
	{
		return 3;
	}

	@Override
	public int getFluidOutSlot()
	{
		return 4;
	}
	
	@Override
	public AbstractContainerMenu createMenu(int id, Inventory inv, Player player)
	{
		return new InfuserMenu(id, inv, this);
	}

	@Override
	public int getEnergyInSlot()
	{
		return 5;
	}

	@Override
	public int getEnergyOutSlot()
	{
		return 6;
	}

	@Override
	public int getKnowledgeChipSlot()
	{
		return 7;
	}

	@Override
	public int getKnowledgePerAction()
	{
		return 150;
	}

	@Override
	public int getUpgradeSlot()
	{
		return 2;
	}
}
