package net.kaneka.planttech2.blocks.entity.machine;

import net.kaneka.planttech2.blocks.entity.machine.baseclasses.ConvertEnergyInventoryBlockEntity;
import net.kaneka.planttech2.enums.CropTraitsTypes;
import net.kaneka.planttech2.hashmaps.TraitsManager;
import net.kaneka.planttech2.inventory.DNACombinerMenu;
import net.kaneka.planttech2.inventory.PT2ItemStackHandler;
import net.kaneka.planttech2.registries.ModBlockEntities;
import net.kaneka.planttech2.registries.ModBlocks;
import net.kaneka.planttech2.registries.ModItems;
import net.kaneka.planttech2.utilities.PlantTechConstants;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.state.BlockState;

import java.util.function.IntConsumer;
import java.util.function.IntSupplier;

public class DNACombinerBlockEntity extends ConvertEnergyInventoryBlockEntity
{
	protected final ContainerData data = createContainerData(new IntSupplier[]{
			() -> energyStorage.getEnergyStored(),
			() -> energyStorage.getMaxEnergyStored(),
			() -> ticksPassed }, new IntConsumer[] {
			(value) -> energyStorage.setEnergyStored(value),
			(value) -> energyStorage.setEnergyMaxStored(value),
			(value) -> ticksPassed = value,
	});

	public DNACombinerBlockEntity()
	{
		this(BlockPos.ZERO, ModBlocks.DNA_COMBINER.get().defaultBlockState());
	}

	public DNACombinerBlockEntity(BlockPos pos, BlockState state)
	{
		super(ModBlockEntities.DNACOMBINER_TE.get(), pos, state, 1000, 8, PlantTechConstants.MACHINETIER_DNA_COMBINER);
	}

	@Override
	protected PT2ItemStackHandler createInsertables()
	{
		return new PT2ItemStackHandler(itemhandler, ofDNAContainer(0, false), ofDNAContainer(1, false), ofDNAContainer(2, true));
	}

	@Override
	protected PT2ItemStackHandler createExtractables()
	{
		return new PT2ItemStackHandler(itemhandler, PT2ItemStackHandler.Handler.of(getOutputSlotIndex()));
	}


	@Override
	protected boolean canProceed(ItemStack input, ItemStack output)
	{
		return !input.isEmpty() && !getInput2().isEmpty() && !getInput3().isEmpty();
	}

	@Override
	protected ItemStack getResult(ItemStack input, ItemStack output)
	{
		CompoundTag nbt = getCombinedNBT(getInput2(), getInput3());
		ItemStack stack = new ItemStack(ModItems.DNA_CONTAINER.get());
		stack.setTag(nbt);
		return stack;
	}

	private ItemStack getInput2()
	{
		return itemhandler.getStackInSlot(0);
	}

	private ItemStack getInput3()
	{
		return itemhandler.getStackInSlot(1);
	}

	@Override
	public ContainerData getContainerData()
	{
		return data;
	}

	private CompoundTag getCombinedNBT(ItemStack stack, ItemStack stack2)
	{
		TraitsManager.ItemImpl trait = TraitsManager.ItemImpl.of(stack);
		TraitsManager.ItemImpl trait2 = TraitsManager.ItemImpl.of(stack2);
		TraitsManager.TypedImpl combined = new TraitsManager.TypedImpl();
		if (trait.type == trait2.type)
			combined.type = trait.type;
		else
			combined.type = trait.type != null ? trait.type : trait2.type;
		for (CropTraitsTypes cropTrait : CropTraitsTypes.values())
		{
			boolean hasT = trait.hasTrait(cropTrait);
			boolean hasT2 = trait2.hasTrait(cropTrait);
			int t = trait.getTrait(cropTrait);
			int t2 = trait2.getTrait(cropTrait);
			if (hasT && hasT2)
				combined.setTrait(cropTrait, Math.min(t, t2));
			else if (hasT)
				combined.setTrait(cropTrait, t);
			else if (hasT2)
				combined.setTrait(cropTrait, t2);
		}
		return combined.serializeNBT();
	}

	@Override
	public String getNameString()
	{
		return "dnacombiner";
	}

	@Override
	public AbstractContainerMenu createMenu(int id, Inventory inv, Player player)
	{
		return new DNACombinerMenu(id, inv, this);
	}

	@Override
	public int getInputSlotIndex()
	{
		return 2;
	}

	@Override
	public int getOutputSlotIndex()
	{
		return 3;
	}

	@Override
	public int getEnergyInSlot()
	{
		return 5;
	}

	@Override
	public int getEnergyOutSlot()
	{
		return 6;
	}

	@Override
	public int getKnowledgeChipSlot()
	{
		return 7;
	}

	@Override
	public int getKnowledgePerAction()
	{
		return 50;
	}

	@Override
	public int getUpgradeSlot()
	{
		return 4;
	}
}
