package net.kaneka.planttech2.blocks.entity.machine.baseclasses;

import net.kaneka.planttech2.fluids.PTBiomassFluidHandler;
import net.kaneka.planttech2.registries.ModFluids;
import net.kaneka.planttech2.utilities.PTCommonUtil;
import net.kaneka.planttech2.utilities.PlantTechConstants;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.item.BucketItem;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandlerItem;

public abstract class EnergyInventoryFluidBlockEntity extends EnergyInventoryBlockEntity implements PTBiomassFluidHandler
{
	private int biomass = 0;
	private int biomassCapacity;
	protected LazyOptional<IFluidHandler> biomassCap;

	public EnergyInventoryFluidBlockEntity(BlockEntityType<?> type, BlockPos pos, BlockState state, int energyStorage, int invSize, int maxBiomassStorage, int tier)
    {
		super(type, pos, state, energyStorage, invSize, tier);
		this.biomassCapacity = maxBiomassStorage;
		biomassCap = LazyOptional.of(() -> this);
    }

	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		return capability == ForgeCapabilities.FLUID_HANDLER ? biomassCap.cast() : super.getCapability(capability, facing);
	}


	@Override
	public void doUpdate()
	{
		super.doUpdate();
		doFluidLoop();
	}

	public void doFluidLoop()
    {
		ItemStack stack = itemhandler.getStackInSlot(getFluidInSlot());
		ItemStack stack2 = itemhandler.getStackInSlot(getFluidOutSlot());
		if (currentBiomass() < getTankCapacity())
		{
			PTCommonUtil.tryAccessBiomass(stack, (storage, index) -> {
				if (storage instanceof IFluidHandlerItem cap)
					if (fill(cap.drain(new FluidStack(ModFluids.BIOMASS.get(), stack.getItem() instanceof BucketItem ? storage.getTankCapacity(index) : 4), IFluidHandler.FluidAction.EXECUTE), FluidAction.EXECUTE) > 0)
						itemhandler.setStackInSlot(getFluidInSlot(), cap.getContainer());
			});
		}
		PTCommonUtil.tryAccessBiomass(stack2, (storage, index) -> {
			if (storage instanceof IFluidHandlerItem cap)
				if (drain(cap.fill(new FluidStack(ModFluids.BIOMASS.get(), Math.min(stack2.getItem() instanceof BucketItem ? storage.getTankCapacity(index) : 4, currentBiomass())), FluidAction.EXECUTE), FluidAction.EXECUTE).getAmount() > 0)
					itemhandler.setStackInSlot(getFluidOutSlot(), cap.getContainer());
		});
    }

	@Override
	protected void saveAdditional(CompoundTag compound)
	{
	 	super.saveAdditional(compound);
		compound.putInt(PlantTechConstants.BIOMASS_TAG, currentBiomass());
		compound.putInt("biomassCapacity", getTankCapacity());

	}

	@Override
	public void load(CompoundTag compound)
	{
		super.load(compound);
		biomass = compound.getInt(PlantTechConstants.BIOMASS_TAG);
		int c = compound.getInt("biomassCapacity");
		if (c > 0)
			biomassCapacity = c;
	}

	public abstract int getFluidInSlot();
    
    public abstract int getFluidOutSlot();

	@Override
	public void forceSetStorage(int amount)
	{
		biomass = amount;
		setChanged();
	}

	@Override
	public boolean validCOntainer()
	{
		return !isRemoved();
	}

	@Override
	public int currentBiomass()
	{
		return biomass;
	}

	public void setTankCapacity(int amount)
	{
		biomassCapacity = amount;
	}

	public final int getTankCapacity()
	{
		return getTankCapacity(0);
	}

	@Override
	public int getTankCapacity(int tank)
	{
		return biomassCapacity;
	}
}
