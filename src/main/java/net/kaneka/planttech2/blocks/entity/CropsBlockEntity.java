package net.kaneka.planttech2.blocks.entity;

import net.kaneka.planttech2.addons.Pair;
import net.kaneka.planttech2.blocks.CropBaseBlock;
import net.kaneka.planttech2.configuration.PlantTech2Configuration;
import net.kaneka.planttech2.entities.passive.plant_mite.PlantMiteEntity;
import net.kaneka.planttech2.entities.passive.snail.SnailEntity;
import net.kaneka.planttech2.enums.CropTraitsTypes;
import net.kaneka.planttech2.hashmaps.TraitsManager;
import net.kaneka.planttech2.registries.ModBlockEntities;
import net.kaneka.planttech2.registries.ModEntityTypes;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.NonNullList;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.Connection;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.Vec3;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class CropsBlockEntity extends BlockEntity
{
	private long startTick = 0;
	private TraitsManager.TypedImpl traits = new TraitsManager.TypedImpl();
	private boolean isSick = false;
	private int sickCheckTick = 0;
	private int lastSnailSpawn = 0;
	private int lastMiteSpawn = 0;
	public final Pair<CropTraitsTypes, Integer> chanceBoosts = Pair.of(null, 0);

	public CropsBlockEntity(BlockPos pos, BlockState state)
	{
		super(ModBlockEntities.CROPS_TE.get(), pos, state);
	}

	public static void tick(Level level, BlockPos pos, BlockState state, BlockEntity be)
	{
		if (be instanceof CropsBlockEntity cbe)
		{
			long time = level.getGameTime() - cbe.startTick;
			boolean update = false;
			if (state.getValue(CropBaseBlock.GROWSTATE) < 7)
			{
				if (time % ((90L - cbe.traits.getTrait(CropTraitsTypes.GROW_SPEED) * 6L) * 20L) == 0L)
					update = true;
			}
			else
			{
				if (time % ((80L - cbe.traits.getTrait(CropTraitsTypes.SPREADING_SPEED) * 6L) * 20L) == 0L)
					update = true;
			}
			if (update)
			{
				Block block = level.getBlockState(cbe.worldPosition).getBlock();
				if (block instanceof CropBaseBlock cbb)
					cbb.updateCrop(level, cbe.worldPosition);
			}
			if (cbe.lastSnailSpawn-- <= 0)
			{
				cbe.lastSnailSpawn = 400 + level.random.nextInt(1000);
				float spawnChance = 0.001F;
				if (level.isRainingAt(pos))
					spawnChance += 0.004F;
				if (level.isNight())
					spawnChance += 0.003F;
				if (level.random.nextFloat() <= spawnChance * PlantTech2Configuration.SNAIL_SPAWN_CHANCE_MULTIPLIER.get())
				{
					SnailEntity snail = new SnailEntity(ModEntityTypes.SNAIL.get(), level);
					snail.setRandomVariant();
					snail.setPos(Vec3.atLowerCornerWithOffset(pos, level.random.nextFloat(), level.random.nextFloat() * 0.5F, level.random.nextFloat()));
					snail.setYRot(level.random.nextFloat() * 360.0F);
					level.addFreshEntity(snail);
				}
			}
			if (cbe.lastMiteSpawn-- <= 0)
			{
				cbe.lastMiteSpawn = 200 + level.random.nextInt(1000);
				float spawnChance = 0.001F;
				if (level.random.nextFloat() <= spawnChance * PlantTech2Configuration.MITE_SPAWN_CHANCE_MULTIPLIER.get())
				{
					PlantMiteEntity mite = new PlantMiteEntity(ModEntityTypes.PLANT_MITE.get(), level);
					mite.setPos(Vec3.atLowerCornerWithOffset(pos, level.random.nextFloat(), level.random.nextFloat() * 0.5F, level.random.nextFloat()));
					mite.setYRot(level.random.nextFloat() * 360.0F);
					level.addFreshEntity(mite);
				}
			}
			if (cbe.sickCheckTick-- <= 0)
			{
				cbe.sickCheckTick = 24000 + level.random.nextInt(2400);
				if (!cbe.isSick())
					cbe.tryInfect();
				else
				{
					if (level.random.nextBoolean())
					{
						if (state.getValue(CropBaseBlock.GROWSTATE) > 1)
							level.setBlockAndUpdate(pos, state.setValue(CropBaseBlock.GROWSTATE, state.getValue(CropBaseBlock.GROWSTATE) - 1));
					}
					else
					{
						Direction.Plane.HORIZONTAL.stream().forEach((d) -> {
							if (level.getBlockEntity(pos.relative(d)) instanceof CropsBlockEntity crop)
								crop.tryInfect();
						});
					}
				}
			}
		}
	}

	public boolean isSick()
	{
		return isSick;
	}

	public void setSick(boolean sick)
	{
		isSick = sick;
		if (level instanceof ServerLevel)
		{
			setChanged();
			level.sendBlockUpdated(worldPosition, getBlockState(), getBlockState(), 3);
		}
	}

	private void tryInfect()
	{
		if (level.random.nextFloat() <= (CropTraitsTypes.GENE_STRENGTH.getMax() - getTraits().getTrait(CropTraitsTypes.GENE_STRENGTH)) * 0.001F)
			setSick(true);
	}

	public void setTraits(TraitsManager.TypedImpl traits)
	{
		this.traits = traits;
	}
	
	public boolean isAnalysed()
	{
		return traits.analysed;
	}
	
	public TraitsManager.TypedImpl setAnalysedAndGetTraits()
	{
		traits.analysed = true;
		return getTraits(); 
	}

	public TraitsManager.TypedImpl getTraits()
	{
		return this.traits;
	}

	public void setStartTick()
	{
		this.startTick = level.getGameTime();
	}

	public List<ItemStack> addDrops(List<ItemStack> drops, int growstate)
	{
		return traits.type.calculateDrops(drops, this.traits, growstate, level.random);
	}

	public void dropsRemoveOneSeed(NonNullList<ItemStack> drops, int growstate)
	{
		traits.type.calculateDropsReduced(drops, this.traits, growstate, level.random);
	}

	@Nullable
	@Override
	public Packet<ClientGamePacketListener> getUpdatePacket()
	{
		return ClientboundBlockEntityDataPacket.create(this);
	}

	@Override
	public void onDataPacket(Connection net, ClientboundBlockEntityDataPacket pkt)
	{
		handleUpdateTag(pkt.getTag());
	}

	@Override
	public CompoundTag getUpdateTag()
	{
		return saveWithoutMetadata();
	}

	@Override
	public void handleUpdateTag(CompoundTag tag)
	{
		if (tag != null)
			load(tag);
	}

	@Override
	protected void saveAdditional(CompoundTag compound)
	{
		super.saveAdditional(compound);
		compound.putLong("starttick", this.startTick);
		compound.merge(traits.serializeNBT());
		compound.putInt("lastSnailSpawn", lastSnailSpawn);
		compound.putInt("lastMiteSpawn", lastMiteSpawn);
		compound.putBoolean("isSick", isSick);
		compound.putInt("sickCheckTick", sickCheckTick);
		if (chanceBoosts.getA() != null)
		{
			compound.putString("chanceBoostsA", chanceBoosts.getA().name());
			compound.putInt("chanceBoostsB", chanceBoosts.getB());
		}
	}
	
	@Override
	public void load(CompoundTag compound)
	{
		super.load(compound);
		this.startTick = compound.getLong("starttick");
		this.traits.deserializeNBT(compound);
		this.lastSnailSpawn = compound.getInt("lastSnailSpawn");
		this.lastMiteSpawn = compound.getInt("lastMiteSpawn");
		this.isSick = compound.getBoolean("isSick");
		this.sickCheckTick = compound.getInt("sickCheckTick");
		if (compound.contains("chanceBoostsA"))
			this.chanceBoosts.set(CropTraitsTypes.valueOf(compound.getString("chanceBoostsA")), compound.getInt("chanceBoostsB"));
	}
}
