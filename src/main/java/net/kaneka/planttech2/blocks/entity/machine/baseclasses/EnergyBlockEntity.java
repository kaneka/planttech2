package net.kaneka.planttech2.blocks.entity.machine.baseclasses;

import net.kaneka.planttech2.energy.BioEnergyStorage;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.util.RandomSource;
import net.minecraft.world.MenuProvider;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.energy.IEnergyStorage;

import java.util.function.IntConsumer;
import java.util.function.IntSupplier;

public abstract class EnergyBlockEntity extends BlockEntity implements MenuProvider
{
	protected BioEnergyStorage energyStorage;
	private LazyOptional<IEnergyStorage> energyCap;
	protected final RandomSource rand = RandomSource.create();

	public EnergyBlockEntity(BlockEntityType<?> type, BlockPos pos, BlockState state, int energyStorage)
	{
		super(type, pos, state);
		createEnergyStorage(energyStorage, false);
	}

	public void createEnergyStorage(int maxEnergy, boolean inherit)
	{
		BioEnergyStorage storage = new BioEnergyStorage(maxEnergy);
		if (inherit && this.energyStorage != null)
			storage.receiveEnergy(energyStorage.getEnergyStored());
		energyStorage = storage;
		energyCap = LazyOptional.of(() -> this.energyStorage);
	}

	public static ContainerData createContainerData(IntSupplier[] get, IntConsumer[] set)
	{
		return new ContainerData()
		{
			@Override
			public int get(int index)
			{
				return get[index].getAsInt();
			}

			@Override
			public void set(int index, int value)
			{
				set[index].accept(value);
			}

			@Override
			public int getCount()
			{
				return Math.min(get.length, set.length);
			}
		};
	}

	public static void tick(Level level, BlockPos pos, BlockState state, BlockEntity be)
	{
		if (be instanceof EnergyBlockEntity ebe)
			ebe.doUpdate();
	}

	@Override
	public CompoundTag getUpdateTag()
	{
		return saveWithoutMetadata();
	}

	public void doUpdate()
	{
	}
	
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		return capability == ForgeCapabilities.ENERGY ? energyCap.cast() : super.getCapability(capability, facing);
	}

	@Override
	protected void saveAdditional(CompoundTag compound)
	{
	 	super.saveAdditional(compound);
		compound.put("energy", this.energyStorage.serializeNBT());
	}

	@Override
	public void load(CompoundTag compound)
	{
		super.load(compound);
		this.energyStorage.deserializeNBT(compound.getCompound("energy"));
	}

	public String getNameString()
	{
		return "default";
	}

	public int getEnergyStored()
	{
		return this.energyStorage.getEnergyStored();
	}

	public int getMaxEnergyStored()
	{
		return this.energyStorage.getMaxEnergyStored();
	}

	public boolean isUsableByPlayer(Player player)
	{
		if (level == null || this.level.getBlockEntity(worldPosition) != this)
			return false;
		return player.distanceToSqr((double) this.worldPosition.getX() + 0.5D, (double) this.worldPosition.getY() + 0.5D, (double) this.worldPosition.getZ() + 0.5D) <= 64.0D;
	}

	public int getUpgradeSlot()
	{
		return -1;
	}

	public abstract ContainerData getContainerData();
	
	@Override
	public Component getDisplayName()
	{
		return Component.translatable("container." + getNameString());
	}

	public void notifyClient()
	{
		if (level != null && !level.isClientSide())
		{
			BlockState state = level.getBlockState(getBlockPos());
			level.sendBlockUpdated(getBlockPos(), state, state, 3);
		}
	}

	/**
	 * Whether this block entity should be marked as changed (sync to client)
	 * on initial interaction of a player
	 * @return if sync is needed
	 */
	public boolean requireSyncUponOpen()
	{
		return false;
	}
}
