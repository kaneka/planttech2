package net.kaneka.planttech2.blocks.entity.machine;

import net.kaneka.planttech2.blocks.entity.machine.baseclasses.EnergyInventoryBlockEntity;
import net.kaneka.planttech2.crops.CropTypes;
import net.kaneka.planttech2.hashmaps.TraitsManager;
import net.kaneka.planttech2.inventory.IdentifierMenu;
import net.kaneka.planttech2.inventory.PT2ItemStackHandler;
import net.kaneka.planttech2.items.CropSeedItem;
import net.kaneka.planttech2.registries.ModBlockEntities;
import net.kaneka.planttech2.registries.ModBlocks;
import net.kaneka.planttech2.utilities.PlantTechConstants;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.state.BlockState;

import java.util.function.IntConsumer;
import java.util.function.IntSupplier;

public class IdentifierBlockEntity extends EnergyInventoryBlockEntity
{
	protected final ContainerData data = createContainerData(new IntSupplier[]{
			() -> energyStorage.getEnergyStored(),
			() -> energyStorage.getMaxEnergyStored(),
			() -> ticksPassed }, new IntConsumer[] {
			(value) -> energyStorage.setEnergyStored(value),
			(value) -> energyStorage.setEnergyMaxStored(value),
			(value) -> ticksPassed = value,
	});

	public IdentifierBlockEntity()
	{
		this(BlockPos.ZERO, ModBlocks.IDENTIFIER.get().defaultBlockState());
	}

	public IdentifierBlockEntity(BlockPos pos, BlockState state)
	{
		super(ModBlockEntities.IDENTIFIER_TE.get(), pos, state, 10000, 22, PlantTechConstants.MACHINETIER_IDENTIFIER);
	}

	@Override
	protected PT2ItemStackHandler createInsertables()
	{
		return new PT2ItemStackHandler(itemhandler, 9, (handler) -> handler.setConditions((stack) -> stack.getItem() instanceof CropSeedItem));
	}

	@Override
	protected PT2ItemStackHandler createExtractables()
	{
		return new PT2ItemStackHandler(itemhandler, 9, 18);
	}

	@Override
	public void doUpdate()
	{
		super.doUpdate();
		if (this.energyStorage.getEnergyStored() > energyPerAction())
		{
			if (this.canIdentify())
			{
				this.energyStorage.extractEnergy(energyPerAction(), false);
				ticksPassed++;
				if (ticksPassed >= ticksPerItem())
				{
					this.identifyItem();
					ticksPassed = 0;
				}
			}
			else
				ticksPassed = 0;
		}
		else if (!this.canIdentify() && ticksPassed > 0)
			ticksPassed = 0;
	}

	@Override
	public ContainerData getContainerData()
	{
		return data;
	}

	private boolean canIdentify()
	{
		if (!this.hasFreeOutputSlot())
			return false;
		for (int i = 0; i < 9; i++)
		{
			ItemStack stack = this.itemhandler.getStackInSlot(i);
			if (!stack.isEmpty())
			{
				CompoundTag nbt = stack.getTag();
				if (nbt != null)
				{
					if (nbt.contains("analysed"))
						if (!nbt.getBoolean("analysed"))
							return true;
				}
				else
					return CropTypes.getBySeed(stack).isPresent();
			}
		}
		return false;
	}

	public void identifyItem()
	{
		if (this.canIdentify())
		{
			for (int i = 0; i < 9; i++)
			{
				ItemStack stack = this.itemhandler.getStackInSlot(i);
				if (!stack.isEmpty())
				{
					CompoundTag nbt = stack.getTag();
					if (nbt != null)
					{
						if (nbt.contains("analysed"))
						{
							if (!nbt.getBoolean("analysed"))
							{
								nbt.putBoolean("analysed", true);
								stack.setTag(nbt);
								this.itemhandler.setStackInSlot(this.getFreeOutputSlot(), stack);
								this.itemhandler.setStackInSlot(i, ItemStack.EMPTY);
								addKnowledge();
								break;
							}
						}
					}
					else
					{
						int finalI = i;
						CropTypes.getBySeed(stack).ifPresent((entry) -> {
							TraitsManager.ItemImpl newtraits = TraitsManager.ItemImpl.of(stack);
							newtraits.type = entry;
							newtraits.analysed = true;
							newtraits.save();
							ItemStack result = TraitsManager.ItemImpl.of(new ItemStack(entry.getConfig().getPrimarySeed().getItem().get(), stack.getCount()), newtraits.serializeNBT()).stack;
							this.itemhandler.setStackInSlot(this.getFreeOutputSlot(), result);
							this.itemhandler.setStackInSlot(finalI, ItemStack.EMPTY);
							addKnowledge();
						});
					}
				}
			}
		}
	}

	public boolean hasFreeOutputSlot()
	{
		return getFreeOutputSlot() != -1;
	}

	public int getFreeOutputSlot()
	{
		for (int i = 9; i < 18; i++)
		{
			ItemStack stack = this.itemhandler.getStackInSlot(i);
			if (stack.isEmpty())
				return i;
		}
		return -1;
	}

	@Override
	public String getNameString()
	{
		return "identifier";
	}

	@Override
	public AbstractContainerMenu createMenu(int id, Inventory inv, Player player)
	{
		return new IdentifierMenu(id, inv, this);
	}

	@Override
	public int getEnergyInSlot()
	{
		return 19;
	}

	@Override
	public int getEnergyOutSlot()
	{
		return 20;
	}

	@Override
	public int getKnowledgeChipSlot()
	{
		return 21;
	}

	@Override
	public int getKnowledgePerAction()
	{
		return 5;
	}

	@Override
	public int getUpgradeSlot()
	{
		return 18;
	}
}
