package net.kaneka.planttech2.blocks.entity.machine;

import net.kaneka.planttech2.blocks.entity.machine.baseclasses.ConvertEnergyInventoryFluidBlockEntity;
import net.kaneka.planttech2.hashmaps.TraitsManager;
import net.kaneka.planttech2.inventory.PT2ItemStackHandler;
import net.kaneka.planttech2.inventory.SeedConstructorMenu;
import net.kaneka.planttech2.registries.ModBlockEntities;
import net.kaneka.planttech2.registries.ModBlocks;
import net.kaneka.planttech2.registries.ModItems;
import net.kaneka.planttech2.utilities.PlantTechConstants;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.state.BlockState;

import java.util.function.IntConsumer;
import java.util.function.IntSupplier;

public class SeedConstructorBlockEntity extends ConvertEnergyInventoryFluidBlockEntity
{
	protected final ContainerData data = createContainerData(new IntSupplier[]{
			() -> energyStorage.getEnergyStored(),
			() -> energyStorage.getMaxEnergyStored(),
			this::currentBiomass,
			this::getTankCapacity,
			() -> ticksPassed, }, new IntConsumer[] {
			(value) -> energyStorage.setEnergyStored(value),
			(value) -> energyStorage.setEnergyMaxStored(value),
			this::forceSetStorage,
			this::setTankCapacity,
			(value) -> ticksPassed = value,
	});

	public SeedConstructorBlockEntity()
	{
		this(BlockPos.ZERO, ModBlocks.SEEDCONSTRUCTOR.get().defaultBlockState());
	}

	public SeedConstructorBlockEntity(BlockPos pos, BlockState state)
	{
		super(ModBlockEntities.SEEDCONSTRUCTOR_TE.get(), pos, state, 1000, 8, 5000, PlantTechConstants.MACHINETIER_SEEDCONSTRUCTOR);
	}

	@Override
	protected PT2ItemStackHandler createInsertables()
	{
		return new PT2ItemStackHandler(itemhandler, ofDNAContainer(getInputSlotIndex(), false));
	}

	@Override
	protected PT2ItemStackHandler createExtractables()
	{
		return new PT2ItemStackHandler(itemhandler, PT2ItemStackHandler.Handler.of(getOutputSlotIndex()));
	}

	@Override
	protected boolean canProceed(ItemStack input, ItemStack output)
	{
		return (input.hasTag() && input.getItem() == ModItems.DNA_CONTAINER.get() && TraitsManager.ItemImpl.of(input).type != null);
	}

	@Override
	protected boolean onProcessFinished(ItemStack input, ItemStack output)
	{
		return itemhandler.insertItem(getOutputSlotIndex(), getResult(input, output), false).isEmpty();
	}

	@Override
	protected FluidConsumptionType getFluidConsumptionType()
	{
		return FluidConsumptionType.PER_PROCESS;
	}

	@Override
	protected ItemStack getResult(ItemStack input, ItemStack output)
	{
		CompoundTag nbt = input.getTag();
		if (nbt == null)
			return ItemStack.EMPTY;
		TraitsManager.ItemImpl traits = TraitsManager.ItemImpl.of(input);
		traits.analysed = true;
		return TraitsManager.ItemImpl.of(new ItemStack(traits.type.getSeed()), traits.serializeNBT()).stack;
	}

	@Override
	public ContainerData getContainerData()
	{
		return data;
	}

	@Override
	public int fluidPerAction()
	{
		return 500;
	}

	@Override
	public String getNameString()
	{
		return "seedconstructor";
	}

	@Override
	public int getFluidInSlot()
	{
		return 3;
	}

	@Override
	public int getFluidOutSlot()
	{
		return 4;
	}
	
	@Override
	public AbstractContainerMenu createMenu(int id, Inventory inv, Player player)
	{
		return new SeedConstructorMenu(id, inv, this);
	}

	@Override
	public int getEnergyInSlot()
	{
		return 5;
	}

	@Override
	public int getEnergyOutSlot()
	{
		return 6;
	}

	@Override
	public int getKnowledgeChipSlot()
	{
		return 7;
	}

	@Override
	public int getKnowledgePerAction()
	{
		return 250;
	}

	@Override
	public int getUpgradeSlot()
	{
		return 2;
	}
}
