package net.kaneka.planttech2.blocks.entity.machine;

import net.kaneka.planttech2.addons.Pair;
import net.kaneka.planttech2.blocks.entity.machine.baseclasses.ConvertEnergyInventoryBlockEntity;
import net.kaneka.planttech2.inventory.CompressorMenu;
import net.kaneka.planttech2.inventory.PT2ItemStackHandler;
import net.kaneka.planttech2.items.ParticleItem;
import net.kaneka.planttech2.recipes.ModRecipeTypes;
import net.kaneka.planttech2.recipes.recipeclasses.CompressorRecipe;
import net.kaneka.planttech2.registries.ModBlockEntities;
import net.kaneka.planttech2.registries.ModBlocks;
import net.kaneka.planttech2.utilities.PlantTechConstants;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.block.state.BlockState;

import java.util.ArrayList;
import java.util.List;
import java.util.function.IntConsumer;
import java.util.function.IntSupplier;

public class CompressorBlockEntity extends ConvertEnergyInventoryBlockEntity
{
	private int selectedId = -1;
	private List<Pair<ItemStack, ItemStack>> recipeList = new ArrayList<>();
	private Item previousInput = null;

	protected final ContainerData data = createContainerData(new IntSupplier[]{
			() -> energyStorage.getEnergyStored(),
			() -> energyStorage.getMaxEnergyStored(),
			() -> ticksPassed,
			() -> selectedId}, new IntConsumer[] {
			(value) -> energyStorage.setEnergyStored(value),
			(value) -> energyStorage.setEnergyMaxStored(value),
			(value) -> ticksPassed = value,
			this::setSelectedId,
	});

	public CompressorBlockEntity()
	{
		this(BlockPos.ZERO, ModBlocks.COMPRESSOR.get().defaultBlockState());
	}

	public CompressorBlockEntity(BlockPos pos, BlockState state)
	{
		super(ModBlockEntities.COMPRESSOR_TE.get(), pos, state, 1000, 26, PlantTechConstants.MACHINETIER_COMPRESSOR);
	}

	@Override
	protected PT2ItemStackHandler createInsertables()
	{
		return new PT2ItemStackHandler(itemhandler, PT2ItemStackHandler.Handler.of(getInputSlotIndex()).setConditions((stack) -> stack.getItem() instanceof ParticleItem));
	}

	@Override
	protected PT2ItemStackHandler createExtractables()
	{
		return new PT2ItemStackHandler(itemhandler, PT2ItemStackHandler.Handler.of(getOutputSlotIndex()));
	}

	@Override
	protected boolean canProceed(ItemStack input, ItemStack output)
	{
		boolean recipeDone = false;
		boolean inputDone = false;
		
		if (recipeList == null || previousInput != input.getItem())
			initRecipeList();
		if (getSelectedId() >= 0)
		{
			if (!recipeList.isEmpty() && recipeList.size() > getSelectedId())
			{
				Pair<ItemStack, ItemStack> recipe = recipeList.get(getSelectedId());
				if (recipe.getB().getItem() == Items.BARRIER)
				{
					setSelectedId(-1);
					return false;
				}
				recipeDone = true;
				ItemStack ingredient = recipe.getA();
				inputDone = ingredient.getItem() == input.getItem() && ingredient.getCount() <= input.getCount();
				if (inputDone)
					previousInput = input.getItem();
			}
			else setSelectedId(-1);
		}
		return getSelectedId() >= 0 && recipeDone && inputDone;
	}

	private int getSelectedId()
	{
		return selectedId - 1;
	}

	@Override
	protected boolean onProcessFinished(ItemStack input, ItemStack output)
	{
		ItemStack result = getResult(input, output);
		if (itemhandler.insertItem(getOutputSlotIndex(), result, false).isEmpty())
		{
			input.shrink(recipeList.get(getSelectedId()).getA().getCount());
			return true;
		}
		return false;
	}

	@Override
	protected ItemStack getResult(ItemStack input, ItemStack output)
	{
		return recipeList.get(getSelectedId()).getB().copy();
	}

	@Override
	public ContainerData getContainerData()
	{
		return data;
	}

	public void setSelectedId(int selectedId)
	{
		this.selectedId = selectedId + 1;
		notifyClient();
	}

	@Override
	public void onContainerUpdated(int slotIndex)
	{
		if (level != null && (previousInput == null || previousInput.asItem() != itemhandler.getStackInSlot(0).getItem()))
			initRecipeList();
	}

	@SuppressWarnings("resource")
	public void initRecipeList()
	{
		// reset old values
		if (level == null || level.isClientSide)
			return;
		for (int i = 0; i < 20; i++)
			itemhandler.setStackInSlot(i + 3, ItemStack.EMPTY);
		selectedId = -1; 
		previousInput = null; 

		// set new values
		recipeList = new ArrayList<>();
		ItemStack input = itemhandler.getStackInSlot(0);
		if(!input.isEmpty())
		{
			if (level != null)
			{
				for (CompressorRecipe recipe : this.level.getRecipeManager().getAllRecipesFor(ModRecipeTypes.COMPRESSING.get()))
				{
					if (recipe.getInput().getItem() == input.getItem())
					{
						for (ItemStack output : recipe.getOutput().getItems())
						{
							if (recipeList.size() <= 18)
							{
								itemhandler.setStackInSlot(recipeList.size() + 3, output.copy());
								recipeList.add(Pair.of(recipe.getInput().copy(), output.copy()));
							}
						}
						previousInput = input.getItem();
					}
				}
			}
		}
		resetProgress(false);
	}

	@Override
	public String getNameString()
	{
		return "compressor";
	}

	@Override
	public List<ItemStack> getInventoryContent()
	{
		List<ItemStack> stacks = new ArrayList<ItemStack>();
		for (int i = 0; i < 3; i++)
			stacks.add(itemhandler.getStackInSlot(i).copy());
		return stacks;
	}

	@Override
	protected void saveAdditional(CompoundTag compound)
	{
		super.saveAdditional(compound);
		compound.putInt("selectedId", getSelectedId());
	}

	@Override
	public void load(CompoundTag compound)
	{
		super.load(compound);
		if (compound.contains("selectedId"))
			setSelectedId(compound.getInt("selectedId"));
	}

	@Override
	public AbstractContainerMenu createMenu(int id, Inventory inv, Player player)
	{
		return new CompressorMenu(id, inv, this);
	}

	@Override
	public int getEnergyInSlot()
	{
		return 23;
	}

	@Override
	public int getEnergyOutSlot()
	{
		return 24;
	}

	@Override
	public int getKnowledgeChipSlot()
	{
		return 25;
	}

	@Override
	public int getKnowledgePerAction()
	{
		return 2;
	}

	@Override
	public int getUpgradeSlot()
	{
		return 2;
	}

	@Override
	public boolean requireSyncUponOpen()
	{
		return true;
	}
}
