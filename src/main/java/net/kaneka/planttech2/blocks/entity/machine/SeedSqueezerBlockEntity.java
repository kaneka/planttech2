package net.kaneka.planttech2.blocks.entity.machine;

import net.kaneka.planttech2.blocks.entity.machine.baseclasses.EnergyInventoryFluidBlockEntity;
import net.kaneka.planttech2.enums.CropTraitsTypes;
import net.kaneka.planttech2.hashmaps.TraitsManager;
import net.kaneka.planttech2.inventory.PT2ItemStackHandler;
import net.kaneka.planttech2.inventory.SeedSqueezerMenu;
import net.kaneka.planttech2.items.CropSeedItem;
import net.kaneka.planttech2.registries.ModBlockEntities;
import net.kaneka.planttech2.registries.ModBlocks;
import net.kaneka.planttech2.utilities.PlantTechConstants;
import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.state.BlockState;

import java.util.function.IntConsumer;
import java.util.function.IntSupplier;

import static net.kaneka.planttech2.items.TierItem.ItemType.SPEED_UPGRADE;

public class SeedSqueezerBlockEntity extends EnergyInventoryFluidBlockEntity
{
	protected final ContainerData data = createContainerData(new IntSupplier[]{
			() -> energyStorage.getEnergyStored(),
			() -> energyStorage.getMaxEnergyStored(),
			this::currentBiomass,
			this::getTankCapacity,
			() -> ticksPassed, }, new IntConsumer[] {
			(value) -> energyStorage.setEnergyStored(value),
			(value) -> energyStorage.setEnergyMaxStored(value),
			this::forceSetStorage,
			this::setTankCapacity,
			(value) -> ticksPassed = value,
	});

	public SeedSqueezerBlockEntity()
	{
		this(BlockPos.ZERO, ModBlocks.SEEDSQUEEZER.get().defaultBlockState());
	}

	public SeedSqueezerBlockEntity(BlockPos pos, BlockState state)
	{
		super(ModBlockEntities.SEEDSQUEEZER_TE.get(), pos, state,10000, 16, 5000, PlantTechConstants.MACHINETIER_SEEDSQUEEZER);
	}

	@Override
	protected PT2ItemStackHandler createInsertables()
	{
		return new PT2ItemStackHandler(itemhandler, 9, (handler) -> handler.setConditions((stack) -> stack.getItem() instanceof CropSeedItem));
	}

	@Override
	public void doUpdate()
	{
		super.doUpdate();
		if (level != null && !level.isClientSide)
		{
			if (itemhandler.getStackInSlot(9).isEmpty())
			{
				int i = this.getSqueezeableItem();
				if (i != -1)
				{
					ItemStack stack = itemhandler.getStackInSlot(i);
					ItemStack stack2 = stack.copy();
					stack2.setCount(1);
					itemhandler.setStackInSlot(9, stack2);
					stack.shrink(1);
				}
			}
			if (!itemhandler.getStackInSlot(9).isEmpty())
			{
				ItemStack stack = itemhandler.getStackInSlot(9);
				if (stack.getCount() == 1 && (stack.getItem() instanceof CropSeedItem))
				{
					ticksPassed += getUpgradeTier(SPEED_UPGRADE) + 1;
					if (ticksPassed >= ticksPerItem())
					{
						squeezeItem();
						addKnowledge();
						forceChangeStorage(10);
						resetProgress(true);
					}
				}
				else if (stack.getCount() > 0)
				{
					if (!level.isClientSide)
					{
						level.addFreshEntity(new ItemEntity(level, worldPosition.getX(), worldPosition.getY() + 1, worldPosition.getZ(), stack));
						itemhandler.setStackInSlot(9, ItemStack.EMPTY);
					}
				}
				else if (ticksPassed > 0)
					resetProgress(false);
			}
		}
	}

	@Override
	public ContainerData getContainerData()
	{
		return data;
	}

	private int getSqueezeableItem()
	{
		for (int i = 0; i < 9; i++)
		{
			ItemStack stack = this.itemhandler.getStackInSlot(i);
			if (!stack.isEmpty())
			{
				if (stack.getItem() instanceof CropSeedItem)
					return i;
			}
		}
		return -1;
	}

	public void squeezeItem()
	{
		this.energyStorage.receiveEnergy(getEnergyPerItem(), false);
		itemhandler.setStackInSlot(9, ItemStack.EMPTY);
	}

	@Override
	public int ticksPerItem()
	{
		return 200;
	}

	public int getEnergyPerItem()
	{
		ItemStack stack = this.itemhandler.getStackInSlot(9);
		if (stack.getItem() instanceof CropSeedItem)
		{
			TraitsManager.ItemImpl manager = TraitsManager.ItemImpl.of(stack);
			return manager.getTrait(CropTraitsTypes.ENERGY_VALUE) * 20;
		}
		return 20;
	}

	@Override
	public String getNameString()
	{
		return "seedsqueezer";
	}

	@Override
	public int getFluidInSlot()
	{
		return 11;
	}

	@Override
	public int getFluidOutSlot()
	{
		return 12;
	}

	@Override
	public AbstractContainerMenu createMenu(int id, Inventory inv, Player player)
	{
		return new SeedSqueezerMenu(id, inv, this);
	}

	@Override
	public int getEnergyInSlot()
	{
		return 13;
	}

	@Override
	public int getEnergyOutSlot()
	{
		return 14;
	}
	
	@Override
	public int getKnowledgeChipSlot()
	{
		return 15;
	}

	@Override
	public int getKnowledgePerAction()
	{
		return 2;
	}

	@Override
	public int getUpgradeSlot()
	{
		return 10;
	}
}
