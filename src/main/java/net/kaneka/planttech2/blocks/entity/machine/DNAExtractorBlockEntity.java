package net.kaneka.planttech2.blocks.entity.machine;

import net.kaneka.planttech2.blocks.entity.machine.baseclasses.EnergyInventoryBlockEntity;
import net.kaneka.planttech2.hashmaps.TraitsManager;
import net.kaneka.planttech2.inventory.DNAExtractorMenu;
import net.kaneka.planttech2.inventory.PT2ItemStackHandler;
import net.kaneka.planttech2.items.CropSeedItem;
import net.kaneka.planttech2.registries.ModBlockEntities;
import net.kaneka.planttech2.registries.ModBlocks;
import net.kaneka.planttech2.registries.ModItems;
import net.kaneka.planttech2.utilities.PlantTechConstants;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.state.BlockState;

import java.util.function.IntConsumer;
import java.util.function.IntSupplier;

public class DNAExtractorBlockEntity extends EnergyInventoryBlockEntity
{
	protected final ContainerData data = createContainerData(new IntSupplier[]{
			() -> energyStorage.getEnergyStored(),
			() -> energyStorage.getMaxEnergyStored(),
			() -> ticksPassed }, new IntConsumer[] {
			(value) -> energyStorage.setEnergyStored(value),
			(value) -> energyStorage.setEnergyMaxStored(value),
			(value) -> ticksPassed = value,
	});

    public DNAExtractorBlockEntity()
	{
		this(BlockPos.ZERO, ModBlocks.DNA_EXTRACTOR.get().defaultBlockState());
	}

    public DNAExtractorBlockEntity(BlockPos pos, BlockState state)
    {
		super(ModBlockEntities.DNAEXTRACTOR_TE.get(), pos, state, 1000, 7, PlantTechConstants.MACHINETIER_DNA_EXTRACTOR);
    }

	@Override
	protected PT2ItemStackHandler createInsertables()
	{
		return new PT2ItemStackHandler(itemhandler, PT2ItemStackHandler.Handler.of(0).setConditions((stack) -> stack.getItem() instanceof CropSeedItem), ofDNAContainer(1, true));
	}

	@Override
	protected PT2ItemStackHandler createExtractables()
	{
		return new PT2ItemStackHandler(itemhandler, PT2ItemStackHandler.Handler.of(2));
	}

    @Override
    public void doUpdate()
    {
    	super.doUpdate();
		if (energyStorage.getEnergyStored() > energyPerAction())
		{
			ItemStack stack1 = itemhandler.getStackInSlot(0);
			ItemStack stack2 = itemhandler.getStackInSlot(1);
			ItemStack stack3 = itemhandler.getStackInSlot(2);
			if (!stack1.isEmpty() && !stack2.isEmpty())
			{
				if (stack1.getItem() instanceof CropSeedItem && stack2.getItem() == ModItems.DNA_CONTAINER_EMPTY.get())
				{
					if (stack1.hasTag())
					{
						if (ticksPassed < ticksPerItem())
						{
							ticksPassed++;
							energyStorage.extractEnergy(energyPerAction(), false);
						}
						else
						{
							CompoundTag nbt = TraitsManager.ItemImpl.of(stack1).serializeNBT();
							nbt.remove("analysed");
							ItemStack result = TraitsManager.ItemImpl.of(new ItemStack(ModItems.DNA_CONTAINER.get()), nbt).stack;
							if (stack3.isEmpty())
							{

								itemhandler.setStackInSlot(2, result);
								endProcess();
								addKnowledge();
							}
							else if (ItemStack.isSameItemSameTags(result, stack3))
							{
								stack3.grow(1);
								addKnowledge();
								endProcess();
							}
						}
					}
				}
			}
		}
    }
    
    @Override
	public ContainerData getContainerData()
	{
		return data;
	}

    private void endProcess()
    {
		ticksPassed = 0;
		itemhandler.getStackInSlot(0).shrink(1);
		itemhandler.getStackInSlot(1).shrink(1);
    }

    @Override
    public String getNameString()
    {
	return "dnaextractor";
    }

    @Override
	public AbstractContainerMenu createMenu(int id, Inventory inv, Player player)
	{
		return new DNAExtractorMenu(id, inv, this);
	}

	@Override
	public int getEnergyInSlot()
	{
		return 4;
	}

	@Override
	public int getEnergyOutSlot()
	{
		return 5;
	}

	@Override
	public int getKnowledgeChipSlot()
	{
		return 6;
	}

	@Override
	public int getKnowledgePerAction()
	{
		return 50;
	}

	@Override
	public int getUpgradeSlot()
	{
		return 3;
	}
}
