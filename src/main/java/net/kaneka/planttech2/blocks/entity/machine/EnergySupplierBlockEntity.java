package net.kaneka.planttech2.blocks.entity.machine;

import net.kaneka.planttech2.blocks.baseclasses.BaseElectricFence;
import net.kaneka.planttech2.blocks.entity.machine.baseclasses.EnergyInventoryBlockEntity;
import net.kaneka.planttech2.blocks.machines.EnergySupplierBlock;
import net.kaneka.planttech2.inventory.EnergySupplierMenu;
import net.kaneka.planttech2.registries.ModBlockEntities;
import net.kaneka.planttech2.registries.ModBlocks;
import net.kaneka.planttech2.utilities.PlantTechConstants;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.level.block.state.BlockState;

import java.util.HashSet;
import java.util.function.IntConsumer;
import java.util.function.IntSupplier;

public class EnergySupplierBlockEntity extends EnergyInventoryBlockEntity
{
	protected final ContainerData data = createContainerData(new IntSupplier[]{
			() -> energyStorage.getEnergyStored(),
			() -> energyStorage.getMaxEnergyStored(),
			() -> ticksPassed }, new IntConsumer[] {
			(value) -> energyStorage.setEnergyStored(value),
			(value) -> energyStorage.setEnergyMaxStored(value),
			(value) -> ticksPassed = value,
	});

	public EnergySupplierBlockEntity()
	{
		this(BlockPos.ZERO, ModBlocks.ENERGY_SUPPLIER.get().defaultBlockState());
	}

	public EnergySupplierBlockEntity(BlockPos pos, BlockState state)
	{
		super(ModBlockEntities.ENERGY_SUPPLIER_TE.get(), pos, state, 1000, 3, PlantTechConstants.MACHINETIER_ENERGY_SUPPLIER);
	}

	@Override
	public void onContainerUpdated(int slotIndex)
	{
		super.onContainerUpdated(slotIndex);
		if (slotIndex == getUpgradeSlot())
			createEnergyStorage(1000, true);
	}

	@Override
	public void createEnergyStorage(int maxEnergy, boolean inherit)
	{
		super.createEnergyStorage((int) (maxEnergy * Math.pow(10, getCapacityUpgrade())), inherit);
	}

	@Override
	public void doUpdate()
	{
		super.doUpdate();
		if (level == null)
			return;
		ticksPassed++;
		if (energyStorage.getEnergyStored() <= 0)
			setPower(false);
		else if (!getConnected().isEmpty() && ticksPassed >= ticksPerItem())
		{
			energyStorage.extractEnergy(energyPerAction());
			setPower(true);
			resetProgress(true);
		}
	}

	private void setPower(boolean powered)
	{
		if (level != null && level.getBlockState(worldPosition).getBlock() instanceof EnergySupplierBlock)
		{
			level.setBlockAndUpdate(worldPosition, level.getBlockState(worldPosition).
					getBlock().defaultBlockState()
					.setValue(EnergySupplierBlock.SUPPLYING, powered));
		}
	}

	private HashSet<BlockPos> getConnected()
	{
		HashSet<BlockPos> list = new HashSet<>();
		if (level != null)
		{
			for (Direction direction : Direction.values())
			{
				BlockPos blockPos = this.worldPosition.relative(direction);
				if (level.getBlockState(blockPos).getBlock() instanceof BaseElectricFence)
					list.add(blockPos);
			}
		}
		return list;
	}

	@Override
	public int energyPerAction()
	{
		return 1;
	}

	@Override
	public int ticksPerItem()
	{
		return 10;
	}

	@Override
	public ContainerData getContainerData()
	{
		return data;
	}

	@Override
	public String getNameString()
	{
		return "energysupplier";
	}

	@Override
	public AbstractContainerMenu createMenu(int id, Inventory inv, Player player)
	{
		return new EnergySupplierMenu(id, inv, this);
	}

	@Override
	public int getEnergyInSlot()
	{
		return 0;
	}

	@Override
	public int getEnergyOutSlot()
	{
		return 1;
	}

	@Override
	public int getKnowledgeChipSlot()
	{
		return 0;
	}

	@Override
	public int getKnowledgePerAction()
	{
		return 0;
	}

	@Override
	public int getUpgradeSlot()
	{
		return 2;
	}
}
