package net.kaneka.planttech2.blocks.entity.machine;

import net.kaneka.planttech2.blocks.entity.machine.baseclasses.EnergyInventoryBlockEntity;
import net.kaneka.planttech2.inventory.SolarGeneratorMenu;
import net.kaneka.planttech2.registries.ModBlockEntities;
import net.kaneka.planttech2.registries.ModBlocks;
import net.kaneka.planttech2.utilities.PlantTechConstants;
import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.level.block.state.BlockState;

import java.util.function.IntConsumer;
import java.util.function.IntSupplier;

import static net.kaneka.planttech2.items.TierItem.ItemType.SOLAR_FOCUS;
import static net.kaneka.planttech2.items.TierItem.ItemType.SPEED_UPGRADE;

public class SolarGeneratorBlockEntity extends EnergyInventoryBlockEntity
{
	protected final ContainerData data = createContainerData(new IntSupplier[]{
			() -> energyStorage.getEnergyStored(),
			() -> energyStorage.getMaxEnergyStored(),
			() -> ticksPassed, }, new IntConsumer[] {
			(value) -> energyStorage.setEnergyStored(value),
			(value) -> energyStorage.setEnergyMaxStored(value),
			(value) -> ticksPassed = value,
	});

	public SolarGeneratorBlockEntity()
	{
		this(BlockPos.ZERO, ModBlocks.SOLARGENERATOR.get().defaultBlockState());
	}

	public SolarGeneratorBlockEntity(BlockPos pos, BlockState state)
	{
		super(ModBlockEntities.SOLARGENERATOR_TE.get(), pos, state, 10000, 5, PlantTechConstants.MACHINETIER_SOLARGENERATOR);
	}

	@Override
	public void doUpdate()
	{
		super.doUpdate();
		if (level != null && level.isDay() && level.canSeeSky(worldPosition.above()))
		{
			if (energyStorage.getMaxEnergyStored() - energyStorage.getEnergyStored() > 0)
			{
				ticksPassed++;
				if (ticksPassed >= getTicksPerAmount())
				{
					energyStorage.receiveEnergy(getEnergyPerTick(getUpgradeTier(0, SOLAR_FOCUS)));
					ticksPassed = 0;
					addKnowledge();
				}
			}
		}
	}

	@Override
	public ContainerData getContainerData()
	{
		return data;
	}

	private int getEnergyPerTick(int focusLevel)
	{
		return switch (focusLevel)
				{
					case 1 -> 20;
					case 2 -> 60;
					case 3 -> 180;
					case 4 -> 540;
					default -> 0;
				};
	}

	public int getTicksPerAmount()
	{
		return 80 - getUpgradeTier(SPEED_UPGRADE) * 15;
	}

	@Override
	public String getNameString()
	{
		return "solargenerator";
	}

	@Override
	public AbstractContainerMenu createMenu(int id, Inventory inv, Player player)
	{
		return new SolarGeneratorMenu(id, inv, this);
	}

	@Override
	public int getEnergyInSlot()
	{
		return 2;
	}

	@Override
	public int getEnergyOutSlot()
	{
		return 3;
	}

	@Override
	public int getKnowledgeChipSlot()
	{
		return 4;
	}

	@Override
	public int getKnowledgePerAction()
	{
		return 50;
	}

	@Override
	public int getUpgradeSlot()
	{
		return 1;
	}
}
