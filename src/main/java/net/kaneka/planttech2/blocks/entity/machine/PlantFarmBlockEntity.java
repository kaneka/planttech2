package net.kaneka.planttech2.blocks.entity.machine;

import net.kaneka.planttech2.blocks.entity.machine.baseclasses.EnergyInventoryFluidBlockEntity;
import net.kaneka.planttech2.enums.CropTraitsTypes;
import net.kaneka.planttech2.hashmaps.TraitsManager;
import net.kaneka.planttech2.inventory.PT2ItemStackHandler;
import net.kaneka.planttech2.inventory.PlantFarmMenu;
import net.kaneka.planttech2.items.CropSeedItem;
import net.kaneka.planttech2.items.TierItem;
import net.kaneka.planttech2.registries.ModBlockEntities;
import net.kaneka.planttech2.registries.ModBlocks;
import net.kaneka.planttech2.utilities.PlantTechConstants;
import net.minecraft.core.BlockPos;
import net.minecraft.core.NonNullList;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.CropBlock;
import net.minecraft.world.level.block.state.BlockState;

import java.util.function.IntConsumer;
import java.util.function.IntSupplier;

import static net.kaneka.planttech2.items.TierItem.ItemType.RANGE_UPGRADE;
import static net.kaneka.planttech2.items.TierItem.ItemType.SPEED_UPGRADE;

public class PlantFarmBlockEntity extends EnergyInventoryFluidBlockEntity
{
	private int[] progress = new int[5];

	protected final ContainerData data = createContainerData(new IntSupplier[]{
			() -> energyStorage.getEnergyStored(),
			() -> energyStorage.getMaxEnergyStored(),
			this::currentBiomass,
			this::getTankCapacity,
			() -> progress[0],
			() -> progress[1],
			() -> progress[2],
			() -> progress[3],
			() -> progress[4], }, new IntConsumer[] {
			(value) -> energyStorage.setEnergyStored(value),
			(value) -> energyStorage.setEnergyMaxStored(value),
			this::forceSetStorage,
			this::setTankCapacity,
			(value) -> progress[0] = value,
			(value) -> progress[1] = value,
			(value) -> progress[2] = value,
			(value) -> progress[3] = value,
			(value) -> progress[4] = value,
	});

	public PlantFarmBlockEntity()
	{
		this(BlockPos.ZERO, ModBlocks.PLANTFARM.get().defaultBlockState());
	}

	public PlantFarmBlockEntity(BlockPos pos, BlockState state)
	{
		super(ModBlockEntities.PLANTFARM_TE.get(), pos, state, 1000, 18, 5000, PlantTechConstants.MACHINETIER_PLANTFARM);
	}

	@Override
	protected PT2ItemStackHandler createInsertables()
	{
		return new PT2ItemStackHandler(itemhandler, PT2ItemStackHandler.Handler.of(0).setConditions(PlantFarmBlockEntity::isSeed));
	}

	@Override
	protected PT2ItemStackHandler createExtractables()
	{
		return new PT2ItemStackHandler(itemhandler, 1, 11);
	}

	@Override
	public void doUpdate()
	{
		super.doUpdate();
		ItemStack seed = itemhandler.getStackInSlot(0);
		if (isSeed(seed))
		{
			for (int i = 0 ; i <= getRange(); i++)
			{
				if (progress[i] < getTicks(seed))
					progress[i] += getUpgradeTier(SPEED_UPGRADE) * 5 + 1;
				else if (energyStorage.getEnergyStored() > energyPerAction())
				{
					NonNullList<ItemStack> drops = getDrops(seed);
					if (!drops.isEmpty())
					{
						for (ItemStack stack : drops)
						{
							for (int k = 0; k < 15; k++)
							{
								if (!stack.isEmpty())
									stack = itemhandler.insertItem(k, stack, false);
							}
							if (!stack.isEmpty())
								spawnAsEntity(level, worldPosition.above(), stack);
						}
						energyStorage.extractEnergy(energyPerAction(), false);
						progress[i] = 0;
						addKnowledge();
					}
				}
				else
				{
					resetProgress(false);
					break;
				}
			}
		}
		else
			resetProgress(false);
	}

	@Override
	protected void resetProgress(boolean forced)
	{
		for (int i = 0; i <= getRange(); i++)
			progress[i] = 0;
	}

	public static boolean isSeed(ItemStack stack)
	{
		Item item = stack.getItem();
		if (item instanceof CropSeedItem)
			return true;
		if (item instanceof BlockItem bi)
		{
			Block block = bi.getBlock();
			return block instanceof CropBlock;
		}
		return false;
	}
	
	@Override
	public ContainerData getContainerData()
	{
		return data;
	}
	
	private int getTicks(ItemStack stack)
	{
		if (stack.getItem() instanceof CropSeedItem)
		{
			TraitsManager.ItemImpl manager = TraitsManager.ItemImpl.of(stack);
			return (90 - manager.getTrait(CropTraitsTypes.GROW_SPEED) * 6) * 20 * 7;
		}
		return 90 * 20 * 7;
	}
	
	public int getTicks()
	{
		return getTicks(itemhandler.getStackInSlot(0));
	}

	private NonNullList<ItemStack> getDrops(ItemStack stack)
	{
		NonNullList<ItemStack> drops = NonNullList.create();
		if (level != null)
		{
			Item item = stack.getItem(); 
			if (item instanceof CropSeedItem)
			{
				TraitsManager.ItemImpl traits = TraitsManager.ItemImpl.of(stack);
				traits.type.calculateDropsReduced(drops, traits, 7, level.random);
				return drops; 
			}
			if (item instanceof BlockItem)
			{
				Block block = ((BlockItem) item).getBlock();
				if (block instanceof CropBlock crop)
					if (level instanceof ServerLevel sl)
						drops.addAll(Block.getDrops(crop.getStateForAge(crop.getMaxAge()), sl, worldPosition, null));
			}
		}
		return drops; 
	}

	private int getRange()
	{
		ItemStack stack = itemhandler.getStackInSlot(12);
		if (!stack.isEmpty())
		{
			if (stack.getItem() instanceof TierItem ti)
			{
				if (ti.getItemType() == RANGE_UPGRADE)
					return ti.getTier();
			}
		}
		return 0;
	}

	@Override
	public int getUpgradeSlot()
	{
		return 11;
	}

	@Override
	public int energyPerAction()
	{
		return 400;
	}

	@Override
	protected void saveAdditional(CompoundTag compound)
	{
	 	super.saveAdditional(compound);
		compound.putIntArray("progress", progress);
	}

	@Override
	public void load(CompoundTag compound)
	{
		super.load(compound);
		progress = compound.getIntArray("progress");
		// We were used the wrong saving method (saveAdditional(compound) should be used instead of save(compound)), and this fucks all the data stored in the
		// existing tile entities, none of them are saved. Hence this check.
		if (progress.length == 0)
			progress = new int[5];
	}

	@Override
	public String getNameString()
	{
		return "plantfarm";
	}

	@Override
	public AbstractContainerMenu createMenu(int id, Inventory inv, Player player)
	{
		return new PlantFarmMenu(id, inv, this);
	}

	@Override
	public int getEnergyInSlot()
	{
		return 15;
	}

	@Override
	public int getEnergyOutSlot()
	{
		return 16;
	}

	@Override
	public int getFluidInSlot()
	{
		return 13;
	}

	@Override
	public int getFluidOutSlot()
	{
		return 14;
	}

	@Override
	public int getKnowledgeChipSlot()
	{
		return 17;
	}

	@Override
	public int getKnowledgePerAction()
	{
		return 50;
	}
}
