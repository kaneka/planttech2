package net.kaneka.planttech2.blocks;

import com.google.common.collect.Lists;
import net.kaneka.planttech2.registries.ModBlocks;
import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.RandomSource;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.IntegerProperty;
import net.minecraft.world.level.storage.loot.LootParams;
import net.minecraftforge.registries.RegistryObject;

import java.util.List;

public class GrowingBlock extends Block
{
	public static final IntegerProperty GROWINGSTATE = IntegerProperty.create("growingstate", 0, 6);
	public final RegistryObject<Block> blockSupplier;
	protected boolean growAlone; 
	
	public GrowingBlock(RegistryObject<Block> blockSupplier, boolean growAlone)
	{
		super(Block.Properties.of().sound(SoundType.METAL).sound(SoundType.METAL).strength(0.9F).randomTicks());
		this.blockSupplier = blockSupplier;
		this.growAlone = growAlone; 
		this.registerDefaultState(this.stateDefinition.any().setValue(GROWINGSTATE, 0));
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void tick(BlockState state, ServerLevel level, BlockPos pos, RandomSource rand)
	{
		if (growAlone)
    		grow(state, level, pos);
	}
	
	public void grow(BlockState state, ServerLevel level, BlockPos pos)
	{
		int i = state.getValue(GROWINGSTATE);
		if (i < 6)
			level.setBlockAndUpdate(pos, state.setValue(GROWINGSTATE, i + 1));
		else
			placeBlock(level, pos, state);
	}
	
	protected void placeBlock(ServerLevel level, BlockPos pos, BlockState state)
	{
		level.setBlockAndUpdate(pos, getBlock().defaultBlockState());
	}
	
	@Override
	protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) 
	{
		builder.add(GROWINGSTATE);
	}
	
	public Block getBlock()
	{
		return blockSupplier.get();
	}
	
	@Override
	public List<ItemStack> getDrops(BlockState state, LootParams.Builder builder)
	{
		List<ItemStack> drops = Lists.newArrayList();
		Block block = state.getBlock();
		if (block == ModBlocks.COMPRESSOR_GROWING.get() || block == ModBlocks.MACHINEBULBREPROCESSOR_GROWING.get() || block == ModBlocks.SEEDSQUEEZER_GROWING.get() || block == ModBlocks.INFUSER_GROWING.get() || block == ModBlocks.IDENTIFIER_GROWING.get())
			drops.add(new ItemStack(ModBlocks.MACHINESHELL_IRON.get()));
		else if (block == ModBlocks.MACHINESHELL_IRON_GROWING.get())
			drops.add(new ItemStack(Blocks.IRON_BLOCK));
		else if (block == ModBlocks.MACHINESHELL_PLANTIUM_GROWING.get())
			drops.add(new ItemStack(ModBlocks.PLANTIUM_BLOCK.get()));
		else
			drops.add(new ItemStack(ModBlocks.MACHINESHELL_PLANTIUM.get()));
		return drops;
	}

}
